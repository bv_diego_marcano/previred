package cl.bicevida.previred.core.config;

import cl.bicevida.previred.core.structure.FieldStructureVo;
import cl.bicevida.previred.core.structure.FileStructureVo;

import cl.bicevida.previred.core.structure.RowStructureVo;

import java.io.File;

import java.io.Serializable;

import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ReaderConfigCore implements Serializable {
    @SuppressWarnings("compatibility:2658087122309705408")
    private static final long serialVersionUID = 1L;

    public ReaderConfigCore() {
    }

    public static Hashtable config;

    /**
     * Lee configuración de procesamiento de archivo de abono
     * @param fileXml
     */
    public static void loadConfig(String fileXml) {
        try {
            //Si ya esta cargo se devuelve
            config = new Hashtable();

            File file = new File(fileXml);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(file);
            doc.getDocumentElement().normalize();

            //Archivos
            NodeList nodeArchivo = doc.getElementsByTagName("archivo");
            for (int node = 0; node < nodeArchivo.getLength(); node++) {
                Node fstNode = nodeArchivo.item(node);
                NamedNodeMap atributos = fstNode.getAttributes();
                FileStructureVo fileStruct = getAtributesFileNode(atributos);
                
                //Recupera el tipo de fila asociado
                if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element fstElmnt = (Element)fstNode;
                    NodeList tiporegistroLts = fstElmnt.getElementsByTagName("tipoderegistro");
                    for (int noderegistro = 0; noderegistro < tiporegistroLts.getLength(); noderegistro++) {
                        Node registroNode = tiporegistroLts.item(noderegistro);
                        NamedNodeMap registroatrNode = registroNode.getAttributes();
                        RowStructureVo registro = getAtributesNodeRegistro(registroatrNode);
                        
                        // recupera los campos asociados al tipo de registro
                        if (registroNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element fstElmntfield = (Element)registroNode;
                            NodeList tipocampoLts = fstElmntfield.getElementsByTagName("campo");
                            for (int nodecampo = 0; nodecampo < tipocampoLts.getLength(); nodecampo++) {
                                Node campoNode = tipocampoLts.item(nodecampo);
                                NamedNodeMap campoatrNode = campoNode.getAttributes();
                                FieldStructureVo campo = getAtributesNodeCampo(campoatrNode);
                                registro.addField(campo.getId(), campo);
                            }
                        }

                        fileStruct.addField(registro.getId(), registro);
                    }
                    //Agrega Configuracion de Archivo
                    config.put(fileStruct.getNombre(), fileStruct);
                }
            }
        
        //FileStructureVo test = (FileStructureVo) config.get("ABONO");
        //RowStructureVo testregistro = (RowStructureVo) test.getTiporegistro().get("resumen");
        //FieldStructureVo testfield01 = (FieldStructureVo) testregistro.getCampos().get("total_monto_fondo");        
        
        } catch (Exception e) {
            //TODO: code log
            e.printStackTrace();
        }
    }

    /**
     * Obtiene las caracteristicas del archivo
     * @param atributos
     * @return
     */
    private static FileStructureVo getAtributesFileNode(NamedNodeMap atributos) {
        String nombre = "";
        String tipo = "";
        String descripcion = "";

        if (atributos.getNamedItem("nombre") != null)
            nombre = atributos.getNamedItem("nombre").getNodeValue();
        if (atributos.getNamedItem("tipo") != null)
            tipo = atributos.getNamedItem("tipo").getNodeValue();
        if (atributos.getNamedItem("descripcion") != null)
            descripcion = atributos.getNamedItem("descripcion").getNodeValue();

        //Setea Archivo
        FileStructureVo fileStruct = new FileStructureVo();
        fileStruct.setNombre(nombre);
        fileStruct.setTipoArchivo(tipo);
        fileStruct.setDescripcion(descripcion);
        return fileStruct;
    }

    /**
     * Obtiene atributos del tipo de registro
     * @param atributos
     * @return
     */
    private static RowStructureVo getAtributesNodeRegistro(NamedNodeMap atributos) {
        //Setea Structura
        String id = null;
        String inicio = "0";
        String largo = "0";
        String tipo = null;
        String valor = "0";

        if (atributos.getNamedItem("id") != null)
            id = atributos.getNamedItem("id").getNodeValue();
        if (atributos.getNamedItem("inicio") != null)
            inicio = atributos.getNamedItem("inicio").getNodeValue();
        if (atributos.getNamedItem("largo") != null)
            largo = atributos.getNamedItem("largo").getNodeValue();
        if (atributos.getNamedItem("tipo") != null)
            tipo = atributos.getNamedItem("tipo").getNodeValue();
        if (atributos.getNamedItem("valor") != null)
            valor = atributos.getNamedItem("valor").getNodeValue();            

        RowStructureVo row = new RowStructureVo();
        row.setId(id);
        row.setLargo(Integer.parseInt(largo));
        row.setPosicionInicial(Integer.parseInt(inicio));
        row.setTipo(tipo);
        row.setValor(Integer.parseInt(valor));
        return row;
    }

    /**
     * Obtiene los campos asociados al registro
     * @param atributos
     * @return
     */
    private static FieldStructureVo getAtributesNodeCampo(NamedNodeMap atributos) {
        //Setea Structura
        String id = null;
        String inicio = "0";
        String largo = "0";
        String tipo = null;
        String formato = null;

        if (atributos.getNamedItem("id") != null)
            id = atributos.getNamedItem("id").getNodeValue();
        if (atributos.getNamedItem("inicio") != null)
            inicio = atributos.getNamedItem("inicio").getNodeValue();
        if (atributos.getNamedItem("largo") != null)
            largo = atributos.getNamedItem("largo").getNodeValue();
        if (atributos.getNamedItem("tipo") != null)
            tipo = atributos.getNamedItem("tipo").getNodeValue();
        if (atributos.getNamedItem("formato") != null)
            formato = atributos.getNamedItem("formato").getNodeValue();

        FieldStructureVo field = new FieldStructureVo();
        field.setId(id);
        field.setLargo(Integer.parseInt(largo));
        field.setPosicionInicial(Integer.parseInt(inicio));
        field.setTipo(tipo);
        field.setFormato(formato);
        return field;
    }
    
    
    /**
     * Testde prueba
     * @param argv
     */
    public static void main(String argv[]) {
       // ReaderConfigCore test = new ReaderConfigCore();
        ReaderConfigCore.loadConfig("C:\\workspace_jdeveloper\\BVNET2_0\\RendicionesPrevired\\Previred\\ViewController\\public_html\\WEB-INF\\config\\configfileabono.xml");
    }    
}
