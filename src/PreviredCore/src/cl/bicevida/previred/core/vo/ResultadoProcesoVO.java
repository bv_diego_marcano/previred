package cl.bicevida.previred.core.vo;

import cl.bicevida.previred.common.dto.ArchivoPlanillaEncabezadoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaHeaderDTO;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;

import java.io.Serializable;

import java.util.List;

public class ResultadoProcesoVO implements Serializable {
    @SuppressWarnings("compatibility:4923700680093287737")
    private static final long serialVersionUID = 1L;

    public ResultadoProcesoVO() {
    }
    
    private boolean ok = false;
    private Exception error = null;
    private List<ResumenAbonoDTO> lst_resumen_abono;
    private List<ArchivoPlanillaHeaderDTO> lst_header_planilla;


    public void setOk(boolean ok) {
        this.ok = ok;
    }

    public boolean isOk() {
        return ok;
    }

    public void setError(Exception error) {
        this.error = error;
    }

    public Exception getError() {
        return error;
    }

    public void setLst_resumen_abono(List<ResumenAbonoDTO> lst_resumen_abono) {
        this.lst_resumen_abono = lst_resumen_abono;
    }

    public List<ResumenAbonoDTO> getLst_resumen_abono() {
        return lst_resumen_abono;
    }

    public void setLst_header_planilla(List<ArchivoPlanillaHeaderDTO> lst_header_planilla) {
        this.lst_header_planilla = lst_header_planilla;
    }

    public List<ArchivoPlanillaHeaderDTO> getLst_header_planilla() {
        return lst_header_planilla;
    }
}
