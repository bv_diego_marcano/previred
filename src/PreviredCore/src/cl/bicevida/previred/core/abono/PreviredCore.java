package cl.bicevida.previred.core.abono;

import cl.bicevida.previred.common.dto.ArchivoAbonoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaAntecedentesDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaDetalleDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaEncabezadoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaHeaderDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaResumenDTO;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;
import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.common.utils.RutUtil;
import cl.bicevida.previred.common.utils.StringUtil;
import cl.bicevida.previred.core.config.ReaderConfigCore;
import cl.bicevida.previred.core.structure.FileStructureVo;
import cl.bicevida.previred.core.structure.RowStructureVo;
import cl.bicevida.previred.core.vo.ResultadoProcesoVO;
import cl.bicevida.previred.common.Constant;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;


public class PreviredCore implements Serializable {
    @SuppressWarnings("compatibility:3432083414665945200")
    private static final long serialVersionUID = 1L;

    /**
     * Logger for this class
     */
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PreviredCore.class);
    

    public PreviredCore() {
    }
    
    private static ReaderConfigCore xmlconfig;
  
    public  void loadConfig(String fileConfig) {
       // if (xmlconfig == null) xmlconfig = new ReaderConfigCore();        
        ReaderConfigCore.loadConfig(fileConfig);        
    }    
    
    public ResultadoProcesoVO doProcesar(String filenamepath, String tipoArchivo, Double idCarga) {
        logger.debug("doProcesar() - Inicio");
        //Investiga el tipo de structura del archivo a procesar
        FileStructureVo filePrc= null;
        //raealizo 3 intentos de lectura
        for(int i = 0; i <= 3; i++){
            try{
                filePrc = (FileStructureVo) ReaderConfigCore.config.get(tipoArchivo);  // ABONO o PLANILLA
                if(filePrc != null) 
                    break;
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        logger.debug("doProcesar() - filePrc "+ filePrc.getTipoArchivo());

        //Inicia el procesamiento segun el tipo de archivo        
        if ( filePrc.getTipoArchivo().equalsIgnoreCase("LargoFijo") ) return doProcesarLargoFijo(filenamepath, tipoArchivo);
        if ( filePrc.getTipoArchivo().equalsIgnoreCase("Excel") ) return doProcesarExcel(filenamepath, tipoArchivo, idCarga);
        //if ( filePrc.getTipoArchivo().equalsIgnoreCase("XML") ) return doProcesarXml(filenamepath, tipoArchivo);
         logger.debug("doProcesar() - termino");
        return null;
    }

    /**
     * Procesa transacciones archivo largo fijo
     * @param fileDat
     * @return
     */
    private static ResultadoProcesoVO doProcesarLargoFijo(String fileDat, String tipoArchivo) {
        logger.debug("doProcesarLargoFijo() - Inicio");
        ResultadoProcesoVO resp = new ResultadoProcesoVO();
        
        try {
            if(tipoArchivo.equalsIgnoreCase("ABONO")){
                logger.debug("doProcesarLargoFijo() - Incio proceso de archivo de ABONO");
                List<ResumenAbonoDTO> listresumenabonodto = new ArrayList<ResumenAbonoDTO>();
                FileStructureVo filePrc = (FileStructureVo) ReaderConfigCore.config.get(tipoArchivo);            
                if (filePrc != null) {                 
                    int row = 0;
                    int registros = getLineCountFile(fileDat);
                    int cantidadresumen = 0;
                    Scanner filescan = new Scanner(new File(fileDat));
                    while(filescan.hasNext()){
                        String linea = filescan.nextLine();                    
                        filePrc.setDataRow(linea);
                            
                        //Recupera campos obligatorios si es el resumen
                        if(filePrc.getTipoRegistroById("resumen").booleanValue()){
                            Integer tipo_de_registro = 1;
                            Integer identificador_iip = (Integer)filePrc.getValueRow("resumen", "identificador_iip");
                            Double total_monto_fondo = (Double)filePrc.getValueRow("resumen", "total_monto_fondo");
                            Double total_monto_institucion = (Double)filePrc.getValueRow("resumen", "total_monto_institucion");
                            Double total_monto_afc = (Double)filePrc.getValueRow("resumen", "total_monto_afc");
                            String cuenta_fondo = (String)filePrc.getValueRow("resumen", "cuenta_fondo");
                            String cuenta_institucion = (String)filePrc.getValueRow("resumen", "cuenta_institucion");
                            String cuenta_afc = (String)filePrc.getValueRow("resumen", "cuenta_afc");
                            String identificacion_banco = (String)filePrc.getValueRow("resumen", "identificacion_banco");
                            String codigo_banco = (String)filePrc.getValueRow("resumen", "codigo_banco");
                            Date fecha_abono = (Date)filePrc.getValueRow("resumen", "fecha_abono");
                            String codigo_recaudador = (String)filePrc.getValueRow("resumen", "codigo_recaudador");
                            String glosa_recaudador = (String)filePrc.getValueRow("resumen", "glosa_recaudador");
                            Integer numero_de_planillas = (Integer)filePrc.getValueRow("resumen", "numero_de_planillas");
                            
                            ResumenAbonoDTO resumen = new ResumenAbonoDTO();
                            resumen.setTipo_de_registro(tipo_de_registro);
                            resumen.setIdentificador_iip(identificador_iip);
                            resumen.setTotal_monto_fondo(total_monto_fondo);
                            resumen.setTotal_monto_institucion(total_monto_institucion);
                            resumen.setTotal_monto_afc(total_monto_afc);
                            resumen.setCuenta_fondo(cuenta_fondo.trim());
                            resumen.setCuenta_institucion(cuenta_institucion.trim());
                            resumen.setCuenta_afc(cuenta_afc.trim());
                            resumen.setIdentificacion_banco(identificacion_banco.trim());
                            resumen.setCodigo_banco(codigo_banco.trim());
                            resumen.setFecha_abono(fecha_abono);
                            resumen.setCodigo_recaudador(codigo_recaudador.trim());
                            resumen.setGlosa_recaudador(glosa_recaudador.trim());
                            resumen.setNumero_de_planillas(numero_de_planillas);
                            resumen.setArchivoabono(new ArrayList<ArchivoAbonoDTO>());
                            
                            row++;
                            cantidadresumen++;
                            listresumenabonodto.add(resumen);
                        }                                       
                        
                        // Recupera los valores del detalle del archivo
                        if(filePrc.getTipoRegistroById("detalle").booleanValue()){
                            Integer tipo_de_registro = 2;
                            Double id_folio_abono = (Double)filePrc.getValueRow("detalle", "id_folio_abono");
                            Double monto_abono_fondo = (Double)filePrc.getValueRow("detalle", "monto_abono_fondo");
                            Double monto_abono_institucion = (Double)filePrc.getValueRow("detalle", "monto_abono_institucion");
                            Double monto_abono_afc = (Double)filePrc.getValueRow("detalle", "monto_abono_afc");                        
                            Integer rut_empleador = (Integer)filePrc.getValueRow("detalle", "rut_empleador");                        
                            String digito_verficador = (String)filePrc.getValueRow("detalle", "digito_verficador");
                            Date periodo = (Date)filePrc.getValueRow("detalle", "periodo");
                            Date fecha_pago = (Date)filePrc.getValueRow("detalle", "fecha_pago");
                            String lote = (String)filePrc.getValueRow("detalle", "lote");
                            String sucursal = (String)filePrc.getValueRow("detalle", "sucursal");
                            
                            ArchivoAbonoDTO detalle = new ArchivoAbonoDTO();
                            detalle.setTipo_de_registro(tipo_de_registro);
                            detalle.setId_folio_abono(id_folio_abono);
                            detalle.setMonto_abono_fondo(monto_abono_fondo);
                            detalle.setMonto_abono_institucion(monto_abono_institucion);
                            detalle.setMonto_abono_afc(monto_abono_afc);
                            detalle.setRut_empleador(rut_empleador);
                            detalle.setDigito_verficador(digito_verficador.trim());
                            detalle.setPeriodo(periodo);
                            detalle.setFecha_pago(fecha_pago);
                            detalle.setLote(lote.trim());
                            detalle.setSucursal(sucursal.trim());
                            
                            row++;
                            
                            if(listresumenabonodto != null){
                                if( listresumenabonodto.size() > 0){
                                    if(listresumenabonodto.get(cantidadresumen-1) != null){
                                        if(listresumenabonodto.get(cantidadresumen-1).getArchivoabono() != null){
                                            listresumenabonodto.get(cantidadresumen-1).getArchivoabono().add(detalle);                                           
                                        }else{
                                            throw new Exception("No existe resumen del detalle del abono en la lista listresumenabonodto.get(" + (cantidadresumen-1) + ").getArchivoabono(): " + listresumenabonodto.get(cantidadresumen-1).getArchivoabono()); 
                                        }
                                    }else{
                                        throw new Exception("No existe resumen del detalle del abono en la lista listresumenabonodto.get(" + (cantidadresumen-1) + "): " + listresumenabonodto.get(cantidadresumen-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe resumen del detalle del abono listresumenabonodto size: " + listresumenabonodto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe resumen del detalle del abono listresumenabonodto: " + listresumenabonodto); 
                            }
                        }                
                    }   
                    
                    filescan.close();
                    
                    if(listresumenabonodto != null){
                        if(listresumenabonodto.size() > 0){
                            Iterator itrlistresumenabonodto =  listresumenabonodto.iterator();
                            while(itrlistresumenabonodto.hasNext()){
                                ResumenAbonoDTO resumen = (ResumenAbonoDTO) itrlistresumenabonodto.next();
                                if(resumen.getNumero_de_planillas().intValue() != resumen.getArchivoabono().size()){
                                    throw new Exception("En el archivo de abono, la cantidad de registros informados: " + resumen.getNumero_de_planillas().intValue() + "; es distinto a los registros procesados: " + resumen.getArchivoabono().size());                            
                                }
                            }
                        }else{
                            throw new Exception("No existe resumen abono terminado listresumenabonodto size: " + listresumenabonodto.size()); 
                        }
                        
                    }else{
                        throw new Exception("No existe resumen del abono terminado listresumenabonodto: " + listresumenabonodto); 
                    }
                    
                    if(registros != row){
                        throw new Exception("No se procesaron todas las filas registros abono: " + registros + "; procesados: " + row); 
                    }    
                    
                    resp.setOk(true);
                    
                    listresumenabonodto = checkDuplicate(listresumenabonodto);
                    
                    
                    resp.setLst_resumen_abono(listresumenabonodto);
                    
                    
                    
                    logger.debug("doProcesarLargoFijo() - Termino proceso de archivo de ABONO");
                }

            }
            
            if(tipoArchivo.equalsIgnoreCase("PLANILLA")){
                logger.debug("doProcesarLargoFijo() - Incio proceso de archivo de PLANILLA");
                List<ArchivoPlanillaHeaderDTO> listheaderdto= new ArrayList<ArchivoPlanillaHeaderDTO>();
                FileStructureVo filePrc = (FileStructureVo) ReaderConfigCore.config.get(tipoArchivo);            
                if (filePrc != null) {                 
                    int row = 0;
                    //Integer numero_declaraciones = new Integer("0");
                    int registros = getLineCountFile(fileDat);
                    int cantidadheader = 0;
                    int cantidadencabezado = 0;
                    Scanner filescan = new Scanner(new File(fileDat));
                    
                    List<ArchivoPlanillaDetalleDTO> lstdetalle = null;
                    while(filescan.hasNext()){
                        String linea = filescan.nextLine();                    
                        filePrc.setDataRow(linea);
                        
                        if(filePrc.getTipoRegistroById("header").booleanValue()){
                            Integer tipo_de_registro = 0;  
                            Integer numero_declaraciones = (Integer)filePrc.getValueRow("header", "numero_declaraciones");
                            ArchivoPlanillaHeaderDTO header = new ArchivoPlanillaHeaderDTO();
                            header.setNumero_declaraciones(numero_declaraciones);
                            header.setTipo_de_registro(tipo_de_registro);
                            header.setLst_detalle_planilla(new ArrayList<ArchivoPlanillaEncabezadoDTO>());
                            
                            row++; 
                            cantidadheader++;
                            listheaderdto.add(header);
                        }
                        
                        //Recupera campos obligatorios si es el resumen
                        if(filePrc.getTipoRegistroById("encabezado").booleanValue()){
                            lstdetalle = new ArrayList<ArchivoPlanillaDetalleDTO>();
                            Integer tipo_de_registro = 1;                            
                            Double id_folio_planilla = (Double)filePrc.getValueRow("encabezado", "id_folio_planilla");
                            Integer tipo_pago = (Integer)filePrc.getValueRow("encabezado", "tipo_pago");
                            Date fecha_operacion = (Date)filePrc.getValueRow("encabezado", "fecha_operacion");
                            Date hora_operacion = (Date)filePrc.getValueRow("encabezado", "hora_operacion");
                            Integer codigo_institucion_apv = (Integer)filePrc.getValueRow("encabezado", "codigo_institucion_apv");
                            Integer rut_pagador = (Integer)filePrc.getValueRow("encabezado", "rut_pagador");
                            String dv_pagador = (String)filePrc.getValueRow("encabezado", "dv_pagador");
                            Integer tipo_pagador = (Integer)filePrc.getValueRow("encabezado", "tipo_pagador");
                            String correo_pagador = (String)filePrc.getValueRow("encabezado", "correo_pagador");
                            
                            Date fecha_hora_operacion = null;                   
                            if(fecha_operacion != null && hora_operacion != null){
                                String fechahora = FechaUtil.getFechaFormateoCustom(fecha_operacion, "dd/MM/yyyy") + " " + FechaUtil.getFechaFormateoCustom(hora_operacion, "HH:mm:ss");
                                fecha_hora_operacion = FechaUtil.getFecha("dd/MM/yyyy HH:mm:ss", fechahora);
                                fechahora = FechaUtil.getFechaFormateoCustom(fecha_hora_operacion, "dd/MM/yyyy HH:mm:ss");
                            }
                                    
                            ArchivoPlanillaEncabezadoDTO encabezado = new ArchivoPlanillaEncabezadoDTO();
                            encabezado.setTipo_de_registro(tipo_de_registro);
                            encabezado.setId_folio_planilla(id_folio_planilla);
                            encabezado.setTipo_pago(tipo_pago);
                            encabezado.setFecha_hora_operacion(fecha_operacion);
                            encabezado.setCodigo_institucion_apv(codigo_institucion_apv);
                            encabezado.setRut_pagador(rut_pagador);
                            encabezado.setDv_pagador(dv_pagador.trim());
                            encabezado.setTipo_pagador(tipo_pagador);
                            encabezado.setCorreo_pagador(correo_pagador.trim());
                            encabezado.setAntecedentes(new ArchivoPlanillaAntecedentesDTO());
                            
                            row++;
                            cantidadencabezado++;
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            listheaderdto.get(cantidadheader-1).getLst_detalle_planilla().add(encabezado);                                           
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            }                            
                        }
                        
                        // Recupera los valores del antecedente del archivo
                        if(filePrc.getTipoRegistroById("antecedentes").booleanValue()){
                            Integer tipo_de_registro = 2;                            
                            String razon_social_pagador = (String)filePrc.getValueRow("antecedentes", "razon_social_pagador");
                            String codigo_actividad_pagador = (String)filePrc.getValueRow("antecedentes", "codigo_actividad_pagador");
                            String calle_pagador = (String)filePrc.getValueRow("antecedentes", "calle_pagador");
                            String numero_pagador = (String)filePrc.getValueRow("antecedentes", "numero_pagador");
                            String depart_pobla_pagador = (String)filePrc.getValueRow("antecedentes", "depart_pobla_pagador");
                            String comuna_pagador = (String)filePrc.getValueRow("antecedentes", "comuna_pagador");
                            String ciudad_pagador = (String)filePrc.getValueRow("antecedentes", "ciudad_pagador");
                            String region_pagador = (String)filePrc.getValueRow("antecedentes", "region_pagador");
                            String telefono_pagador = (String)filePrc.getValueRow("antecedentes", "telefono_pagador");
                            String nombre_representante_legal = (String)filePrc.getValueRow("antecedentes", "nombre_representante_legal");
                            Integer rut_representante_legal = (Integer)filePrc.getValueRow("antecedentes", "rut_representante_legal");
                            String dv_representante_legal = (String)filePrc.getValueRow("antecedentes", "dv_representante_legal");
                                                                
                            ArchivoPlanillaAntecedentesDTO antecedentes = new ArchivoPlanillaAntecedentesDTO();
                            antecedentes.setTipo_de_registro(tipo_de_registro);
                            antecedentes.setRazon_social_pagador(razon_social_pagador.trim());
                            antecedentes.setCodigo_actividad_pagador(codigo_actividad_pagador.trim());
                            antecedentes.setCalle_pagador(calle_pagador.trim());
                            antecedentes.setNumero_pagador(numero_pagador.trim());
                            antecedentes.setDepart_pobla_pagador(depart_pobla_pagador.trim());
                            antecedentes.setComuna_pagador(comuna_pagador.trim());
                            antecedentes.setCiudad_pagador(ciudad_pagador.trim());
                            antecedentes.setRegion_pagador(region_pagador.trim());
                            antecedentes.setTelefono_pagador(telefono_pagador.trim());
                            antecedentes.setNombre_representante_legal(nombre_representante_legal.trim());
                            antecedentes.setRut_representante_legal(rut_representante_legal);
                            antecedentes.setDv_representante_legal(dv_representante_legal.trim());  
                            
                            row++; 
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                            if(listencabezadodto != null){
                                                if( listencabezadodto.size() > 0){
                                                    if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                        if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                            listencabezadodto.get(cantidadencabezado-1).setAntecedentes(antecedentes);
                                                            listencabezadodto.get(cantidadencabezado-1).setDetalle(new ArrayList<ArchivoPlanillaDetalleDTO>());  
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                    }
                                                }else{
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                }
                                            }else{                                
                                                throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                            }
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            } 
                            
                        }
                        
                        // Recupera los valores del detalle del archivo
                        if(filePrc.getTipoRegistroById("detalle").booleanValue()){
                            Integer tipo_de_registro = 3;                            
                            Integer rut_trabajador = (Integer)filePrc.getValueRow("detalle", "rut_trabajador");
                            String dv_trabajador = (String)filePrc.getValueRow("detalle", "dv_trabajador");
                            String apellido_paterno_trabajador = (String)filePrc.getValueRow("detalle", "apellido_paterno_trabajador");
                            String apellido_materno_trabajador = (String)filePrc.getValueRow("detalle", "apellido_materno_trabajador");
                            String nombres_trabajador = (String)filePrc.getValueRow("detalle", "nombres_trabajador");
                            String sexo = (String)filePrc.getValueRow("detalle", "sexo");
                            Integer nacionalidad = (Integer)filePrc.getValueRow("detalle", "nacionalidad");
                            Double renta_imponible_trabajador = (Double)filePrc.getValueRow("detalle", "renta_imponible_trabajador");
                            Double cotizacion_voluntaria_apvi = (Double)filePrc.getValueRow("detalle", "cotizacion_voluntaria_apvi");
                            Double cotizacion_voluntaria_apvi_b = (Double)filePrc.getValueRow("detalle", "cotizacion_voluntaria_apvi_b");
                            String nro_contrato_apvi = (String)filePrc.getValueRow("detalle", "nro_contrato_apvi");
                            Double deposito_convenio = (Double)filePrc.getValueRow("detalle", "deposito_convenio");
                            Double total_a_pagar = (Double)filePrc.getValueRow("detalle", "total_a_pagar");
                            Date periodo_pago = (Date)filePrc.getValueRow("detalle", "periodo_pago");
                            Double apv_colectivo_empl = (Double)filePrc.getValueRow("detalle", "apv_colectivo_empl");
                            Double apv_colectivo_trabaj = (Double)filePrc.getValueRow("detalle", "apv_colectivo_trabaj");
                            String n_contrato_apvc = (String)filePrc.getValueRow("detalle", "n_contrato_apvc");
                            Integer codigo_mov_personal = (Integer)filePrc.getValueRow("detalle", "codigo_mov_personal");
                            Date fecha_inicio = (Date)filePrc.getValueRow("detalle", "fecha_inicio");
                            Date fecha_termino = (Date)filePrc.getValueRow("detalle", "fecha_termino");
                            boolean verifica = true;
                            
                            ArchivoPlanillaDetalleDTO detalle = new ArchivoPlanillaDetalleDTO();
                            detalle.setTipo_de_registro(tipo_de_registro);
                            detalle.setRut_trabajador(rut_trabajador);
                            detalle.setDv_trabajador(dv_trabajador.trim());
                            detalle.setApellido_paterno_trabajador(apellido_paterno_trabajador.trim());
                            detalle.setApellido_materno_trabajador(apellido_materno_trabajador.trim());
                            detalle.setNombres_trabajador(nombres_trabajador.trim());
                            detalle.setSexo(sexo.trim());
                            detalle.setNacionalidad(nacionalidad);
                            detalle.setRenta_imponible_trabajador(renta_imponible_trabajador);
                            detalle.setCotizacion_voluntaria_apvi(cotizacion_voluntaria_apvi);
                            detalle.setCotizacion_voluntaria_apvi_b(cotizacion_voluntaria_apvi_b);
                            detalle.setNro_contrato_apvi(nro_contrato_apvi.trim());
                            detalle.setDeposito_convenio(deposito_convenio);
                            detalle.setTotal_a_pagar(total_a_pagar);
                            detalle.setPeriodo_pago(periodo_pago);
                            detalle.setApv_colectivo_empl(apv_colectivo_empl);
                            detalle.setApv_colectivo_trabaj(apv_colectivo_trabaj);
                            detalle.setN_contrato_apvc(n_contrato_apvc.trim());
                            detalle.setCodigo_mov_personal(codigo_mov_personal);
                            detalle.setFecha_inicio(fecha_inicio);
                            detalle.setFecha_termino(fecha_termino);
                            
                            //lstdetalle.add(detalle);
                            
                            //row++;
                            
                           /* TODO:
                            * Verifica Rut Trabajador monto existe en el detalle de la planilla
                            * y montos sean mayor que cero.
                            */
                            
                            //if (cotizacion_voluntaria_apvi == 0 && deposito_convenio == 0) {
                             if (cotizacion_voluntaria_apvi == 0 && deposito_convenio == 0 && cotizacion_voluntaria_apvi_b ==0) {
                                verifica = false;
                            }else{
                                if (lstdetalle.size() > 0){ 
                                    for (int j=0;j<lstdetalle.size();j++){
                                        //Recorre la coleccion de detalles de la planilla                                    
                                        if (!lstdetalle.get(j).getRut_trabajador().equals(rut_trabajador)){
                                                System.out.println("Rut: "+ rut_trabajador + " no existe en la lista" );
                                                verifica = true; //Rut no existe en la lista
                                            }else{
                                                System.out.println("Rut " + rut_trabajador + " ya existe en la lista" );
                                                verifica = false; //Rut ya existe en la lista
                                                
                                                Double monto_apv = lstdetalle.get(j).getCotizacion_voluntaria_apvi() + cotizacion_voluntaria_apvi;
                                                Double monto_deposito_convenido = lstdetalle.get(j).getDeposito_convenio() + deposito_convenio;
                                                Double monto_apvb = lstdetalle.get(j).getCotizacion_voluntaria_apvi_b() + cotizacion_voluntaria_apvi_b;
                                                //Double monto_total_a_pagar = monto_apv + monto_deposito_convenido;
                                                Double monto_total_a_pagar = monto_apv + monto_deposito_convenido + monto_apvb;
                                                
                                                //detalle.setCotizacion_voluntaria_apvi(monto_apv);
                                                //detalle.setDeposito_convenio(monto_deposito_convenido);
                                                //detalle.setTotal_a_pagar(monto_total_a_pagar);
                                                
                                                ArchivoPlanillaDetalleDTO aux = lstdetalle.get(j);
                                                aux.setCotizacion_voluntaria_apvi(monto_apv);
                                                aux.setDeposito_convenio(monto_deposito_convenido);
                                                aux.setTotal_a_pagar(monto_total_a_pagar);
        
                                            }
                                        }
                                    } // fin for       
                            }                            
                            
                            if (verifica == true) {
                                //Agrega a la lista de detalles de la planilla
                                 System.out.println("Agrega a la lista de detalles de la planilla, Rut: "+ rut_trabajador);
                                lstdetalle.add(detalle);                                                                
                            }
                            
                            row++;
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                            if(listencabezadodto != null){
                                                if( listencabezadodto.size() > 0){
                                                    if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                        if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                            if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){
                                                                listencabezadodto.get(cantidadencabezado-1).setDetalle(lstdetalle); 
                                                                listencabezadodto.get(cantidadencabezado-1).setResumen(new ArchivoPlanillaResumenDTO());
                                                            }else{
                                                                throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                            }
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                    }
                                                }else{
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                }
                                            }else{                                
                                                throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                            }    
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            }                 
                        }
                        
                        // Recupera los valores del resumen del archivo
                        if(filePrc.getTipoRegistroById("resumen").booleanValue()){
                            Integer tipo_de_registro = 4;                            
                            Double total_cotiza_voluntaria_apvi = (Double)filePrc.getValueRow("resumen", "total_cotiza_voluntaria_apvi");
                            Double total_cotiza_voluntaria_apvi_b = (Double)filePrc.getValueRow("resumen", "total_cotiza_voluntaria_apvi_b");
                            Double total_apv_colect_empleador = (Double)filePrc.getValueRow("resumen", "total_apv_colect_empleador");
                            Double total_apv_colect_trabajador = (Double)filePrc.getValueRow("resumen", "total_apv_colect_trabajador");
                            Double total_depositos_convenidos = (Double)filePrc.getValueRow("resumen", "total_depositos_convenidos");
                            Double total_pagar_ia = (Double)filePrc.getValueRow("resumen", "total_pagar_ia");
                            Date periodo_pago = (Date)filePrc.getValueRow("resumen", "periodo_pago");
                            Integer numero_afiliados_informados = (Integer)filePrc.getValueRow("resumen", "numero_afiliados_informados");
                            
                            ArchivoPlanillaResumenDTO resumen = new ArchivoPlanillaResumenDTO();
                            resumen.setTipo_de_registro(tipo_de_registro);
                            resumen.setTotal_cotiza_voluntaria_apvi(total_cotiza_voluntaria_apvi);
                            resumen.setTotal_cotiza_voluntaria_apvi_b(total_cotiza_voluntaria_apvi_b);
                            resumen.setTotal_apv_colect_empleador(total_apv_colect_empleador);
                            resumen.setTotal_apv_colect_trabajador(total_apv_colect_trabajador);
                            resumen.setTotal_depositos_convenidos(total_depositos_convenidos);
                            resumen.setTotal_pagar_ia(total_pagar_ia);
                            resumen.setPeriodo_pago(periodo_pago);
                            resumen.setNumero_afiliados_informados(numero_afiliados_informados);
                            
                            row++; 
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                            if(listencabezadodto != null){
                                                if( listencabezadodto.size() > 0){
                                                    if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                        if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                            if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){
                                                                if(listencabezadodto.get(cantidadencabezado-1).getResumen() != null){
                                                                    listencabezadodto.get(cantidadencabezado-1).setResumen(resumen); 
                                                                }else{
                                                                    throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getResumen(): " + listencabezadodto.get(cantidadencabezado-1).getResumen()); 
                                                                }                                                
                                                            }else{
                                                                throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                            }
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                    }
                                                }else{
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                }
                                            }else{                                
                                                throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                            }  
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            }              
                        }                        
                                           
                    }                  
                                        
                    filescan.close();
                    
                    if(listheaderdto != null){
                        if(listheaderdto.size() > 0){
                            Iterator itrlistheaderdto =  listheaderdto.iterator();
                            while(itrlistheaderdto.hasNext()){
                                ArchivoPlanillaHeaderDTO header = (ArchivoPlanillaHeaderDTO) itrlistheaderdto.next();
                                if(header.getNumero_declaraciones().intValue() != header.getLst_detalle_planilla().size()){
                                    throw new Exception("En el archivo de abono, la cantidad de registros informados: " + header.getNumero_declaraciones().intValue() + "; es distinto a los registros procesados: " + header.getLst_detalle_planilla().size());                            
                                }
                            }
                        }else{
                            throw new Exception("No existe planilla del abono terminado listresumenabonodto size: " + listheaderdto.size()); 
                        }
                        
                    }else{
                        throw new Exception("No existe planilla del abono terminado listresumenabonodto: " + listheaderdto); 
                    }
                    
                    
                    if(registros != row){
                        throw new Exception("No se procesaron todas las filas registros planilla: " + registros + "; procesados: " + row); 
                    }
                    
                    
                    resp.setOk(true);
                    resp.setLst_header_planilla(listheaderdto);
                    logger.debug("doProcesarLargoFijo() - Termino proceso de archivo de PLANILLA");
                }
            }
            
            
            if(tipoArchivo.equalsIgnoreCase("PLANILLAANDES")){
                logger.debug("doProcesarLargoFijo() - Incio proceso de archivo de PLANILLAANDES");
                List<ArchivoPlanillaHeaderDTO> listheaderdto= new ArrayList<ArchivoPlanillaHeaderDTO>();
                FileStructureVo filePrc = (FileStructureVo) ReaderConfigCore.config.get(tipoArchivo);            
                if (filePrc != null) {                 
                    int row = 0;
                    //Integer numero_declaraciones = new Integer("0");
                    int registros = getLineCountFile(fileDat);
                    int cantidadheader = 0;
                    int cantidadencabezado = 0;
                    Scanner filescan = new Scanner(new File(fileDat));
                    
                    List<ArchivoPlanillaDetalleDTO> lstdetalle = null;
                    //Definiciones Abono
                    List<ArchivoAbonoDTO> lstdetalleAbono = new ArrayList<ArchivoAbonoDTO>();
                    
                    Double totalInstitucion = 0.0; 
                    
                    
                    while(filescan.hasNext()){
                        String linea = filescan.nextLine();                    
                        filePrc.setDataRow(linea);
                        
                        
                        if(filePrc.getTipoRegistroById("header").booleanValue()){
                            Integer tipo_de_registro = 0;  
                            Integer numero_declaraciones = (Integer)filePrc.getValueRow("header", "numero_declaraciones");
                            ArchivoPlanillaHeaderDTO header = new ArchivoPlanillaHeaderDTO();
                            header.setNumero_declaraciones(numero_declaraciones);
                            header.setTipo_de_registro(tipo_de_registro);
                            header.setLst_detalle_planilla(new ArrayList<ArchivoPlanillaEncabezadoDTO>());
                            
                            row++; 
                            cantidadheader++;
                            listheaderdto.add(header);
                        }
                        
                        
                        
                        //Recupera campos obligatorios si es el resumen
                        
                        if(filePrc.getTipoRegistroById("encabezado").booleanValue()){                            
                            lstdetalle = new ArrayList<ArchivoPlanillaDetalleDTO>();
                            Integer tipo_de_registro = 1;                            
                            Double id_folio_planilla = (Double)filePrc.getValueRow("encabezado", "id_folio_planilla");
                            
                            String folioplanilla = StringUtil.rellenarTextoIzquierda(String.valueOf(id_folio_planilla.longValue()), 16, "0");
                            folioplanilla = folioplanilla.substring(4, folioplanilla.length());
                            
                            folioplanilla = FechaUtil.getFechaFormateoCustom(new Date(), "yyMM") + folioplanilla;
                            
                            id_folio_planilla = Double.valueOf(folioplanilla);
                            
                            Integer tipo_pago = (Integer)filePrc.getValueRow("encabezado", "tipo_pago");
                            Date fecha_operacion = (Date)filePrc.getValueRow("encabezado", "fecha_operacion");
                            Date hora_operacion = (Date)filePrc.getValueRow("encabezado", "hora_operacion");
                            Integer codigo_institucion_apv = (Integer)filePrc.getValueRow("encabezado", "codigo_institucion_apv");
                            Integer rut_pagador = (Integer)filePrc.getValueRow("encabezado", "rut_pagador");
                            String dv_pagador = (String)filePrc.getValueRow("encabezado", "dv_pagador");
                            Integer tipo_pagador = (Integer)filePrc.getValueRow("encabezado", "tipo_pagador");
                            String correo_pagador = (String)filePrc.getValueRow("encabezado", "correo_pagador");
                            
                            Date fecha_hora_operacion = null;                   
                            if(fecha_operacion != null && hora_operacion != null){
                                String fechahora = FechaUtil.getFechaFormateoCustom(fecha_operacion, "dd/MM/yyyy") + " " + FechaUtil.getFechaFormateoCustom(hora_operacion, "HH:mm:ss");
                                fecha_hora_operacion = FechaUtil.getFecha("dd/MM/yyyy HH:mm:ss", fechahora);
                                fechahora = FechaUtil.getFechaFormateoCustom(fecha_hora_operacion, "dd/MM/yyyy HH:mm:ss");
                            }
                                    
                            ArchivoPlanillaEncabezadoDTO encabezado = new ArchivoPlanillaEncabezadoDTO();
                            encabezado.setTipo_de_registro(tipo_de_registro);
                            encabezado.setId_folio_planilla(id_folio_planilla);
                            encabezado.setTipo_pago(tipo_pago);
                            encabezado.setFecha_hora_operacion(fecha_operacion);
                            encabezado.setCodigo_institucion_apv(codigo_institucion_apv);
                            encabezado.setRut_pagador(rut_pagador);
                            encabezado.setDv_pagador(dv_pagador.trim());
                            encabezado.setTipo_pagador(tipo_pagador);
                            encabezado.setCorreo_pagador(correo_pagador.trim());
                            encabezado.setAntecedentes(new ArchivoPlanillaAntecedentesDTO());
                            
                            row++;
                            cantidadencabezado++;
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            listheaderdto.get(cantidadheader-1).getLst_detalle_planilla().add(encabezado);                                           
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            }                            
                        }
                        
                        // Recupera los valores del antecedente del archivo
                        if(filePrc.getTipoRegistroById("antecedentes").booleanValue()){
                            Integer tipo_de_registro = 2;                            
                            String razon_social_pagador = (String)filePrc.getValueRow("antecedentes", "razon_social_pagador");
                            String codigo_actividad_pagador = (String)filePrc.getValueRow("antecedentes", "codigo_actividad_pagador");
                            String calle_pagador = (String)filePrc.getValueRow("antecedentes", "calle_pagador");
                            String numero_pagador = (String)filePrc.getValueRow("antecedentes", "numero_pagador");
                            String depart_pobla_pagador = (String)filePrc.getValueRow("antecedentes", "depart_pobla_pagador");
                            String comuna_pagador = (String)filePrc.getValueRow("antecedentes", "comuna_pagador");
                            String ciudad_pagador = (String)filePrc.getValueRow("antecedentes", "ciudad_pagador");
                            String region_pagador = (String)filePrc.getValueRow("antecedentes", "region_pagador");
                            String telefono_pagador = (String)filePrc.getValueRow("antecedentes", "telefono_pagador");
                            String nombre_representante_legal = (String)filePrc.getValueRow("antecedentes", "nombre_representante_legal");
                            Integer rut_representante_legal = (Integer)filePrc.getValueRow("antecedentes", "rut_representante_legal");
                            String dv_representante_legal = (String)filePrc.getValueRow("antecedentes", "dv_representante_legal");
                                                                
                            ArchivoPlanillaAntecedentesDTO antecedentes = new ArchivoPlanillaAntecedentesDTO();
                            antecedentes.setTipo_de_registro(tipo_de_registro);
                            antecedentes.setRazon_social_pagador(razon_social_pagador.trim());
                            antecedentes.setCodigo_actividad_pagador(codigo_actividad_pagador.trim());
                            antecedentes.setCalle_pagador(calle_pagador.trim());
                            antecedentes.setNumero_pagador(numero_pagador.trim());
                            antecedentes.setDepart_pobla_pagador(depart_pobla_pagador.trim());
                            antecedentes.setComuna_pagador(comuna_pagador.trim());
                            antecedentes.setCiudad_pagador(ciudad_pagador.trim());
                            antecedentes.setRegion_pagador(region_pagador.trim());
                            antecedentes.setTelefono_pagador(telefono_pagador.trim());
                            antecedentes.setNombre_representante_legal(nombre_representante_legal.trim());
                            antecedentes.setRut_representante_legal(rut_representante_legal);
                            antecedentes.setDv_representante_legal(dv_representante_legal.trim());  
                            
                            row++; 
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                            if(listencabezadodto != null){
                                                if( listencabezadodto.size() > 0){
                                                    if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                        if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                            listencabezadodto.get(cantidadencabezado-1).setAntecedentes(antecedentes);
                                                            listencabezadodto.get(cantidadencabezado-1).setDetalle(new ArrayList<ArchivoPlanillaDetalleDTO>());  
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                    }
                                                }else{
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                }
                                            }else{                                
                                                throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                            }
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            } 
                            
                        }
                        
                        // Recupera los valores del detalle del archivo
                        if(filePrc.getTipoRegistroById("detalle").booleanValue()){
                            Integer tipo_de_registro = 3;                            
                            Integer rut_trabajador = (Integer)filePrc.getValueRow("detalle", "rut_trabajador");
                            String dv_trabajador = (String)filePrc.getValueRow("detalle", "dv_trabajador");
                            String apellido_paterno_trabajador = (String)filePrc.getValueRow("detalle", "apellido_paterno_trabajador");
                            String apellido_materno_trabajador = (String)filePrc.getValueRow("detalle", "apellido_materno_trabajador");
                            String nombres_trabajador = (String)filePrc.getValueRow("detalle", "nombres_trabajador");
                            String sexo = (String)filePrc.getValueRow("detalle", "sexo");
                            Double renta_imponible_trabajador = (Double)filePrc.getValueRow("detalle", "renta_imponible_trabajador");
                            Double cotizacion_voluntaria_apvi = (Double)filePrc.getValueRow("detalle", "cotizacion_voluntaria_apvi");
                            Double deposito_convenio = (Double)filePrc.getValueRow("detalle", "deposito_convenio");
                            Double total_a_pagar = (Double)filePrc.getValueRow("detalle", "total_a_pagar");
                            Date periodo_pago = (Date)filePrc.getValueRow("detalle", "periodo_pago");
                            
                            Integer nacionalidad = 0;
                            Double cotizacion_voluntaria_apvi_b = 0.0;
                            String nro_contrato_apvi = null;
                            Double apv_colectivo_empl = 0.0;
                            Double apv_colectivo_trabaj = 0.0;
                            String n_contrato_apvc = null;
                            Integer codigo_mov_personal = null;
                            Date fecha_inicio = new Date();
                            Date fecha_termino = new Date();
                            boolean verifica = true;
                                                                
                            ArchivoPlanillaDetalleDTO detalle = new ArchivoPlanillaDetalleDTO();
                            detalle.setTipo_de_registro(tipo_de_registro);
                            detalle.setRut_trabajador(rut_trabajador);
                            detalle.setDv_trabajador(dv_trabajador.trim());
                            detalle.setApellido_paterno_trabajador(apellido_paterno_trabajador.trim());
                            detalle.setApellido_materno_trabajador(apellido_materno_trabajador.trim());
                            detalle.setNombres_trabajador(nombres_trabajador.trim());
                            detalle.setSexo(sexo.trim());
                            detalle.setNacionalidad(nacionalidad);
                            detalle.setRenta_imponible_trabajador(renta_imponible_trabajador);
                            detalle.setCotizacion_voluntaria_apvi(cotizacion_voluntaria_apvi);
                            detalle.setCotizacion_voluntaria_apvi_b(cotizacion_voluntaria_apvi_b);
                            detalle.setNro_contrato_apvi(nro_contrato_apvi);
                            detalle.setDeposito_convenio(deposito_convenio);
                            detalle.setTotal_a_pagar(total_a_pagar);
                            detalle.setPeriodo_pago(periodo_pago);
                            detalle.setApv_colectivo_empl(apv_colectivo_empl);
                            detalle.setApv_colectivo_trabaj(apv_colectivo_trabaj);
                            detalle.setN_contrato_apvc(n_contrato_apvc);
                            detalle.setCodigo_mov_personal(codigo_mov_personal);
                            detalle.setFecha_inicio(fecha_inicio);
                            detalle.setFecha_termino(fecha_termino);
                            
                            //lstdetalle.add(detalle);
                            
                            /*
                             * TODO: 
                             * Verifica Rut Trabajador monto existe en el detalle de la planilla
                             * y montos sean mayor que cero.                             
                             */
                             
                             //if (cotizacion_voluntaria_apvi == 0 && deposito_convenio == 0) {
                             if (cotizacion_voluntaria_apvi == 0 && deposito_convenio == 0 && cotizacion_voluntaria_apvi_b ==0) {                              
                                 verifica = false;
                             }else{ 
                                if (lstdetalle.size() > 0){ 
                                    for (int j=0;j<lstdetalle.size();j++){
                                        //Recorre la coleccion de detalles de la planilla                                    
                                        if (!lstdetalle.get(j).getRut_trabajador().equals(rut_trabajador)){
                                            System.out.println("Rut: "+ rut_trabajador + " no existe en la lista" );
                                            verifica = true; //Rut no existe en la lista
                                        }else{
                                            System.out.println("Rut: "+ rut_trabajador + " ya existe en la lista" );
                                            verifica = false; //Rut ya existe en la lista

                                            Double monto_apv = lstdetalle.get(j).getCotizacion_voluntaria_apvi() + cotizacion_voluntaria_apvi;
                                            Double monto_deposito_convenido = lstdetalle.get(j).getDeposito_convenio() + deposito_convenio;
                                            Double monto_apvb = lstdetalle.get(j).getCotizacion_voluntaria_apvi_b() + cotizacion_voluntaria_apvi_b;
                                            //Double monto_total_a_pagar = monto_apv + monto_deposito_convenido;
                                            Double monto_total_a_pagar = (monto_apv + monto_deposito_convenido + monto_apvb);

                                            //detalle.setCotizacion_voluntaria_apvi(monto_apv);
                                            //detalle.setDeposito_convenio(monto_deposito_convenido);
                                            //detalle.setTotal_a_pagar(monto_total_a_pagar);
                                                  
                                            ArchivoPlanillaDetalleDTO aux = lstdetalle.get(j);
                                            aux.setCotizacion_voluntaria_apvi(monto_apv);
                                            aux.setDeposito_convenio(monto_deposito_convenido);
                                            aux.setTotal_a_pagar(monto_total_a_pagar);
        
                                        }
                                    }// fin for
                                } 
                             }
                              
                            if (verifica == true) {
                                //Agrega a la lista de detalles de la planilla
                                lstdetalle.add(detalle); 
                                                                  
                            }
                            
                            row++; 
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                            if(listencabezadodto != null){
                                                if( listencabezadodto.size() > 0){
                                                    if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                        if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                            if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){
                                                                listencabezadodto.get(cantidadencabezado-1).setDetalle(lstdetalle); 
                                                                listencabezadodto.get(cantidadencabezado-1).setResumen(new ArchivoPlanillaResumenDTO());
                                                            }else{
                                                                throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                            }
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                    }
                                                }else{
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                }
                                            }else{                                
                                                throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                            }    
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            }                 
                        }
                        
                        // Recupera los valores del resumen del archivo
                        if(filePrc.getTipoRegistroById("resumen").booleanValue()){
                            Integer tipo_de_registro = 4;                            
                            Double total_cotiza_voluntaria_apvi = (Double)filePrc.getValueRow("resumen", "total_cotiza_voluntaria_apvi");
                            Double total_depositos_convenidos = (Double)filePrc.getValueRow("resumen", "total_depositos_convenidos");
                            Double total_pagar_ia = (Double)filePrc.getValueRow("resumen", "total_pagar_ia");
                            Date periodo_pago = (Date)filePrc.getValueRow("resumen", "periodo_pago");
                            Integer numero_afiliados_informados = (Integer)filePrc.getValueRow("resumen", "numero_afiliados_informados");
                            
                            
                            Double total_cotiza_voluntaria_apvi_b = 0.0;
                            Double total_apv_colect_empleador = 0.0;
                            Double total_apv_colect_trabajador = 0.0;
                            
                            ArchivoPlanillaResumenDTO resumen = new ArchivoPlanillaResumenDTO();
                            resumen.setTipo_de_registro(tipo_de_registro);
                            resumen.setTotal_cotiza_voluntaria_apvi(total_cotiza_voluntaria_apvi);
                            resumen.setTotal_cotiza_voluntaria_apvi_b(total_cotiza_voluntaria_apvi_b);
                            resumen.setTotal_apv_colect_empleador(total_apv_colect_empleador);
                            resumen.setTotal_apv_colect_trabajador(total_apv_colect_trabajador);
                            resumen.setTotal_depositos_convenidos(total_depositos_convenidos);
                            resumen.setTotal_pagar_ia(total_pagar_ia);
                            resumen.setPeriodo_pago(periodo_pago);
                            resumen.setNumero_afiliados_informados(numero_afiliados_informados);
                            
                            
                            
                            row++; 
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                            if(listencabezadodto != null){
                                                if( listencabezadodto.size() > 0){
                                                    if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                        if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                            if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){
                                                                if(listencabezadodto.get(cantidadencabezado-1).getResumen() != null){
                                                                    listencabezadodto.get(cantidadencabezado-1).setResumen(resumen);                                                                    
                                                                    
                                                                    // Definicion del abono
                                                                    ArchivoAbonoDTO detalleAbono = new ArchivoAbonoDTO();                                                                    
                                                                    //Incia carga datos Abono                            
                                                                    detalleAbono.setTipo_de_registro(2);                           
                                                                    detalleAbono.setId_folio_abono(listencabezadodto.get(cantidadencabezado-1).getId_folio_planilla());                            
                                                                    detalleAbono.setMonto_abono_fondo(0.0);
                                                                    detalleAbono.setMonto_abono_afc(0.0);
                                                                    detalleAbono.setRut_empleador(listencabezadodto.get(cantidadencabezado-1).getRut_pagador());
                                                                    detalleAbono.setDigito_verficador(listencabezadodto.get(cantidadencabezado-1).getDv_pagador());
                                                                    detalleAbono.setFecha_pago(listencabezadodto.get(cantidadencabezado-1).getFecha_hora_operacion());
                                                                    detalleAbono.setLote(null);
                                                                    detalleAbono.setSucursal(null);
                                                                    detalleAbono.setMonto_abono_institucion(resumen.getTotal_pagar_ia());                              
                                                                    detalleAbono.setPeriodo(resumen.getPeriodo_pago());
                                                                    totalInstitucion = totalInstitucion + detalleAbono.getMonto_abono_institucion();
                                                                    
                                                                    // Generar lista de Abono
                                                                    if(detalleAbono != null){
                                                                        lstdetalleAbono.add(detalleAbono);
                                                                    }
                                                                    
                                                                }else{
                                                                    throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getResumen(): " + listencabezadodto.get(cantidadencabezado-1).getResumen()); 
                                                                }                                                
                                                            }else{
                                                                throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                            }
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                    }
                                                }else{
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                }
                                            }else{                                
                                                throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                            }  
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            }  
                            
                        }
                        
                        
                    } // fin while                 
                                        
                    filescan.close();
                    
                    
                    ResumenAbonoDTO resumen = new ResumenAbonoDTO();
                    resumen.setTipo_de_registro(1);
                    resumen.setIdentificador_iip(0);
                    resumen.setTotal_monto_fondo(0.0);
                    resumen.setTotal_monto_institucion(totalInstitucion);
                    resumen.setTotal_monto_afc(0.0);
                    resumen.setCuenta_fondo(null);
                    resumen.setCuenta_institucion(Constant.CUENTA_BANCOCHILE_ANDES);
                    resumen.setCuenta_afc(null);
                    resumen.setIdentificacion_banco(Constant.GLOSA_BANCOCHILE_ANDES);
                    resumen.setCodigo_banco(Constant.COD_BANCOCHILE_ANDES);
                    resumen.setFecha_abono(new Date());
                    resumen.setCodigo_recaudador(Constant.COD_RECAUDADOR_ANDES);
                    resumen.setGlosa_recaudador(Constant.GLOSA_RECAUDADOR_ANDES);
                    resumen.setArchivoabono(lstdetalleAbono);
                    
                    
                    if(listheaderdto != null){
                        if(listheaderdto.size() > 0){
                            Iterator itrlistheaderdto =  listheaderdto.iterator();
                            while(itrlistheaderdto.hasNext()){
                                ArchivoPlanillaHeaderDTO header = (ArchivoPlanillaHeaderDTO) itrlistheaderdto.next();
                                if(header.getNumero_declaraciones().intValue() != header.getLst_detalle_planilla().size()){
                                    throw new Exception("En el archivo de abono, la cantidad de registros informados: " + header.getNumero_declaraciones().intValue() + "; es distinto a los registros procesados: " + header.getLst_detalle_planilla().size());                            
                                }else{
                                    // Set Cantidad resumen del Abono                                    
                                    resumen.setNumero_de_planillas(header.getNumero_declaraciones());
                                }
                            }
                        }else{
                            throw new Exception("No existe planilla del abono terminado listresumenabonodto size: " + listheaderdto.size()); 
                        }
                        
                    }else{
                        throw new Exception("No existe planilla del abono terminado listresumenabonodto: " + listheaderdto); 
                    }
                    
                    
                    if(registros != row){
                        throw new Exception("No se procesaron todas las filas registros planilla: " + registros + "; procesados: " + row); 
                    }
                    
                    List<ResumenAbonoDTO> lstResumenAbonoDTO = new ArrayList<ResumenAbonoDTO>();
                    lstResumenAbonoDTO.add(resumen);                    
                    
                    resp.setOk(true);
                    resp.setLst_header_planilla(listheaderdto);
                    resp.setLst_resumen_abono(lstResumenAbonoDTO);
                    logger.debug("doProcesarLargoFijo() - Termino proceso de archivo de PLANILLA");
                }
                
                
            }
            
            
            if(tipoArchivo.equalsIgnoreCase("PLANILLALAARAUCANA")){
                logger.debug("doProcesarLargoFijo() - Incio proceso de archivo de PLANILLALAARAUCANA");
                List<ArchivoPlanillaHeaderDTO> listheaderdto= new ArrayList<ArchivoPlanillaHeaderDTO>();
                FileStructureVo filePrc = (FileStructureVo) ReaderConfigCore.config.get(tipoArchivo);            
                if (filePrc != null) {                 
                    int row = 0;
                    //Integer numero_declaraciones = new Integer("0");
                    int registros = getLineCountFile(fileDat);
                    int cantidadheader = 0;
                    int cantidadencabezado = 0;
                    Scanner filescan = new Scanner(new File(fileDat));
                    
                    List<ArchivoPlanillaDetalleDTO> lstdetalle = null;
                    //Definiciones Abono
                    List<ArchivoAbonoDTO> lstdetalleAbono = new ArrayList<ArchivoAbonoDTO>();
                    
                    Double totalInstitucion = 0.0; 
                    
                    
                    while(filescan.hasNext()){
                        String linea = filescan.nextLine();                    
                        filePrc.setDataRow(linea);
                        
                        
                        if(filePrc.getTipoRegistroById("header").booleanValue()){
                            Integer tipo_de_registro = 0;  
                            Integer numero_declaraciones = (Integer)filePrc.getValueRow("header", "numero_declaraciones");
                            ArchivoPlanillaHeaderDTO header = new ArchivoPlanillaHeaderDTO();
                            header.setNumero_declaraciones(numero_declaraciones);
                            header.setTipo_de_registro(tipo_de_registro);
                            header.setLst_detalle_planilla(new ArrayList<ArchivoPlanillaEncabezadoDTO>());
                            
                            row++; 
                            cantidadheader++;
                            listheaderdto.add(header);
                        }
                        
                        
                        
                        //Recupera campos obligatorios si es el resumen
                        
                        if(filePrc.getTipoRegistroById("encabezado").booleanValue()){                            
                            lstdetalle = new ArrayList<ArchivoPlanillaDetalleDTO>();
                            Integer tipo_de_registro = 1;                            
                            Double id_folio_planilla = (Double)filePrc.getValueRow("encabezado", "id_folio_planilla");
                            
                            String folioplanilla = StringUtil.rellenarTextoIzquierda(String.valueOf(id_folio_planilla.longValue()), 16, "0");
                            folioplanilla = folioplanilla.substring(4, folioplanilla.length());
                            
                            folioplanilla = FechaUtil.getFechaFormateoCustom(new Date(), "yyMM") + folioplanilla;
                            
                            id_folio_planilla = Double.valueOf(folioplanilla);
                            
                            Integer tipo_pago = (Integer)filePrc.getValueRow("encabezado", "tipo_pago");
                            Date fecha_operacion = (Date)filePrc.getValueRow("encabezado", "fecha_operacion");
                            Date hora_operacion = (Date)filePrc.getValueRow("encabezado", "hora_operacion");
                            Integer codigo_institucion_apv = (Integer)filePrc.getValueRow("encabezado", "codigo_institucion_apv");
                            Integer rut_pagador = (Integer)filePrc.getValueRow("encabezado", "rut_pagador");
                            String dv_pagador = (String)filePrc.getValueRow("encabezado", "dv_pagador");
                            Integer tipo_pagador = (Integer)filePrc.getValueRow("encabezado", "tipo_pagador");
                            String correo_pagador = (String)filePrc.getValueRow("encabezado", "correo_pagador");
                            
                            Date fecha_hora_operacion = null;                   
                            if(fecha_operacion != null && hora_operacion != null){
                                String fechahora = FechaUtil.getFechaFormateoCustom(fecha_operacion, "dd/MM/yyyy") + " " + FechaUtil.getFechaFormateoCustom(hora_operacion, "HH:mm:ss");
                                fecha_hora_operacion = FechaUtil.getFecha("dd/MM/yyyy HH:mm:ss", fechahora);
                                fechahora = FechaUtil.getFechaFormateoCustom(fecha_hora_operacion, "dd/MM/yyyy HH:mm:ss");
                            }
                                    
                            ArchivoPlanillaEncabezadoDTO encabezado = new ArchivoPlanillaEncabezadoDTO();
                            encabezado.setTipo_de_registro(tipo_de_registro);
                            encabezado.setId_folio_planilla(id_folio_planilla);
                            encabezado.setTipo_pago(tipo_pago);
                            encabezado.setFecha_hora_operacion(fecha_operacion);
                            encabezado.setCodigo_institucion_apv(codigo_institucion_apv);
                            encabezado.setRut_pagador(rut_pagador);
                            encabezado.setDv_pagador(dv_pagador.trim());
                            encabezado.setTipo_pagador(tipo_pagador);
                            encabezado.setCorreo_pagador(correo_pagador.trim());
                            encabezado.setAntecedentes(new ArchivoPlanillaAntecedentesDTO());
                            
                            row++;
                            cantidadencabezado++;
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            listheaderdto.get(cantidadheader-1).getLst_detalle_planilla().add(encabezado);                                           
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            }                            
                        }
                        
                        // Recupera los valores del antecedente del archivo
                        if(filePrc.getTipoRegistroById("antecedentes").booleanValue()){
                            Integer tipo_de_registro = 2;                            
                            String razon_social_pagador = (String)filePrc.getValueRow("antecedentes", "razon_social_pagador");
                            String codigo_actividad_pagador = (String)filePrc.getValueRow("antecedentes", "codigo_actividad_pagador");
                            String calle_pagador = (String)filePrc.getValueRow("antecedentes", "calle_pagador");
                            String numero_pagador = (String)filePrc.getValueRow("antecedentes", "numero_pagador");
                            String depart_pobla_pagador = (String)filePrc.getValueRow("antecedentes", "depart_pobla_pagador");
                            String comuna_pagador = (String)filePrc.getValueRow("antecedentes", "comuna_pagador");
                            String ciudad_pagador = (String)filePrc.getValueRow("antecedentes", "ciudad_pagador");
                            String region_pagador = (String)filePrc.getValueRow("antecedentes", "region_pagador");
                            String telefono_pagador = (String)filePrc.getValueRow("antecedentes", "telefono_pagador");
                            String nombre_representante_legal = (String)filePrc.getValueRow("antecedentes", "nombre_representante_legal");
                            Integer rut_representante_legal = (Integer)filePrc.getValueRow("antecedentes", "rut_representante_legal");
                            String dv_representante_legal = (String)filePrc.getValueRow("antecedentes", "dv_representante_legal");
                                                                
                            ArchivoPlanillaAntecedentesDTO antecedentes = new ArchivoPlanillaAntecedentesDTO();
                            antecedentes.setTipo_de_registro(tipo_de_registro);
                            antecedentes.setRazon_social_pagador(razon_social_pagador.trim());
                            antecedentes.setCodigo_actividad_pagador(codigo_actividad_pagador.trim());
                            antecedentes.setCalle_pagador(calle_pagador.trim());
                            antecedentes.setNumero_pagador(numero_pagador.trim());
                            antecedentes.setDepart_pobla_pagador(depart_pobla_pagador.trim());
                            antecedentes.setComuna_pagador(comuna_pagador.trim());
                            antecedentes.setCiudad_pagador(ciudad_pagador.trim());
                            antecedentes.setRegion_pagador(region_pagador.trim());
                            antecedentes.setTelefono_pagador(telefono_pagador.trim());
                            antecedentes.setNombre_representante_legal(nombre_representante_legal.trim());
                            antecedentes.setRut_representante_legal(rut_representante_legal);
                            antecedentes.setDv_representante_legal(dv_representante_legal.trim());  
                            
                            row++; 
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                            if(listencabezadodto != null){
                                                if( listencabezadodto.size() > 0){
                                                    if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                        if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                            listencabezadodto.get(cantidadencabezado-1).setAntecedentes(antecedentes);
                                                            listencabezadodto.get(cantidadencabezado-1).setDetalle(new ArrayList<ArchivoPlanillaDetalleDTO>());  
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                    }
                                                }else{
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                }
                                            }else{                                
                                                throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                            }
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            } 
                            
                        }
                        
                        // Recupera los valores del detalle del archivo
                        if(filePrc.getTipoRegistroById("detalle").booleanValue()){
                            Integer tipo_de_registro = 3;                            
                            Integer rut_trabajador = (Integer)filePrc.getValueRow("detalle", "rut_trabajador");
                            String dv_trabajador = (String)filePrc.getValueRow("detalle", "dv_trabajador");
                            String apellido_paterno_trabajador = (String)filePrc.getValueRow("detalle", "apellido_paterno_trabajador");
                            String apellido_materno_trabajador = (String)filePrc.getValueRow("detalle", "apellido_materno_trabajador");
                            String nombres_trabajador = (String)filePrc.getValueRow("detalle", "nombres_trabajador");
                            String sexo = (String)filePrc.getValueRow("detalle", "sexo");
                            Double renta_imponible_trabajador = (Double)filePrc.getValueRow("detalle", "renta_imponible_trabajador");
                            Double cotizacion_voluntaria_apvi = (Double)filePrc.getValueRow("detalle", "cotizacion_voluntaria_apvi");
                            Double deposito_convenio = (Double)filePrc.getValueRow("detalle", "deposito_convenio");
                            Double total_a_pagar = (Double)filePrc.getValueRow("detalle", "total_a_pagar");
                            Date periodo_pago = (Date)filePrc.getValueRow("detalle", "periodo_pago");
                            
                            Integer nacionalidad = 0;
                            Double cotizacion_voluntaria_apvi_b = 0.0;
                            String nro_contrato_apvi = null;
                            Double apv_colectivo_empl = 0.0;
                            Double apv_colectivo_trabaj = 0.0;
                            String n_contrato_apvc = null;
                            Integer codigo_mov_personal = null;
                            Date fecha_inicio = new Date();
                            Date fecha_termino = new Date();
                            boolean verifica = true;
                                                                
                            ArchivoPlanillaDetalleDTO detalle = new ArchivoPlanillaDetalleDTO();
                            detalle.setTipo_de_registro(tipo_de_registro);
                            detalle.setRut_trabajador(rut_trabajador);
                            detalle.setDv_trabajador(dv_trabajador.trim());
                            detalle.setApellido_paterno_trabajador(apellido_paterno_trabajador.trim());
                            detalle.setApellido_materno_trabajador(apellido_materno_trabajador.trim());
                            detalle.setNombres_trabajador(nombres_trabajador.trim());
                            detalle.setSexo(sexo.trim());
                            detalle.setNacionalidad(nacionalidad);
                            detalle.setRenta_imponible_trabajador(renta_imponible_trabajador);
                            detalle.setCotizacion_voluntaria_apvi(cotizacion_voluntaria_apvi);
                            detalle.setCotizacion_voluntaria_apvi_b(cotizacion_voluntaria_apvi_b);
                            detalle.setNro_contrato_apvi(nro_contrato_apvi);
                            detalle.setDeposito_convenio(deposito_convenio);
                            detalle.setTotal_a_pagar(total_a_pagar);
                            detalle.setPeriodo_pago(periodo_pago);
                            detalle.setApv_colectivo_empl(apv_colectivo_empl);
                            detalle.setApv_colectivo_trabaj(apv_colectivo_trabaj);
                            detalle.setN_contrato_apvc(n_contrato_apvc);
                            detalle.setCodigo_mov_personal(codigo_mov_personal);
                            detalle.setFecha_inicio(fecha_inicio);
                            detalle.setFecha_termino(fecha_termino);
                            
                            //lstdetalle.add(detalle);
                            
                            /*
                             * TODO: 
                             * Verifica Rut Trabajador monto existe en el detalle de la planilla
                             * y montos sean mayor que cero.                             
                             */
                             
                             //if (cotizacion_voluntaria_apvi == 0 && deposito_convenio == 0) {
                             if (cotizacion_voluntaria_apvi == 0 && deposito_convenio == 0 && cotizacion_voluntaria_apvi_b ==0) {                              
                                 verifica = false;
                             }else{ 
                                if (lstdetalle.size() > 0){ 
                                    for (int j=0;j<lstdetalle.size();j++){
                                        //Recorre la coleccion de detalles de la planilla                                    
                                        if (!lstdetalle.get(j).getRut_trabajador().equals(rut_trabajador)){
                                            System.out.println("Rut: "+ rut_trabajador + " no existe en la lista" );
                                            verifica = true; //Rut no existe en la lista
                                        }else{
                                            System.out.println("Rut: "+ rut_trabajador + " ya existe en la lista" );
                                            verifica = false; //Rut ya existe en la lista

                                            Double monto_apv = lstdetalle.get(j).getCotizacion_voluntaria_apvi() + cotizacion_voluntaria_apvi;
                                            Double monto_deposito_convenido = lstdetalle.get(j).getDeposito_convenio() + deposito_convenio;
                                            Double monto_apvb = lstdetalle.get(j).getCotizacion_voluntaria_apvi_b() + cotizacion_voluntaria_apvi_b;
                                            //Double monto_total_a_pagar = monto_apv + monto_deposito_convenido;
                                            Double monto_total_a_pagar = (monto_apv + monto_deposito_convenido + monto_apvb);

                                            //detalle.setCotizacion_voluntaria_apvi(monto_apv);
                                            //detalle.setDeposito_convenio(monto_deposito_convenido);
                                            //detalle.setTotal_a_pagar(monto_total_a_pagar);
                                                  
                                            ArchivoPlanillaDetalleDTO aux = lstdetalle.get(j);
                                            aux.setCotizacion_voluntaria_apvi(monto_apv);
                                            aux.setDeposito_convenio(monto_deposito_convenido);
                                            aux.setTotal_a_pagar(monto_total_a_pagar);
            
                                        }
                                    }// fin for
                                } 
                             }
                              
                            if (verifica == true) {
                                //Agrega a la lista de detalles de la planilla
                                lstdetalle.add(detalle); 
                                                                  
                            }
                            
                            row++; 
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                            if(listencabezadodto != null){
                                                if( listencabezadodto.size() > 0){
                                                    if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                        if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                            if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){
                                                                listencabezadodto.get(cantidadencabezado-1).setDetalle(lstdetalle); 
                                                                listencabezadodto.get(cantidadencabezado-1).setResumen(new ArchivoPlanillaResumenDTO());
                                                            }else{
                                                                throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                            }
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                    }
                                                }else{
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                }
                                            }else{                                
                                                throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                            }    
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            }                 
                        }
                        
                        // Recupera los valores del resumen del archivo
                        if(filePrc.getTipoRegistroById("resumen").booleanValue()){
                            Integer tipo_de_registro = 4;                            
                            Double total_cotiza_voluntaria_apvi = (Double)filePrc.getValueRow("resumen", "total_cotiza_voluntaria_apvi");
                            Double total_depositos_convenidos = (Double)filePrc.getValueRow("resumen", "total_depositos_convenidos");
                            Double total_pagar_ia = (Double)filePrc.getValueRow("resumen", "total_pagar_ia");
                            Date periodo_pago = (Date)filePrc.getValueRow("resumen", "periodo_pago");
                            Integer numero_afiliados_informados = (Integer)filePrc.getValueRow("resumen", "numero_afiliados_informados");
                            
                            
                            Double total_cotiza_voluntaria_apvi_b = 0.0;
                            Double total_apv_colect_empleador = 0.0;
                            Double total_apv_colect_trabajador = 0.0;
                            
                            ArchivoPlanillaResumenDTO resumen = new ArchivoPlanillaResumenDTO();
                            resumen.setTipo_de_registro(tipo_de_registro);
                            resumen.setTotal_cotiza_voluntaria_apvi(total_cotiza_voluntaria_apvi);
                            resumen.setTotal_cotiza_voluntaria_apvi_b(total_cotiza_voluntaria_apvi_b);
                            resumen.setTotal_apv_colect_empleador(total_apv_colect_empleador);
                            resumen.setTotal_apv_colect_trabajador(total_apv_colect_trabajador);
                            resumen.setTotal_depositos_convenidos(total_depositos_convenidos);
                            resumen.setTotal_pagar_ia(total_pagar_ia);
                            resumen.setPeriodo_pago(periodo_pago);
                            resumen.setNumero_afiliados_informados(numero_afiliados_informados);
                            
                            
                            
                            row++; 
                            
                            if(listheaderdto != null){
                                if( listheaderdto.size() > 0){
                                    if(listheaderdto.get(cantidadheader-1) != null){
                                        if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                            List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                            if(listencabezadodto != null){
                                                if( listencabezadodto.size() > 0){
                                                    if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                        if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                            if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){
                                                                if(listencabezadodto.get(cantidadencabezado-1).getResumen() != null){
                                                                    listencabezadodto.get(cantidadencabezado-1).setResumen(resumen);                                                                    
                                                                    
                                                                    // Definicion del abono
                                                                    ArchivoAbonoDTO detalleAbono = new ArchivoAbonoDTO();                                                                    
                                                                    //Incia carga datos Abono                            
                                                                    detalleAbono.setTipo_de_registro(2);                           
                                                                    detalleAbono.setId_folio_abono(listencabezadodto.get(cantidadencabezado-1).getId_folio_planilla());                            
                                                                    detalleAbono.setMonto_abono_fondo(0.0);
                                                                    detalleAbono.setMonto_abono_afc(0.0);
                                                                    detalleAbono.setRut_empleador(listencabezadodto.get(cantidadencabezado-1).getRut_pagador());
                                                                    detalleAbono.setDigito_verficador(listencabezadodto.get(cantidadencabezado-1).getDv_pagador());
                                                                    detalleAbono.setFecha_pago(listencabezadodto.get(cantidadencabezado-1).getFecha_hora_operacion());
                                                                    detalleAbono.setLote(null);
                                                                    detalleAbono.setSucursal(null);
                                                                    detalleAbono.setMonto_abono_institucion(resumen.getTotal_pagar_ia());                              
                                                                    detalleAbono.setPeriodo(resumen.getPeriodo_pago());
                                                                    totalInstitucion = totalInstitucion + detalleAbono.getMonto_abono_institucion();
                                                                    
                                                                    // Generar lista de Abono
                                                                    if(detalleAbono != null){
                                                                        lstdetalleAbono.add(detalleAbono);
                                                                    }
                                                                    
                                                                }else{
                                                                    throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getResumen(): " + listencabezadodto.get(cantidadencabezado-1).getResumen()); 
                                                                }                                                
                                                            }else{
                                                                throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                            }
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                    }
                                                }else{
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                }
                                            }else{                                
                                                throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                            }  
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                    }
                                }else{
                                    throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                }
                            }else{                                
                                throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                            }  
                            
                        }
                        
                        
                    } // fin while                 
                                        
                    filescan.close();
                    
                    
                    ResumenAbonoDTO resumen = new ResumenAbonoDTO();
                    resumen.setTipo_de_registro(1);
                    resumen.setIdentificador_iip(0);
                    resumen.setTotal_monto_fondo(0.0);
                    resumen.setTotal_monto_institucion(totalInstitucion);
                    resumen.setTotal_monto_afc(0.0);
                    resumen.setCuenta_fondo(null);
                    resumen.setCuenta_institucion(Constant.CUENTA_BANCOSANTANDER_LAARAUCANA);
                    resumen.setCuenta_afc(null);
                    resumen.setIdentificacion_banco(Constant.GLOSA_BANCOSANTANDER_LAARAUCANA);
                    resumen.setCodigo_banco(Constant.COD_BANCOSANTANDER_LAARAUCANA);
                    resumen.setFecha_abono(new Date());
                    resumen.setCodigo_recaudador(Constant.COD_RECAUDADOR_LAARAUCANA);
                    resumen.setGlosa_recaudador(Constant.GLOSA_RECAUDADOR_LAARAUCANA);
                    resumen.setArchivoabono(lstdetalleAbono);
                    
                    
                    if(listheaderdto != null){
                        if(listheaderdto.size() > 0){
                            Iterator itrlistheaderdto =  listheaderdto.iterator();
                            while(itrlistheaderdto.hasNext()){
                                ArchivoPlanillaHeaderDTO header = (ArchivoPlanillaHeaderDTO) itrlistheaderdto.next();
                                if(header.getNumero_declaraciones().intValue() != header.getLst_detalle_planilla().size()){
                                    throw new Exception("En el archivo de abono, la cantidad de registros informados: " + header.getNumero_declaraciones().intValue() + "; es distinto a los registros procesados: " + header.getLst_detalle_planilla().size());                            
                                }else{
                                    // Set Cantidad resumen del Abono                                    
                                    resumen.setNumero_de_planillas(header.getNumero_declaraciones());
                                }
                            }
                        }else{
                            throw new Exception("No existe planilla del abono terminado listresumenabonodto size: " + listheaderdto.size()); 
                        }
                        
                    }else{
                        throw new Exception("No existe planilla del abono terminado listresumenabonodto: " + listheaderdto); 
                    }
                    
                    
                    if(registros != row){
                        throw new Exception("No se procesaron todas las filas registros planilla: " + registros + "; procesados: " + row); 
                    }
                    
                    List<ResumenAbonoDTO> lstResumenAbonoDTO = new ArrayList<ResumenAbonoDTO>();
                    lstResumenAbonoDTO.add(resumen);                    
                    
                    resp.setOk(true);
                    resp.setLst_header_planilla(listheaderdto);
                    resp.setLst_resumen_abono(lstResumenAbonoDTO);
                    logger.debug("doProcesarLargoFijo() - Termino proceso de archivo de PLANILLA");
                }
            }
            
        } catch (Exception e) {
            logger.error("doProcesarLargoFijo() - Se ha producido un error: " + e.getMessage(), e);
            resp.setOk(false);
            resp.setError(e);            
        }
        
        logger.debug("doProcesarLargoFijo() - termino");
        return resp;
    }
    
    
    private static ResultadoProcesoVO doProcesarExcel(String fileDat, String tipoArchivo, Double idCarga) {
        logger.debug("doProcesarExcel() - Inicio");
        ResultadoProcesoVO resp = new ResultadoProcesoVO();
        try {
            if(tipoArchivo.equalsIgnoreCase("PLANILLAANDESMANUAL")){
                logger.debug("doProcesarExcel() - Incio proceso de archivo de PLANILLAANDESMANUAL");
                List<ArchivoPlanillaHeaderDTO> listheaderdto= new ArrayList<ArchivoPlanillaHeaderDTO>();
                logger.debug("doProcesarExcel() - Obteniendo estructura del archivo de configuracion");
                FileStructureVo fileStructure = (FileStructureVo) ReaderConfigCore.config.get(tipoArchivo);                
                if (fileStructure != null) {
                    logger.debug("doProcesarExcel() - Estructura distinta de null");    
                    int row = 0;
                    // Lectura del archivo Excel
                    logger.debug("doProcesarExcel() - Lectura archivo excel " + fileDat);  
                    InputStream xlsinput = new FileInputStream(fileDat);
                    logger.debug("doProcesarExcel() - Archivo excel leido " + xlsinput);    
                    
                    
                    HSSFWorkbook workbook = new HSSFWorkbook(xlsinput);
                    logger.debug("doProcesarExcel() - Libro Excel a leer " + workbook);    
                    HSSFSheet sheet = workbook.getSheetAt(0);
                    logger.debug("doProcesarExcel() - Hoja de datos " + sheet);    
                                        
                    int registros = sheet.getLastRowNum() ;
                    logger.debug("doProcesarExcel() - Cantidad de registros " + registros);    
                    int cantidadheader = 0;
                    int cantidadencabezado = 0;
                    
                    List<ArchivoPlanillaDetalleDTO> lstdetalle = null;
                    //Definiciones Abono
                    List<ArchivoAbonoDTO> lstdetalleAbono = new ArrayList<ArchivoAbonoDTO>();                    
                    Double totalInstitucion = 0.0; 
                    
                    Iterator<Row> rows = sheet.rowIterator();
                    
                    // Obtiene structura de la linea excel
                    
                    RowStructureVo rowStructure = fileStructure.getStructureRegistroExcelById("lineaExcel");
                    logger.debug("doProcesarExcel() - Se obtiene estructura del excel " + rowStructure);    
                    
                    while(rows.hasNext()){
                        logger.debug("doProcesarExcel() - Se inicia While de formacion de datos");    
                        
                        if( rowStructure != null ){                           
                            HSSFRow fila = (HSSFRow) rows.next();                            
                            if(fila.getRowNum() >= rowStructure.getPosicionInicial()){
                                fileStructure.setDataRowExcel(fila);
                                
                                String rut_trabajador_excel = (String)fileStructure.getValueRow("lineaExcel", "rut_trabajador");
                                String nombre_trabajador_excel = (String)fileStructure.getValueRow("lineaExcel", "nombre_trabajador");
                                String apellido_paterno_trabajador_excel = (String)fileStructure.getValueRow("lineaExcel", "apellido_paterno_trabajador");
                                String apellido_materno_trabajador_excel = (String)fileStructure.getValueRow("lineaExcel", "apellido_materno_trabajador");
                                String rut_pagador_excel = (String)fileStructure.getValueRow("lineaExcel", "rut_pagador");
                                String nombre_pagador_excel = (String)fileStructure.getValueRow("lineaExcel", "nombre_pagador");
                                Date periodo_pago_excel = (Date)fileStructure.getValueRow("lineaExcel", "periodo_pago");
                                Date fecha_operacion_excel = (Date)fileStructure.getValueRow("lineaExcel", "fecha_operacion");
                                Double cotizacion_voluntaria_apvi_excel = (Double)fileStructure.getValueRow("lineaExcel", "cotizacion_voluntaria_apvi");
                                Double deposito_convenio_excel = (Double)fileStructure.getValueRow("lineaExcel", "deposito_convenio");
                                Double total_pagar_ia_excel = (Double)fileStructure.getValueRow("lineaExcel", "total_pagar_ia");
                                
                                logger.debug("doProcesarExcel() - rut_trabajador_excel " + rut_trabajador_excel);   
                                logger.debug("doProcesarExcel() - nombre_trabajador_excel " + nombre_trabajador_excel);   
                                logger.debug("doProcesarExcel() - apellido_paterno_trabajador_excel " + apellido_paterno_trabajador_excel);   
                                logger.debug("doProcesarExcel() - apellido_materno_trabajador_excel " + apellido_materno_trabajador_excel);   
                                logger.debug("doProcesarExcel() - cotizacion_voluntaria_apvi_excel " + cotizacion_voluntaria_apvi_excel);   
                                logger.debug("doProcesarExcel() - total_pagar_ia_excel " + total_pagar_ia_excel);   
                                
                                
                                // Creacion encabezado 
                                logger.debug("doProcesarExcel() - Creacion encabezado");       
                                if(listheaderdto != null){
                                    if( listheaderdto.size() > 0){
                                        if(listheaderdto.get(cantidadheader-1) != null){
                                            if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){                                            
                                                logger.debug("doProcesarExcel() - Inicio Creacion encabezado");       
                                                lstdetalle = new ArrayList<ArchivoPlanillaDetalleDTO>();
                                                Integer tipo_de_registro = 1;
                                                Double id_folio_planilla = Double.valueOf(String.valueOf(idCarga.longValue()) + String.valueOf(new Double(fila.getRowNum()))) + 9000000000000000.0 ;
                                                Integer tipo_pago = 0;
                                                Date fecha_operacion = fecha_operacion_excel;                                    
                                                Integer codigo_institucion_apv = 0;
                                                Integer rut_pagador = Integer.parseInt(RutUtil.extractRut(rut_pagador_excel));
                                                String dv_pagador = RutUtil.extractDvRut(rut_pagador_excel);
                                                Integer tipo_pagador = 0;
                                                String correo_pagador = null;
                                                
                                                ArchivoPlanillaEncabezadoDTO encabezado = new ArchivoPlanillaEncabezadoDTO();
                                                encabezado.setTipo_de_registro(tipo_de_registro);
                                                encabezado.setId_folio_planilla(id_folio_planilla);
                                                encabezado.setTipo_pago(tipo_pago);
                                                encabezado.setFecha_hora_operacion(fecha_operacion);
                                                encabezado.setCodigo_institucion_apv(codigo_institucion_apv);
                                                encabezado.setRut_pagador(rut_pagador);
                                                encabezado.setDv_pagador(dv_pagador.trim());
                                                encabezado.setTipo_pagador(tipo_pagador);
                                                encabezado.setCorreo_pagador(correo_pagador);
                                                encabezado.setAntecedentes(new ArchivoPlanillaAntecedentesDTO());
                                                
                                                cantidadencabezado++;
                                            
                                                listheaderdto.get(cantidadheader-1).getLst_detalle_planilla().add(encabezado);                                           
                                                logger.debug("doProcesarExcel() - Termino Creacion encabezado");       
                                            }else{
                                                throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                            }
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                    }
                                }else{                                
                                    throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                                }
                                
                                // Creacion Antecedentes
                                 logger.debug("doProcesarExcel() - Creacion Antecedentes");       
                                 if(listheaderdto != null){
                                     if( listheaderdto.size() > 0){
                                         if(listheaderdto.get(cantidadheader-1) != null){
                                             if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                                 List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                                 if(listencabezadodto != null){
                                                     if( listencabezadodto.size() > 0){
                                                         if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                             if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                                 logger.debug("doProcesarExcel() - Inicio Creacion Antecedentes");       
                                                                 Integer tipo_de_registro = 2;                            
                                                                 String razon_social_pagador = nombre_pagador_excel;
                                                                 String codigo_actividad_pagador = null;
                                                                 String calle_pagador = null;
                                                                 String numero_pagador = null;
                                                                 String depart_pobla_pagador = null;
                                                                 String comuna_pagador = null;
                                                                 String ciudad_pagador = null;
                                                                 String region_pagador = null;
                                                                 String telefono_pagador = null;
                                                                 String nombre_representante_legal = null;
                                                                 Integer rut_representante_legal = 0;
                                                                 String dv_representante_legal = null;
                                                                                                     
                                                                 ArchivoPlanillaAntecedentesDTO antecedentes = new ArchivoPlanillaAntecedentesDTO();
                                                                 antecedentes.setTipo_de_registro(tipo_de_registro);
                                                                 antecedentes.setRazon_social_pagador(razon_social_pagador.trim());
                                                                 antecedentes.setCodigo_actividad_pagador(codigo_actividad_pagador);
                                                                 antecedentes.setCalle_pagador(calle_pagador);
                                                                 antecedentes.setNumero_pagador(numero_pagador);
                                                                 antecedentes.setDepart_pobla_pagador(depart_pobla_pagador);
                                                                 antecedentes.setComuna_pagador(comuna_pagador);
                                                                 antecedentes.setCiudad_pagador(ciudad_pagador);
                                                                 antecedentes.setRegion_pagador(region_pagador);
                                                                 antecedentes.setTelefono_pagador(telefono_pagador);
                                                                 antecedentes.setNombre_representante_legal(nombre_representante_legal);
                                                                 antecedentes.setRut_representante_legal(rut_representante_legal);
                                                                 antecedentes.setDv_representante_legal(dv_representante_legal);                                                                 
                                                             
                                                                 listencabezadodto.get(cantidadencabezado-1).setAntecedentes(antecedentes);
                                                                 listencabezadodto.get(cantidadencabezado-1).setDetalle(new ArrayList<ArchivoPlanillaDetalleDTO>());  
                                                                 logger.debug("doProcesarExcel() - Termino Creacion Antecedentes");       
                                                             }else{
                                                                 throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                             }
                                                         }else{
                                                             throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                         }
                                                     }else{
                                                         throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                     }
                                                 }else{                                
                                                     throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                                 }
                                             }else{
                                                 throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                             }
                                         }else{
                                             throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                         }
                                     }else{
                                         throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                     }
                                 }else{                                
                                     throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                                 } 
                                 
                                // Creacion del detalle
                                logger.debug("doProcesarExcel() - Creacion detalle");       
                                if(listheaderdto != null){
                                    if( listheaderdto.size() > 0){
                                        if(listheaderdto.get(cantidadheader-1) != null){
                                            if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                                List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                                if(listencabezadodto != null){
                                                    if( listencabezadodto.size() > 0){
                                                        if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                            if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                                if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){                                                                    
                                                                    logger.debug("doProcesarExcel() - Inicio Creacion detalle");       
                                                                    Integer tipo_de_registro = 3;                            
                                                                    Integer rut_trabajador = Integer.parseInt(RutUtil.extractRut(rut_trabajador_excel));
                                                                    String dv_trabajador = RutUtil.extractDvRut(rut_trabajador_excel);
                                                                    String apellido_paterno_trabajador = apellido_paterno_trabajador_excel;
                                                                    String apellido_materno_trabajador = apellido_materno_trabajador_excel;
                                                                    String nombres_trabajador = nombre_trabajador_excel;
                                                                    String sexo = null;
                                                                    Integer nacionalidad = 0;
                                                                    Double renta_imponible_trabajador = 0.0;
                                                                    Double cotizacion_voluntaria_apvi = cotizacion_voluntaria_apvi_excel;
                                                                    Double cotizacion_voluntaria_apvi_b = 0.0;
                                                                    String nro_contrato_apvi = null;
                                                                    Double deposito_convenio = deposito_convenio_excel;
                                                                    Double total_a_pagar = total_pagar_ia_excel;
                                                                    Date periodo_pago = periodo_pago_excel;
                                                                    Double apv_colectivo_empl = 0.0;
                                                                    Double apv_colectivo_trabaj = 0.0;
                                                                    String n_contrato_apvc = null;
                                                                    Integer codigo_mov_personal = 0;
                                                                    Date fecha_inicio = new Date();
                                                                    Date fecha_termino = new Date();;
                                                                                                        
                                                                    ArchivoPlanillaDetalleDTO detalle = new ArchivoPlanillaDetalleDTO();
                                                                    detalle.setTipo_de_registro(tipo_de_registro);
                                                                    detalle.setRut_trabajador(rut_trabajador);
                                                                    detalle.setDv_trabajador(dv_trabajador.trim());
                                                                    detalle.setApellido_paterno_trabajador(apellido_paterno_trabajador.trim());
                                                                    detalle.setApellido_materno_trabajador(apellido_materno_trabajador.trim());
                                                                    detalle.setNombres_trabajador(nombres_trabajador.trim());
                                                                    detalle.setSexo(sexo);
                                                                    detalle.setNacionalidad(nacionalidad);
                                                                    detalle.setRenta_imponible_trabajador(renta_imponible_trabajador);
                                                                    detalle.setCotizacion_voluntaria_apvi(cotizacion_voluntaria_apvi);
                                                                    detalle.setCotizacion_voluntaria_apvi_b(cotizacion_voluntaria_apvi_b);
                                                                    detalle.setNro_contrato_apvi(nro_contrato_apvi);
                                                                    detalle.setDeposito_convenio(deposito_convenio);
                                                                    detalle.setTotal_a_pagar(total_a_pagar);
                                                                    detalle.setPeriodo_pago(periodo_pago);
                                                                    detalle.setApv_colectivo_empl(apv_colectivo_empl);
                                                                    detalle.setApv_colectivo_trabaj(apv_colectivo_trabaj);
                                                                    detalle.setN_contrato_apvc(n_contrato_apvc);
                                                                    detalle.setCodigo_mov_personal(codigo_mov_personal);
                                                                    detalle.setFecha_inicio(fecha_inicio);
                                                                    detalle.setFecha_termino(fecha_termino);
                                                                    
                                                                    lstdetalle.add(detalle);
                                                                
                                                                    listencabezadodto.get(cantidadencabezado-1).setDetalle(lstdetalle); 
                                                                    listencabezadodto.get(cantidadencabezado-1).setResumen(new ArchivoPlanillaResumenDTO());
                                                                    logger.debug("doProcesarExcel() - Termino Creacion detalle");       
                                                                }else{
                                                                    throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                                }
                                                            }else{
                                                                throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                            }
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                    }
                                                }else{                                
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                                }    
                                            }else{
                                                throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                            }
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                    }
                                }else{                                
                                    throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                                } 
                                
                                
                                // Creacion Resumen
                                 logger.debug("doProcesarExcel() - Creacion Resumen");       
                                 if(listheaderdto != null){
                                     if( listheaderdto.size() > 0){
                                         if(listheaderdto.get(cantidadheader-1) != null){
                                             if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                                 List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                                 if(listencabezadodto != null){
                                                     if( listencabezadodto.size() > 0){
                                                         if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                             if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                                 if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){
                                                                     if(listencabezadodto.get(cantidadencabezado-1).getResumen() != null){
                                                                         logger.debug("doProcesarExcel() - Inicio Creacion Resumen");       
                                                                         Integer tipo_de_registro = 4;                            
                                                                         Double total_cotiza_voluntaria_apvi = cotizacion_voluntaria_apvi_excel;
                                                                         Double total_cotiza_voluntaria_apvi_b = 0.0;
                                                                         Double total_apv_colect_empleador = 0.0;
                                                                         Double total_apv_colect_trabajador = 0.0;
                                                                         Double total_depositos_convenidos = deposito_convenio_excel;
                                                                         Double total_pagar_ia = total_pagar_ia_excel;
                                                                         Date periodo_pago = periodo_pago_excel;
                                                                         Integer numero_afiliados_informados = 1;
                                                                         
                                                                         ArchivoPlanillaResumenDTO resumen = new ArchivoPlanillaResumenDTO();
                                                                         resumen.setTipo_de_registro(tipo_de_registro);
                                                                         resumen.setTotal_cotiza_voluntaria_apvi(total_cotiza_voluntaria_apvi);
                                                                         resumen.setTotal_cotiza_voluntaria_apvi_b(total_cotiza_voluntaria_apvi_b);
                                                                         resumen.setTotal_apv_colect_empleador(total_apv_colect_empleador);
                                                                         resumen.setTotal_apv_colect_trabajador(total_apv_colect_trabajador);
                                                                         resumen.setTotal_depositos_convenidos(total_depositos_convenidos);
                                                                         resumen.setTotal_pagar_ia(total_pagar_ia);
                                                                         resumen.setPeriodo_pago(periodo_pago);
                                                                         resumen.setNumero_afiliados_informados(numero_afiliados_informados);
                                                                                                                                             
                                                                         listencabezadodto.get(cantidadencabezado-1).setResumen(resumen); 
                                                                         logger.debug("doProcesarExcel() - Termino Creacion Resumen");     
                                                                         
                                                                         // Creacion del Abono
                                                                          // Definicion del abono
                                                                          ArchivoAbonoDTO detalleAbono = new ArchivoAbonoDTO(); 
                                                                         logger.debug("doProcesarExcel() - Creacion del Abono");       
                                                                          //Incia carga datos Abono                            
                                                                          detalleAbono.setTipo_de_registro(2);                           
                                                                          detalleAbono.setId_folio_abono(listencabezadodto.get(cantidadencabezado-1).getId_folio_planilla());                            
                                                                          detalleAbono.setMonto_abono_fondo(0.0);
                                                                          detalleAbono.setMonto_abono_afc(0.0);
                                                                          detalleAbono.setRut_empleador(listencabezadodto.get(cantidadencabezado-1).getRut_pagador());
                                                                          detalleAbono.setDigito_verficador(listencabezadodto.get(cantidadencabezado-1).getDv_pagador());
                                                                          detalleAbono.setFecha_pago(listencabezadodto.get(cantidadencabezado-1).getFecha_hora_operacion());
                                                                          detalleAbono.setLote(null);
                                                                          detalleAbono.setSucursal(null);
                                                                          detalleAbono.setMonto_abono_institucion(resumen.getTotal_pagar_ia());                              
                                                                          detalleAbono.setPeriodo(resumen.getPeriodo_pago());
                                                                          totalInstitucion = totalInstitucion + detalleAbono.getMonto_abono_institucion();
                                                                          
                                                                          // Generar lista de Abono
                                                                          if(detalleAbono != null){
                                                                              lstdetalleAbono.add(detalleAbono);
                                                                          }
                                                                          
                                                                         logger.debug("doProcesarExcel() - Termino Creacion del Abono");  
                                                                         row++;    
                                                                         
                                                                         logger.debug("doProcesarExcel() - Fila leida de excel " + row);  
                                                                         
                                                                     }else{
                                                                         throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getResumen(): " + listencabezadodto.get(cantidadencabezado-1).getResumen()); 
                                                                     }                                                
                                                                 }else{
                                                                     throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                                 }
                                                             }else{
                                                                 throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                             }
                                                         }else{
                                                             throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                         }
                                                     }else{
                                                         throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                     }
                                                 }else{                                
                                                     throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                                 }  
                                             }else{
                                                 throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                             }
                                         }else{
                                             throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                         }
                                     }else{
                                         throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                     }
                                 }else{                                
                                     throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                                 }
                            }else{
                                // Creacion del Header
                                 logger.debug("doProcesarExcel() - Creacion del Header");  
                                 Integer tipo_de_registro = 0;  
                                 Integer numero_declaraciones = registros;
                                 ArchivoPlanillaHeaderDTO header = new ArchivoPlanillaHeaderDTO();
                                 header.setNumero_declaraciones(numero_declaraciones);
                                 header.setTipo_de_registro(tipo_de_registro);
                                 header.setLst_detalle_planilla(new ArrayList<ArchivoPlanillaEncabezadoDTO>());                                 
                                 
                                 cantidadheader++;
                                 listheaderdto.add(header);
                                logger.debug("doProcesarExcel() - Termino Creacion del Header");  
                            }
                        }
                    } // fin while     
                                        
                    logger.debug("doProcesarExcel() - Creacion del Resumen del Abono");  
                    ResumenAbonoDTO resumen = new ResumenAbonoDTO();
                    resumen.setTipo_de_registro(1);
                    resumen.setIdentificador_iip(0);
                    resumen.setTotal_monto_fondo(0.0);
                    resumen.setTotal_monto_institucion(totalInstitucion);
                    resumen.setTotal_monto_afc(0.0);
                    resumen.setCuenta_fondo(null);
                    resumen.setCuenta_institucion(Constant.CUENTA_BANCOCHILE_ANDES);
                    resumen.setCuenta_afc(null);
                    resumen.setIdentificacion_banco(Constant.GLOSA_BANCOCHILE_ANDES);
                    resumen.setCodigo_banco(Constant.COD_BANCOCHILE_ANDES);
                    resumen.setFecha_abono(new Date());
                    resumen.setCodigo_recaudador(Constant.COD_RECAUDADOR_ANDES);
                    resumen.setGlosa_recaudador(Constant.GLOSA_RECAUDADOR_ANDES);
                    resumen.setArchivoabono(lstdetalleAbono);
                    logger.debug("doProcesarExcel() - Termino Creacion del Resumen del Abono");
                    
                    if(listheaderdto != null){
                        if(listheaderdto.size() > 0){
                            Iterator itrlistheaderdto =  listheaderdto.iterator();
                            while(itrlistheaderdto.hasNext()){
                                ArchivoPlanillaHeaderDTO header = (ArchivoPlanillaHeaderDTO) itrlistheaderdto.next();
                                if(header.getNumero_declaraciones().intValue() != header.getLst_detalle_planilla().size()){
                                    logger.debug("doProcesarExcel() - En el archivo de abono, la cantidad de registros informados: " + header.getNumero_declaraciones().intValue() + "; es distinto a los registros procesados: " + header.getLst_detalle_planilla().size());
                                    throw new Exception("En el archivo de abono, la cantidad de registros informados: " + header.getNumero_declaraciones().intValue() + "; es distinto a los registros procesados: " + header.getLst_detalle_planilla().size());                            
                                }else{
                                    // Set Cantidad resumen del Abono                                    
                                    resumen.setNumero_de_planillas(header.getNumero_declaraciones());
                                }
                            }
                        }else{
                            throw new Exception("No existe planilla del abono terminado listresumenabonodto size: " + listheaderdto.size()); 
                        }                            
                    }else{
                        throw new Exception("No existe planilla del abono terminado listresumenabonodto: " + listheaderdto); 
                    }
                    
                    if(registros != row){
                        throw new Exception("No se procesaron todas las filas registros planilla: " + registros + "; procesados: " + row); 
                    }
                    
                    List<ResumenAbonoDTO> lstResumenAbonoDTO = new ArrayList<ResumenAbonoDTO>();
                    lstResumenAbonoDTO.add(resumen);    
                    
                    resp.setOk(true);
                    resp.setLst_header_planilla(listheaderdto);
                    resp.setLst_resumen_abono(lstResumenAbonoDTO);                    
                    logger.debug("doProcesarExcel() - Termino proceso de archivo de PLANILLAANDESMANUAL");
                    
                }
            }
            
            if(tipoArchivo.equalsIgnoreCase("PLANILLALAARAUCANAMANUAL")){
                logger.debug("doProcesarExcel() - Incio proceso de archivo de PLANILLAANDESMANUAL");
                List<ArchivoPlanillaHeaderDTO> listheaderdto= new ArrayList<ArchivoPlanillaHeaderDTO>();
                logger.debug("doProcesarExcel() - Obteniendo estructura del archivo de configuracion");
                FileStructureVo fileStructure = (FileStructureVo) ReaderConfigCore.config.get(tipoArchivo);                
                if (fileStructure != null) {
                    logger.debug("doProcesarExcel() - Estructura distinta de null");    
                    int row = 0;
                    // Lectura del archivo Excel
                    logger.debug("doProcesarExcel() - Lectura archivo excel " + fileDat);    
                    InputStream xlsinput = new FileInputStream(fileDat);
                    logger.debug("doProcesarExcel() - Archivo excel leido " + xlsinput);    
                    
                    
                    HSSFWorkbook workbook = new HSSFWorkbook(xlsinput);
                    logger.debug("doProcesarExcel() - Libro Excel a leer " + workbook);    
                    HSSFSheet sheet = workbook.getSheetAt(0);
                    logger.debug("doProcesarExcel() - Hoja de datos " + sheet);    
                                        
                    int registros = sheet.getLastRowNum() ;
                    logger.debug("doProcesarExcel() - Cantidad de registros " + registros);    
                    int cantidadheader = 0;
                    int cantidadencabezado = 0;
                    
                    List<ArchivoPlanillaDetalleDTO> lstdetalle = null;
                    //Definiciones Abono
                    List<ArchivoAbonoDTO> lstdetalleAbono = new ArrayList<ArchivoAbonoDTO>();                    
                    Double totalInstitucion = 0.0; 
                    
                    Iterator<Row> rows = sheet.rowIterator();
                    
                    // Obtiene structura de la linea excel
                    
                    RowStructureVo rowStructure = fileStructure.getStructureRegistroExcelById("lineaExcel");
                    logger.debug("doProcesarExcel() - Se obtiene estructura del excel " + rowStructure);    
                    
                    while(rows.hasNext()){
                        logger.debug("doProcesarExcel() - Se inicia While de formacion de datos");    
                        
                        if( rowStructure != null ){                           
                            HSSFRow fila = (HSSFRow) rows.next();                            
                            if(fila.getRowNum() >= rowStructure.getPosicionInicial()){
                                fileStructure.setDataRowExcel(fila);
                                
                                String rut_trabajador_excel = (String)fileStructure.getValueRow("lineaExcel", "rut_trabajador");
                                String nombre_trabajador_excel = (String)fileStructure.getValueRow("lineaExcel", "nombre_trabajador");
                                String apellido_paterno_trabajador_excel = (String)fileStructure.getValueRow("lineaExcel", "apellido_paterno_trabajador");
                                String apellido_materno_trabajador_excel = (String)fileStructure.getValueRow("lineaExcel", "apellido_materno_trabajador");
                                String rut_pagador_excel = (String)fileStructure.getValueRow("lineaExcel", "rut_pagador");
                                String nombre_pagador_excel = (String)fileStructure.getValueRow("lineaExcel", "nombre_pagador");
                                Date periodo_pago_excel = (Date)fileStructure.getValueRow("lineaExcel", "periodo_pago");
                                Date fecha_operacion_excel = (Date)fileStructure.getValueRow("lineaExcel", "fecha_operacion");
                                Double cotizacion_voluntaria_apvi_excel = (Double)fileStructure.getValueRow("lineaExcel", "cotizacion_voluntaria_apvi");
                                Double deposito_convenio_excel = (Double)fileStructure.getValueRow("lineaExcel", "deposito_convenio");
                                Double total_pagar_ia_excel = (Double)fileStructure.getValueRow("lineaExcel", "total_pagar_ia");
                                
                                logger.debug("doProcesarExcel() - rut_trabajador_excel " + rut_trabajador_excel);   
                                logger.debug("doProcesarExcel() - nombre_trabajador_excel " + nombre_trabajador_excel);   
                                logger.debug("doProcesarExcel() - apellido_paterno_trabajador_excel " + apellido_paterno_trabajador_excel);   
                                logger.debug("doProcesarExcel() - apellido_materno_trabajador_excel " + apellido_materno_trabajador_excel);   
                                logger.debug("doProcesarExcel() - cotizacion_voluntaria_apvi_excel " + cotizacion_voluntaria_apvi_excel);   
                                logger.debug("doProcesarExcel() - total_pagar_ia_excel " + total_pagar_ia_excel);   
                                
                                
                                // Creacion encabezado 
                                logger.debug("doProcesarExcel() - Creacion encabezado");       
                                if(listheaderdto != null){
                                    if( listheaderdto.size() > 0){
                                        if(listheaderdto.get(cantidadheader-1) != null){
                                            if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){                                            
                                                logger.debug("doProcesarExcel() - Inicio Creacion encabezado");       
                                                lstdetalle = new ArrayList<ArchivoPlanillaDetalleDTO>();
                                                Integer tipo_de_registro = 1;
                                                Double id_folio_planilla = Double.valueOf(String.valueOf(idCarga.longValue()) + String.valueOf(new Double(fila.getRowNum()))) + 9000000000000000.0 ;
                                                Integer tipo_pago = 0;
                                                Date fecha_operacion = fecha_operacion_excel;                                    
                                                Integer codigo_institucion_apv = 0;
                                                Integer rut_pagador = Integer.parseInt(RutUtil.extractRut(rut_pagador_excel));
                                                String dv_pagador = RutUtil.extractDvRut(rut_pagador_excel);
                                                Integer tipo_pagador = 0;
                                                String correo_pagador = null;
                                                
                                                ArchivoPlanillaEncabezadoDTO encabezado = new ArchivoPlanillaEncabezadoDTO();
                                                encabezado.setTipo_de_registro(tipo_de_registro);
                                                encabezado.setId_folio_planilla(id_folio_planilla);
                                                encabezado.setTipo_pago(tipo_pago);
                                                encabezado.setFecha_hora_operacion(fecha_operacion);
                                                encabezado.setCodigo_institucion_apv(codigo_institucion_apv);
                                                encabezado.setRut_pagador(rut_pagador);
                                                encabezado.setDv_pagador(dv_pagador.trim());
                                                encabezado.setTipo_pagador(tipo_pagador);
                                                encabezado.setCorreo_pagador(correo_pagador);
                                                encabezado.setAntecedentes(new ArchivoPlanillaAntecedentesDTO());
                                                
                                                cantidadencabezado++;
                                            
                                                listheaderdto.get(cantidadheader-1).getLst_detalle_planilla().add(encabezado);                                           
                                                logger.debug("doProcesarExcel() - Termino Creacion encabezado");       
                                            }else{
                                                throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                            }
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                    }
                                }else{                                
                                    throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                                }
                                
                                // Creacion Antecedentes
                                 logger.debug("doProcesarExcel() - Creacion Antecedentes");       
                                 if(listheaderdto != null){
                                     if( listheaderdto.size() > 0){
                                         if(listheaderdto.get(cantidadheader-1) != null){
                                             if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                                 List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                                 if(listencabezadodto != null){
                                                     if( listencabezadodto.size() > 0){
                                                         if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                             if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                                 logger.debug("doProcesarExcel() - Inicio Creacion Antecedentes");       
                                                                 Integer tipo_de_registro = 2;                            
                                                                 String razon_social_pagador = nombre_pagador_excel;
                                                                 String codigo_actividad_pagador = null;
                                                                 String calle_pagador = null;
                                                                 String numero_pagador = null;
                                                                 String depart_pobla_pagador = null;
                                                                 String comuna_pagador = null;
                                                                 String ciudad_pagador = null;
                                                                 String region_pagador = null;
                                                                 String telefono_pagador = null;
                                                                 String nombre_representante_legal = null;
                                                                 Integer rut_representante_legal = 0;
                                                                 String dv_representante_legal = null;
                                                                                                     
                                                                 ArchivoPlanillaAntecedentesDTO antecedentes = new ArchivoPlanillaAntecedentesDTO();
                                                                 antecedentes.setTipo_de_registro(tipo_de_registro);
                                                                 antecedentes.setRazon_social_pagador(razon_social_pagador.trim());
                                                                 antecedentes.setCodigo_actividad_pagador(codigo_actividad_pagador);
                                                                 antecedentes.setCalle_pagador(calle_pagador);
                                                                 antecedentes.setNumero_pagador(numero_pagador);
                                                                 antecedentes.setDepart_pobla_pagador(depart_pobla_pagador);
                                                                 antecedentes.setComuna_pagador(comuna_pagador);
                                                                 antecedentes.setCiudad_pagador(ciudad_pagador);
                                                                 antecedentes.setRegion_pagador(region_pagador);
                                                                 antecedentes.setTelefono_pagador(telefono_pagador);
                                                                 antecedentes.setNombre_representante_legal(nombre_representante_legal);
                                                                 antecedentes.setRut_representante_legal(rut_representante_legal);
                                                                 antecedentes.setDv_representante_legal(dv_representante_legal);                                                                 
                                                             
                                                                 listencabezadodto.get(cantidadencabezado-1).setAntecedentes(antecedentes);
                                                                 listencabezadodto.get(cantidadencabezado-1).setDetalle(new ArrayList<ArchivoPlanillaDetalleDTO>());  
                                                                 logger.debug("doProcesarExcel() - Termino Creacion Antecedentes");       
                                                             }else{
                                                                 throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                             }
                                                         }else{
                                                             throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                         }
                                                     }else{
                                                         throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                     }
                                                 }else{                                
                                                     throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                                 }
                                             }else{
                                                 throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                             }
                                         }else{
                                             throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                         }
                                     }else{
                                         throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                     }
                                 }else{                                
                                     throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                                 } 
                                 
                                // Creacion del detalle
                                logger.debug("doProcesarExcel() - Creacion detalle");       
                                if(listheaderdto != null){
                                    if( listheaderdto.size() > 0){
                                        if(listheaderdto.get(cantidadheader-1) != null){
                                            if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                                List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                                if(listencabezadodto != null){
                                                    if( listencabezadodto.size() > 0){
                                                        if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                            if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                                if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){                                                                    
                                                                    logger.debug("doProcesarExcel() - Inicio Creacion detalle");       
                                                                    Integer tipo_de_registro = 3;                            
                                                                    Integer rut_trabajador = Integer.parseInt(RutUtil.extractRut(rut_trabajador_excel));
                                                                    String dv_trabajador = RutUtil.extractDvRut(rut_trabajador_excel);
                                                                    String apellido_paterno_trabajador = apellido_paterno_trabajador_excel;
                                                                    String apellido_materno_trabajador = apellido_materno_trabajador_excel;
                                                                    String nombres_trabajador = nombre_trabajador_excel;
                                                                    String sexo = null;
                                                                    Integer nacionalidad = 0;
                                                                    Double renta_imponible_trabajador = 0.0;
                                                                    Double cotizacion_voluntaria_apvi = cotizacion_voluntaria_apvi_excel;
                                                                    Double cotizacion_voluntaria_apvi_b = 0.0;
                                                                    String nro_contrato_apvi = null;
                                                                    Double deposito_convenio = deposito_convenio_excel;
                                                                    Double total_a_pagar = total_pagar_ia_excel;
                                                                    Date periodo_pago = periodo_pago_excel;
                                                                    Double apv_colectivo_empl = 0.0;
                                                                    Double apv_colectivo_trabaj = 0.0;
                                                                    String n_contrato_apvc = null;
                                                                    Integer codigo_mov_personal = 0;
                                                                    Date fecha_inicio = new Date();
                                                                    Date fecha_termino = new Date();;
                                                                                                        
                                                                    ArchivoPlanillaDetalleDTO detalle = new ArchivoPlanillaDetalleDTO();
                                                                    detalle.setTipo_de_registro(tipo_de_registro);
                                                                    detalle.setRut_trabajador(rut_trabajador);
                                                                    detalle.setDv_trabajador(dv_trabajador.trim());
                                                                    detalle.setApellido_paterno_trabajador(apellido_paterno_trabajador.trim());
                                                                    detalle.setApellido_materno_trabajador(apellido_materno_trabajador.trim());
                                                                    detalle.setNombres_trabajador(nombres_trabajador.trim());
                                                                    detalle.setSexo(sexo);
                                                                    detalle.setNacionalidad(nacionalidad);
                                                                    detalle.setRenta_imponible_trabajador(renta_imponible_trabajador);
                                                                    detalle.setCotizacion_voluntaria_apvi(cotizacion_voluntaria_apvi);
                                                                    detalle.setCotizacion_voluntaria_apvi_b(cotizacion_voluntaria_apvi_b);
                                                                    detalle.setNro_contrato_apvi(nro_contrato_apvi);
                                                                    detalle.setDeposito_convenio(deposito_convenio);
                                                                    detalle.setTotal_a_pagar(total_a_pagar);
                                                                    detalle.setPeriodo_pago(periodo_pago);
                                                                    detalle.setApv_colectivo_empl(apv_colectivo_empl);
                                                                    detalle.setApv_colectivo_trabaj(apv_colectivo_trabaj);
                                                                    detalle.setN_contrato_apvc(n_contrato_apvc);
                                                                    detalle.setCodigo_mov_personal(codigo_mov_personal);
                                                                    detalle.setFecha_inicio(fecha_inicio);
                                                                    detalle.setFecha_termino(fecha_termino);
                                                                    
                                                                    lstdetalle.add(detalle);
                                                                
                                                                    listencabezadodto.get(cantidadencabezado-1).setDetalle(lstdetalle); 
                                                                    listencabezadodto.get(cantidadencabezado-1).setResumen(new ArchivoPlanillaResumenDTO());
                                                                    logger.debug("doProcesarExcel() - Termino Creacion detalle");       
                                                                }else{
                                                                    throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                                }
                                                            }else{
                                                                throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                            }
                                                        }else{
                                                            throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                        }
                                                    }else{
                                                        throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                    }
                                                }else{                                
                                                    throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                                }    
                                            }else{
                                                throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                            }
                                        }else{
                                            throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                        }
                                    }else{
                                        throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                    }
                                }else{                                
                                    throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                                } 
                                
                                
                                // Creacion Resumen
                                 logger.debug("doProcesarExcel() - Creacion Resumen");       
                                 if(listheaderdto != null){
                                     if( listheaderdto.size() > 0){
                                         if(listheaderdto.get(cantidadheader-1) != null){
                                             if(listheaderdto.get(cantidadheader-1).getLst_detalle_planilla() != null){
                                                 List<ArchivoPlanillaEncabezadoDTO> listencabezadodto = listheaderdto.get(cantidadheader-1).getLst_detalle_planilla();
                                                 if(listencabezadodto != null){
                                                     if( listencabezadodto.size() > 0){
                                                         if(listencabezadodto.get(cantidadencabezado-1) != null){
                                                             if(listencabezadodto.get(cantidadencabezado-1).getAntecedentes() != null){
                                                                 if(listencabezadodto.get(cantidadencabezado-1).getDetalle() != null){
                                                                     if(listencabezadodto.get(cantidadencabezado-1).getResumen() != null){
                                                                         logger.debug("doProcesarExcel() - Inicio Creacion Resumen");       
                                                                         Integer tipo_de_registro = 4;                            
                                                                         Double total_cotiza_voluntaria_apvi = cotizacion_voluntaria_apvi_excel;
                                                                         Double total_cotiza_voluntaria_apvi_b = 0.0;
                                                                         Double total_apv_colect_empleador = 0.0;
                                                                         Double total_apv_colect_trabajador = 0.0;
                                                                         Double total_depositos_convenidos = deposito_convenio_excel;
                                                                         Double total_pagar_ia = total_pagar_ia_excel;
                                                                         Date periodo_pago = periodo_pago_excel;
                                                                         Integer numero_afiliados_informados = 1;
                                                                         
                                                                         ArchivoPlanillaResumenDTO resumen = new ArchivoPlanillaResumenDTO();
                                                                         resumen.setTipo_de_registro(tipo_de_registro);
                                                                         resumen.setTotal_cotiza_voluntaria_apvi(total_cotiza_voluntaria_apvi);
                                                                         resumen.setTotal_cotiza_voluntaria_apvi_b(total_cotiza_voluntaria_apvi_b);
                                                                         resumen.setTotal_apv_colect_empleador(total_apv_colect_empleador);
                                                                         resumen.setTotal_apv_colect_trabajador(total_apv_colect_trabajador);
                                                                         resumen.setTotal_depositos_convenidos(total_depositos_convenidos);
                                                                         resumen.setTotal_pagar_ia(total_pagar_ia);
                                                                         resumen.setPeriodo_pago(periodo_pago);
                                                                         resumen.setNumero_afiliados_informados(numero_afiliados_informados);
                                                                                                                                             
                                                                         listencabezadodto.get(cantidadencabezado-1).setResumen(resumen); 
                                                                         logger.debug("doProcesarExcel() - Termino Creacion Resumen");     
                                                                         
                                                                         // Creacion del Abono
                                                                          // Definicion del abono
                                                                          ArchivoAbonoDTO detalleAbono = new ArchivoAbonoDTO(); 
                                                                         logger.debug("doProcesarExcel() - Creacion del Abono");       
                                                                          //Incia carga datos Abono                            
                                                                          detalleAbono.setTipo_de_registro(2);                           
                                                                          detalleAbono.setId_folio_abono(listencabezadodto.get(cantidadencabezado-1).getId_folio_planilla());                            
                                                                          detalleAbono.setMonto_abono_fondo(0.0);
                                                                          detalleAbono.setMonto_abono_afc(0.0);
                                                                          detalleAbono.setRut_empleador(listencabezadodto.get(cantidadencabezado-1).getRut_pagador());
                                                                          detalleAbono.setDigito_verficador(listencabezadodto.get(cantidadencabezado-1).getDv_pagador());
                                                                          detalleAbono.setFecha_pago(listencabezadodto.get(cantidadencabezado-1).getFecha_hora_operacion());
                                                                          detalleAbono.setLote(null);
                                                                          detalleAbono.setSucursal(null);
                                                                          detalleAbono.setMonto_abono_institucion(resumen.getTotal_pagar_ia());                              
                                                                          detalleAbono.setPeriodo(resumen.getPeriodo_pago());
                                                                          totalInstitucion = totalInstitucion + detalleAbono.getMonto_abono_institucion();
                                                                          
                                                                          // Generar lista de Abono
                                                                          if(detalleAbono != null){
                                                                              lstdetalleAbono.add(detalleAbono);
                                                                          }
                                                                          
                                                                         logger.debug("doProcesarExcel() - Termino Creacion del Abono");  
                                                                         row++;    
                                                                         
                                                                         logger.debug("doProcesarExcel() - Fila leida de excel " + row);  
                                                                         
                                                                     }else{
                                                                         throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getResumen(): " + listencabezadodto.get(cantidadencabezado-1).getResumen()); 
                                                                     }                                                
                                                                 }else{
                                                                     throw new Exception("No existe antecedentes del encabezado de la planilla en listencabezadodto.get(" + (cantidadencabezado-1) + ").getDetalle(): " + listencabezadodto.get(cantidadencabezado-1).getDetalle()); 
                                                                 }
                                                             }else{
                                                                 throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + ").getAntecedentes(): " + listencabezadodto.get(cantidadencabezado-1).getAntecedentes()); 
                                                             }
                                                         }else{
                                                             throw new Exception("No existe encabezado de la planilla en la lista listencabezadodto.get(" + (cantidadencabezado-1) + "): " + listencabezadodto.get(cantidadencabezado-1)); 
                                                         }
                                                     }else{
                                                         throw new Exception("No existe encabezado de la planilla listencabezadodto size: " + listencabezadodto.size()); 
                                                     }
                                                 }else{                                
                                                     throw new Exception("No existe encabezado de la planilla listencabezadodto: " + listencabezadodto); 
                                                 }  
                                             }else{
                                                 throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + ").getLst_detalle_planilla(): " + listheaderdto.get(cantidadheader-1).getLst_detalle_planilla()); 
                                             }
                                         }else{
                                             throw new Exception("No existe header de la planilla del abono en la lista listheaderdto.get(" + (cantidadheader-1) + "): " + listheaderdto.get(cantidadheader-1)); 
                                         }
                                     }else{
                                         throw new Exception("No existe header de la planilla del abono listheaderdto size: " + listheaderdto.size()); 
                                     }
                                 }else{                                
                                     throw new Exception("No existe header de la planilla del abono listheaderdto: " + listheaderdto); 
                                 }
                            }else{
                                // Creacion del Header
                                 logger.debug("doProcesarExcel() - Creacion del Header");  
                                 Integer tipo_de_registro = 0;  
                                 Integer numero_declaraciones = registros;
                                 ArchivoPlanillaHeaderDTO header = new ArchivoPlanillaHeaderDTO();
                                 header.setNumero_declaraciones(numero_declaraciones);
                                 header.setTipo_de_registro(tipo_de_registro);
                                 header.setLst_detalle_planilla(new ArrayList<ArchivoPlanillaEncabezadoDTO>());                                 
                                 
                                 cantidadheader++;
                                 listheaderdto.add(header);
                                logger.debug("doProcesarExcel() - Termino Creacion del Header");  
                            }
                        }
                    } // fin while     
                                        
                    logger.debug("doProcesarExcel() - Creacion del Resumen del Abono");  
                    ResumenAbonoDTO resumen = new ResumenAbonoDTO();
                    resumen.setTipo_de_registro(1);
                    resumen.setIdentificador_iip(0);
                    resumen.setTotal_monto_fondo(0.0);
                    resumen.setTotal_monto_institucion(totalInstitucion);
                    resumen.setTotal_monto_afc(0.0);
                    resumen.setCuenta_fondo(null);
                    resumen.setCuenta_institucion(Constant.CUENTA_BANCOSANTANDER_LAARAUCANA);
                    resumen.setCuenta_afc(null);
                    resumen.setIdentificacion_banco(Constant.GLOSA_BANCOSANTANDER_LAARAUCANA);
                    resumen.setCodigo_banco(Constant.COD_BANCOSANTANDER_LAARAUCANA);
                    resumen.setFecha_abono(new Date());
                    resumen.setCodigo_recaudador(Constant.COD_RECAUDADOR_LAARAUCANA);
                    resumen.setGlosa_recaudador(Constant.GLOSA_RECAUDADOR_LAARAUCANA);
                    resumen.setArchivoabono(lstdetalleAbono);
                    logger.debug("doProcesarExcel() - Termino Creacion del Resumen del Abono");
                    
                    if(listheaderdto != null){
                        if(listheaderdto.size() > 0){
                            Iterator itrlistheaderdto =  listheaderdto.iterator();
                            while(itrlistheaderdto.hasNext()){
                                ArchivoPlanillaHeaderDTO header = (ArchivoPlanillaHeaderDTO) itrlistheaderdto.next();
                                if(header.getNumero_declaraciones().intValue() != header.getLst_detalle_planilla().size()){
                                    logger.debug("doProcesarExcel() - En el archivo de abono, la cantidad de registros informados: " + header.getNumero_declaraciones().intValue() + "; es distinto a los registros procesados: " + header.getLst_detalle_planilla().size());
                                    throw new Exception("En el archivo de abono, la cantidad de registros informados: " + header.getNumero_declaraciones().intValue() + "; es distinto a los registros procesados: " + header.getLst_detalle_planilla().size());                            
                                }else{
                                    // Set Cantidad resumen del Abono                                    
                                    resumen.setNumero_de_planillas(header.getNumero_declaraciones());
                                }
                            }
                        }else{
                            throw new Exception("No existe planilla del abono terminado listresumenabonodto size: " + listheaderdto.size()); 
                        }                            
                    }else{
                        throw new Exception("No existe planilla del abono terminado listresumenabonodto: " + listheaderdto); 
                    }
                    
                    if(registros != row){
                        throw new Exception("No se procesaron todas las filas registros planilla: " + registros + "; procesados: " + row); 
                    }
                    
                    List<ResumenAbonoDTO> lstResumenAbonoDTO = new ArrayList<ResumenAbonoDTO>();
                    lstResumenAbonoDTO.add(resumen);    
                    
                    resp.setOk(true);
                    resp.setLst_header_planilla(listheaderdto);
                    resp.setLst_resumen_abono(lstResumenAbonoDTO);                    
                    logger.debug("doProcesarExcel() - Termino proceso de archivo de PLANILLAANDESMANUAL");
                    
                }
            }
        } catch (Exception e) {
            logger.error("doProcesarExcel() - Se ha producido un error: " + e.getMessage(), e);            
            resp.setOk(false);
            resp.setError(e);            
        }
        
        logger.debug("doProcesarExcel() - termino");
        return resp;
    }
    
    /**
     * Cuenta el numero de lineas
     * del un archivo 
     * @param filePath
     * @return
     * @throws IOException
     */
    private static int getLineCountFile(String filePath) throws IOException {
        logger.debug("getLineCountFile() - Inicio");
        Scanner filescan = new Scanner(new File(filePath));
        int lineCount = 0;
        while(filescan.hasNext()){
            filescan.nextLine();    
            lineCount++;
        }    
        filescan.close();
        logger.debug("getLineCountFile() - Termino");
        return lineCount;
    }
    
    /**
     * 
     * @param listresumenabonodto
     * @return
     */
    public static List<ResumenAbonoDTO> checkDuplicate(List<ResumenAbonoDTO> listresumenabonodto) {
        HashSet set = new HashSet();
        int contador= 1;
        for (int i = 0; i < listresumenabonodto.size(); i++) {
            boolean val = set.add(listresumenabonodto.get(i).getCodigo_recaudador());
            if (val == false) {
                listresumenabonodto.get(i).setCodigo_recaudador(listresumenabonodto.get(i).getCodigo_recaudador() + contador);
                if(listresumenabonodto.get(i).getArchivoabono() != null && listresumenabonodto.get(i).getArchivoabono().size() > 0){
                    for (int j = 0; j < listresumenabonodto.get(i).getArchivoabono().size(); j++) {
                        listresumenabonodto.get(i).getArchivoabono().get(j).setCod_recaudador(new Integer(listresumenabonodto.get(i).getCodigo_recaudador()));
                    }
                }
                contador++;
            }
        }
       
        return listresumenabonodto;
    }
    
    /**
     * Testde prueba
     * @param argv
     */
    public static void main(String argv[]) {
        PreviredCore test = new PreviredCore();
        
        test.loadConfig("C:\\WSLongName\\Previred_6299_100420\\src\\Previred\\public_html\\WEB-INF\\config\\configfileabono.xml");
        //test.doProcesar("C:\\workspace_jdeveloper\\BVNET2_0\\RendicionesPrevired\\Previred\\ViewController\\public_html\\WEB-INF\\rendicion\\AAMABCO_20081204_1947B.txt","ABONO");        
        //ResultadoProcesoVO resp = test.doProcesarExcel("C:\\Documents and Settings\\alejandro.fernandez\\Escritorio\\PREVIRED\\LS100635\\datos\\Planilla_carga_APVCAJALOSANDES.xlsx","PLANILLAANDESMANUAL");        
        //System.out.println(resp);
        
        /*List<ResumenAbonoDTO> lista = new ArrayList<ResumenAbonoDTO>();
         
        ResumenAbonoDTO resumen = new ResumenAbonoDTO();
        resumen.setCodigo_recaudador("1");        
        lista.add(resumen);
        
        ResumenAbonoDTO resumen1 = new ResumenAbonoDTO();
        resumen1.setCodigo_recaudador("2");        
        lista.add(resumen1);
        
        ResumenAbonoDTO resumen2 = new ResumenAbonoDTO();
        resumen2.setCodigo_recaudador("3");        
        lista.add(resumen2);
        
        ResumenAbonoDTO resumen3 = new ResumenAbonoDTO();
        resumen3.setCodigo_recaudador("2");        
        lista.add(resumen3);
        
        ResumenAbonoDTO resumen4 = new ResumenAbonoDTO();
        resumen4.setCodigo_recaudador("4");        
        lista.add(resumen4);
        
        checkDuplicate(lista);*/
        
    }   
}

