package cl.bicevida.previred.core.structure;

import java.io.Serializable;

import java.util.Hashtable;

public class RowStructureVo implements Serializable{
    @SuppressWarnings("compatibility:-6264548647851818493")
    private static final long serialVersionUID = 1L;

    public RowStructureVo() {
    }
    
    private String id;           //Identificador en el sistema
    private int posicionInicial; //Posicion inicial de lectura en caso de largo fijo
    private int largo;           //Largo del campo
    private String tipo;         //Tipo de campo a convertir        
    private Hashtable campos;    //Campos asociados al tipo de fila  
    private int valor;           //Valor del registro
    
    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setPosicionInicial(int posicionInicial) {
        this.posicionInicial = posicionInicial;
    }

    public int getPosicionInicial() {
        return posicionInicial;
    }

    public void setLargo(int largo) {
        this.largo = largo;
    }

    public int getLargo() {
        return largo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setCampos(Hashtable campos) {
        this.campos = campos;
    }

    public Hashtable getCampos() {
        return campos;
    }
    
    public void addField(String pk, FieldStructureVo field) {
        if (getCampos()==null) campos = new Hashtable();
        campos.put(pk, field);
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getValor() {
        return valor;
    }
}
