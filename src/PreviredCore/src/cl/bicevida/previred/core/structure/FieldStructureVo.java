package cl.bicevida.previred.core.structure;

import java.io.Serializable;

public class FieldStructureVo implements Serializable{
    @SuppressWarnings("compatibility:3372059813624315478")
    private static final long serialVersionUID = 1L;

    public FieldStructureVo() {
    }
    
    private String id;           //Identificador en el sistema
    private int posicionInicial; //Posicion inicial de lectura en caso de largo fijo
    private int largo;           //Largo del campo
    private String tipo;         //Tipo de campo a convertir
    private String formato;      //En caso de fechas con formato de lectura    

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setPosicionInicial(int posicionInicial) {
        this.posicionInicial = posicionInicial;
    }

    public int getPosicionInicial() {
        return posicionInicial;
    }

    public void setLargo(int largo) {
        this.largo = largo;
    }

    public int getLargo() {
        return largo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getFormato() {
        return formato;
    }
}
