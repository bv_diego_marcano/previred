package cl.bicevida.previred.core.structure;

import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.common.utils.NumeroUtil;

import java.io.Serializable;

import java.util.Hashtable;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;


public class FileStructureVo implements Serializable {
    @SuppressWarnings("compatibility:-3002698818345874097")
    private static final long serialVersionUID = 1L;

    public FileStructureVo() {
    }
    
    private String nombre; //nombre del tipo de archivo
    private String tipoArchivo; //tipo de archivo, si es de largo fijo
    private String descripcion; //descripción del tipo de archivo
    private Hashtable tiporegistro; // tipo de registro de la fila leida
    private String linea;
    private HSSFRow lineaExcel;

    /**
     * Setea data de registro para que esta clase la interpretse
     * y sea capas de lozalizar los valores segun se lo pida directamente
     * el interpresador.     * 
     * @param dataRow
     */
    public void setDataRow(String dataRow) {
        linea = dataRow;
    }
    
    public void setDataRowExcel(HSSFRow dataRow) {
        lineaExcel = dataRow;
    }
    
    
    /**
     * recupera el valor de un campo segun la data especificada
     * para la lectura, en caso de ser encontrador el campo
     * este responde un Object con el tipo de valor solicitado
     * <p>
     *   <code>
     *     FileStructureVo filestr = config.get(0);
     *     while (String linea = archivo.getNextLine()) {
     *        filestr.setDataRow(linea);
     *        String vlId = (java.lang.String) filestr.getValueRow("id");
     *        Date vlFecha = (java.util.Date) filestr.getValueRow("fecha");
     *        Long vlMonto = (java.lang.Long) filestr.getValueRow("monto");       
     *     }
     *   </code>
     * </p>
     * @param idField
     * @return
     */
    public Object getValueRow(String registro, String idField) {
        return localizeValueFromId(registro, idField);
    }    
    
    /**
     * Recuper ael valor de un campo
     * @param idField
     * @return
     */
    private Object localizeValueFromId(String registro, String idField) {
        Object resp = null;
        if (linea != null) {
            RowStructureVo registrorow = (RowStructureVo) tiporegistro.get(registro);
            if (registrorow!=null) {
                FieldStructureVo campo = (FieldStructureVo) registrorow.getCampos().get(idField);
                if ( tipoArchivo.equalsIgnoreCase("LargoFijo") ) resp = getLargoFijoValue(campo);  
                if ( tipoArchivo.equalsIgnoreCase("Excel") ) resp = getExcelValue(campo);
            }           
        }
        
        if (lineaExcel != null) {
            RowStructureVo registrorow = (RowStructureVo) tiporegistro.get(registro);
            if (registrorow!=null) {
                FieldStructureVo campo = (FieldStructureVo) registrorow.getCampos().get(idField);                
                if ( tipoArchivo.equalsIgnoreCase("Excel") ) resp = getExcelValue(campo);
            }           
        }
        return resp;
    }
    
    /**
     * recupera el valor en base a largo fijo
     * @param campo
     * @return
     */
    private Object getLargoFijoValue(FieldStructureVo campo) {
        Object data = null;
        int posini = campo.getPosicionInicial()-1;
        int largov = posini + campo.getLargo();
        if (linea != null && largov <= linea.length() ) {
            String valor = linea.substring(posini,largov);        
            data = convertirValorToType(valor, campo);
        }
        return data;
    }
    
    /**
     * recupera el valor en base a largo fijo
     * @param campo
     * @return
     */
    private Object getExcelValue(FieldStructureVo campo) {
        Object data = null;
        int posini = campo.getPosicionInicial();
        if (lineaExcel != null) {
            HSSFCell celda = lineaExcel.getCell(posini);
            String valor = getCellValor(celda);
            data = convertirValorToType(valor, campo);
        }
        return data;
    }
    
    /**
     * Recupera celda valor String
     * @param cell
     * @return
     */
    private String getCellValor(HSSFCell cell) {
       String valor = null;
       /*
        * Now we will get the cell type and display the values
        * accordingly.
        */
        if (cell != null) {
           switch (cell.getCellType ())       {
                   case HSSFCell.CELL_TYPE_NUMERIC :               
                           valor = Double.toString(cell.getNumericCellValue());
                           break;
                   case HSSFCell.CELL_TYPE_STRING : 
                           HSSFRichTextString richTextString = cell.getRichStringCellValue ();                       
                           valor =  richTextString.getString();                       
                           break;
                   default : 
                           valor = "";
                           break;
            }
        }
        return valor;
    }
    
    
    /**
     * Convierte un dato a un valor especifico
     * segun el tipo de dato solicitado
     * @param valor
     * @param campo
     * @return
     */
    private Object convertirValorToType(String valor, FieldStructureVo campo) {
        Object rep = null;
        try {
            if (campo.getTipo().equalsIgnoreCase("java.lang.String")) rep = new String(valor);
            if (campo.getTipo().equalsIgnoreCase("java.lang.Long")) rep = new Long(NumeroUtil.removeSeparadoresMilesConDecimales(valor));
            if (campo.getTipo().equalsIgnoreCase("java.lang.Double")) rep = new Double(NumeroUtil.removeSeparadoresMilesConDecimales(valor));
            if (campo.getTipo().equalsIgnoreCase("java.lang.Boolean")) rep = new Boolean(NumeroUtil.removeSeparadoresMilesConDecimales(valor));
            if (campo.getTipo().equalsIgnoreCase("java.lang.Float")) rep = new Float(NumeroUtil.removeSeparadoresMilesConDecimales(valor));
            if (campo.getTipo().equalsIgnoreCase("java.lang.Integer")) rep = new Integer(NumeroUtil.removeSeparadoresMilesConDecimales(valor));
            if (campo.getTipo().equalsIgnoreCase("java.util.Date")) rep = FechaUtil.getFecha(campo.getFormato(), valor);            
        } catch (Exception e) {
            e.printStackTrace();    
        }
        return rep;
    }
    
    /**
     * Convierte un dato a un valor especifico
     * segun el tipo de dato solicitado
     * @param objeto
     * @return
     */
    private String getObjectToString(Object objeto) {
        String rep = null;
        try {
            if (objeto instanceof java.lang.String) rep = (String) objeto;
            if (objeto instanceof java.lang.Long) rep = Long.toString(((Long) objeto));
            if (objeto instanceof java.lang.Double) rep = Double.toString(((Double)objeto));
            if (objeto instanceof java.lang.Boolean) rep = Boolean.toString(((Boolean)objeto));
            if (objeto instanceof java.lang.Float) rep = Float.toString(((Float)objeto));
            if (objeto instanceof java.lang.Integer) rep = Integer.toString(((Integer)objeto));
            if (objeto instanceof java.util.Date) rep = FechaUtil.getFechaFormateoStandar(((java.util.Date)objeto));            
        } catch (Exception e) {
            e.printStackTrace();    
        }
        return rep;
    }  
    
    
    
    /**
     * recupera el valor de un campo segun la data especificada
     * para la lectura, en caso de ser encontrador el campo
     * este responde un Object con el tipo de valor solicitado
     * <p>
     *   <code>
     *     FileStructureVo filestr = config.get(0);
     *     while (String linea = archivo.getNextLine()) {
     *        filestr.setDataRow(linea);
     *        String vlId = (java.lang.String) filestr.getValueRow("id");
     *        Date vlFecha = (java.util.Date) filestr.getValueRow("fecha");
     *        Long vlMonto = (java.lang.Long) filestr.getValueRow("monto");       
     *     }
     *   </code>
     * </p>
     * @param idRegistro
     * @return
     */
    public Boolean getTipoRegistroById(String idRegistro) {
        Boolean resp = false;
        if (linea != null) {
            RowStructureVo registro = (RowStructureVo) tiporegistro.get(idRegistro);
            if (registro!=null) {
                if ( tipoArchivo.equalsIgnoreCase("LargoFijo") ) resp = getRegistroValue(registro);                
            } 
        }
        return resp;
    }   
    
    
    /**
     * recupera el valor en base a largo fijo
     * @param registro
     * @return
     */
    private Boolean getRegistroValue(RowStructureVo registro) {
        Integer data = 0;
        Boolean resp = false; 
        int posini = registro.getPosicionInicial()-1;
        int largov = posini + registro.getLargo();
        if (linea != null && largov <= linea.length() ) {
            String valor = linea.substring(posini,largov);                             
            if (registro.getTipo().equalsIgnoreCase("java.lang.Integer")){
                data = new Integer(NumeroUtil.removeSeparadoresMilesConDecimales(valor));
                if(data.intValue() == registro.getValor()){
                    resp = true;
                }
            }
        }
        return resp;
    }
    
    public RowStructureVo getStructureRegistroExcelById(String idRegistro) {
        RowStructureVo resp = (RowStructureVo) tiporegistro.get(idRegistro);        
        return resp;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setTiporegistro(Hashtable tiporegistro) {
        this.tiporegistro = tiporegistro;
    }

    public Hashtable getTiporegistro() {
        return tiporegistro;
    }

    public void addField(String pk, RowStructureVo registro) {
        if (getTiporegistro()==null) tiporegistro = new Hashtable();
        tiporegistro.put(pk, registro);
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getLinea() {
        return linea;
    }

    public void setLineaExcel(HSSFRow lineaExcel) {
        this.lineaExcel = lineaExcel;
    }

    public HSSFRow getLineaExcel() {
        return lineaExcel;
    }
}
