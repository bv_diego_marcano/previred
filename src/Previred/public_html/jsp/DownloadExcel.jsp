<%@ page contentType="text/plain;charset=windows-1252"%>
<%@ page import="javax.faces.context.FacesContext"%>
<%@ page import="cl.bicevida.previred.view.backing.DownloadExcelMB"%>

<%
    response.setContentType("application/xls");
    response.setHeader("Content-Disposition", "attachment; filename=\"RecaudacionTesoreria.xls\"");
    out.clear();
    FacesContext ctx = FacesContext.getCurrentInstance();
    //DownloadExcelMB dTxt = (DownloadExcelMB)ctx.getApplication().createValueBinding("#{DownloadExcelMB}").getValue(ctx);
    DownloadExcelMB dTxt = (DownloadExcelMB)ctx.getApplication().getExpressionFactory().createValueExpression(ctx.getELContext(),"#{DownloadExcelMB}",Object.class).getValue(ctx.getELContext());
    dTxt.getArchivoExcel(response.getOutputStream());
    response.getOutputStream().flush();
    
%>
<%-- oracle-jdev-comment:auto-binding-backing-bean-name:DownloadExcelMB--%>