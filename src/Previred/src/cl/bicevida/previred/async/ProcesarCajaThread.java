package cl.bicevida.previred.async;

import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.common.dto.ArchivoPlanillaAntecedentesDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaDetalleDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaEncabezadoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaResumenDTO;
import cl.bicevida.previred.common.dto.InformarRecaudacionIn;
import cl.bicevida.previred.common.dto.PKTransaccionPagoDTO;
import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;

import cl.bicevida.previred.common.utils.RutUtil;
import cl.bicevida.previred.locator.ServiceLocator;

import cl.bicevida.previred.model.PreviredEJB;

import cl.bicevida.previred.util.ResourcesAsyncUtil;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

public class ProcesarCajaThread extends Thread  implements Serializable {
    @SuppressWarnings("compatibility:954268748659669049")
    private static final long serialVersionUID = 1L;
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ProcesarCajaThread.class);
    private List<ResumenRendicionesVwDTO> dtoCajas;
    private PreviredEJB ejb;
    private String tipopl;
    private Integer medioPago;
    
    /**
     * Constructor Inicial
     * @param procesar
     */
    public ProcesarCajaThread(List<ResumenRendicionesVwDTO> procesar, String tipoRend, Integer medioPagotmp) {
        dtoCajas = procesar;
        ejb = ServiceLocator.getSessionEJBPrevired();
        tipopl = tipoRend;
        this.medioPago = medioPagotmp;
    }
    
    /**
     * Inicial el proceso de revision y ejecucion
     * de transacciones en sistema de cajas en forma
     * asyncrona
     * 
     * @return nada
     */
    public void run() {
     if (dtoCajas != null) {
       Iterator rowsa = dtoCajas.iterator();
       java.util.Date fechaTurno = null;
    
       //===========================================================
       //APERTURA TURNO Y ENVIA TRANSACCIONES   
       //===========================================================
       Long idturno = ejb.callNewAperturaTurnoCaja(tipopl);
      
       if (idturno != null && idturno.longValue() > 0) {
            Long turnoBackup = new Long(""+idturno.longValue());
             
           //RECUPERA FECHA DE APERTURA DEL TURNO
           fechaTurno = ejb.getNewFechaHoraTurno(idturno);
             
           while (rowsa.hasNext()) {
               ResumenRendicionesVwDTO dto = (ResumenRendicionesVwDTO) rowsa.next();
               //logger.info("Procesando Registro :" + dto.getId_folio_abono());
               if (dto.getEstado_proceso_planilla() != Constant.PROCESO_EJECUCION_MANUAL_CAJAS) {
                    ArchivoPlanillaEncabezadoDTO exe = new ArchivoPlanillaEncabezadoDTO();
                    ArchivoPlanillaDetalleDTO  dtoDet = new ArchivoPlanillaDetalleDTO();
                    ArchivoPlanillaAntecedentesDTO dtoAn = new ArchivoPlanillaAntecedentesDTO();
                    ArchivoPlanillaResumenDTO dtoRe = new ArchivoPlanillaResumenDTO();
                    
                    dtoDet.setRut_trabajador(Integer.parseInt(dto.getRut_trabajador()));                
                    dtoDet.setDv_trabajador(RutUtil.calculaDv(dtoDet.getRut_trabajador()));
                    dtoDet.setNombreTrabajador(dto.getNombre_trabajador());
                    dtoDet.setTotal_a_pagar(dto.getMonto_no_aplicado());
                    dtoDet.setPeriodo_pago(dto.getPeriodo_pago());
                    dtoDet.setDeposito_convenio(dto.getDeposito_convenio());
                    
                    dtoRe.setTotal_pagar_ia(dto.getMonto_no_aplicado()); //INPUTETEXT            
                    dtoRe.setPeriodo_pago(dto.getPeriodo_pago());             
                    dtoAn.setRazon_social_pagador(dto.getRazon_social_empleador());               
                    exe.setRut_pagador(Integer.parseInt(dto.getRut_empleador()));
                    exe.setDv_pagador(RutUtil.calculaDv(exe.getRut_pagador()));
                    exe.setId_folio_planilla(dto.getId_folio_abono());
                    exe.setFecha_hora_operacion(dto.getFecha_hora_operacion());   
                                       
                    exe.setDetalle(new ArrayList<ArchivoPlanillaDetalleDTO>());
                    exe.getDetalle().add(dtoDet);
                                        
                    exe.setAntecedentes(dtoAn);
                    exe.setResumen(dtoRe);
                    
                    //LLAMA PLSQL CAJA
                    
                    ArchivoPlanillaEncabezadoDTO res = ejb.callGeneraTransaccion(exe,  true, tipopl, medioPago);
                                        
                    List<PKTransaccionPagoDTO> fallo = new ArrayList<PKTransaccionPagoDTO>();
                    PKTransaccionPagoDTO upt = new PKTransaccionPagoDTO();
                    upt.setIdCarga(dto.getId_carga());
                    upt.setIdCargaPlanilla(dto.getId_carga_planilla());
                    upt.setFolioPlanilla(dto.getId_folio_abono());
                    upt.setCodigoRecaudador(dto.getCodigo_recaudador());
                    
                    
                    //Actualiza resultado operacion
                    if (res != null && res.getDetalle().get(0).getId_folio_pagocaja() != 0) {
                        upt.setNewEstado(Constant.PROCESO_AUTORIZADO);
                        upt.setNewDescripcion("Procesado Correctamente");
                        upt.setFolioCaja(res.getDetalle().get(0).getId_folio_pagocaja().doubleValue());
                        upt.setPoliza(res.getDetalle().get(0).getPolizaAsociada());
                        upt.setConceptoCaja(res.getDetalle().get(0).getConceptoRecaudacion());
                        
                        
                    } else {
                        upt.setNewEstado(Constant.PROCESO_PROBLEMA_PAGO_CAJA);
                        upt.setNewDescripcion("Folio " + dto.getId_folio_abono().longValue() + " No fue procesado correctamente por el proceso de caja: " + res.getDetalle().get(0).getDecsResultPago());                    
                    }
                    
                   fallo.add(upt);
                   ejb.changeStatusTransaccionProccess(fallo);
               }
            }
            
            //===========================================================
            //CIERRA EL TURNO DE CAJA
            //===========================================================
            ejb.callNewCierreTurnoCaja(tipopl, idturno);
            
            //INFORMAR A AR MEDIANTE WS
             // ============ LLAMA A WEBSERVICE PARA NOTIFICACION DE TERMINO =========
              try {
                      logger.info("NOTIFICACION AR : PREVIRED  - INICIANDO...." );
                      //Notifica Webservices de Recaudacion
                      //RecaudacionServiceClient myPort = new RecaudacionServiceClient();
                      //myPort.setEndpoint(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.endpoint"));
                      //InformarRecaudacionIn in = new InformarRecaudacionIn();
                      InformarRecaudacionIn in = new InformarRecaudacionIn();
                      in.setCallerSystem(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.callersystem"));
                      in.setCallerUser(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.calleruser"));
                      
                      //RECUPERA EL FOLIO DE CAJA DIARIO
                      Long foliocajadiario = ejb.getNewFolioCajaByTurno(fechaTurno);
                      in.setFolioCajaDiario(foliocajadiario);//SELECT
                      in.setFolioPago(null);
                      Calendar cale = new GregorianCalendar();
                      cale.setTime(fechaTurno);
                      in.setTurno(cale);
                      in.setIdTurno(turnoBackup);
                      in.setUsuarioCaja(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.usuariocaja"));
                      
                      logger.info("NOTIFICACION --> Ejecutamos distribucion en caja y creamos concurrente");
                      ejb.ejecutarDistribucionEnCajayConcurrente(in);
                            
                      logger.info("NOTIFICANDO USUARIO PREVIRED MODO MANUAL :" +in.getUsuarioCaja());
                      logger.info("NOTIFICACION - getCallerSystem:" + in.getCallerSystem());
                      logger.info("NOTIFICACION - getCallerUser:" + in.getCallerUser());
                      logger.info("NOTIFICACION - getTurno:" + in.getTurno());
                      logger.info("NOTIFICACION - getIdTurno:" + in.getIdTurno());
                      logger.info("NOTIFICACION - getFolioCajaDiario:" + in.getFolioCajaDiario());
                      logger.info("NOTIFICACION - getUsuarioCaja:" + in.getUsuarioCaja());
                      logger.info("NOTIFICACION AR : OK  - ID TURNO CAJA PREVIRED:"+turnoBackup );
                      //logger.info("NOTIFICACION AR VIA WS : OK  - ID TURNO CAJA PREVIRED:"+turnoBackup );
              } catch (Exception e) {
                e.printStackTrace();
              }
         }    
     }
     
     
    }
}
