package cl.bicevida.previred.async.procesoautomatico;

import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.common.dto.CargaArchivosDTO;
import cl.bicevida.previred.common.dto.ConvenioPagoDTO;
import cl.bicevida.previred.common.dto.RendProcesoAutomatico;
import cl.bicevida.previred.common.dto.TipoArchivoDTO;
import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.common.utils.StringUtil;
import cl.bicevida.previred.core.abono.PreviredCore;
import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;
import cl.bicevida.previred.util.FileUtil;
import cl.bicevida.previred.util.ResourcesAsyncUtil;
import cl.bicevida.previred.util.ResourcesMessagesUtil;
import cl.bicevida.previred.view.vo.ProcesoRendicionVO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.net.InetAddress;
import java.net.URL;

import java.net.UnknownHostException;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

/*
 * GR: 04-05-2018
 * Clase que procesa las recaudaciones de la Caja Andes y Caja la Araucana
 * */
public class RecaudacionAutomaticaCajas {


    private static final Logger logger = Logger.getLogger(RecaudacionAutomaticaCajas.class);

    private static PreviredCore previredCore = null;
    private PreviredEJB ejb;
    private String dirWebContent;
    private String fileupload;
    private Double idCargaConsulta;
    private List<TipoArchivoDTO> tiposArchivos;
    private String nombreServidor;

    public RecaudacionAutomaticaCajas() {
        previredCore = new PreviredCore();
        ejb = ServiceLocator.getSessionEJBPrevired();
        ClassLoader classLoader = getClass().getClassLoader();
        String path = classLoader.getResource("/").getPath();
        dirWebContent = path.substring(0, path.indexOf("WEB-INF"));
        String fileconfig = dirWebContent + "config" + File.separator + "configfileabono.xml";
        previredCore.loadConfig(fileconfig);
    }

    /**
     * Proceso de carga y validacion
     * antes de procesar achivo
     * @return
     */
    public String doProcesarActionCaja(String tipoPL, Integer medioPago, String rutaPuntoMontaje) {
        String directorioOrigen = rutaPuntoMontaje + "/";
        String directorioDestino = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.procesados") + "/";
        String directorioDestinoNoProcesados =
            ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.no.procesados") + "/";
        String nombreArchivoGeneral = "";
        try {

            ejb = ServiceLocator.getSessionEJBPrevired();
            if (ejb == null)
                return null;
            File directorio = new File(rutaPuntoMontaje);
            if (ejb.validaDiaHabil(new Date())) {
                if (directorio.exists() && directorio.listFiles().length > 0) {
                    for (File filePM : directorio.listFiles()) {
                        Thread.sleep(5000);
                        logger.info("Cantidad de archivos a procesar [" + directorio.listFiles().length + "]");
                        String filename = filePM.getName();
                        logger.info("Archivo a procesar [" + filename + "]");
                        nombreArchivoGeneral = filename;
                        // Validar existencia del convenio empresa-Medio pago
                        // Obtener convenio
                        // (1) Corresponde a Bicevida
                        ConvenioPagoDTO convenioPagoDTO = ejb.getConvenioDePago(1, medioPago);

                        if (convenioPagoDTO != null) {

                            //Validacion del archivo
                            if (doValidarArchivo(filename, convenioPagoDTO, tipoPL)) {
                                Boolean isProcesoCorriendo = ejb.existProcesoEnEjecucion(Constant.PROCESANDO);
                                Thread.sleep(5000);
                                if (!isProcesoCorriendo) {
                                    // Carga de Archivo para su proceso
                                    if (doLoadFile(filename, convenioPagoDTO, directorio)) {
                                        cargaNombreServer();
                                        //  Insercion de la carga a la Base de Datos
                                        Date fecha_creacion = null;
                                        if (Constant.PLANILLA_ANDES.equals(tipoPL) ||
                                            Constant.PLANILLA_LAARAUCANA.equals(tipoPL)) {

                                            //fecha_creacion = FechaUtil.getFecha("ddMMyyyy", filename.substring(8, 8));
                                            if (Constant.PLANILLA_ANDES.equals(tipoPL)) {
                                                fecha_creacion =
                                                    FechaUtil.getFecha("ddMMyyyy", filename.substring(8, 16));
                                            } else {
                                                fecha_creacion =
                                                    FechaUtil.getFecha("ddMMyyyy",
                                                                       new SimpleDateFormat("ddMMyyyy").format(new Date()));
                                            }
                                            logger.info("Fecha Creacion " + fecha_creacion);
                                            // Creacion de Carga Abono
                                            CargaArchivosDTO cargaDTOAbono = new CargaArchivosDTO();

                                            cargaDTOAbono.setConvenio(convenioPagoDTO);
                                            cargaDTOAbono.setFecha_creacion(fecha_creacion);
                                            cargaDTOAbono.setId_tipo_archivo(1);
                                            cargaDTOAbono.setEstado_proceso(new Integer("0"));
                                            cargaDTOAbono.setFecha_y_hora_upload(new Date());
                                            cargaDTOAbono.setNombre_fisico_archivo(filename);
                                            cargaDTOAbono.setRemote_user("PROCESO_AUTOMATICO_CAJAS");
                                            ejb = ServiceLocator.getSessionEJBPrevired();
                                            cargaDTOAbono = ejb.insertCargaArchivos(cargaDTOAbono);

                                            //Cracion Catga Planilla
                                            CargaArchivosDTO cargaDTO = new CargaArchivosDTO();
                                            cargaDTO.setConvenio(convenioPagoDTO);
                                            cargaDTO.setFecha_creacion(fecha_creacion);
                                            cargaDTO.setEstado_proceso(new Integer("0"));
                                            cargaDTO.setId_tipo_archivo(2); // Corresponde a Planilla
                                            cargaDTO.setFecha_y_hora_upload(new Date());
                                            cargaDTO.setNombre_fisico_archivo(filename);
                                            cargaDTO.setRemote_user("PROCESO_AUTOMATICO_CAJAS");
                                            ejb = ServiceLocator.getSessionEJBPrevired();
                                            cargaDTO = ejb.insertCargaArchivos(cargaDTO);

                                            ejb.guardarProcesoRendicion(new RendProcesoAutomatico(cargaDTO.getId_carga().longValue(),
                                                                                                  2, medioPago,
                                                                                                  new Date(),
                                                                                                  this.nombreServidor,
                                                                                                  Constant.PROCESANDO,
                                                                                                  filename,
                                                                                                  Constant.DESC_PROCESANDO,
                                                                                                  "Procesando..."));
                                            if (cargaDTO.isProcesado()) {
                                                idCargaConsulta = cargaDTO.getId_carga();

                                                if (tipoPL.equals(Constant.PLANILLA_ANDES)) {
                                                    ProcesoRendicionVO process = new ProcesoRendicionVO();
                                                    if (process.doProcesarAbonoPlanillaAndes(cargaDTO.getId_carga(),
                                                                                             fileupload, previredCore,
                                                                                             cargaDTOAbono.getId_carga())) {

                                                        FileUtil.moverArchivo(directorioOrigen, directorioDestino,
                                                                              filename, true);
                                                        ejb.actualizaProcesoRendicion(Constant.FINALIZADO,
                                                                                      Constant.DESC_FINALIZADO,
                                                                                      cargaDTO.getId_carga().longValue(),
                                                                                      "OK");
                                                    }
                                                }

                                                if (tipoPL.equals(Constant.PLANILLA_LAARAUCANA)) {
                                                    ProcesoRendicionVO process = new ProcesoRendicionVO();
                                                    if (process.doProcesarAbonoPlanillaLaAraucana(cargaDTO.getId_carga(),
                                                                                                  fileupload,
                                                                                                  previredCore,
                                                                                                  cargaDTOAbono.getId_carga(),
                                                                                                  Constant.PROCESO_AUTOMATICO)) {

                                                        FileUtil.moverArchivo(directorioOrigen, directorioDestino,
                                                                              filename, true);

                                                        ejb.actualizaProcesoRendicion(Constant.FINALIZADO,
                                                                                      Constant.DESC_FINALIZADO,
                                                                                      cargaDTO.getId_carga().longValue(),
                                                                                      "OK");
                                                    }
                                                }

                                            } else {
                                                logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.crearcarga.problema"));
                                                FileUtil.moverArchivo(directorioOrigen, directorioDestinoNoProcesados,
                                                                      filename, true);
                                            }
                                        } else {
                                            logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.proceso.automatico.convenio.no.corresponde"));
                                            FileUtil.moverArchivo(directorioOrigen, directorioDestinoNoProcesados,
                                                                  filename, true);
                                        }
                                    } else {
                                        logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.cargaarchivo.load.problema"));
                                        FileUtil.moverArchivo(directorioOrigen, directorioDestinoNoProcesados, filename,
                                                              true);
                                    }
                                } else {
                                    logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.proceso.automatico.en.ejecucion"));
                                }
                            } else {
                                logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.nombrearchivo.tipoarchivo.incorrecto"));
                                FileUtil.moverArchivo(directorioOrigen, directorioDestinoNoProcesados, filename, true);
                            }
                        } else {
                            logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.convenio.noexiste"));
                            FileUtil.moverArchivo(directorioOrigen, directorioDestinoNoProcesados, filename, true);
                        }
                    }
                } else {
                    logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.proceso.automatico.no.hay.archivos"));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            FileUtil.moverArchivo(directorioOrigen, directorioDestinoNoProcesados, nombreArchivoGeneral, true);
            logger.info("idCargaConsulta " + idCargaConsulta);
            ejb = ServiceLocator.getSessionEJBPrevired();
            try {
                ejb.actualizaProcesoRendicion(Constant.FINALIZADO_CON_ERROR, Constant.DESC_FINALIZADO_CON_ERROR,
                                              idCargaConsulta.longValue(),
                                              "Se ha producido un error: " + ex.getMessage());

            } catch (NamingException nex) {
                nex.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.exception"));
        }
        return null;
    }

    /**
     * Validacion de archivo
     * @param filename
     * @param convenioPagoDTO
     * @return
     * @throws Exception
     */
    private boolean doValidarArchivo(String filename, ConvenioPagoDTO convenioPagoDTO, String tipoPL) {
        boolean ret = false;
        try {
            String extension = filename.substring(filename.length() - 3, filename.length());
            logger.info("Extension " + extension);
            if (Constant.PLANILLA_ANDES.equalsIgnoreCase(tipoPL) ||
                Constant.PLANILLA_LAARAUCANA.equalsIgnoreCase(tipoPL)) {


                if (convenioPagoDTO.getId_medio_pago().intValue() == Constant.PLANILLA_ANDES_CONVENIO_MANUAL ||
                    convenioPagoDTO.getId_medio_pago().intValue() == Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL) {
                    if (extension.equalsIgnoreCase("xls")) {
                        ret = true;
                    }
                } else {
                    if (extension.equalsIgnoreCase("txt")) {
                        ret = true;
                    }
                }

            } else {
                if (extension.equalsIgnoreCase("txt")) {
                    String tipoArchivo = filename.substring(0, 2);
                    String recaudador = filename.substring(4, 7);
                    boolean isRecaudador = false;
                    if (convenioPagoDTO.getTipo_codbanco_medio_pago().equalsIgnoreCase(recaudador)) {
                        Iterator tiposArchivositr = tiposArchivos.iterator();
                        while (tiposArchivositr.hasNext()) {
                            TipoArchivoDTO tipo = (TipoArchivoDTO) tiposArchivositr.next();
                            if (tipo.getTipoArchivo().equalsIgnoreCase(tipoArchivo)) {
                                isRecaudador = true;
                                break;
                            }
                        }

                        if (!isRecaudador) {
                            ret = false;
                        } else {
                            ret = true;
                        }


                    } else {
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            ret = false;
        }
        return ret;
    }

    /**
     * Carga el archivo en el servidor
     * segun el path de contexto en base a path real
     * @param file
     * @param convenioPagoDTO
     * @return
     * @throws Exception
     */
    private boolean doLoadFile(String filePM, ConvenioPagoDTO convenioPagoDTO,
                               File directorioArchivo) throws Exception {
        boolean ret = false;

        fileupload =
            dirWebContent + "rendicion" + File.separator + convenioPagoDTO.getDirectorio_upload() + File.separator +
            StringUtil.replaceString(filePM, ".", "_") + "_" + System.currentTimeMillis() + ".data";
        logger.debug("doLoadFile() - Path archivo creado: " + fileupload);

        //ruta del archivo del punto de montaje
        String rutaArchivoPM = directorioArchivo + File.separator + filePM;

        //leyendo bytes
        InputStream in = new URL("file://" + rutaArchivoPM).openStream();
        File archivoTmp = new File(fileupload);
        archivoTmp.createNewFile();
        FileOutputStream out = new FileOutputStream(archivoTmp);
        int data;
        while ((data = in.read()) != -1) {
            out.write(data);
        }
        in.close();
        out.close();
        ret = true;

        return ret;
    }

    private void cargaNombreServer() {

        InetAddress address;
        try {
            address = InetAddress.getLocalHost();
            this.nombreServidor = address.getHostName();
        } catch (UnknownHostException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}
