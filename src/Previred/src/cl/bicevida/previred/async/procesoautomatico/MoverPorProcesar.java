package cl.bicevida.previred.async.procesoautomatico;

import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.dao.RendicionDAO;
import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;
import cl.bicevida.previred.pattern.dao.DAOFactory;
import cl.bicevida.previred.pattern.dao.OracleDAOFactory;
import cl.bicevida.previred.util.FileUtil;
import cl.bicevida.previred.util.ResourcesAsyncUtil;
import cl.bicevida.previred.util.SftpUtil;

import com.jcraft.jsch.JSchException;

import java.io.File;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.persistence.internal.helper.SimpleDatabaseType;

/**
 * GR 03-07-2018: Clase para leer los archivos del sftp de previred y dejarlos en punto de montaje de WL12
 */
public class MoverPorProcesar {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(MoverPorProcesar.class);



    
    private PreviredEJB ejb;

    public MoverPorProcesar() {
        super();
        ejb = ServiceLocator.getSessionEJBPrevired();
    }

    /**
     * Metodo encargado de conectarse, leer y escribir en punto de montaje ubicado en weblogic
     */
    public void doMoverArchivos() {

        try {
            //ejb = ServiceLocator.getSessionEJBPrevired();

            String directorioEscrituraPM = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.previred")+"/";
            String directorioEscrituraPorProcesarPM = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.por.procesar")+"/";

            //if(ejb.validaDiaHabil(new Date())){
                // Muevo los archivos del directorio por procesar al directorio de previred
                // una vez que la fecha limite sea menor al dia en curso.
                File directorio = new File(directorioEscrituraPorProcesarPM);
                if (directorio.listFiles().length > 0) {
                    for (File filePM : directorio.listFiles()) {
                        String filename = filePM.getName();
                        if (ejb.existeArchivoCargado(filename.trim()) < 1) {
                            FileUtil.moverArchivo(directorioEscrituraPorProcesarPM, directorioEscrituraPM, filePM.getName(), true);
                            logger.info("Archivo ["+filename +"] movido para ser procesado");
                        }else{logger.info("Archivo ya se encuentra cargado");}
    
                    }
                }else{logger.info("No hay archivos en el directorio");}
            //}else{logger.info("No es dia habil");}

        } catch (IllegalAccessException e) {
            //SftpUtil.disconnect();
            logger.error("Ha ocurrido un error [" + e.getMessage() + "]");
        } catch (JSchException e) {
            //SftpUtil.disconnect();
            logger.error("Ha ocurrido un error [" + e.getMessage() + "]");
        } catch (Exception e) {
            logger.error("Ha ocurrido un error [" + e.getMessage() + "]");
        }
    }
    public void doMoverArchivosByFecha(Integer anio,Integer mes) {

        try {
            //ejb = ServiceLocator.getSessionEJBPrevired();
            logger.info("Anio a procesar ["+anio+"] Mes[" +mes+"]");
            String directorioEscrituraPM = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.previred")+"/";
            String directorioEscrituraPorProcesarPM = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.por.procesar")+"/";

            //if(ejb.validaDiaHabil(new Date())){
                // Muevo los archivos del directorio por procesar al directorio de previred
                // una vez que la fecha limite sea menor al dia en curso.
                File directorio = new File(directorioEscrituraPorProcesarPM);
                if (directorio.listFiles().length > 0) {
                    for (File filePM : directorio.listFiles()) {
                        String filename = filePM.getName();
                        logger.info("Anio archivo["+Integer.parseInt(filename.substring(8, 12))+"] Mes[" +Integer.parseInt(filename.substring(12, 14))+"]");
                        if (ejb.existeArchivoCargado(filename.trim()) < 1) {
                            if(Integer.parseInt(filename.substring(8, 12)) == anio && Integer.parseInt(filename.substring(12, 14))== mes){
                                FileUtil.moverArchivo(directorioEscrituraPorProcesarPM, directorioEscrituraPM, filePM.getName(), true);
                                logger.info("Archivo ["+filename +"] movido para ser procesado");
                            }
                            
                        }else{logger.info("Archivo ya se encuentra cargado");}
    
                    }
                }else{logger.info("No hay archivos en el directorio");}
            //}else{logger.info("No es dia habil");}

        } catch (IllegalAccessException e) {
            //SftpUtil.disconnect();
            logger.error("Ha ocurrido un error [" + e.getMessage() + "]");
        } catch (JSchException e) {
            //SftpUtil.disconnect();
            logger.error("Ha ocurrido un error [" + e.getMessage() + "]");
        } catch (Exception e) {
            logger.error("Ha ocurrido un error [" + e.getMessage() + "]");
        }
    }
}
