package cl.bicevida.previred.async.procesoautomatico;

import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.common.dto.CargaArchivosDTO;
import cl.bicevida.previred.common.dto.ConvenioPagoDTO;
import cl.bicevida.previred.common.dto.EstadoDeProcesoDTO;
import cl.bicevida.previred.common.dto.MediosDePagoDTO;
import cl.bicevida.previred.common.dto.RendProcesoAutomatico;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;
import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;
import cl.bicevida.previred.common.dto.TipoArchivoDTO;
import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.common.utils.NumeroUtil;
import cl.bicevida.previred.common.utils.StringUtil;
import cl.bicevida.previred.core.abono.PreviredCore;
import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;
import cl.bicevida.previred.util.FileUtil;
import cl.bicevida.previred.util.ResourcesAsyncUtil;
import cl.bicevida.previred.util.ResourcesMessagesUtil;
import cl.bicevida.previred.view.backing.MensajesMB;
import cl.bicevida.previred.view.job.TriggerRendicion;
import cl.bicevida.previred.view.util.EmailUtil;
import cl.bicevida.previred.view.util.JsfUtil;
import cl.bicevida.previred.view.vo.ProcesoRendicionVO;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.net.InetAddress;
import java.net.URL;

import java.net.UnknownHostException;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.event.ActionEvent;

import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.apache.myfaces.trinidad.component.core.input.CoreInputHidden;
import org.apache.myfaces.trinidad.event.PollEvent;


public class RecaudacionAutomaticaPrevired {
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(RecaudacionAutomaticaPrevired.class);


    private PreviredEJB ejb;

    private List<TipoArchivoDTO> tiposArchivos;
    private List<ResumenAbonoDTO> resumenAbonoProcesado;
    private List<ResumenAbonoDTO> resumenAbonoProcesadoAutomatico;
    private List<ResumenAbonoDTO> planillaNoInformados;
    private TriggerRendicion trigger;
    private String fileupload;
    private Double idCargaConsulta;
    private boolean renderpoll;
    private int timeout;
    private MensajesMB mensajesmb;
    private Boolean showDetail = Boolean.FALSE;
    private Boolean showMensajeEspera = Boolean.FALSE;
    private Boolean showDetailPlanilla = Boolean.FALSE;
    private Boolean showDescarga = Boolean.FALSE;
    private String pathAbsoluteExcel;
    private String tipopl;
    private String nombreServidor;

    private static PreviredCore previredCore = null;
    private String errorFinalizar = null;

    //Atributo medioDePago proviene del nombre del archivo
    private Integer medioDePago;
    private String dirWebContent;

    public RecaudacionAutomaticaPrevired() {
        previredCore = new PreviredCore();
        ejb = ServiceLocator.getSessionEJBPrevired();
        ClassLoader classLoader = getClass().getClassLoader();
        String path = classLoader.getResource("/").getPath();
        dirWebContent = path.substring(0, path.indexOf("WEB-INF"));
        String fileconfig = dirWebContent + "config" + File.separator + "configfileabono.xml";
        previredCore.loadConfig(fileconfig);
    }

    /**
     * Proceso de carga y validacion
     * antes de procesar achivo
     * @return
     */
    public String doProcesarAction() {
        try {
            if (ejb == null)
                return null;

            File directorio = new File(ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.previred"));
            String directorioOrigen = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.previred") + "/";
            String directorioDestino = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.procesados") + "/";
            if (ejb.validaDiaHabil(new Date())) {
                if (directorio.exists() && directorio.listFiles().length > 0) {
                    logger.info("Cantidad de archivos a procesar [" + directorio.listFiles().length + "]");
                    for (File filePM : directorio.listFiles()) {

                        String filename = filePM.getName();
                        logger.info("Archivo a procesar [" + filename + "]");

                        if (ejb.existeArchivoCargado(filename.trim()) < 1) {
                            Date fecha_creacion = null;
                            fecha_creacion = FechaUtil.getFecha("yyyyMMdd_HHmm", filename.substring(8, 21));

                            // Verifica si es abono o planilla
                            Integer tipoArchivo = doVerificaTipoArchivo(filename);
                            tiposArchivos = ejb.getTipoArchivo(tipoArchivo);
                            // medioDePago = Corresponde al medio de pago obtenido desde el nombre del archivo
                            medioDePago = doObtenerMedioDePagoByFile(filename);

                            if (medioDePago != null) {
                                Boolean isProcesoCorriendo = ejb.existProcesoEnEjecucion(Constant.PROCESANDO);
                                if (!isProcesoCorriendo) { //validar que no haya proceso corriendo
                                    // Validar existencia del convenio empresa-Medio pago
                                    //Obtener convenio
                                    // (1) = Corresponde a Bicevida
                                    ConvenioPagoDTO convenioPagoDTO = ejb.getConvenioDePago(1, medioDePago);

                                    if (convenioPagoDTO != null) {

                                        //Validacion del archivo
                                        if (doValidarArchivo(filename, convenioPagoDTO)) {

                                            //Carga de Archivo para su proceso
                                            if (doLoadFile(filename, convenioPagoDTO, directorio)) {
                                                cargaNombreServer();
                                                //Insercion de la carga a la Base de Datos
                                                CargaArchivosDTO cargaDTO = new CargaArchivosDTO();
                                                cargaDTO.setConvenio(convenioPagoDTO);
                                                cargaDTO.setFecha_creacion(fecha_creacion);
                                                //(1) = Corresponde a Abono
                                                //(2) = Corresponde a Planilla
                                                cargaDTO.setId_tipo_archivo(tipoArchivo);
                                                //cargaDTO.setId_tipo_archivo((Integer) this.selectOneChoiceNombreArchivo.getValue());

                                                cargaDTO.setEstado_proceso(new Integer("0"));
                                                cargaDTO.setFecha_y_hora_upload(new Date());
                                                cargaDTO.setNombre_fisico_archivo(filename);
                                                cargaDTO.setRemote_user("PROCESO_AUTOMATICO_PREVIRED");
                                                ejb = ServiceLocator.getSessionEJBPrevired();
                                                cargaDTO = ejb.insertCargaArchivos(cargaDTO);
                                                ejb.guardarProcesoRendicion(new RendProcesoAutomatico(cargaDTO.getId_carga().longValue(),
                                                                                                      tipoArchivo,
                                                                                                      medioDePago,
                                                                                                      new Date(),
                                                                                                      this.nombreServidor,
                                                                                                      Constant.PROCESANDO,
                                                                                                      filename,
                                                                                                      Constant.DESC_PROCESANDO,
                                                                                                      "Procesando..."));

                                                if (cargaDTO.isProcesado()) {
                                                    idCargaConsulta = cargaDTO.getId_carga();

                                                    //Inicio del job
                                                    ProcesoRendicionVO process = new ProcesoRendicionVO();
                                                    if (tipoArchivo == 1) {
                                                        logger.info("nombre archivo a consultar Abono: [" + filename +
                                                                    "] ID Carga [" + cargaDTO.getId_carga() + "]");
                                                        Boolean isOk =
                                                            process.doProcesarAbonoAut(cargaDTO.getId_carga(), fileupload,
                                                                                    previredCore);

                                                        if (isOk) {
                                                            boolean isAutorizaAbono = false;
                                                            Integer contadorProcesados = 0;
                                                            CargaArchivosDTO cargaArchivos =
                                                                ejb.getCargaArchivosByIdCarga(idCargaConsulta);
                                                            if (cargaArchivos.getEstado_proceso().equals(Constant.PROCESO_FINALIZADO_CARGA)) {
                                                                resumenAbonoProcesadoAutomatico =
                                                                    ejb.getAbonoByIdCarga(idCargaConsulta);
                                                                for (ResumenAbonoDTO resumen :
                                                                     resumenAbonoProcesadoAutomatico) {
                                                                    isAutorizaAbono =
                                                                        doAutorizaAbono(resumen, filename);
                                                                    if (isAutorizaAbono) {
                                                                        contadorProcesados++;
                                                                    }
                                                                }
                                                                logger.info("cantidad de archivos en lista " +
                                                                            resumenAbonoProcesadoAutomatico.size());
                                                                logger.info("cantidad de archivos procesados " +
                                                                            contadorProcesados);
                                                                if (resumenAbonoProcesadoAutomatico.size() ==
                                                                    contadorProcesados) {
                                                                    ejb.updateCargaArchivosEstadoProceso(idCargaConsulta,
                                                                                                         Constant.PROCESO_FINALIZADO_AUTORIZADO,
                                                                                                         null, null);
                                                                    FileUtil.moverArchivo(directorioOrigen,
                                                                                          directorioDestino, filename,
                                                                                          true);
                                                                    ejb.actualizaProcesoRendicion(Constant.FINALIZADO,
                                                                                                  Constant.DESC_FINALIZADO,
                                                                                                  cargaDTO.getId_carga().longValue(),
                                                                                                  "OK");


                                                                } else {
                                                                    //se incluye eliminacion para los registros fuera de horarios y/o no procesados
                                                                    ejb.deleteDetalleAbono(cargaDTO.getId_carga());
                                                                    ejb.deleteResumenAbono(cargaDTO.getId_carga());
                                                                    ejb.deleteCargaArchivo(cargaDTO.getId_carga());
                                                                    ejb.deleteProcesoAutomatico(cargaDTO.getId_carga());
                                                                    //NO SE AUTORIZARON TODOS LOS REGISTROS DEL ARCHIVO
                                                                    logger.error("No se han autorizado todos los registros del archivo.");
                                                                    throw new Exception("No se han autorizado todos los registros del archivo.");
                                                                }
                                                            }
                                                        }
                                                    } else if (tipoArchivo == 2) {
                                                        logger.info("nombre archivo a consultar Planilla: [" +
                                                                    filename + "] ID Carga [" + cargaDTO.getId_carga() +
                                                                    "]");
                                                        logger.info("nombre archivo a consultar cortado: " +
                                                                    filename.substring(4));

                                                        Boolean existeAbonoPrevio =
                                                            ejb.existAbonoFinalizado(filename.substring(4),
                                                                                     Constant.PROCESO_FINALIZADO_AUTORIZADO);
                                                        logger.info("Pregunto si existe abono: " + existeAbonoPrevio);
                                                        Boolean flag = false;
                                                        String mensaje = null;
                                                        if (existeAbonoPrevio) {
                                                            flag =
                                                                process.doProcesarPlanillaAut(cargaDTO.getId_carga(),
                                                                                           fileupload, previredCore);

                                                            ejb.actualizaProcesoRendicion(Constant.FINALIZADO,
                                                                                          Constant.DESC_FINALIZADO,
                                                                                          cargaDTO.getId_carga().longValue(),
                                                                                          "OK");
                                                            if (flag) {
                                                                mensaje =
                                                                    FileUtil.moverArchivo(directorioOrigen,
                                                                                          directorioDestino, filename,
                                                                                          true);
                                                            }
                                                            logger.info(mensaje);
                                                        } else {
                                                            //se incluye eliminacion para los registros fuera de horarios y/o no procesados
                                                            ejb.deletePlanillaAbono(cargaDTO.getId_carga());
                                                            ejb.deleteCargaArchivo(cargaDTO.getId_carga());
                                                            ejb.deleteProcesoAutomatico(cargaDTO.getId_carga());
                                                            /* ejb.actualizaProcesoRendicion(Constant.FINALIZADO_CON_ERROR,
                                                                                         Constant.DESC_FINALIZADO_CON_ERROR,
                                                                                         cargaDTO.getId_carga().longValue(),
                                                                                         "ERROR, no existe abono previo");*/


                                                        }
                                                    }

                                                    // Incorporar time out
                                                    Thread.sleep(5000);

                                                } else {
                                                    logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.crearcarga.problema"));
                                                }

                                            } else {
                                                logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.cargaarchivo.load.problema"));
                                                throw new Exception(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.cargaarchivo.load.problema"));
                                            }
                                        } else {
                                            logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.nombrearchivo.tipoarchivo.incorrecto"));
                                        }
                                    } else {
                                        logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.convenio.noexiste"));
                                    }
                                } else {
                                    logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.proceso.automatico.en.ejecucion"));
                                }
                            }
                        } else {
                            logger.info("El archivo [" + filename + "] ya fue cargado");
                        }

                    }
                } else {
                    logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.proceso.automatico.no.hay.archivos"));
                }
            }else{
                logger.info("no es dia habil");
            }
                 
        } catch (Exception e) {
            ejb = ServiceLocator.getSessionEJBPrevired();
            try {
                ejb.actualizaProcesoRendicion(Constant.FINALIZADO_CON_ERROR, Constant.DESC_FINALIZADO_CON_ERROR,
                                              idCargaConsulta.longValue(),
                                              "Se ha producido un error: " + e.getMessage());
            } catch (NamingException nex) {
                nex.printStackTrace();
            } catch (Exception f) {
                f.printStackTrace();
            }
            logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.exception"));
            logger.error("doProcesarAction() - Se ha producido un error: " + e.getMessage(), e);
        }

        return "";
    }

    /**
     * Validacion de archivo
     * @param filename
     * @param convenioPagoDTO
     * @return
     * @throws Exception
     */
    private boolean doValidarArchivo(String filename, ConvenioPagoDTO convenioPagoDTO) {
        boolean ret = false;
        try {
            String extension = filename.substring(filename.length() - 3, filename.length());
            logger.info("Extension " + extension);
            if (Constant.PLANILLA_ANDES.equalsIgnoreCase(this.tipopl) ||
                Constant.PLANILLA_LAARAUCANA.equalsIgnoreCase(this.tipopl)) {


                if (convenioPagoDTO.getId_medio_pago().intValue() == Constant.PLANILLA_ANDES_CONVENIO_MANUAL ||
                    convenioPagoDTO.getId_medio_pago().intValue() == Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL) {
                    if (extension.equalsIgnoreCase("xls")) {
                        ret = true;
                    }
                } else {
                    if (extension.equalsIgnoreCase("txt")) {
                        ret = true;
                    }
                }

            } else {
                if (extension.equalsIgnoreCase("txt")) {
                    String tipoArchivo = filename.substring(0, 2);
                    String recaudador = filename.substring(4, 7);
                    boolean isRecaudador = false;
                    if (convenioPagoDTO.getTipo_codbanco_medio_pago().equalsIgnoreCase(recaudador)) {
                        Iterator tiposArchivositr = tiposArchivos.iterator();
                        while (tiposArchivositr.hasNext()) {
                            TipoArchivoDTO tipo = (TipoArchivoDTO) tiposArchivositr.next();
                            if (tipo.getTipoArchivo().equalsIgnoreCase(tipoArchivo)) {
                                isRecaudador = true;
                                break;
                            }
                        }

                        if (!isRecaudador) {
                            ret = false;
                        } else {
                            ret = true;
                        }


                    } else {
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            ret = false;
        }
        return ret;
    }

    /**
     * Carga el archivo en el servidor
     * segun el path de contexto en base a path real
     * @param file
     * @param convenioPagoDTO
     * @return
     * @throws Exception
     */
    private boolean doLoadFile(String filePM, ConvenioPagoDTO convenioPagoDTO,
                               File directorioArchivo) throws Exception {
        boolean ret = false;

        //Directorio Absoluto del Contenedor
        //String dirWebContent = JsfUtil.getServletContext().getRealPath("/");

        fileupload =
            dirWebContent + "rendicion" + File.separator + convenioPagoDTO.getDirectorio_upload() + File.separator +
            StringUtil.replaceString(filePM, ".", "_") + "_" + System.currentTimeMillis() + ".data";
        logger.debug("doLoadFile() - Path archivo creado: " + fileupload);

        //ruta del archivo del punto de montaje
        String rutaArchivoPM = directorioArchivo + File.separator + filePM;

        //leyendo bytes
        InputStream in = new URL("file://" + rutaArchivoPM).openStream();
        File archivoTmp = new File(fileupload);
        archivoTmp.createNewFile();
        FileOutputStream out = new FileOutputStream(archivoTmp);
        int data;
        while ((data = in.read()) != -1) {
            out.write(data);
        }
        in.close();
        out.close();
        ret = true;

        return ret;
    }

    /**
     * Finaliza el proceso
     * @return
     */
    public String doFinProcesoSendCorreoAction() {
        try {
            boolean problemasPorSolucionar = false;
            boolean rechazados = false;
            this.errorFinalizar = null;

            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getEstadoProcesoAutorizacion() == null ||
                        resumenAbonoProcesado.get(i).getEstadoProcesoAutorizacion().equals("W")) {
                        problemasPorSolucionar = true;
                    }
                    if (resumenAbonoProcesado.get(i).getEstadoProcesoAutorizacion() != null &&
                        resumenAbonoProcesado.get(i).getEstadoProcesoAutorizacion().equals("R")) {
                        rechazados = true;
                    }
                }
            }
            if (problemasPorSolucionar) {
                this.errorFinalizar = "Existen procesos sin aprobar o con observaci�n, debe aprobar antes de finalizar";
            } else {
                if (rechazados) {
                    this.errorFinalizar =
                        "Existen al menos 1 rechazo, debe aprobar todo para poder finalizar y guardar";
                }
            }


            if (this.errorFinalizar == null) {
                //GRABAR
                boolean result = true;
                boolean rechazado = false;
                // Verifica si fue rechazado
                for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                    if (resumenAbonoProcesado.get(i).getId_motivo_rechazo().intValue() > 0) {
                        rechazado = true;
                        break;
                    }
                }
                /*
                 * Actualiza estado dependiendo si se enci�uentra rechazado
                 */
                if (rechazado) {
                    ejb.updateCargaArchivosEstadoProceso(idCargaConsulta, Constant.PROCESO_FINALIZADO_CON_RECHAZOS,
                                                         null, null);
                } else {
                    ejb.updateCargaArchivosEstadoProceso(idCargaConsulta, Constant.PROCESO_FINALIZADO_AUTORIZADO, null,
                                                         null);
                }

                if (result) {
                    //Envio de correo
                    PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
                    CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCargaConsulta);
                    String estadoProcesoAbono = cargaArchivos.getDescripcion_estado_proceso();
                    String nombreArchivoAbono = cargaArchivos.getNombre_fisico_archivo();
                    String estadoProcesoPlanilla = "Pendiente";
                    String nombreArchivoPlanilla =
                        StringUtil.replaceString(cargaArchivos.getNombre_fisico_archivo(), "AA", "PL");
                    String detalle = EmailUtil.generateDetalleMail(resumenAbonoProcesado);
                    String to = null;
                    String from = null;
                    String fechaactual = FechaUtil.getFechaFormateoCustom(new Date(), "dd/MM/yyy HH:mm:ss");
                    String subject = "Rendiciones Previred " + fechaactual;

                    Boolean dummy =
                        Boolean.parseBoolean(JsfUtil.getMessage("configurationbice", "cl.bicevida.email.dummy"));
                    if (dummy) {
                        from = JsfUtil.getMessage("configurationbice", "cl.bicevida.email.dummy.from");
                        to = JsfUtil.getMessage("configurationbice", "cl.bicevida.email.dummy.to");
                    } else {
                        from = JsfUtil.getMessage("configurationbice", "cl.bicevida.email.from");
                        to = JsfUtil.getMessage("configurationbice", "cl.bicevida.email.to");
                    }

                    if (EmailUtil.enviarEmailWS(to, from, null, null, subject, nombreArchivoAbono, estadoProcesoAbono,
                                                estadoProcesoPlanilla, nombreArchivoPlanilla, detalle)) {
                        logger.info(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.email.enviado"));
                    } else {
                        this.errorFinalizar =
                            ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.email.problema");
                    }

                }
            }

        } catch (Exception e) {
            logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.exception"));
            logger.error("doProcesarAction() - Se ha producido un error: " + e.getMessage(), e);
        }

        return null;
    }


    /**
     * Metodo que obtiene el medio de pago de acuerdo a la glosa cortada proveniente del archivo
     * @param nombreArchivo
     * @return
     */
    private Integer doObtenerMedioDePagoByFile(String nombreArchivo) {
        String glosaCorta = nombreArchivo.substring(4, 7);
        Integer medioPago = null;
        List<MediosDePagoDTO> listmedioPago = ejb.getMediosDePago(null);
        for (MediosDePagoDTO medio : listmedioPago) {

            if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //BCH (1)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //CAG (2)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //SPG (3)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //BCI (4)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //BBC (5)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //CPR (6)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //BAA (7)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //BCO (8)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //PVR (9)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //LAN (10)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            } else if (glosaCorta.equals(medio.getTipoMedioPago())) {
                //ARA (12)
                medioPago = medio.getIdMedioPagoRendicion();
                break;
            }
        }
        return medioPago;
    }

    /**
     * Verifica el tipo de archivo, de acuerdo al nombre del archivo que se esta leyendo
     * @param nombreArchivo
     * @return
     */
    private Integer doVerificaTipoArchivo(String nombreArchivo) {
        String glosaArchivo = nombreArchivo.substring(0, 2);
        Integer tipoArchivo = null;
        if ("AA".equals(glosaArchivo)) {
            tipoArchivo = 1;
        } else {
            tipoArchivo = 2;
        }
        return tipoArchivo;
    }

    /**
     *
     * @param resumen
     */
    private boolean doAutorizaAbono(ResumenAbonoDTO resumen, String nombreArchivo) {
        Boolean isAutorizado = false;
        String glosaRecaudador = null;
        try {
            if (resumen != null) {
                logger.info("resumen.getCodigo_recaudador() " + resumen.getCodigo_recaudador());

                //String glosaRecaudador = ejb.getGlosaRecaudador(resumen.getCodigo_recaudador());
                /*List<String> glosaListRecaudador = ejb.getListGlosaRecaudador(resumen.getCodigo_recaudador(),resumen.getGlosa_recaudador());
                for(String s : glosaListRecaudador){
                    if(s != null){
                        if(s.equals(nombreArchivo.substring(4, 7))){
                            glosaRecaudador = s;
                            logger.info("glosaRecaudador " + glosaRecaudador);
                            break;
                        }
                    }
                }*/

                //logger.info("nombreArchivo.substring(4, 7) " + nombreArchivo.substring(4, 7));
                if (resumen.getCodigo_recaudador() != null) {
                    //if (glosaRecaudador.equals(nombreArchivo.substring(4, 7))) {
                    if (resumen.getTotalAbono() != null) {

                        Double montoInformado = new Double(resumen.getTotalAbono());
                        if (montoInformado > 0.0) {

                            resumen.setId_motivo_rechazo(0);
                            resumen.setObservacion_rechazo("");
                            resumen.setObservacion_autorizacion("");
                            resumen.setValidacion_cartola_cta_cte(1);
                            resumen.setTotal_monto_inf_cartola(montoInformado);
                            resumen.setEstado_proceso(Constant.PROCESO_ABONO_AUTORIZADO);
                            resumen.setEstado_proceso_excepcion("");

                            resumen.setBool_button_autobs(false);
                            resumen.setBool_button_rechazado(false);
                            resumen.setProcesadoAprobacion(true);

                            ejb = ServiceLocator.getSessionEJBPrevired();

                            EstadoDeProcesoDTO estadoDeProcesoDTO =
                                ejb.getEstadoDeProcesoByIdProceso(Constant.PROCESO_ABONO_AUTORIZADO);
                            if (estadoDeProcesoDTO != null) {
                                resumen.setDescripcion_estado_proceso(estadoDeProcesoDTO.getDecripcionEstado());
                                resumen.setMensajeSegunEstado("Autorizado");
                                resumen.setEstadoProcesoAutorizacion("A");
                            }
                            ejb.updateResumenYDetalleAbonoByIdCargaResumen(idCargaConsulta, resumen);
                            isAutorizado = true;

                        } else {
                            logger.error("Monto informado es inferior a cero");
                            resumen.setMensajeSegunEstado(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.montoinformado.menor"));
                            resumen.setEstadoProcesoAutorizacion("W");
                            isAutorizado = false;
                            throw new Exception("Monto informado es inferior a cero");

                        }
                    } else {
                        logger.error("No viene el total del abono en el detalle");
                        isAutorizado = false;
                        throw new Exception("No viene el total del abono en el detalle");
                    }
                    /*} else {
                        logger.error("El codigo del recaudador no es igual al que viene en el archivo");
                        isAutorizado = false;
                    }*/
                } else {
                    logger.error("No viene codigo recaudador");
                    isAutorizado = false;
                    throw new Exception("No viene codigo recaudador");
                }

            } else {
                logger.error("Detalle del abono no viene");
                isAutorizado = false;
                throw new Exception("Detalle del abono no viene");
            }
        } catch (Exception e) {
            logger.error("Ha ocurrido el siguiente error: " + e.getMessage());
            logger.error("No se pudo obtener el la glosa del recaudador");
            isAutorizado = false;
        }
        return isAutorizado;
    }

    public void doVolverAutorizadoObs(ActionEvent actionEvent) {
        try {
            String filacodrecaudador =
                ((CoreInputHidden) actionEvent.getComponent().getParent().getChildren().get(0)).getValue().toString();

            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getCodigo_recaudador() != null) {
                        if (resumenAbonoProcesado.get(i).getCodigo_recaudador().equalsIgnoreCase(filacodrecaudador)) {
                            resumenAbonoProcesado.get(i).setBool_button_autobs(false);
                            resumenAbonoProcesado.get(i).setId_motivo_rechazo(0);
                            resumenAbonoProcesado.get(i).setObservacion_rechazo("");
                            resumenAbonoProcesado.get(i).setObservacion_autorizacion("");
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.recaudacion.exception"));
            logger.error("doVolverAutorizadoObs() -Se ha producido una excepcion: " + e.getMessage(), e);
        }
    }

    public void setRenderpoll(boolean renderpoll) {
        this.renderpoll = renderpoll;
    }

    public boolean isRenderpoll() {
        return renderpoll;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setResumenAbonoProcesado(List<ResumenAbonoDTO> resumenAbonoProcesado) {
        this.resumenAbonoProcesado = resumenAbonoProcesado;
    }

    public List<ResumenAbonoDTO> getResumenAbonoProcesado() {
        return resumenAbonoProcesado;
    }

    public void setShowMensajeEspera(Boolean showMensajeEspera) {
        this.showMensajeEspera = showMensajeEspera;
    }

    public Boolean getShowMensajeEspera() {
        return showMensajeEspera;
    }

    public void setShowDetailPlanilla(Boolean showDetailPlanilla) {
        this.showDetailPlanilla = showDetailPlanilla;
    }

    public Boolean getShowDetailPlanilla() {
        return showDetailPlanilla;
    }

    public void setPlanillaNoInformados(List<ResumenAbonoDTO> planillaNoInformados) {
        this.planillaNoInformados = planillaNoInformados;
    }

    public List<ResumenAbonoDTO> getPlanillaNoInformados() {
        return planillaNoInformados;
    }

    public void setShowDescarga(Boolean showDescarga) {
        this.showDescarga = showDescarga;
    }

    public Boolean getShowDescarga() {
        return showDescarga;
    }

    public void setPathAbsoluteExcel(String pathAbsoluteExcel) {
        this.pathAbsoluteExcel = pathAbsoluteExcel;
    }

    public String getPathAbsoluteExcel() {
        return pathAbsoluteExcel;
    }

    public void setTipopl(String tipopl) {
        this.tipopl = tipopl;
    }

    public String getTipopl() {
        return tipopl;
    }

    public void setErrorFinalizar(String errorFinalizar) {
        this.errorFinalizar = errorFinalizar;
    }

    public String getErrorFinalizar() {
        return errorFinalizar;
    }

    public void procesoManual() {
        trigger.manualAction();
    }

    private void cargaNombreServer() {

        InetAddress address;
        try {
            address = InetAddress.getLocalHost();
            this.nombreServidor = address.getHostName();
        } catch (UnknownHostException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

}

