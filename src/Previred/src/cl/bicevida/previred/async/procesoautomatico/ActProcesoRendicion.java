package cl.bicevida.previred.async.procesoautomatico;

import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;

import javax.naming.NamingException;

public class ActProcesoRendicion {
    
    
    private PreviredEJB ejb;
    
    public ActProcesoRendicion() {
        super();
    }
    
    public void actualizarProceso(Integer estado,String descripcionEstado,Long idCarga,String observacion ){
        ejb = ServiceLocator.getSessionEJBPrevired();

        try {
            ejb.actualizaProcesoRendicion(estado, descripcionEstado, idCarga, observacion);
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
