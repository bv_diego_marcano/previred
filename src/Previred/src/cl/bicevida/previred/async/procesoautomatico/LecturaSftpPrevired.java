package cl.bicevida.previred.async.procesoautomatico;

import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.dao.RendicionDAO;
import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;
import cl.bicevida.previred.pattern.dao.DAOFactory;
import cl.bicevida.previred.pattern.dao.OracleDAOFactory;
import cl.bicevida.previred.util.FileUtil;
import cl.bicevida.previred.util.ResourcesAsyncUtil;
import cl.bicevida.previred.util.SftpUtil;

import com.jcraft.jsch.JSchException;

import java.io.File;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.persistence.internal.helper.SimpleDatabaseType;

/**
 * GR 03-07-2018: Clase para leer los archivos del sftp de previred y dejarlos en punto de montaje de WL12
 */
public class LecturaSftpPrevired {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(LecturaSftpPrevired.class);

    private final static String username = ResourcesAsyncUtil.getProperty("cl.bicevida.sftp.previred.username");
    private final static String password = ResourcesAsyncUtil.getProperty("cl.bicevida.sftp.previred.password");
    private final static String host = ResourcesAsyncUtil.getProperty("cl.bicevida.sftp.previred.host");
    private final static Integer port = Integer.parseInt(ResourcesAsyncUtil.getProperty("cl.bicevida.sftp.previred.port"));
    private final static String directorioSFTP = ResourcesAsyncUtil.getProperty("cl.bicevida.sftp.previred.directorio");
    private final static String directorioEscrituraPM = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.previred");
    //private final static String directorioEscrituraPM = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.no.procesados");
    private final static String directorioEscrituraPorProcesarPM = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.por.procesar");
    
    private PreviredEJB ejb;

    public LecturaSftpPrevired() {
        super();
    }

    /**
     * Metodo encargado de conectarse, leer y escribir en punto de montaje ubicado en weblogic
     */
    public void doProcesarSFTP() {

        try {
            ejb = ServiceLocator.getSessionEJBPrevired();
            String fechaPeriodoActual = FechaUtil.getFechaFormateoCustom(new Date(), "yyyyMM");
            
            Date fechaValida = null;
            Date primerDiaMes  = FechaUtil.getFistDayOfMonth();
            // valida el ultimo dia del periodo actual
            // 10 dias porque el contador parte de 0
            for (int i = 0; i <= 16; i++){
                fechaValida = ejb.retornaDiaHabil(primerDiaMes);
                primerDiaMes = FechaUtil.sumarDiasFecha(fechaValida, 1);
            }
            Integer diaLimite = FechaUtil.getDiaFecha(fechaValida);
            logger.info("Dia limite " + diaLimite);
            //String strDiaLimite = diaLimite < 10 ? "0" + diaLimite : String.valueOf(diaLimite); no se utiliza por ahora
            
            logger.info("conectandose al SFTP de Previred");
            SftpUtil.connect(username, password, host, port);

            logger.info("Directorio de lectura [" + directorioSFTP  + fechaPeriodoActual + "]");
            //Metodo retorna una lista de string, con los nombres de los archivos.
            SftpUtil.leeYEscribeArchivoSFTP(directorioSFTP + fechaPeriodoActual, directorioEscrituraPM,diaLimite,directorioEscrituraPorProcesarPM,ejb);
            SftpUtil.disconnect();
            
        } catch (IllegalAccessException e) {
            SftpUtil.disconnect();
            logger.error("Ha ocurrido un error [" + e.getMessage() + "]");
        } catch (JSchException e) {
            SftpUtil.disconnect();
            logger.error("Ha ocurrido un error [" + e.getMessage() + "]");
        } catch (Exception e) {
            logger.error("Ha ocurrido un error [" + e.getMessage() + "]");
        }
    }
}
