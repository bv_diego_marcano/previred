package cl.bicevida.previred.view.vo;

import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.common.dto.ArchivoPlanillaHeaderDTO;
import cl.bicevida.previred.common.dto.CargaArchivosDTO;
import cl.bicevida.previred.common.dto.EstadoDeProcesoDTO;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;
import cl.bicevida.previred.core.abono.PreviredCore;
import cl.bicevida.previred.core.vo.ResultadoProcesoVO;
import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;
import cl.bicevida.previred.common.dto.InformarRecaudacionIn;

//import cl.bicevida.previred.proxy.RecaudacionServiceClient;
import cl.bicevida.previred.services.svi.InformarRecaudacion;
import cl.bicevida.previred.util.ResourcesAsyncUtil;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;


public class ProcesoRendicionVO {
    /**
     * Logger for this class
     */
    private transient org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ProcesoRendicionVO.class);
   
    public ProcesoRendicionVO() {                       
    }
  
    public boolean doProcesarAbono(Double idCarga, String pathArchivoRendicion, PreviredCore core)throws Exception{
        logger.debug("doProcesarAbono() - Inicio");
        boolean resp = false;
        Integer estadoProceso = 0;
        
        try{
            /*
             * Obtiene el estado de la carga para iniciar proceso
             */   
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCarga);
            // Si el id de carga esta pendiente se procesa, en caso contrario no se realiza el proceso
            if(cargaArchivos != null && cargaArchivos.getEstado_proceso().intValue() == Constant.PROCESO_PENDIENTE.intValue()){
                // Se cambia el estado de la carga en la base de datos y se actualiza la fecha de inicio del proceso
                ejb.updateCargaArchivosInicioProceso(idCarga);
                if(ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_LECTURA_ARCHIVO, null, null)){
                    /*
                     * Ontencion de los datos del archivo
                     */
                    ResultadoProcesoVO resultadoProcesoVO = core.doProcesar(pathArchivoRendicion, "ABONO", idCarga);
                    // Si los datos obtenidos son correctos se continua el proceso
                    if(resultadoProcesoVO != null && resultadoProcesoVO.isOk() && resultadoProcesoVO.getLst_resumen_abono() != null && resultadoProcesoVO.getLst_resumen_abono().size()>0){
                        // se itera la cantidad de abonos del archivo
                        Iterator lstAbono =  resultadoProcesoVO.getLst_resumen_abono().iterator();
                        int cantidadResumenProcesada = 0;
                        while(lstAbono.hasNext()){
                            /*
                             * Carga en la base de datos del resumen 
                             */
                            ResumenAbonoDTO resumenAbono = (ResumenAbonoDTO)lstAbono.next();
                            resumenAbono.setProcesado(false);
                            if(resumenAbono != null && resumenAbono.getArchivoabono() != null && resumenAbono.getArchivoabono().size() > 0){
                                ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_CARGA_BASEDATOS, null, null);
                                resumenAbono = ejb.insertResumenAbono(resumenAbono, idCarga);
                                // Si el resultado de isProcesado()  es verdadero se continual con el detalle del abono
                                if(resumenAbono.isProcesado()){
                                    cantidadResumenProcesada++;
                                    if(resumenAbono.getArchivoabono().size() > 0){                                        
                                        if(resumenAbono.getArchivoabono().size() == resumenAbono.getNumero_de_planillas().intValue()){
                                        
                                            int cantidadDetalleProcesada = 0;                                            
                                            for(int i = 0; i < resumenAbono.getArchivoabono().size(); i++ ) {
                                                resumenAbono.setProceso_detalle(false);
                                                resumenAbono = ejb.insertDetalleAbono(resumenAbono, idCarga, i, 100,null);
                                                if(resumenAbono.isProceso_detalle()){
                                                    i = resumenAbono.getNumero_detalle_procesados().intValue();
                                                    cantidadDetalleProcesada = i+1;
                                                    logger.debug("doProcesarAbono() - Detalles de Abonos procesados: " + cantidadDetalleProcesada);
                                                }else{
                                                    logger.error("doProcesarAbono() - Problema en la carga del detalle del abono, no pudo ser procesado en la Base de Datos");
                                                    estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                                    throw new Exception("Problema en la carga del detalle del abono, no pudo ser procesado en la Base de Datos");
                                                }
                                            }
                                            
                                            if(resumenAbono.getArchivoabono().size() != cantidadDetalleProcesada && resumenAbono.isProceso_detalle()){
                                                logger.error("doProcesarAbono() - Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                                ejb.updateCargaArchivosCantidadProcesada(idCarga, cantidadDetalleProcesada);
                                                estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                                throw new Exception("Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");
                                            }                                      
                                        }else{
                                            logger.error("doProcesarAbono() - Problema en la carga del detalle del abono, la cantidad informada no coincide con la cantidad a procesar");
                                            estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                            throw new Exception("Problema en la carga del detalle del abono, la cantidad informada no coincide con la cantidad a procesar"); 
                                        }
                                    }else{
                                        logger.error("doProcesarAbono() - Problema en la carga del detalle del abono, no posee datos");
                                        estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                        throw new Exception("Problema en la carga del detalle del abono, no posee datos");
                                    }
                                }else{
                                    logger.error("doProcesarAbono() - Problema en la carga del resumen del abono, no pudo ser procesado en la Base de Datos");
                                    estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                    throw new Exception("Problema en la carga del resumen del abono, no pudo ser procesado en la Base de Datos");                                     
                                }
                            }else{
                                logger.error("doProcesarAbono() - Problema en la carga del resumen del abono, no posee datos");   
                                estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                throw new Exception("Problema en la carga del resumen del abono, no posee datos");
                            }
                        }
                        
                        if(resultadoProcesoVO.getLst_resumen_abono().size() != cantidadResumenProcesada){
                            logger.error("doProcesarAbono() - Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");
                            estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                            throw new Exception("Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");                            
                        }else{                            
                            ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_FINALIZADO_CARGA, null, null);
                            ejb.updateCargaArchivosTerminoProceso(idCarga);
                            ejb.updateCargaArchivosCantidadProcesada(idCarga, resultadoProcesoVO.getLst_resumen_abono().size());
                            resp = true;
                        }
                    }else{
                        logger.error("doProcesarAbono() - Problema en la carga del abono, el archivo no se ley� correctamente");
                        estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                        throw new Exception("Problema en la carga del abono, el archivo no se ley� correctamente");                        
                    }
                }
            }//else{
                    // No realiza nada
                //}
        } catch(Exception e){
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            ejb.updateCargaArchivosEstadoProceso(idCarga, estadoProceso, "Problema en la carga del resumen y del detalle del abono, se produjo un excepcion: " + e.getMessage(), null);
            /*ejb.deleteDetalleAbono(idCarga);
            ejb.deleteResumenAbono(idCarga);
            ejb.deleteCargaArchivo(idCarga);*/
            logger.error("doProcesarAbono() - Problema en la carga del resumen y del detalle del abono, se produjo un exepcion: " + e.getMessage(), e);            
            throw new Exception("Problema en la carga: " + e.getMessage());
        }
        logger.debug("doProcesarAbono() - Temino");
        return resp;
    }
    
    
    public boolean doProcesarPlanilla(Double idCarga, String pathArchivoRendicion, PreviredCore core)throws Exception {
        logger.debug("doProcesarPlanilla() - Inicio");
        boolean resp = false;
        Integer estadoProceso = 0;
        Long turno = null;
        try {
            /*
             * Obtiene el estado de la carga para iniciar proceso
             */
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();

            CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCarga);
            // Si el id de carga esta pendiente se procesa, en caso contrario no se realiza el proceso
            if (cargaArchivos != null && cargaArchivos.getEstado_proceso().intValue() == Constant.PROCESO_PENDIENTE.intValue()) {
                turno = ejb.callNewAperturaTurnoCaja(Constant.PLANILLA_PREVIRED);

                java.util.Date fechaTurno = ejb.getFechaHoraTurno(turno);
                Long foliocajadiario = ejb.getFolioCajaByTurno(fechaTurno);

                logger.info("id turno obtenido: " + turno);
                logger.info("fecha de turno caja: " + fechaTurno);
                logger.info("folio de caja diario: " + foliocajadiario);


                //BUSCAR DATOS ADICIONALES

                // Se cambia el estado de la carga en la base de datos y se actualiza la fecha de inicio del proceso
                ejb.updateCargaArchivosInicioProceso(idCarga);
                if (ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_LECTURA_ARCHIVO, null, null)) {
                    /*
                     * Obtencion de los datos del archivo
                     */
                    ResultadoProcesoVO resultadoProcesoVO = core.doProcesar(pathArchivoRendicion, "PLANILLA", idCarga);
                    // Si los datos obtenidos son correctos se continua el proceso
                    if (resultadoProcesoVO != null && resultadoProcesoVO.isOk() && resultadoProcesoVO.getLst_header_planilla() != null &&
                        resultadoProcesoVO.getLst_header_planilla().size() > 0) {
                        Iterator lstPlanilla = resultadoProcesoVO.getLst_header_planilla().iterator();


                        while (lstPlanilla.hasNext()) {
                            /*
                             * Carga en la base de datos de la planilla
                             */
                            ArchivoPlanillaHeaderDTO planillaHeader = (ArchivoPlanillaHeaderDTO) lstPlanilla.next();
                            //Verifica si posee datos
                            if (planillaHeader != null && planillaHeader.getLst_detalle_planilla() != null && planillaHeader.getLst_detalle_planilla().size() > 0) {
                                ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_CARGA_BASEDATOS, null, null);

                                int cantidadDetalleProcesada = 0;
                                for (int i = 0; i < planillaHeader.getLst_detalle_planilla().size(); i++) {
                                    planillaHeader.setProceso_detalle(false);


                                    //====================================================
                                    // PAGO EN CAJA
                                    // CONJUNTO CON INSERTAR ESTE PROCESO PAGA EN CAJA
                                    //====================================================
                                    planillaHeader =
                                        ejb.insertArchivoPlanillaProceso(turno, fechaTurno, foliocajadiario, planillaHeader, idCarga, i, 25, Constant.PLANILLA_PREVIRED);
                                    if (planillaHeader.isProceso_detalle()) {
                                        i = planillaHeader.getNumero_detalle_procesados();
                                        cantidadDetalleProcesada = i + 1;

                                        logger.debug("doProcesarPlanilla() - Detalles de Planilla procesados: " + cantidadDetalleProcesada);
                                    } else {
                                        logger.error("doProcesarPlanilla() - Problema en la carga del detalle de la planilla, no pudo ser procesado en la Base de Datos");
                                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                        throw new Exception("Problema en la carga del detalle de la planilla, no pudo ser procesado en la Base de Datos");
                                    }
                                }


                                if (planillaHeader.getLst_detalle_planilla().size() == cantidadDetalleProcesada && planillaHeader.isProceso_detalle()) {
                                    /*if(planillaHeader.getIdCargaAbono() != null){
                                        ejb.updateEstadoPorcesoFolioResumenByIdCarga(planillaHeader.getIdCargaAbono(), Constant.PROCESO_PENDIENTE, Constant.PROCESO_PROBLEMA_PLANILLA_ABONO, "El folio abono no se encuentra en el detalle");
                                    }*/


                                    ejb.updateCargaArchivosEstadoProceso(planillaHeader.getIdCargaAbono(), Constant.PROCESO_FINALIZADO_, null, null);
                                    ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_FINALIZADO_, null, planillaHeader.getIdCargaAbono());
                                    ejb.updateCargaArchivosTerminoProceso(idCarga);
                                    ejb.updateCargaArchivosCantidadProcesada(idCarga, planillaHeader.getLst_detalle_planilla().size());
                                    ejb.updateFolioAbonoConDatosNoProcesados(planillaHeader.getIdCargaAbono(), Constant.PROCESO_PROBLEMA_RESUMEN_ABONO,
                                                                             "El folio no fue procesado, pues no se encontraba en el archivo de planilla");
                                    resp = true;
                                } else {
                                    logger.error("doProcesarAbono() - Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                    ejb.updateCargaArchivosCantidadProcesada(idCarga, cantidadDetalleProcesada);
                                    estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                    throw new Exception("Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                }
                            } else {
                                logger.error("doProcesarPlanilla() - Problema en la carga del resumen del abono, no posee datos");
                                estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                throw new Exception("Problema en la carga del resumen de la planilla, no posee datos");
                            }
                        } // TERMINO WHILE


                        //TODO:NOTIFICAR A CAJA
                        //INFORMAR A AR MEDIANTE WS
                        // ============ LLAMA A WEBSERVICE PARA NOTIFICACION DE TERMINO =========
                        try {
                            logger.info("NOTIFICACION AR VIA : PREVIRED  - INICIANDO....CASO PLANILLA");
                            //Notifica Webservices de Recaudacion
                            // RecaudacionServiceClient myPort = new RecaudacionServiceClient();
                            // myPort.setEndpoint(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.endpoint"));
                            InformarRecaudacionIn in = new InformarRecaudacionIn();

                            in.setCallerSystem(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.callersystem"));
                            in.setCallerUser(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.calleruser"));

                            //RECUPERA EL FOLIO DE CAJA DIARIO
                            in.setFolioCajaDiario(foliocajadiario); //SELECT
                            in.setFolioPago(null);
                            Calendar cale = new GregorianCalendar();
                            cale.setTime(fechaTurno);
                            in.setTurno(cale);
                            in.setIdTurno(turno); //numero turno
                            in.setUsuarioCaja(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.usuariocaja"));
                            logger.info("NOTIFICANDO USUARIO CAJA PLANILLA:" + in.getUsuarioCaja());
                            logger.info("NOTIFICANDO TURNO:" + in.getTurno());
                            logger.info("NOTIFICANDO FOLIO CAJA DIARIO:" + in.getFolioCajaDiario());
                            logger.info("NOTIFICANDO FOLIO PAGO:" + in.getFolioPago());
                            logger.info("NOTIFICANDO ID TURNO:" + in.getIdTurno());
                            //logger.info("NOTIFICANDO ENDPOINT:" +myPort.getEndpoint());
                            //logger.info("NOTIFICACION AR VIA WS : OK  - ID TURNO CAJA PREVIRED:" );
                            logger.info("NOTIFICACION --> Ejecutamos distribucion en caja y creamos concurrente");
                            ejb.ejecutarDistribucionEnCajayConcurrente(in);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {
                        if (resultadoProcesoVO != null) {
                            /*ejb.deleteDetalleAbono(idCarga);
                            ejb.deleteResumenAbono(idCarga);
                            ejb.deleteCargaArchivo(idCarga);
                            ejb.deleteProcesoAutomatico(idCarga);*/
                            logger.error("doProcesarPlanilla() - Problema en la carga del abono, el archivo no se ley� correctamente");
                            estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                            throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente" + resultadoProcesoVO.getError());
                        } else {
                            /*ejb.deleteDetalleAbono(idCarga);
                            ejb.deleteResumenAbono(idCarga);
                            ejb.deleteCargaArchivo(idCarga);
                            ejb.deleteProcesoAutomatico(idCarga);*/
                            logger.error("doProcesarPlanilla() - Problema en la carga del abono, el archivo no se ley� correctamente");
                            estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                            throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente");
                        }
                    }
                }

                ejb.callNewCierreTurnoCaja(Constant.PLANILLA_PREVIRED, turno);

            } //else{
            // No realiza nada
            //}


        } catch (Exception e) {
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            /*ejb.deletePlanillaAbono(idCarga);
            ejb.deleteCargaArchivo(idCarga);
            ejb.deleteProcesoAutomatico(idCarga);*/
            ejb.updateCargaArchivosEstadoProceso(idCarga, estadoProceso, "Problema en la carga de la planilla, se produjo un excepcion: " + e.getMessage(), null);
            if (turno != null)
                ejb.callNewCierreTurnoCaja(Constant.PLANILLA_PREVIRED, turno);
            logger.error("doProcesarPlanilla() - Problema en la carga de la planilla, se produjo un excepcion: " + e.getMessage(), e);
            throw new Exception("Problema en la carga: " + e.getMessage());

        }
        return resp;
    }

    
    
    public boolean doProcesarAbonoPlanillaAndes(Double idCarga, String pathArchivoRendicion, PreviredCore core, Double idCargaAbono)throws Exception{
        logger.debug("doProcesarAbonoPlanillaAndes() - Inicio");
        boolean resp = false;
        Integer estadoProceso = 0;
        Long turno = null;
        try{
        
            /*
             * Obtiene el estado de la carga para iniciar proceso
             */
            logger.debug("doProcesarAbonoPlanillaAndes() - Conectando a EJB");
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            
            logger.debug("doProcesarAbonoPlanillaAndes() - Obteniedo carga de idcarga: " + idCarga);
            CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCarga);
            // Si el id de carga esta pendiente se procesa, en caso contrario no se realiza el proceso
             logger.debug("doProcesarAbonoPlanillaAndes() - Verificando estado pendiente");
            if(cargaArchivos != null && cargaArchivos.getEstado_proceso().intValue() == Constant.PROCESO_PENDIENTE.intValue()){
                logger.debug("doProcesarAbonoPlanillaAndes() - Estado Pendiente");
                
                //abre caja
                turno = ejb.callNewAperturaTurnoCaja(Constant.PLANILLA_ANDES);
                
                java.util.Date fechaTurno = ejb.getFechaHoraTurno(turno);
                Long foliocajadiario = ejb.getFolioCajaByTurno(fechaTurno);
                
                logger.info("id turno obtenido: " + turno);
                logger.info("fecha de turno caja: " + fechaTurno);
                logger.info("folio de caja diario: " + foliocajadiario);
                
                
                logger.debug("doProcesarAbonoPlanillaAndes() - Turno abierto");
                // Se cambia el estado de la carga en la base de datos y se actualiza la fecha de inicio del proceso
                ejb.updateCargaArchivosInicioProceso(idCarga);
                logger.debug("doProcesarAbonoPlanillaAndes() - Update a proceso de carga");
                if(ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_LECTURA_ARCHIVO, null, null)){                
                    logger.debug("doProcesarAbonoPlanillaAndes() - Update a estado dl proceso de carga");
                    /*
                     * Obtencion de los datos del archivo
                     */
                    String tipoplanilla = ""; 
                    if(cargaArchivos.getConvenio().getNumero_de_convenio().equalsIgnoreCase(Constant.PLANILLA_ANDES_CONVENIO_MANUAL.toString())){                        
                        tipoplanilla = "PLANILLAANDESMANUAL";
                        logger.debug("doProcesarAbonoPlanillaAndes() - Tipo Planilla " +tipoplanilla);
                    }else if (cargaArchivos.getConvenio().getNumero_de_convenio().equalsIgnoreCase(Constant.PLANILLA_ANDES_CONVENIO.toString())){
                        tipoplanilla = "PLANILLAANDES";
                        logger.debug("doProcesarAbonoPlanillaAndes() - Tipo Planilla " +tipoplanilla);
                    }else{
                        throw new Exception("Problema en la carga al obtener el tipo de planilla Andes a procesar");
                    }
                    
                    logger.debug("doProcesarAbonoPlanillaAndes() - Comienza lectura de Archivo");
                    ResultadoProcesoVO resultadoProcesoVO = core.doProcesar(pathArchivoRendicion, tipoplanilla, idCarga);
                    logger.debug("doProcesarAbonoPlanillaAndes() - Termino lectura de Archivo");
                    /*
                     * Procesar Abono
                     */
                    if(doProcesarAbonoAndes(idCargaAbono, resultadoProcesoVO.getLst_resumen_abono()) ){
                        // Si los datos obtenidos son correctos se continua el proceso
                        if(resultadoProcesoVO != null && resultadoProcesoVO.isOk() && resultadoProcesoVO.getLst_header_planilla() != null && resultadoProcesoVO.getLst_header_planilla().size()>0){
                            Iterator lstPlanilla =  resultadoProcesoVO.getLst_header_planilla().iterator();
                            while(lstPlanilla.hasNext()){
                                /*
                                 * Carga en la base de datos de la planilla 
                                 */
                                ArchivoPlanillaHeaderDTO planillaHeader = (ArchivoPlanillaHeaderDTO)lstPlanilla.next();
                                //Verifica si posee datos
                                if(planillaHeader != null && planillaHeader.getLst_detalle_planilla() != null && planillaHeader.getLst_detalle_planilla().size() > 0){
                                    ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_CARGA_BASEDATOS, null, null);
                                    
                                    int cantidadDetalleProcesada = 0;
                                    for(int i = 0; i < planillaHeader.getLst_detalle_planilla().size(); i++ ){
                                        planillaHeader.setProceso_detalle(false);
                                        
                                        //INSERTA REGISTRO EN DB 
                                        //PAGA EN SISTEMA DE CAJAS
                                        planillaHeader = ejb.insertArchivoPlanillaProceso(turno, fechaTurno, foliocajadiario, planillaHeader, idCarga, i, 25, Constant.PLANILLA_ANDES);
                                        
                                         
                                        
                                        if(planillaHeader.isProceso_detalle()){
                                            i = planillaHeader.getNumero_detalle_procesados();
                                            cantidadDetalleProcesada = i+1;
                                            logger.debug("doProcesarAbonoPlanillaAndes() - Detalles de Planilla procesados: " + cantidadDetalleProcesada);
                                        }else{
                                            logger.error("doProcesarAbonoPlanillaAndes() - Problema en la carga del detalle de la planilla, no pudo ser procesado en la Base de Datos");
                                            estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                            throw new Exception("Problema en la carga del detalle de la planilla, no pudo ser procesado en la Base de Datos");
                                        }
                                    }
                                    
                                    if( planillaHeader.getLst_detalle_planilla().size() == cantidadDetalleProcesada && planillaHeader.isProceso_detalle() ) {
                                        /*if(planillaHeader.getIdCargaAbono() != null){
                                            ejb.updateEstadoPorcesoFolioResumenByIdCarga(planillaHeader.getIdCargaAbono(), Constant.PROCESO_PENDIENTE, Constant.PROCESO_PROBLEMA_PLANILLA_ABONO, "El folio abono no se encuentra en el detalle");    
                                        }*/
                                        
                                        ejb.updateCargaArchivosEstadoProceso(planillaHeader.getIdCargaAbono(), Constant.PROCESO_FINALIZADO_, null, null);
                                        ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_FINALIZADO_, null, planillaHeader.getIdCargaAbono());
                                        ejb.updateCargaArchivosTerminoProceso(idCarga);
                                        ejb.updateCargaArchivosCantidadProcesada(idCarga, planillaHeader.getLst_detalle_planilla().size());
                                        ejb.updateFolioAbonoConDatosNoProcesados(planillaHeader.getIdCargaAbono(), Constant.PROCESO_PROBLEMA_RESUMEN_ABONO, "El folio no fue procesado, pues no se encontraba en el archivo de planilla");
                                        ejb.actualizaProcesoRendicion(Constant.FINALIZADO,Constant.DESC_FINALIZADO,idCarga.longValue(),"OK");
                                        resp = true;
                                    }else{
                                        logger.error("doProcesarAbonoPlanillaAndes() - Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                        ejb.updateCargaArchivosCantidadProcesada(idCarga, cantidadDetalleProcesada);
                                        ejb.deleteProcesoAutomatico(idCarga);
                                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                        throw new Exception("Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                    }                            
                                }else{                    
                                    ejb.deleteProcesoAutomatico(idCarga);
                                    logger.error("doProcesarAbonoPlanillaAndes() - Problema en la carga del resumen del abono, no posee datos");
                                    estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                    throw new Exception("Problema en la carga del resumen de la planilla, no posee datos");                                
                                }
                            } //END WHILE
                            
                            
                            //TODO:NOTIFICAR A CAJA
                            //INFORMAR A AR MEDIANTE WS
                            // ============ LLAMA A WEBSERVICE PARA NOTIFICACION DE TERMINO =========
                            try {
                                  logger.info("NOTIFICACION AR : PREVIRED  - INICIANDO....CASO PLANILLA" );
                                  //Notifica Webservices de Recaudacion
                                 // RecaudacionServiceClient myPort = new RecaudacionServiceClient();
                                //  myPort.setEndpoint(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.endpoint"));
                                  InformarRecaudacionIn in = new InformarRecaudacionIn();
                                  in.setCallerSystem(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.callersystem"));
                                  in.setCallerUser(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.calleruser"));
                                  
                                  //RECUPERA EL FOLIO DE CAJA DIARIO
                                  in.setFolioCajaDiario(foliocajadiario);//SELECT
                                  in.setFolioPago(null);
                                  Calendar cale = new GregorianCalendar();
                                  cale.setTime(fechaTurno);
                                  in.setTurno(cale);
                                  in.setIdTurno(turno); //numero turno
                                  in.setUsuarioCaja(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.usuariocaja"));
                                  logger.info("NOTIFICANDO USUARIO CAJA PLANILLA:" +in.getUsuarioCaja());
                                  logger.info("NOTIFICANDO TURNO:" +in.getTurno());
                                  logger.info("NOTIFICANDO FOLIO CAJA DIARIO:" +in.getFolioCajaDiario());
                                  logger.info("NOTIFICANDO FOLIO PAGO:" +in.getFolioPago());
                                  logger.info("NOTIFICANDO ID TURNO:" +in.getIdTurno());
                                  //logger.info("NOTIFICANDO ENDPOINT:" +myPort.getEndpoint());
                                  //myPort.informarRecaudacion(in);
                                  //logger.info("NOTIFICACION AR VIA WS : OK  - ID TURNO CAJA PREVIRED:" );
                                  logger.info("NOTIFICACION --> Ejecutamos distribucion en caja y creamos concurrente");
                                  ejb.ejecutarDistribucionEnCajayConcurrente(in); 
                                
                              } catch (Exception e) {
                                e.printStackTrace();
                              }
                            
                        }else{
                            if(resultadoProcesoVO != null){
                                ejb.deleteProcesoAutomatico(idCarga);
                                logger.error("doProcesarAbonoPlanillaAndes() - Problema en la carga del abono, el archivo no se ley� correctamente");
                                estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente" + resultadoProcesoVO.getError());          
                            }else{
                                ejb.deleteProcesoAutomatico(idCarga);
                                logger.error("doProcesarAbonoPlanillaAndes() - Problema en la carga del abono, el archivo no se ley� correctamente");
                                estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente");
                            }
                        }
                    }else{
                        ejb.deleteProcesoAutomatico(idCarga);
                        //ejb.deleteCargaArchivo(idCarga);
                        logger.error("doProcesarAbonoPlanillaAndes() - Problema en la carga del abono, el archivo no se ley� correctamente");
                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                        throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente");
                    }
                        
                    ejb.callNewCierreTurnoCaja(Constant.PLANILLA_ANDES, turno);
                }
            }//else{
                // No realiza nada
            //}
            
            
        } catch(Exception e){
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            ejb.updateCargaArchivosEstadoProceso(idCarga, estadoProceso, "Problema en la carga de la planilla, se produjo un excepcion: " + e.getMessage(), null);
            //ejb.deletePlanillaAbono(idCarga);
            //ejb.deleteCargaArchivo(idCarga);
            ejb.deleteProcesoAutomatico(idCarga);
            if (turno != null) ejb.callNewCierreTurnoCaja(Constant.PLANILLA_ANDES, turno);
            logger.error("doProcesarAbonoPlanillaAndes() - Problema en la carga de la planilla, se produjo un excepcion: " + e.getMessage(), e);
            throw new Exception("Problema en la carga: " + e.getMessage());
        }
        return resp;
    }
  
    
    
    public boolean doProcesarAbonoAndes(Double idCarga, List<ResumenAbonoDTO> lstResumenAbonoDTO){
        logger.debug("doProcesarAbonoAndes() - Inicio");
        boolean resp = false;
        Integer estadoProceso = 0;
        
        try{
            /*
             * Obtiene el estado de la carga para iniciar proceso
             */   
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCarga);
            // Si el id de carga esta pendiente se procesa, en caso contrario no se realiza el proceso
            if(cargaArchivos != null && cargaArchivos.getEstado_proceso().intValue() == Constant.PROCESO_PENDIENTE.intValue()){
                // Se cambia el estado de la carga en la base de datos y se actualiza la fecha de inicio del proceso
                ejb.updateCargaArchivosInicioProceso(idCarga);
                if(ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_LECTURA_ARCHIVO, null, null)){
                    
                    if(lstResumenAbonoDTO != null && lstResumenAbonoDTO.size()>0){
                        // se itera la cantidad de abonos del archivo
                        Iterator lstAbono =  lstResumenAbonoDTO.iterator();
                        int cantidadResumenProcesada = 0;
                        while(lstAbono.hasNext()){
                            /*
                             * Carga en la base de datos del resumen 
                             */
                            ResumenAbonoDTO resumenAbono = (ResumenAbonoDTO)lstAbono.next();
                            resumenAbono.setProcesado(false);
                            if(resumenAbono != null && resumenAbono.getArchivoabono() != null && resumenAbono.getArchivoabono().size() > 0){
                                ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_CARGA_BASEDATOS, null, null);
                                resumenAbono = ejb.insertResumenAbono(resumenAbono, idCarga);
                                // Si el resultado de isProcesado()  es verdadero se continual con el detalle del abono
                                if(resumenAbono.isProcesado()){
                                    cantidadResumenProcesada++;
                                    if(resumenAbono.getArchivoabono().size() > 0){                                        
                                        if(resumenAbono.getArchivoabono().size() == resumenAbono.getNumero_de_planillas().intValue()){
                                        
                                            int cantidadDetalleProcesada = 0;                                            
                                            for(int i = 0; i < resumenAbono.getArchivoabono().size(); i++ ) {
                                                resumenAbono.setProceso_detalle(false);
                                                resumenAbono = ejb.insertDetalleAbono(resumenAbono, idCarga, i, 100,null);
                                                if(resumenAbono.isProceso_detalle()){
                                                    i = resumenAbono.getNumero_detalle_procesados().intValue();
                                                    cantidadDetalleProcesada = i+1;
                                                    logger.debug("doProcesarAbonoAndes() - Detalles de Abonos procesados: " + cantidadDetalleProcesada);
                                                }else{
                                                    logger.error("doProcesarAbonoAndes() - Problema en la carga del detalle del abono, no pudo ser procesado en la Base de Datos");
                                                    estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                                    throw new Exception("Problema en la carga del detalle del abono, no pudo ser procesado en la Base de Datos");
                                                }
                                            }
                                            
                                            if(resumenAbono.getArchivoabono().size() != cantidadDetalleProcesada && resumenAbono.isProceso_detalle()){
                                                logger.error("doProcesarAbonoAndes() - Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                                ejb.updateCargaArchivosCantidadProcesada(idCarga, cantidadDetalleProcesada);
                                                estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                                throw new Exception("Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");
                                            }                                      
                                        }else{
                                            logger.error("doProcesarAbonoAndes() - Problema en la carga del detalle del abono, la cantidad informada no coincide con la cantidad a procesar");
                                            estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                            throw new Exception("Problema en la carga del detalle del abono, la cantidad informada no coincide con la cantidad a procesar"); 
                                        }
                                    }else{
                                        logger.error("doProcesarAbonoAndes() - Problema en la carga del detalle del abono, no posee datos");
                                        estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                        throw new Exception("Problema en la carga del detalle del abono, no posee datos");
                                    }
                                }else{
                                    logger.error("doProcesarAbonoAndes() - Problema en la carga del resumen del abono, no pudo ser procesado en la Base de Datos");
                                    estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                    throw new Exception("Problema en la carga del resumen del abono, no pudo ser procesado en la Base de Datos");                                     
                                }
                            }else{
                                logger.error("doProcesarAbonoAndes() - Problema en la carga del resumen del abono, no posee datos");   
                                estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                throw new Exception("Problema en la carga del resumen del abono, no posee datos");
                            }
                        }
                        
                        if(lstResumenAbonoDTO.size() != cantidadResumenProcesada){
                            logger.error("doProcesarAbonoAndes() - Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");
                            estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                            throw new Exception("Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");                            
                        }else{                            
                            ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_FINALIZADO_CARGA, null, null);
                            ejb.updateCargaArchivosTerminoProceso(idCarga);
                            ejb.updateCargaArchivosCantidadProcesada(idCarga, lstResumenAbonoDTO.size());
                            resp = true;
                        }
                    }else{
                        logger.error("doProcesarAbonoAndes() - Problema en la carga del abono, el archivo no se ley� correctamente");
                        estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                        throw new Exception("Problema en la carga del abono, el archivo no se ley� correctamente");                        
                    }
                }
            }
            
            /*
             * Procesa la autorizaci�n
             */
            
             for (int i = 0; i < lstResumenAbonoDTO.size(); i++) {
                 lstResumenAbonoDTO.get(i).setId_motivo_rechazo(0);
                 lstResumenAbonoDTO.get(i).setObservacion_rechazo("");
                 lstResumenAbonoDTO.get(i).setObservacion_autorizacion("");
                 lstResumenAbonoDTO.get(i).setValidacion_cartola_cta_cte(1);
                 lstResumenAbonoDTO.get(i).setTotal_monto_inf_cartola(lstResumenAbonoDTO.get(i).getTotal_monto_institucion());
                 lstResumenAbonoDTO.get(i).setEstado_proceso(Constant.PROCESO_ABONO_AUTORIZADO);
                 lstResumenAbonoDTO.get(i).setEstado_proceso_excepcion("");
                 
                 lstResumenAbonoDTO.get(i).setBool_button_autobs(false);
                 lstResumenAbonoDTO.get(i).setBool_button_rechazado(false);
                 lstResumenAbonoDTO.get(i).setProcesadoAprobacion(true);
                 
                 ejb = ServiceLocator.getSessionEJBPrevired();
                 
                 EstadoDeProcesoDTO estadoDeProcesoDTO = ejb.getEstadoDeProcesoByIdProceso(Constant.PROCESO_ABONO_AUTORIZADO);
                 if(estadoDeProcesoDTO != null){
                     lstResumenAbonoDTO.get(i).setDescripcion_estado_proceso(estadoDeProcesoDTO.getDecripcionEstado());
                 }
                 
                 ejb.updateResumenYDetalleAbonoByIdCargaResumen(idCarga, lstResumenAbonoDTO.get(i));
                 
             }
             
             
             //Finaliza el proceso de autorzacion
             
              ejb.updateCargaArchivosEstadoProceso(idCarga, 
                                                   Constant.PROCESO_FINALIZADO_AUTORIZADO, 
                                                   null, 
                                                   null);
            
            
        } catch(Exception e){
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            ejb.updateCargaArchivosEstadoProceso(idCarga, estadoProceso, "Problema en la carga del resumen y del detalle del abono, se produjo un excepcion: " + e.getMessage(), null);
            logger.error("doProcesarAbonoAndes() - Problema en la carga del resumen y del detalle del abono, se produjo un exepcion: " + e.getMessage(), e);            
        }
        logger.debug("doProcesarAbonoAndes() - Temino");
        return resp;
    }
    
    
    
    public boolean doProcesarAbonoPlanillaLaAraucana(Double idCarga, String pathArchivoRendicion, PreviredCore core, Double idCargaAbono,Integer tipoProceso)throws Exception{
        logger.debug("doProcesarAbonoPlanillaLaAraucana() - Inicio");
        boolean resp = false;
        Integer estadoProceso = 0;
        Long turno = null;
        try{
        
            /*
             * Obtiene el estado de la carga para iniciar proceso
             */
            logger.debug("doProcesarAbonoPlanillaLaAraucana() - Conectando a EJB");
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            
            logger.debug("doProcesarAbonoPlanillaLaAraucana() - Obteniedo carga de idcarga: " + idCarga);
            CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCarga);
            // Si el id de carga esta pendiente se procesa, en caso contrario no se realiza el proceso
            logger.debug("doProcesarAbonoPlanillaLaAraucana() - Verificando estado pendiente");
            logger.info(cargaArchivos != null ? "no es null" : "es null");
            logger.info(cargaArchivos != null ?  cargaArchivos.getEstado_proceso() : "es null");
            
            if(cargaArchivos != null && cargaArchivos.getEstado_proceso().intValue() == Constant.PROCESO_PENDIENTE.intValue()){
                logger.debug("doProcesarAbonoPlanillaLaAraucana() - Estado Pendiente");
                
                //abre caja
                turno = ejb.callNewAperturaTurnoCaja(Constant.PLANILLA_LAARAUCANA);
                
                java.util.Date fechaTurno = ejb.getFechaHoraTurno(turno);
                Long foliocajadiario = ejb.getFolioCajaByTurno(fechaTurno);
                
                logger.info("id turno obtenido: " + turno);
                logger.info("fecha de turno caja: " + fechaTurno);
                logger.info("folio de caja diario: " + foliocajadiario);
                
                logger.debug("doProcesarAbonoPlanillaLaAraucana() - Turno abierto");
                // Se cambia el estado de la carga en la base de datos y se actualiza la fecha de inicio del proceso
                ejb.updateCargaArchivosInicioProceso(idCarga);
                logger.debug("doProcesarAbonoPlanillaLaAraucana() - Update a proceso de carga");
                if(ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_LECTURA_ARCHIVO, null, null)){                
                    logger.debug("doProcesarAbonoPlanillaLaAraucana() - Update a estado del proceso de carga");
                    /*
                     * Obtencion de los datos del archivo
                     */
                    String tipoplanilla = ""; 
                    if(cargaArchivos.getConvenio().getNumero_de_convenio().equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL.toString())){                        
                        tipoplanilla = "PLANILLALAARAUCANAMANUAL";
                        logger.debug("doProcesarAbonoPlanillaLaAraucana() - Tipo Planilla " +tipoplanilla);
                    }else if (cargaArchivos.getConvenio().getNumero_de_convenio().equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA_CONVENIO.toString())){
                        tipoplanilla = "PLANILLALAARAUCANA";
                        logger.debug("doProcesarAbonoPlanillaLaAraucana() - Tipo Planilla " +tipoplanilla);
                    }else{
                        throw new Exception("Problema en la carga al obtener el tipo de planilla Andes a procesar");
                    }
                    
                    logger.debug("doProcesarAbonoPlanillaLaAraucana() - Comienza lectura de Archivo");
                    ResultadoProcesoVO resultadoProcesoVO = core.doProcesar(pathArchivoRendicion, tipoplanilla, idCarga);
                    logger.debug("doProcesarAbonoPlanillaLaAraucana() - Termino lectura de Archivo");
                    /*
                     * Procesar Abono
                     */
                    if(doProcesarAbonoLaAraucana(idCargaAbono, resultadoProcesoVO.getLst_resumen_abono(),tipoProceso) ){
                        // Si los datos obtenidos son correctos se continua el proceso
                        if(resultadoProcesoVO != null && resultadoProcesoVO.isOk() && resultadoProcesoVO.getLst_header_planilla() != null && resultadoProcesoVO.getLst_header_planilla().size()>0){
                            Iterator lstPlanilla =  resultadoProcesoVO.getLst_header_planilla().iterator();
                            while(lstPlanilla.hasNext()){
                                /*
                                 * Carga en la base de datos de la planilla 
                                 */
                                ArchivoPlanillaHeaderDTO planillaHeader = (ArchivoPlanillaHeaderDTO)lstPlanilla.next();
                                //Verifica si posee datos
                                if(planillaHeader != null && planillaHeader.getLst_detalle_planilla() != null && planillaHeader.getLst_detalle_planilla().size() > 0){
                                    ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_CARGA_BASEDATOS, null, null);
                                    
                                    int cantidadDetalleProcesada = 0;
                                    for(int i = 0; i < planillaHeader.getLst_detalle_planilla().size(); i++ ){
                                        planillaHeader.setProceso_detalle(false);
                                                                               
                                        planillaHeader = ejb.insertArchivoPlanillaProceso(turno, fechaTurno, foliocajadiario, planillaHeader, idCarga, i, 25, Constant.PLANILLA_LAARAUCANA);
                                        
                                        if(planillaHeader.isProceso_detalle()){
                                            i = planillaHeader.getNumero_detalle_procesados();
                                            cantidadDetalleProcesada = i+1;
                                            logger.debug("doProcesarAbonoPlanillaLaAraucana() - Detalles de Planilla procesados: " + cantidadDetalleProcesada);
                                        }else{
                                            logger.error("doProcesarAbonoPlanillaLaAraucana() - Problema en la carga del detalle de la planilla, no pudo ser procesado en la Base de Datos");
                                            estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                            throw new Exception("Problema en la carga del detalle de la planilla, no pudo ser procesado en la Base de Datos");
                                        }
                                    }
                                    
                                    if( planillaHeader.getLst_detalle_planilla().size() == cantidadDetalleProcesada && planillaHeader.isProceso_detalle() ) {
                                        /*if(planillaHeader.getIdCargaAbono() != null){
                                            ejb.updateEstadoPorcesoFolioResumenByIdCarga(planillaHeader.getIdCargaAbono(), Constant.PROCESO_PENDIENTE, Constant.PROCESO_PROBLEMA_PLANILLA_ABONO, "El folio abono no se encuentra en el detalle");    
                                        }*/
                                        
                                        ejb.updateCargaArchivosEstadoProceso(planillaHeader.getIdCargaAbono(), Constant.PROCESO_FINALIZADO_, null, null);
                                        ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_FINALIZADO_, null, planillaHeader.getIdCargaAbono());
                                        ejb.updateCargaArchivosTerminoProceso(idCarga);
                                        ejb.updateCargaArchivosCantidadProcesada(idCarga, planillaHeader.getLst_detalle_planilla().size());
                                        ejb.updateFolioAbonoConDatosNoProcesados(planillaHeader.getIdCargaAbono(), Constant.PROCESO_PROBLEMA_RESUMEN_ABONO, "El folio no fue procesado, pues no se encontraba en el archivo de planilla");
                                        resp = true;
                                    }else{
                                        logger.error("doProcesarAbonoPlanillaLaAraucana() - Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                        ejb.updateCargaArchivosCantidadProcesada(idCarga, cantidadDetalleProcesada);
                                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                        throw new Exception("Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                    }                            
                                }else{                                
                                    logger.error("doProcesarAbonoPlanillaLaAraucana() - Problema en la carga del resumen del abono, no posee datos");
                                    estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                    throw new Exception("Problema en la carga del resumen de la planilla, no posee datos");                                
                                }
                            } //END WHILE
                            
                            
                            //TODO:NOTIFICAR A CAJA
                            //INFORMAR A AR MEDIANTE WS
                            // ============ LLAMA A WEBSERVICE PARA NOTIFICACION DE TERMINO =========
                            try {
                                      logger.info("NOTIFICACION AR : LA ARAUCANA  - INICIANDO...." );
                                      //Notifica Webservices de Recaudacion
                                      //RecaudacionServiceClient myPort = new RecaudacionServiceClient();
                                      //myPort.setEndpoint(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.endpoint"));
                                      InformarRecaudacionIn in = new InformarRecaudacionIn();
                                      in.setCallerSystem(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.callersystem"));
                                      in.setCallerUser(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.calleruser"));
                                      
                                      //RECUPERA EL FOLIO DE CAJA DIARIO
                                      in.setFolioCajaDiario(foliocajadiario);//SELECT
                                      in.setFolioPago(null);
                                      Calendar cale = new GregorianCalendar();
                                      cale.setTime(fechaTurno);
                                      in.setTurno(cale);
                                      in.setIdTurno(turno); //turno
                                      in.setUsuarioCaja(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.usuariocaja"));
                                      logger.info("NOTIFICANDO USUARIO CAJA ARAUCANA:" +in.getUsuarioCaja());
                                      logger.info("NOTIFICANDO TURNO:" +in.getTurno());
                                      logger.info("NOTIFICANDO FOLIO CAJA DIARIO:" +in.getFolioCajaDiario());
                                      logger.info("NOTIFICANDO FOLIO PAGO:" +in.getFolioPago());
                                      logger.info("NOTIFICANDO ID TURNO:" +in.getIdTurno());
                                      //logger.info("NOTIFICANDO ENDPOINT:" +myPort.getEndpoint());
                                      //myPort.informarRecaudacion(in); 
                                      //logger.info("NOTIFICACION AR VIA WS : OK  - ID TURNO CAJA LA ARAUCANA:" );
                                      logger.info("NOTIFICACION --> Ejecutamos distribucion en caja y creamos concurrente");
                                      ejb.ejecutarDistribucionEnCajayConcurrente(in);
                              } catch (Exception e) {
                                e.printStackTrace();
                              }
                        }else{
                            if(resultadoProcesoVO != null){
                                logger.error("doProcesarAbonoPlanillaLaAraucana() - Problema en la carga del abono, el archivo no se ley� correctamente");
                                estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente" + resultadoProcesoVO.getError());          
                            }else{
                                logger.error("doProcesarAbonoPlanillaLaAraucana() - Problema en la carga del abono, el archivo no se ley� correctamente");
                                estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente");
                            }
                        }
                    }else{
                        logger.error("doProcesarAbonoPlanillaLaAraucana() - Problema en la carga del abono, el archivo no se ley� correctamente");
                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                        throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente");
                    }
                        
                    ejb.callNewCierreTurnoCaja(Constant.PLANILLA_LAARAUCANA, turno);
                }
            }else{
                // No realiza nada
                logger.info("no se cumple alguna condicion");
            }
            
            
        } catch(Exception e){
            e.printStackTrace();
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            ejb.deletePlanillaAbono(idCarga);
            ejb.updateCargaArchivosEstadoProceso(idCarga, estadoProceso, "Problema en la carga de la planilla, se produjo un excepcion: " + e.getMessage(), null);
            if (turno != null) ejb.callNewCierreTurnoCaja(Constant.PLANILLA_LAARAUCANA, turno);
            logger.error("doProcesarAbonoPlanillaLaAraucana() - Problema en la carga de la planilla, se produjo un excepcion: " + e.getMessage(), e);            
            throw new Exception("Problema en la carga: " + e.getMessage());
        }
        return resp;
    }
    
    
    public boolean doProcesarAbonoLaAraucana(Double idCarga, List<ResumenAbonoDTO> lstResumenAbonoDTO,Integer tipoProceso) throws Exception {
        logger.debug("doProcesarAbonoLaAraucana() - Inicio");
        boolean resp = false;
        Integer estadoProceso = 0;
        
        try{
            /*
             * Obtiene el estado de la carga para iniciar proceso
             */   
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCarga);
            // Si el id de carga esta pendiente se procesa, en caso contrario no se realiza el proceso
            if(cargaArchivos != null && cargaArchivos.getEstado_proceso().intValue() == Constant.PROCESO_PENDIENTE.intValue()){
                // Se cambia el estado de la carga en la base de datos y se actualiza la fecha de inicio del proceso
                ejb.updateCargaArchivosInicioProceso(idCarga);
                if(ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_LECTURA_ARCHIVO, null, null)){
                    
                    if(lstResumenAbonoDTO != null && lstResumenAbonoDTO.size()>0){
                        // se itera la cantidad de abonos del archivo
                        Iterator lstAbono =  lstResumenAbonoDTO.iterator();
                        int cantidadResumenProcesada = 0;
                        while(lstAbono.hasNext()){
                            /*
                             * Carga en la base de datos del resumen 
                             */
                            ResumenAbonoDTO resumenAbono = (ResumenAbonoDTO)lstAbono.next();
                            resumenAbono.setProcesado(false);
                            if(resumenAbono != null && resumenAbono.getArchivoabono() != null && resumenAbono.getArchivoabono().size() > 0){
                                ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_CARGA_BASEDATOS, null, null);
                                resumenAbono = ejb.insertResumenAbono(resumenAbono, idCarga);
                                // Si el resultado de isProcesado()  es verdadero se continual con el detalle del abono
                                if(resumenAbono.isProcesado()){
                                    cantidadResumenProcesada++;
                                    if(resumenAbono.getArchivoabono().size() > 0){                                        
                                        if(resumenAbono.getArchivoabono().size() == resumenAbono.getNumero_de_planillas().intValue()){
                                        
                                            int cantidadDetalleProcesada = 0;                                            
                                            for(int i = 0; i < resumenAbono.getArchivoabono().size(); i++ ) {
                                                resumenAbono.setProceso_detalle(false);
                                                
                                                resumenAbono = ejb.insertDetalleAbono(resumenAbono, idCarga, i, 100,tipoProceso);
                                                
                                                if(resumenAbono.isProceso_detalle()){
                                                    i = resumenAbono.getNumero_detalle_procesados().intValue();
                                                    cantidadDetalleProcesada = i+1;
                                                    logger.debug("doProcesarAbonoLaAraucana() - Detalles de Abonos procesados: " + cantidadDetalleProcesada);
                                                }else{
                                                    logger.error("doProcesarAbonoLaAraucana() - Problema en la carga del detalle del abono, no pudo ser procesado en la Base de Datos");
                                                    estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                                    throw new Exception("Problema en la carga del detalle del abono, no pudo ser procesado en la Base de Datos");
                                                }
                                            }
                                            
                                            if(resumenAbono.getArchivoabono().size() != cantidadDetalleProcesada && resumenAbono.isProceso_detalle()){
                                                logger.error("doProcesarAbonoLaAraucana() - Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                                ejb.updateCargaArchivosCantidadProcesada(idCarga, cantidadDetalleProcesada);
                                                estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                                throw new Exception("Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");
                                            }                                      
                                        }else{
                                            logger.error("doProcesarAbonoLaAraucana() - Problema en la carga del detalle del abono, la cantidad informada no coincide con la cantidad a procesar");
                                            estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                            throw new Exception("Problema en la carga del detalle del abono, la cantidad informada no coincide con la cantidad a procesar"); 
                                        }
                                    }else{
                                        logger.error("doProcesarAbonoLaAraucana() - Problema en la carga del detalle del abono, no posee datos");
                                        estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                        throw new Exception("Problema en la carga del detalle del abono, no posee datos");
                                    }
                                }else{
                                    logger.error("doProcesarAbonoLaAraucana() - Problema en la carga del resumen del abono, no pudo ser procesado en la Base de Datos");
                                    estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                    throw new Exception("Problema en la carga del resumen del abono, no pudo ser procesado en la Base de Datos");                                     
                                }
                            }else{
                                logger.error("doProcesarAbonoLaAraucana() - Problema en la carga del resumen del abono, no posee datos");   
                                estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                throw new Exception("Problema en la carga del resumen del abono, no posee datos");
                            }
                        }
                        
                        if(lstResumenAbonoDTO.size() != cantidadResumenProcesada){
                            logger.error("doProcesarAbonoLaAraucana() - Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");
                            estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                            throw new Exception("Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");                            
                        }else{                            
                            ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_FINALIZADO_CARGA, null, null);
                            ejb.updateCargaArchivosTerminoProceso(idCarga);
                            ejb.updateCargaArchivosCantidadProcesada(idCarga, lstResumenAbonoDTO.size());
                            resp = true;
                        }
                    }else{
                        logger.error("doProcesarAbonoLaAraucana() - Problema en la carga del abono, el archivo no se ley� correctamente");
                        estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                        throw new Exception("Problema en la carga del abono, el archivo no se ley� correctamente");                        
                    }
                }
            }
            
            /*
             * Procesa la autorizaci�n
             */
            
             for (int i = 0; i < lstResumenAbonoDTO.size(); i++) {
                 lstResumenAbonoDTO.get(i).setId_motivo_rechazo(0);
                 lstResumenAbonoDTO.get(i).setObservacion_rechazo("");
                 lstResumenAbonoDTO.get(i).setObservacion_autorizacion("");
                 lstResumenAbonoDTO.get(i).setValidacion_cartola_cta_cte(1);
                 lstResumenAbonoDTO.get(i).setTotal_monto_inf_cartola(lstResumenAbonoDTO.get(i).getTotal_monto_institucion());
                 lstResumenAbonoDTO.get(i).setEstado_proceso(Constant.PROCESO_ABONO_AUTORIZADO);
                 lstResumenAbonoDTO.get(i).setEstado_proceso_excepcion("");
                 
                 lstResumenAbonoDTO.get(i).setBool_button_autobs(false);
                 lstResumenAbonoDTO.get(i).setBool_button_rechazado(false);
                 lstResumenAbonoDTO.get(i).setProcesadoAprobacion(true);
                 
                 ejb = ServiceLocator.getSessionEJBPrevired();
                 
                 EstadoDeProcesoDTO estadoDeProcesoDTO = ejb.getEstadoDeProcesoByIdProceso(Constant.PROCESO_ABONO_AUTORIZADO);
                 if(estadoDeProcesoDTO != null){
                     lstResumenAbonoDTO.get(i).setDescripcion_estado_proceso(estadoDeProcesoDTO.getDecripcionEstado());
                 }
                 
                 ejb.updateResumenYDetalleAbonoByIdCargaResumen(idCarga, lstResumenAbonoDTO.get(i));
                 
             }
             
             
             //Finaliza el proceso de autorzacion
             
              ejb.updateCargaArchivosEstadoProceso(idCarga, 
                                                   Constant.PROCESO_FINALIZADO_AUTORIZADO, 
                                                   null, 
                                                   null);
            
            
        } catch(Exception e){
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            ejb.deleteDetalleAbono(idCarga);
            ejb.updateCargaArchivosEstadoProceso(idCarga, estadoProceso, "Problema en la carga del resumen y del detalle del abono, se produjo un excepcion: " + e.getMessage(), null);
            logger.error("doProcesarAbonoLaAraucana() - Problema en la carga del resumen y del detalle del abono, se produjo un exepcion: " + e.getMessage(), e);
            throw new Exception("Error al procesar " + e.getMessage());
        }
        logger.debug("doProcesarAbonoLaAraucana() - Temino");
        return resp;
    }
    
    public boolean doProcesarAbonoAut(Double idCarga, String pathArchivoRendicion, PreviredCore core)throws Exception{
        logger.debug("doProcesarAbono() - Inicio");
        boolean resp = false;
        Integer estadoProceso = 0;
        
        try{
            /*
             * Obtiene el estado de la carga para iniciar proceso
             */   
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCarga);
            // Si el id de carga esta pendiente se procesa, en caso contrario no se realiza el proceso
            if(cargaArchivos != null && cargaArchivos.getEstado_proceso().intValue() == Constant.PROCESO_PENDIENTE.intValue()){
                // Se cambia el estado de la carga en la base de datos y se actualiza la fecha de inicio del proceso
                ejb.updateCargaArchivosInicioProceso(idCarga);
                if(ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_LECTURA_ARCHIVO, null, null)){
                    /*
                     * Ontencion de los datos del archivo
                     */
                    ResultadoProcesoVO resultadoProcesoVO = core.doProcesar(pathArchivoRendicion, "ABONO", idCarga);
                    // Si los datos obtenidos son correctos se continua el proceso
                    if(resultadoProcesoVO != null && resultadoProcesoVO.isOk() && resultadoProcesoVO.getLst_resumen_abono() != null && resultadoProcesoVO.getLst_resumen_abono().size()>0){
                        // se itera la cantidad de abonos del archivo
                        Iterator lstAbono =  resultadoProcesoVO.getLst_resumen_abono().iterator();
                        int cantidadResumenProcesada = 0;
                        while(lstAbono.hasNext()){
                            /*
                             * Carga en la base de datos del resumen 
                             */
                            ResumenAbonoDTO resumenAbono = (ResumenAbonoDTO)lstAbono.next();
                            resumenAbono.setProcesado(false);
                            if(resumenAbono != null && resumenAbono.getArchivoabono() != null && resumenAbono.getArchivoabono().size() > 0){
                                ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_CARGA_BASEDATOS, null, null);
                                resumenAbono = ejb.insertResumenAbono(resumenAbono, idCarga);
                                // Si el resultado de isProcesado()  es verdadero se continual con el detalle del abono
                                if(resumenAbono.isProcesado()){
                                    cantidadResumenProcesada++;
                                    if(resumenAbono.getArchivoabono().size() > 0){                                        
                                        if(resumenAbono.getArchivoabono().size() == resumenAbono.getNumero_de_planillas().intValue()){
                                        
                                            int cantidadDetalleProcesada = 0;                                            
                                            for(int i = 0; i < resumenAbono.getArchivoabono().size(); i++ ) {
                                                resumenAbono.setProceso_detalle(false);
                                                resumenAbono = ejb.insertDetalleAbono(resumenAbono, idCarga, i, 100,null);
                                                if(resumenAbono.isProceso_detalle()){
                                                    i = resumenAbono.getNumero_detalle_procesados().intValue();
                                                    cantidadDetalleProcesada = i+1;
                                                    logger.debug("doProcesarAbono() - Detalles de Abonos procesados: " + cantidadDetalleProcesada);
                                                }else{
                                                    logger.error("doProcesarAbono() - Problema en la carga del detalle del abono, no pudo ser procesado en la Base de Datos");
                                                    estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                                    throw new Exception("Problema en la carga del detalle del abono, no pudo ser procesado en la Base de Datos");
                                                }
                                            }
                                            
                                            if(resumenAbono.getArchivoabono().size() != cantidadDetalleProcesada && resumenAbono.isProceso_detalle()){
                                                logger.error("doProcesarAbono() - Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                                ejb.updateCargaArchivosCantidadProcesada(idCarga, cantidadDetalleProcesada);
                                                estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                                throw new Exception("Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");
                                            }                                      
                                        }else{
                                            logger.error("doProcesarAbono() - Problema en la carga del detalle del abono, la cantidad informada no coincide con la cantidad a procesar");
                                            estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                            throw new Exception("Problema en la carga del detalle del abono, la cantidad informada no coincide con la cantidad a procesar"); 
                                        }
                                    }else{
                                        logger.error("doProcesarAbono() - Problema en la carga del detalle del abono, no posee datos");
                                        estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                        throw new Exception("Problema en la carga del detalle del abono, no posee datos");
                                    }
                                }else{
                                    logger.error("doProcesarAbono() - Problema en la carga del resumen del abono, no pudo ser procesado en la Base de Datos");
                                    estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                    throw new Exception("Problema en la carga del resumen del abono, no pudo ser procesado en la Base de Datos");                                     
                                }
                            }else{
                                logger.error("doProcesarAbono() - Problema en la carga del resumen del abono, no posee datos");   
                                estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                                throw new Exception("Problema en la carga del resumen del abono, no posee datos");
                            }
                        }
                        
                        if(resultadoProcesoVO.getLst_resumen_abono().size() != cantidadResumenProcesada){
                            logger.error("doProcesarAbono() - Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");
                            estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                            throw new Exception("Problema en la carga del resumen del abono, la cantidad informada no coincide con la cantidad a procesada");                            
                        }else{                            
                            ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_FINALIZADO_CARGA, null, null);
                            ejb.updateCargaArchivosTerminoProceso(idCarga);
                            ejb.updateCargaArchivosCantidadProcesada(idCarga, resultadoProcesoVO.getLst_resumen_abono().size());
                            resp = true;
                        }
                    }else{
                        logger.error("doProcesarAbono() - Problema en la carga del abono, el archivo no se ley� correctamente");
                        estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                        throw new Exception("Problema en la carga del abono, el archivo no se ley� correctamente");                        
                    }
                }
            }//else{
                    // No realiza nada
                //}
        } catch(Exception e){
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            ejb.updateCargaArchivosEstadoProceso(idCarga, estadoProceso, "Problema en la carga del resumen y del detalle del abono, se produjo un excepcion: " + e.getMessage(), null);
            ejb.deleteDetalleAbono(idCarga);
            ejb.deleteResumenAbono(idCarga);
            ejb.deleteCargaArchivo(idCarga);
            logger.error("doProcesarAbono() - Problema en la carga del resumen y del detalle del abono, se produjo un exepcion: " + e.getMessage(), e);            
            throw new Exception("Problema en la carga: " + e.getMessage());
        }
        logger.debug("doProcesarAbono() - Temino");
        return resp;
    }
    
    public boolean doProcesarPlanillaAut(Double idCarga, String pathArchivoRendicion, PreviredCore core)throws Exception {
        logger.debug("doProcesarPlanilla() - Inicio");
        boolean resp = false;
        Integer estadoProceso = 0;
        Long turno = null;
        try {
            /*
             * Obtiene el estado de la carga para iniciar proceso
             */
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();

            CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCarga);
            // Si el id de carga esta pendiente se procesa, en caso contrario no se realiza el proceso
            if (cargaArchivos != null && cargaArchivos.getEstado_proceso().intValue() == Constant.PROCESO_PENDIENTE.intValue()) {
                turno = ejb.callNewAperturaTurnoCaja(Constant.PLANILLA_PREVIRED);

                java.util.Date fechaTurno = ejb.getFechaHoraTurno(turno);
                Long foliocajadiario = ejb.getFolioCajaByTurno(fechaTurno);

                logger.info("id turno obtenido: " + turno);
                logger.info("fecha de turno caja: " + fechaTurno);
                logger.info("folio de caja diario: " + foliocajadiario);


                //BUSCAR DATOS ADICIONALES

                // Se cambia el estado de la carga en la base de datos y se actualiza la fecha de inicio del proceso
                ejb.updateCargaArchivosInicioProceso(idCarga);
                if (ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_LECTURA_ARCHIVO, null, null)) {
                    /*
                     * Obtencion de los datos del archivo
                     */
                    ResultadoProcesoVO resultadoProcesoVO = core.doProcesar(pathArchivoRendicion, "PLANILLA", idCarga);
                    // Si los datos obtenidos son correctos se continua el proceso
                    if (resultadoProcesoVO != null && resultadoProcesoVO.isOk() && resultadoProcesoVO.getLst_header_planilla() != null &&
                        resultadoProcesoVO.getLst_header_planilla().size() > 0) {
                        Iterator lstPlanilla = resultadoProcesoVO.getLst_header_planilla().iterator();


                        while (lstPlanilla.hasNext()) {
                            /*
                             * Carga en la base de datos de la planilla
                             */
                            ArchivoPlanillaHeaderDTO planillaHeader = (ArchivoPlanillaHeaderDTO) lstPlanilla.next();
                            //Verifica si posee datos
                            if (planillaHeader != null && planillaHeader.getLst_detalle_planilla() != null && planillaHeader.getLst_detalle_planilla().size() > 0) {
                                ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_CARGA_BASEDATOS, null, null);

                                int cantidadDetalleProcesada = 0;
                                for (int i = 0; i < planillaHeader.getLst_detalle_planilla().size(); i++) {
                                    planillaHeader.setProceso_detalle(false);


                                    //====================================================
                                    // PAGO EN CAJA
                                    // CONJUNTO CON INSERTAR ESTE PROCESO PAGA EN CAJA
                                    //====================================================
                                    planillaHeader =
                                        ejb.insertArchivoPlanillaProceso(turno, fechaTurno, foliocajadiario, planillaHeader, idCarga, i, 25, Constant.PLANILLA_PREVIRED);
                                    if (planillaHeader.isProceso_detalle()) {
                                        i = planillaHeader.getNumero_detalle_procesados();
                                        cantidadDetalleProcesada = i + 1;

                                        logger.debug("doProcesarPlanilla() - Detalles de Planilla procesados: " + cantidadDetalleProcesada);
                                    } else {
                                        logger.error("doProcesarPlanilla() - Problema en la carga del detalle de la planilla, no pudo ser procesado en la Base de Datos");
                                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                        throw new Exception("Problema en la carga del detalle de la planilla, no pudo ser procesado en la Base de Datos");
                                    }
                                }


                                if (planillaHeader.getLst_detalle_planilla().size() == cantidadDetalleProcesada && planillaHeader.isProceso_detalle()) {
                                    /*if(planillaHeader.getIdCargaAbono() != null){
                                        ejb.updateEstadoPorcesoFolioResumenByIdCarga(planillaHeader.getIdCargaAbono(), Constant.PROCESO_PENDIENTE, Constant.PROCESO_PROBLEMA_PLANILLA_ABONO, "El folio abono no se encuentra en el detalle");
                                    }*/


                                    ejb.updateCargaArchivosEstadoProceso(planillaHeader.getIdCargaAbono(), Constant.PROCESO_FINALIZADO_, null, null);
                                    ejb.updateCargaArchivosEstadoProceso(idCarga, Constant.PROCESO_FINALIZADO_, null, planillaHeader.getIdCargaAbono());
                                    ejb.updateCargaArchivosTerminoProceso(idCarga);
                                    ejb.updateCargaArchivosCantidadProcesada(idCarga, planillaHeader.getLst_detalle_planilla().size());
                                    ejb.updateFolioAbonoConDatosNoProcesados(planillaHeader.getIdCargaAbono(), Constant.PROCESO_PROBLEMA_RESUMEN_ABONO,
                                                                             "El folio no fue procesado, pues no se encontraba en el archivo de planilla");
                                    resp = true;
                                } else {
                                    logger.error("doProcesarAbono() - Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                    ejb.updateCargaArchivosCantidadProcesada(idCarga, cantidadDetalleProcesada);
                                    estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                    throw new Exception("Problema en la carga del resumen de la planilla, la cantidad informada no coincide con la cantidad a procesada");
                                }
                            } else {
                                logger.error("doProcesarPlanilla() - Problema en la carga del resumen del abono, no posee datos");
                                estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                throw new Exception("Problema en la carga del resumen de la planilla, no posee datos");
                            }
                        } // TERMINO WHILE


                        //TODO:NOTIFICAR A CAJA
                        //INFORMAR A AR MEDIANTE WS
                        // ============ LLAMA A WEBSERVICE PARA NOTIFICACION DE TERMINO =========
                        try {
                            logger.info("NOTIFICACION AR VIA : PREVIRED  - INICIANDO....CASO PLANILLA");
                            //Notifica Webservices de Recaudacion
                            // RecaudacionServiceClient myPort = new RecaudacionServiceClient();
                            // myPort.setEndpoint(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.endpoint"));
                            InformarRecaudacionIn in = new InformarRecaudacionIn();

                            in.setCallerSystem(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.callersystem"));
                            in.setCallerUser(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.calleruser"));

                            //RECUPERA EL FOLIO DE CAJA DIARIO
                            in.setFolioCajaDiario(foliocajadiario); //SELECT
                            in.setFolioPago(null);
                            Calendar cale = new GregorianCalendar();
                            cale.setTime(fechaTurno);
                            in.setTurno(cale);
                            in.setIdTurno(turno); //numero turno
                            in.setUsuarioCaja(ResourcesAsyncUtil.getProperty("integracion.webservice.notificacion.recaudacion.usuariocaja"));
                            logger.info("NOTIFICANDO USUARIO CAJA PLANILLA:" + in.getUsuarioCaja());
                            logger.info("NOTIFICANDO TURNO:" + in.getTurno());
                            logger.info("NOTIFICANDO FOLIO CAJA DIARIO:" + in.getFolioCajaDiario());
                            logger.info("NOTIFICANDO FOLIO PAGO:" + in.getFolioPago());
                            logger.info("NOTIFICANDO ID TURNO:" + in.getIdTurno());
                            //logger.info("NOTIFICANDO ENDPOINT:" +myPort.getEndpoint());
                            //logger.info("NOTIFICACION AR VIA WS : OK  - ID TURNO CAJA PREVIRED:" );
                            logger.info("NOTIFICACION --> Ejecutamos distribucion en caja y creamos concurrente");
                            ejb.ejecutarDistribucionEnCajayConcurrente(in);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else {
                        if (resultadoProcesoVO != null) {
                            ejb.deleteDetalleAbono(idCarga);
                            ejb.deleteResumenAbono(idCarga);
                            ejb.deleteCargaArchivo(idCarga);
                            ejb.deleteProcesoAutomatico(idCarga);
                            logger.error("doProcesarPlanilla() - Problema en la carga del abono, el archivo no se ley� correctamente");
                            estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                            throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente" + resultadoProcesoVO.getError());
                        } else {
                            ejb.deleteDetalleAbono(idCarga);
                            ejb.deleteResumenAbono(idCarga);
                            ejb.deleteCargaArchivo(idCarga);
                            ejb.deleteProcesoAutomatico(idCarga);
                            logger.error("doProcesarPlanilla() - Problema en la carga del abono, el archivo no se ley� correctamente");
                            estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                            throw new Exception("Problema en la carga de la planilla , el archivo no se ley� correctamente");
                        }
                    }
                }

                ejb.callNewCierreTurnoCaja(Constant.PLANILLA_PREVIRED, turno);

            } //else{
            // No realiza nada
            //}


        } catch (Exception e) {
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            ejb.deletePlanillaAbono(idCarga);
            ejb.deleteCargaArchivo(idCarga);
            ejb.deleteProcesoAutomatico(idCarga);
            ejb.updateCargaArchivosEstadoProceso(idCarga, estadoProceso, "Problema en la carga de la planilla, se produjo un excepcion: " + e.getMessage(), null);
            if (turno != null)
                ejb.callNewCierreTurnoCaja(Constant.PLANILLA_PREVIRED, turno);
            logger.error("doProcesarPlanilla() - Problema en la carga de la planilla, se produjo un excepcion: " + e.getMessage(), e);
            throw new Exception("Problema en la carga: " + e.getMessage());

        }
        return resp;
    }
    
    public static void main(String argv[]) {
        PreviredCore previredCore = new PreviredCore();        
        String fileconfig = "C:\\WSLongName\\Previred_4946\\src\\Previred\\public_html\\WEB-INF\\config\\configfileabono.xml";
        previredCore.loadConfig(fileconfig);
    
        ProcesoRendicionVO test = new ProcesoRendicionVO();
        try{
        test.doProcesarAbonoPlanillaAndes(1158.0, "C:\\Documents and Settings\\alejandro.fernandez\\Escritorio\\PREVIRED\\LSAndes\\biceseg_13042010_FULL_13.txt", previredCore, 1157.0);        
        }catch(Exception e){
            e.printStackTrace();
        }
                
    }   
}
