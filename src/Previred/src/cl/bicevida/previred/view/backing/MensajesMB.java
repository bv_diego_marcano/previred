package cl.bicevida.previred.view.backing;

import javax.faces.component.html.HtmlForm;

import org.apache.myfaces.trinidad.component.core.CoreForm;
import org.apache.myfaces.trinidad.component.html.HtmlBody;
import org.apache.myfaces.trinidad.component.html.HtmlHead;
import org.apache.myfaces.trinidad.component.html.HtmlHtml;

import org.apache.myfaces.trinidad.component.core.layout.CorePanelBox;
import org.apache.myfaces.trinidad.component.core.layout.CorePanelHorizontalLayout;
import org.apache.myfaces.trinidad.component.core.output.CoreOutputText;

public class MensajesMB {    
    private boolean advertencia;
    private String mensaje;
    private HtmlHtml html1;
    private HtmlHead head1;
    private HtmlBody body1;
    private CoreForm form1;
    private CorePanelBox panelBox1;
    private CorePanelHorizontalLayout panelHorizontalLayout1;
    private CoreOutputText outputText1;


    public void setAdvertencia(boolean advertencia) {
        this.advertencia = advertencia;
    }

    public boolean isAdvertencia() {
        return advertencia;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setHtml1(HtmlHtml html1) {
        this.html1 = html1;
    }

    public HtmlHtml getHtml1() {
        return html1;
    }

    public void setHead1(HtmlHead head1) {
        this.head1 = head1;
    }

    public HtmlHead getHead1() {
        return head1;
    }

    public void setBody1(HtmlBody body1) {
        this.body1 = body1;
    }

    public HtmlBody getBody1() {
        return body1;
    }

    public void setForm1(CoreForm form1) {
        this.form1 = form1;
    }

    public CoreForm getForm1() {
        return form1;
    }

    public void setPanelBox1(CorePanelBox panelBox1) {
        this.panelBox1 = panelBox1;
    }

    public CorePanelBox getPanelBox1() {
        return panelBox1;
    }

    public void setPanelHorizontalLayout1(CorePanelHorizontalLayout panelHorizontalLayout1) {
        this.panelHorizontalLayout1 = panelHorizontalLayout1;
    }

    public CorePanelHorizontalLayout getPanelHorizontalLayout1() {
        return panelHorizontalLayout1;
    }

    public void setOutputText1(CoreOutputText outputText1) {
        this.outputText1 = outputText1;
    }

    public CoreOutputText getOutputText1() {
        return outputText1;
    }
}
