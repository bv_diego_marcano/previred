package cl.bicevida.previred.view.backing;

import cl.bicevida.previred.view.backing.consultas.ConsultaRendicionMB;
import cl.bicevida.previred.view.backing.consultas.ResumenRendicionMB;
import cl.bicevida.previred.view.util.JsfUtil;

import java.util.Date;
import java.util.Map;
import java.util.Random;

import javax.faces.component.html.HtmlForm;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import javax.servlet.http.HttpServletRequest;

public class FramePaginasMB {
    /**
     * Logger for this class
     */
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(FramePaginasMB.class);


    private String url;


    public String getExternalParameters() {
        boolean ret = true;
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = ctx.getExternalContext();
        Map m = ectx.getRequestParameterMap();
        Random rnd = new Random((new Date()).getTime());

        String pagina;
        String tipopl;
        
        /*
         * Reset manages bean
         */
        JsfUtil.resetManagedBean("RecaudacionTesoreriaMB");
        JsfUtil.resetManagedBean("MensajesMB");
        JsfUtil.resetManagedBean("DownloadExcelMB");
        JsfUtil.resetManagedBean("ResumenRendicionMB");
        JsfUtil.resetManagedBean("ConsultaRendicionMB");


        /*
         * Obtener Pagina de Inicio
         */
        if (m.get("pagina") != null) {
            pagina = m.get("pagina").toString();
        } else {
            pagina = "Mensajes"; //pagina por defecto
        }
        
         /*
          * Obtener tipo de planilla a procesar
          */
         if (m.get("tipopl") != null) {
             tipopl = m.get("tipopl").toString();
         } else {
             tipopl = ""; //pagina por defecto
         }
        
        if (pagina.equalsIgnoreCase("RecaudacionTesoreria")) {
            /*
             * Carga de Datos Administración
             */
            RecaudacionTesoreriaMB recaudacionTesoreriaMB = 
                (RecaudacionTesoreriaMB)JsfUtil.getManagedBean("RecaudacionTesoreriaMB");
            ret = recaudacionTesoreriaMB.loadData(1, "");  
            pagina = "RecaudacionTesoreria.jspx";
        }
        
        if (pagina.equalsIgnoreCase("RecaudacionOperacion")) {
            /*
             * Carga de Datos Administración
             */
            RecaudacionTesoreriaMB recaudacionTesoreriaMB = 
                (RecaudacionTesoreriaMB)JsfUtil.getManagedBean("RecaudacionTesoreriaMB");
            ret = recaudacionTesoreriaMB.loadData(2, tipopl);  
            pagina = "RecaudacionOperacion.jspx";
        }
        
        if (pagina.equalsIgnoreCase("ResumenRendicion")) {
            /*
             * Carga de Datos Administración
             */
            ResumenRendicionMB resumenRendicionMB = 
                (ResumenRendicionMB)JsfUtil.getManagedBean("ResumenRendicionMB");
            ret = resumenRendicionMB.loadData(tipopl);  
            pagina = "consultas/ResumenRendicion.jspx";
        }
        
        if (pagina.equalsIgnoreCase("ConsultaRendicion")) {
            /*
             * Carga de Datos Administración
             */
            ConsultaRendicionMB consultaRendicionMB = 
                (ConsultaRendicionMB)JsfUtil.getManagedBean("ConsultaRendicionMB");
            ret = consultaRendicionMB.loadData(tipopl);  
            pagina = "consultas/ConsultaRendicion.jspx";
        }
        
        if(!ret){
            /*
             * Mensaje de problemas
             */
            
            MensajesMB mensajesMB = 
                (MensajesMB)JsfUtil.getManagedBean("MensajesMB");
            mensajesMB.setMensaje(JsfUtil.getMessage("messages", "cl.bicevida.previred.frame.carga.datos"));
            pagina = "Mensajes.jspx";
        }
        
        if(pagina.equalsIgnoreCase("Mensajes")){
            /*
             * Mensaje de problemas
             */
            
            MensajesMB mensajesMB = 
                (MensajesMB)JsfUtil.getManagedBean("MensajesMB");
            mensajesMB.setMensaje(JsfUtil.getMessage("messages", "cl.bicevida.previred.frame.carga.datos"));
            pagina = "Mensajes.jspx";
        }
        

        /*if (!ret) {
            MensajesMB mensajesmb =
                (MensajesMB)JsfUtil.getManagedBean("MensajesMB");
            mensajesmb.setMensaje(JsfUtil.getMessage("messages", "cl.bicevida.mandato.error.general") + "<br>");
            mensajesmb.setErrorrendered(true);
            mensajesmb.setValidrendered(false);
            pagina = "Mensajes.jspx";
        }*/


        /*
         * Redireccionando
         */
        HttpServletRequest req = (HttpServletRequest)ectx.getRequest();
        return "http://" + req.getServerName() + ":" + req.getServerPort() + 
            ectx.getRequestContextPath() + "/faces/jsp/" + pagina + 
            "?nocache=" + rnd.nextInt();
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        url = this.getExternalParameters();
        return url;
    }
}
