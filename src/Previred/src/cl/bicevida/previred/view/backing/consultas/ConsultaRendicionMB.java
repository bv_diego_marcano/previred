package cl.bicevida.previred.view.backing.consultas;

import cl.bicevida.previred.common.dto.ConceptosDePagoDTO;
import cl.bicevida.previred.common.dto.EstadoDeProcesoDTO;
import cl.bicevida.previred.common.dto.RecaudadoresDTO;
import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;
import cl.bicevida.previred.common.utils.ExcelUtil;
import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.common.utils.NumeroUtil;
import cl.bicevida.previred.common.utils.RutUtil;
import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;
import cl.bicevida.previred.view.backing.DownloadExcelMB;
import cl.bicevida.previred.view.backing.MensajesMB;
import cl.bicevida.previred.view.util.JsfUtil;

import java.io.File;
import java.io.FileOutputStream;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import javax.servlet.http.HttpServletResponse;

import org.apache.myfaces.trinidad.component.core.input.CoreInputText;
import org.apache.myfaces.trinidad.component.core.input.CoreInputDate;
import org.apache.myfaces.trinidad.component.core.input.CoreSelectOneChoice;
import org.apache.myfaces.trinidad.component.core.nav.CoreCommandButton;

import org.apache.log4j.Logger;
import org.apache.myfaces.trinidad.component.core.CoreForm;
import org.apache.myfaces.trinidad.component.core.layout.CorePanelBox;
import org.apache.myfaces.trinidad.component.core.layout.CorePanelHorizontalLayout;
import org.apache.myfaces.trinidad.component.core.output.CoreOutputText;
import org.apache.myfaces.trinidad.component.core.output.CoreSpacer;
import org.apache.myfaces.trinidad.component.html.HtmlBody;
import org.apache.myfaces.trinidad.component.html.HtmlCellFormat;
import org.apache.myfaces.trinidad.component.html.HtmlHead;
import org.apache.myfaces.trinidad.component.html.HtmlHtml;
import org.apache.myfaces.trinidad.component.html.HtmlRowLayout;
import org.apache.myfaces.trinidad.component.html.HtmlTableLayout;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


public class ConsultaRendicionMB {

    /**
     * Logger for this class
     */
    private static final Logger logger = 
        Logger.getLogger(ConsultaRendicionMB.class);
    private PreviredEJB ejb;
    private MensajesMB mensajesmb;
    private UISelectItems selectItemsMedioPago = new UISelectItems();
    private UISelectItems selectItemsConceptoRec = new UISelectItems();
    private UISelectItems selectItemsEstadoProc = new UISelectItems();
    private HashMap<Integer, ConceptosDePagoDTO> conceptosDePago;
    private List<ResumenRendicionesVwDTO> lstResumenRendiciones;
    private Boolean showDetail = false;
    private Boolean consultaRecauProcesado = false;

    private String pathAbsoluteExcel;
    
    private String tipopl;
    private CoreInputText inputNumeroFolio;
    private CoreInputText inputRutContratante;
    private CoreSelectOneChoice selectOneChoiceMedioPago;
    private CoreInputText inputRutEmpleador;
    private CoreSelectOneChoice selectOneChoiceConceptoRec;
    private CoreSelectOneChoice selectOneChoiceEstadoProc;
    private CoreInputDate selectInputDateMenor;
    private CoreInputDate selectInputDateMayor;
    private CoreCommandButton commandButton1;
    private CoreCommandButton commandButton2;
    
    public boolean loadData(String tipopl) {
        boolean ret = false;
        try {
            ejb = ServiceLocator.getSessionEJBPrevired();

            setMedioDePagoComboBox(new ArrayList<RecaudadoresDTO>(ejb.getRecaudadores().values()));
            setEstadoProcesoComboBox(ejb.getEstadoDeProceso());
            setConceptoRecaudacionComboBox(new ArrayList<ConceptosDePagoDTO>((conceptosDePago = 
           
                                                                              ejb.getConceptosDePago()).values()));
                                                                              
            this.tipopl = tipopl;

            ret = true;
        } catch (Exception e) {
            logger.error("Se ha producido un error: " + e.getMessage(), e);
        }

        return ret;
    }

    private void setMedioDePagoComboBox(List<RecaudadoresDTO> recaudadores) throws Exception {
        List<SelectItem> recaudadoresitem = new ArrayList<SelectItem>();
        Iterator recaudadoresitr = recaudadores.iterator();
        while (recaudadoresitr.hasNext()) {
            RecaudadoresDTO recaudador = 
                (RecaudadoresDTO)recaudadoresitr.next();
            if (recaudador != null) {
                if(recaudador.getCod_banco() != null && recaudador.getCod_banco().length() > 0){
                    recaudadoresitem.add(new SelectItem(recaudador.getCod_banco(), 
                                                        recaudador.getDescripcion()));
                }
            }
        }

        this.selectItemsMedioPago.setValue(recaudadoresitem);
    }

    private void setEstadoProcesoComboBox(List<EstadoDeProcesoDTO> estadoProcesos) {
        List<SelectItem> estadoProcesositem = new ArrayList<SelectItem>();
        Iterator estadoProcesositr = estadoProcesos.iterator();
        while (estadoProcesositr.hasNext()) {
            EstadoDeProcesoDTO estadoProceso = 
                (EstadoDeProcesoDTO)estadoProcesositr.next();
            if (estadoProceso != null) {
                estadoProcesositem.add(new SelectItem(estadoProceso.getCodigoEstado(), 
                                                      estadoProceso.getDecripcionEstado()));
            }
        }

        this.selectItemsEstadoProc.setValue(estadoProcesositem);
    }

    private void setConceptoRecaudacionComboBox(List<ConceptosDePagoDTO> conceptoPagos) {
        List<SelectItem> conceptoPagositem = new ArrayList<SelectItem>();
        Iterator conceptoPagositr = conceptoPagos.iterator();
        while (conceptoPagositr.hasNext()) {
            ConceptosDePagoDTO conceptoPago = 
                (ConceptosDePagoDTO)conceptoPagositr.next();
            if (conceptoPago != null) {
                conceptoPagositem.add(new SelectItem(conceptoPago.getCodigoConcepto(), 
                                                     conceptoPago.getDescripcionConcepto()));
            }
        }

        this.selectItemsConceptoRec.setValue(conceptoPagositem);
    }


    public String doBuscarAction() {
        showDetail = false;
        consultaRecauProcesado = false;
        try {
            if (validateItemsConsulta()) {
                Date datefechadesdehora = null;
                Date datefechahastahora = null;
                Double folio = null;
                Integer rutEmpleador = null;
                Integer rutTrabajador = null;
                String conceptoPago = null;
                String idMedioPago = null;
                Integer idEstado = null;

                /*
                 * Get datos consulta
                 */

                /*
                 * Obteniendo fechas
                 */
                String fechadesdehora = 
                    FechaUtil.getFechaFormateoCustom((Date)selectInputDateMenor.getValue(), 
                                                     "dd/MM/yyyy") + 
                    " 00:00:00";
                String fechahastahora = 
                    FechaUtil.getFechaFormateoCustom((Date)selectInputDateMayor.getValue(), 
                                                     "dd/MM/yyyy") + 
                    " 23:59:59";
                datefechadesdehora = 
                        FechaUtil.toDate(fechadesdehora, "dd/MM/yyyy HH:mm:ss");
                datefechahastahora = 
                        FechaUtil.toDate(fechahastahora, "dd/MM/yyyy HH:mm:ss");

                /*
                 * Obteniendo folio
                 */
                if (inputNumeroFolio.getValue() != null && 
                    inputNumeroFolio.getValue().toString().trim().length() > 
                    0) {
                    folio = 
                            Double.parseDouble((String)inputNumeroFolio.getValue());
                }

                /*
                 * Obteniendo rutEmpleador
                 */
                if (inputRutEmpleador.getValue() != null && 
                    inputRutEmpleador.getValue().toString().trim().length() > 
                    0) {
                    String rut = 
                        RutUtil.desformateaRut((String)inputRutEmpleador.getValue());
                    rut = RutUtil.extractRut(rut);
                    rutEmpleador = Integer.parseInt(rut);
                }

                /*
                 * Obteniendo rutTrabajador
                 */
                if (inputRutContratante.getValue() != null && 
                    inputRutContratante.getValue().toString().trim().length() > 
                    0) {
                    String rut = 
                        RutUtil.desformateaRut((String)inputRutContratante.getValue());
                    rut = RutUtil.extractRut(rut);
                    rutTrabajador = Integer.parseInt(rut);
                }

                /*
                 * Obteniendo conceptoPago
                 */
                if (selectOneChoiceConceptoRec.getValue() != null) {
                    Integer cod_concepto = 
                        (Integer)selectOneChoiceConceptoRec.getValue();
                    ConceptosDePagoDTO conceptosDePagoDTO = 
                        conceptosDePago.get(cod_concepto);
                    conceptoPago = conceptosDePagoDTO.getCodigoIndstrumento();
                }

                /*
                 * Obteniendo idMedioPago
                 */
                if (selectOneChoiceMedioPago.getValue() != null) {
                    idMedioPago = (String)selectOneChoiceMedioPago.getValue();
                }

                /*
                 * Obteniendo idEstado
                 */
                if (selectOneChoiceEstadoProc.getValue() != null) {
                    idEstado = (Integer)selectOneChoiceEstadoProc.getValue();
                }

                ejb = ServiceLocator.getSessionEJBPrevired();
                lstResumenRendiciones = 
                        ejb.getConsultaRendiciones(datefechadesdehora, 
                                                   datefechahastahora, folio, 
                                                   rutEmpleador, rutTrabajador, 
                                                   conceptoPago, idMedioPago, 
                                                   idEstado, this.tipopl);
                if (lstResumenRendiciones != null && 
                    lstResumenRendiciones.size() > 0) {
                    showDetail = true;
                    consultaRecauProcesado = true;
                } else {
                    setAdvertencia(JsfUtil.getMessage("messages", 
                                                      "cl.bicevida.previred.consulta.busqueda.sinresultado"));
                }


            }
        } catch (Exception e) {
            logger.error("Se ha producido un excepci�n: " + e.getMessage(), e);
            setAdvertencia(JsfUtil.getMessage("messages", 
                                              "cl.bicevida.previred.consulta.exception"));
        }

        return "";

    }

    public String doLimpiarAction() {
        try {
            inputRutContratante.setValue(null);
            inputRutEmpleador.setValue(null);
            inputNumeroFolio.setValue(null);
            selectInputDateMayor.setValue(null);
            selectInputDateMenor.setValue(null);
            selectOneChoiceConceptoRec.setValue(null);
            selectOneChoiceMedioPago.setValue(null);
            selectOneChoiceEstadoProc.setValue(null);
            
            showDetail = false;
            consultaRecauProcesado = false;
        } catch (Exception e) {
            logger.error("Se ha producido un error: " + e.getMessage(), e);
            consultaRecauProcesado = false;
            showDetail = false;
        }
        
        return null;
       
    }

    public String doExcelAction() {
        String ret = "";
        try {
            if (generateExcel(this.lstResumenRendiciones)) {
                if (this.pathAbsoluteExcel != null) {
                    HttpServletResponse response = JsfUtil.getServletResponse();
                    response.setContentType("application/xls");
                    response.setHeader("Content-Disposition", "attachment; filename=\"RecaudacionTesoreria.xls\"");
                    FacesContext ctx = FacesContext.getCurrentInstance();
                    //DownloadExcelMB dTxt = (DownloadExcelMB)ctx.getApplication().createValueBinding("#{DownloadExcelMB}").getValue(ctx);
                    DownloadExcelMB dTxt = (DownloadExcelMB)ctx.getApplication().getExpressionFactory().createValueExpression(ctx.getELContext(),"#{DownloadExcelMB}",Object.class).getValue(ctx.getELContext());
                    dTxt.setAbsolutePathExcel(this.pathAbsoluteExcel);
                    dTxt.getArchivoExcel(response.getOutputStream());
                    response.getOutputStream().flush();
                   
                   // DownloadExcelMB downloadExcelMB = (DownloadExcelMB) JsfUtil.getManagedBean("DownloadExcelMB");
                   // downloadExcelMB.setAbsolutePathExcel(this.pathAbsoluteExcel);
                    ret = "excel";
                } else {
                    setAdvertencia(JsfUtil.getMessage("messages", 
                                                      "cl.bicevida.previred.recaudacion.excel.error.generacion"));
                }
            } else {
                setAdvertencia(JsfUtil.getMessage("messages", 
                                                  "cl.bicevida.previred.recaudacion.excel.error.generacion"));
            }


        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", 
                                              "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doProcesarAction() - Se ha producido un error: " + 
                         e.getMessage(), e);
        }

        return ret;
    }

    private boolean generateExcel(List<ResumenRendicionesVwDTO> lstResumenRendiciones) {
        boolean ret = false;
        try {
            HSSFWorkbook wb = 
                ExcelUtil.generateExcelConsultaRecaudacion(lstResumenRendiciones);
            if (wb != null) {
                /*
                 * Generacion del archivo excel
                 */
                String filename = 
                    "consultarec" + System.currentTimeMillis() + ".xls";
                //Directorio Absoluto del Contenedor
                //String dirWebContent = JsfUtil.getServletContext().getRealPath("/");
                String dirWebContent = JsfUtil.getSession().getServletContext().getRealPath("/");
                //Directorio Absoluto del Contenedor en donde se generar� excel
                this.pathAbsoluteExcel = dirWebContent + File.separator + "excel" + File.separator + filename;

                FileOutputStream out = new FileOutputStream(pathAbsoluteExcel);
                wb.write(out);
                out.flush();
                out.close();

                ret = true;
            }
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: " + e.getMessage(), e);
        }

        return ret;
    }

    private boolean validateItemsConsulta() {
        String mensaje = "";
        boolean ret = false;
        try {
            /*
             * Valida Fechas
             */
            if (selectInputDateMenor.getValue() != null && 
                selectInputDateMayor != null) {
                String fechaactual = 
                    FechaUtil.getFechaFormateoCustom(new java.util.Date(), 
                                                     "dd/MM/yyyy") + 
                    " 23:59:59";
                String fechadesdehora = 
                    FechaUtil.getFechaFormateoCustom((Date)selectInputDateMenor.getValue(), 
                                                     "dd/MM/yyyy") + 
                    " 00:00:00";
                String fechahastahora = 
                    FechaUtil.getFechaFormateoCustom((Date)selectInputDateMayor.getValue(), 
                                                     "dd/MM/yyyy") + 
                    " 23:59:59";

                Date datefechadesdehora = 
                    FechaUtil.toDate(fechadesdehora, "dd/MM/yyyy HH:mm:ss");
                Date datefechahastahora = 
                    FechaUtil.toDate(fechahastahora, "dd/MM/yyyy HH:mm:ss");
                Date datefechaactual = 
                    FechaUtil.toDate(fechaactual, "dd/MM/yyyy HH:mm:ss");

                //Valida fecha hasta mayor a la actual
                if (datefechahastahora.compareTo(datefechaactual) != 1) {
                    if (datefechadesdehora.compareTo(datefechahastahora) != 
                        1) {
                        /*
                         * Valida Rut Contratante
                         */
                        if (inputRutContratante.getValue() != null && 
                            inputRutContratante.getValue().toString().trim().length() > 
                            0) {
                            String rut = 
                                inputRutContratante.getValue().toString();
                            rut = RutUtil.desformateaRut(rut);

                            if (RutUtil.isRut(rut)) {
                                if (rut.indexOf("-") > -1) {
                                    String[] rutsplit = rut.split("-");
                                    String dv = RutUtil.calculaDv(rutsplit[0]);
                                    if (dv.equalsIgnoreCase(rutsplit[1])) {
                                        mensaje = mensaje + "";
                                        inputRutContratante.setValue(RutUtil.formateaRut(rut));
                                    } else {
                                        mensaje = 
                                                mensaje + JsfUtil.getMessage("messages", 
                                                                             "cl.bicevida.previred.consulta.rut.contratante.invalido");
                                    }
                                } else {
                                    mensaje = 
                                            mensaje + JsfUtil.getMessage("messages", 
                                                                         "cl.bicevida.previred.consulta.rut.contratante.invalido");
                                }
                            } else {
                                mensaje = 
                                        mensaje + JsfUtil.getMessage("messages", 
                                                                     "cl.bicevida.previred.consulta.rut.contratante.invalido");
                            }
                        }

                        /*
                         * Valida Rut Empleador
                         */
                        if (inputRutEmpleador.getValue() != null && 
                            inputRutEmpleador.getValue().toString().trim().length() > 
                            0) {
                            String rut = 
                                inputRutEmpleador.getValue().toString();
                            rut = RutUtil.desformateaRut(rut);
                            if (RutUtil.isRut(rut)) {
                                if (rut.indexOf("-") > -1) {
                                    String[] rutsplit = rut.split("-");
                                    String dv = RutUtil.calculaDv(rutsplit[0]);
                                    if (dv.equalsIgnoreCase(rutsplit[1])) {
                                        mensaje = mensaje + "";
                                        inputRutEmpleador.setValue(RutUtil.formateaRut(rut));
                                    } else {
                                        mensaje = 
                                                mensaje + JsfUtil.getMessage("messages", 
                                                                             "cl.bicevida.previred.consulta.rut.empleador.invalido");
                                    }
                                } else {
                                    mensaje = 
                                            mensaje + JsfUtil.getMessage("messages", 
                                                                         "cl.bicevida.previred.consulta.rut.empleador.invalido");
                                }

                            } else {
                                mensaje = 
                                        mensaje + JsfUtil.getMessage("messages", 
                                                                     "cl.bicevida.previred.consulta.rut.empleador.invalido");
                            }
                        }

                        /*
                         * Valida Numero de Folio
                         */
                        if (inputNumeroFolio.getValue() != null && 
                            inputNumeroFolio.getValue().toString().trim().length() > 0) {
                            if (NumeroUtil.isNumero(inputNumeroFolio.getValue().toString().trim())) {
                                if(Double.parseDouble(inputNumeroFolio.getValue().toString().trim()) < 0.0){
                                    mensaje = 
                                            mensaje + JsfUtil.getMessage("messages", 
                                                                         "cl.bicevida.previred.consulta.folio.menor");
                                }else{
                                    mensaje = mensaje + "";
                                }
                            } else {
                                mensaje = 
                                        mensaje + JsfUtil.getMessage("messages", 
                                                                     "cl.bicevida.previred.consulta.folio.invalido");
                            }
                        }
                    } else {
                        mensaje = 
                                mensaje + JsfUtil.getMessage("messages", "cl.bicevida.previred.consulta.fecha.desdemayorhasta");
                    }
                } else {
                    mensaje = 
                            mensaje + JsfUtil.getMessage("messages", "cl.bicevida.previred.consulta.fecha.hastamayoractual");
                }
            } else {
                mensaje = 
                        mensaje + JsfUtil.getMessage("messages", "cl.bicevida.previred.consulta.fecha.noingresado");
            }
        } catch (Exception e) {
            logger.error("Se ha producido una excepci�n: " + e.getMessage(), 
                         e);
            mensaje = 
                    JsfUtil.getMessage("messages", "cl.bicevida.previred.consulta.exception");
        }

        if (mensaje != null && mensaje.length() > 0) {
            setAdvertencia(mensaje);
        } else {
            ret = true;
        }

        return ret;
    }

    public void setAdvertencia(String mensaje) {
        mensajesmb = (MensajesMB)JsfUtil.getManagedBean("MensajesMB");
        mensajesmb.setAdvertencia(true);
        mensajesmb.setMensaje(mensaje);
    }


    public void setSelectItemsMedioPago(UISelectItems selectItemsMedioPago) {
        this.selectItemsMedioPago = selectItemsMedioPago;
    }

    public UISelectItems getSelectItemsMedioPago() {
        return selectItemsMedioPago;
    }


    public void setSelectItemsConceptoRec(UISelectItems selectItemsConceptoRec) {
        this.selectItemsConceptoRec = selectItemsConceptoRec;
    }

    public UISelectItems getSelectItemsConceptoRec() {
        return selectItemsConceptoRec;
    }


    public void setSelectItemsEstadoProc(UISelectItems selectItemsEstadoProc) {
        this.selectItemsEstadoProc = selectItemsEstadoProc;
    }

    public UISelectItems getSelectItemsEstadoProc() {
        return selectItemsEstadoProc;
    }


    public void setLstResumenRendiciones(List<ResumenRendicionesVwDTO> lstResumenRendiciones) {
        this.lstResumenRendiciones = lstResumenRendiciones;
    }

    public List<ResumenRendicionesVwDTO> getLstResumenRendiciones() {
        return lstResumenRendiciones;
    }

    public void setShowDetail(Boolean showDetail) {
        this.showDetail = showDetail;
    }

    public Boolean getShowDetail() {
        return showDetail;
    }

    public void setConsultaRecauProcesado(Boolean consultaRecauProcesado) {
        this.consultaRecauProcesado = consultaRecauProcesado;
    }

    public Boolean getConsultaRecauProcesado() {
        return consultaRecauProcesado;
    }

    public void setTipopl(String tipopl) {
        this.tipopl = tipopl;
    }

    public String getTipopl() {
        return tipopl;
    }

    public void setInputNumeroFolio(CoreInputText inputNumeroFolio) {
        this.inputNumeroFolio = inputNumeroFolio;
    }

    public CoreInputText getInputNumeroFolio() {
        return inputNumeroFolio;
    }

    public void setInputRutContratante(CoreInputText inputRutContratante) {
        this.inputRutContratante = inputRutContratante;
    }

    public CoreInputText getInputRutContratante() {
        return inputRutContratante;
    }

    public void setSelectOneChoiceMedioPago(CoreSelectOneChoice selectOneChoiceMedioPago) {
        this.selectOneChoiceMedioPago = selectOneChoiceMedioPago;
    }

    public CoreSelectOneChoice getSelectOneChoiceMedioPago() {
        return selectOneChoiceMedioPago;
    }

    public void setInputRutEmpleador(CoreInputText inputRutEmpleador) {
        this.inputRutEmpleador = inputRutEmpleador;
    }

    public CoreInputText getInputRutEmpleador() {
        return inputRutEmpleador;
    }

    public void setSelectOneChoiceConceptoRec(CoreSelectOneChoice selectOneChoiceConceptoRec) {
        this.selectOneChoiceConceptoRec = selectOneChoiceConceptoRec;
    }

    public CoreSelectOneChoice getSelectOneChoiceConceptoRec() {
        return selectOneChoiceConceptoRec;
    }

    public void setSelectOneChoiceEstadoProc(CoreSelectOneChoice selectOneChoiceEstadoProc) {
        this.selectOneChoiceEstadoProc = selectOneChoiceEstadoProc;
    }

    public CoreSelectOneChoice getSelectOneChoiceEstadoProc() {
        return selectOneChoiceEstadoProc;
    }

    public void setSelectInputDateMenor(CoreInputDate selectInputDateMenor) {
        this.selectInputDateMenor = selectInputDateMenor;
    }

    public CoreInputDate getSelectInputDateMenor() {
        return selectInputDateMenor;
    }

    public void setSelectInputDateMayor(CoreInputDate selectInputDateMayor) {
        this.selectInputDateMayor = selectInputDateMayor;
    }

    public CoreInputDate getSelectInputDateMayor() {
        return selectInputDateMayor;
    }

    public void setCommandButton1(CoreCommandButton commandButton1) {
        this.commandButton1 = commandButton1;
    }

    public CoreCommandButton getCommandButton1() {
        return commandButton1;
    }

    public void setCommandButton2(CoreCommandButton commandButton2) {
        this.commandButton2 = commandButton2;
    }

    public CoreCommandButton getCommandButton2() {
        return commandButton2;
    }

}
