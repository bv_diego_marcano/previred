package cl.bicevida.previred.view.backing;

import cl.bicevida.previred.async.ProcesarCajaThread;
import cl.bicevida.previred.async.procesoautomatico.MoverPorProcesar;
import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.common.dto.PKTransaccionPagoDTO;
import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;
import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;

import cl.bicevida.previred.view.util.JsfUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.myfaces.trinidad.component.core.data.CoreTable;
//import org.apache.myfaces.trinidad.component.core.data.CoreTableSelectMany;


/**
 * Managed Bean para reprocesasmiento de transacciones
 * que fueron rachazas por sistema de caja y se deben
 * enviar de forma manual.
 */
public class ReprocesadorManualPagosMB {
    /**
     * Logger for this class
     */
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ReprocesadorManualPagosMB.class);
    private List<ResumenRendicionesVwDTO> transacciones;
    private PreviredEJB ejb;
    private CoreTable tabla;
  //  private CoreTableSelectMany tableSelectManyRow;
    private String totalRegistros;
    private MoverPorProcesar mpp;
    private Integer mesPeriodo;
    private Integer anioPeriodo;
    private String mensaje;
    private boolean isProcesado;
    private Integer anioAProcesar;
    private Integer mesAProcesar;
    
    /**
     * Constructor inicial
     */
    public ReprocesadorManualPagosMB(){
        ejb = ServiceLocator.getSessionEJBPrevired();
        FacesContext ctx = FacesContext.getCurrentInstance();
        ExternalContext ectx = ctx.getExternalContext();
        Map m = ectx.getRequestParameterMap();
        String tipopl;
        
        if (m.get("tipopl") != null) {
            tipopl = m.get("tipopl").toString();
        } else {
            tipopl = ""; //pagina por defecto
        }
        
        if(tipopl != null && tipopl.length() > 0){
            JsfUtil.setAttributeInSession("tipoRend", tipopl);
        }else{
            tipopl = JsfUtil.getAttributeInSession("tipoRend");
            JsfUtil.setAttributeInSession("tipoRend", tipopl);
        }
        java.util.Date fechaCon = FechaUtil.sumarMesesFecha(new java.util.Date(), -1);
        this.mesPeriodo = FechaUtil.getMesFecha(fechaCon);
        this.anioPeriodo = FechaUtil.getAnoFecha(fechaCon);
        this.mesAProcesar = FechaUtil.getMesFecha(new java.util.Date())-1;
        this.anioAProcesar = FechaUtil.getAnoFecha(new java.util.Date());
        mpp = new MoverPorProcesar();
        
    }
 
 
    /**
     * Accion de Filtrado de Datos
     */
    public String doFiltrarDatos() {
    //    this.tableSelectManyRow = new CoreTableSelectMany();
        this.tabla = new CoreTable();
        
        String tipopl = JsfUtil.getAttributeInSession("tipoRend");
        transacciones = ejb.getReprocesosManualPagosByPeriodo(tipopl, this.mesPeriodo, this.anioPeriodo);
        this.setTotalRegistros("Total Registros :" + (transacciones != null ? transacciones.size() : new Integer(0)));
        
        return null;
    }
    
    public String doLimpiar() {
      //  this.tableSelectManyRow = new CoreTableSelectMany();
        this.tabla = new CoreTable();
        transacciones = null;
        this.mensaje=null;
        return null;
    }
    /**
     * Procesa el envio manual de los resgistros
     * y adicionalmente actualiza su estado en estado
     * de procesamiento en modo asyncrono
     */
    public String doReprocesar() {
        try {

            //Recupera Items seleccionados
            String tipopl = JsfUtil.getAttributeInSession("tipoRend");
            List<ResumenRendicionesVwDTO> procesar = new ArrayList<ResumenRendicionesVwDTO>();
            List<PKTransaccionPagoDTO> updates = new ArrayList<PKTransaccionPagoDTO>();


            //Set selection = this.tabla.getSelectionState().getKeySet();

            Iterator selection = tabla.getSelectedRowKeys().iterator();
            Object oldKey = tabla.getRowKey();
            //if(selection.size() > 0){
            if (tabla.getSelectedRowKeys().size() > 0) {
                //fix Iterator itrfilas = selection.iterator();
                //while (itrfilas.hasNext()) {
                while (selection.hasNext()) {
                    //fix: String rowKey = (String)itrfilas.next();
                    Object rowKey = selection.next();
                    Object rowobj = this.tabla.getRowData(rowKey);
                    if (rowobj != null) {
                        ResumenRendicionesVwDTO row = (ResumenRendicionesVwDTO) rowobj;
                        row.setTipoTransaccion(tipopl);
                        PKTransaccionPagoDTO upt = new PKTransaccionPagoDTO();
                        upt.setIdCarga(row.getId_carga());
                        upt.setIdCargaPlanilla(row.getId_carga_planilla());
                        upt.setFolioPlanilla(row.getId_folio_abono());
                        upt.setCodigoRecaudador(row.getCodigo_recaudador());
                        upt.setNewDescripcion("");
                        upt.setNewEstado(Constant.PROCESO_EJECUCION_MANUAL_CAJAS);
                        upt.setFolioCaja(0.0);

                        //AGREGA A PROCESAMIENTO
                        procesar.add(row);

                        //AGREGA A ACTUALIZXACION DE DATOS
                        updates.add(upt);

                    }
                }

                // ACTUALIZA EL ESTADO EN BASE DE DATOS DE LAS TRANSACCIONES Y ENVIA A COLA DE PROCESAMIENTO
                if (updates.size() > 0) {
                    //Actualiza el paquete de transacciones
                    System.out.println("Actualizando estado de transacciones en DB para que volver a enviarlas");
                    ejb.changeStatusTransaccionProccess(updates);
                    //Marcar Transacciones en estado de Procesamiento
                    System.out.println("Procesando en Cajas Asyncronamente con Cola JMS");
                    ejb.doReprocesarJMSAsync(procesar);
                    //Actualiza lista de presentacion
                    doFiltrarDatos();
                }
            }
            tabla.getSelectedRowKeys().clear();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Se ha producido una excepcion: " + e.getMessage());
        }


        return null;
    }
    
   

    public void setTransacciones(List<ResumenRendicionesVwDTO> transacciones) {
        this.transacciones = transacciones;
    }

    public List<ResumenRendicionesVwDTO> getTransacciones() {
        return transacciones;
    }

    public void setTabla(CoreTable tabla) {
        this.tabla = tabla;
    }

    public CoreTable getTabla() {
        return tabla;
    }

   
    /**
     * Actualizacion
     * @return
     */
    public String doRefresh() {
        doFiltrarDatos();
        return null;
    }

    public void setTotalRegistros(String totalRegistros) {
        this.totalRegistros = totalRegistros;
    }

    public String getTotalRegistros() {
        return totalRegistros;
    }


    public void setMesPeriodo(Integer mesPeriodo) {
        this.mesPeriodo = mesPeriodo;
    }

    public Integer getMesPeriodo() {
        return mesPeriodo;
    }

    public void setAnioPeriodo(Integer anioPeriodo) {
        this.anioPeriodo = anioPeriodo;
    }

    public Integer getAnioPeriodo() {
        return anioPeriodo;
    }

  /*  public void setTableSelectManyRow(CoreTableSelectMany tableSelectManyRow) {
        this.tableSelectManyRow = tableSelectManyRow;
    }

    public CoreTableSelectMany getTableSelectManyRow() {
        return tableSelectManyRow;
    }
  */

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setIsProcesado(boolean isProcesado) {
        this.isProcesado = isProcesado;
    }

    public boolean isIsProcesado() {
        return isProcesado;
    }
    public void moverArchivosFinMes(){
        mpp.doMoverArchivosByFecha(this.anioAProcesar,this.mesAProcesar);
        this.mensaje= "Se comienza a mover los archivos para ser procesados.";
        this.isProcesado=true;
    }

    public void setAnioAProcesar(Integer anioAProcesar) {
        this.anioAProcesar = anioAProcesar;
    }

    public Integer getAnioAProcesar() {
        return anioAProcesar;
    }

    public void setMesAProcesar(Integer mesAProcesar) {
        this.mesAProcesar = mesAProcesar;
    }

    public Integer getMesAProcesar() {
        return mesAProcesar;
    }


}
