package cl.bicevida.previred.view.backing;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.faces.component.html.HtmlForm;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class DownloadExcelMB {
    private String absolutePathExcel;

    public void getArchivoExcel(OutputStream out) {
        try {
            if (this.absolutePathExcel != null) {

                InputStream inp = new FileInputStream(this.absolutePathExcel);
                HSSFWorkbook wb = new HSSFWorkbook(new POIFSFileSystem(inp));
                inp.close();
                wb.write(out);
                out.flush();
                
                File f = new File(this.absolutePathExcel);
                f.delete();

                /*InputStream inputStream =
                    new FileInputStream(this.absolutePathExcel);

                byte buf[] = new byte[1024];
                int len;
                while ((len = inputStream.read(buf)) > 0){
                    out.write(buf, 0, len);
                }
                out.flush();
                //out.close();
                inputStream.close();*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void setAbsolutePathExcel(String absolutePathExcel) {
        this.absolutePathExcel = absolutePathExcel;
    }

    public String getAbsolutePathExcel() {
        return absolutePathExcel;
    }
}
