package cl.bicevida.previred.view.backing;

import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.common.dto.CargaArchivosDTO;
import cl.bicevida.previred.common.dto.ConvenioPagoDTO;
import cl.bicevida.previred.common.dto.EmpresasDTO;
import cl.bicevida.previred.common.dto.EstadoDeProcesoDTO;
import cl.bicevida.previred.common.dto.MediosDePagoDTO;
import cl.bicevida.previred.common.dto.MotivoRechazosDTO;
import cl.bicevida.previred.common.dto.RendProcesoAutomatico;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;
import cl.bicevida.previred.common.dto.TipoArchivoDTO;
import cl.bicevida.previred.common.utils.ExcelUtil;
import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.common.utils.NumeroUtil;
import cl.bicevida.previred.common.utils.StringUtil;
import cl.bicevida.previred.core.abono.PreviredCore;
import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;
import cl.bicevida.previred.util.ResourcesAsyncUtil;
import cl.bicevida.previred.util.ResourcesMessagesUtil;
import cl.bicevida.previred.view.job.JobRendicion;
import cl.bicevida.previred.view.job.TriggerRendicion;
import cl.bicevida.previred.view.util.EmailUtil;
import cl.bicevida.previred.view.util.JsfUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import java.io.OutputStream;

import java.net.InetAddress;
import java.net.UnknownHostException;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import javax.servlet.http.HttpServletResponse;

import org.apache.myfaces.trinidad.component.core.input.CoreInputFile;
import org.apache.myfaces.trinidad.component.core.input.CoreInputHidden;
import org.apache.myfaces.trinidad.component.core.input.CoreSelectOneChoice;
import org.apache.myfaces.trinidad.component.core.nav.CoreCommandButton;
import org.apache.myfaces.trinidad.event.PollEvent;
import org.apache.myfaces.trinidad.model.UploadedFile;

//import org.ajax4jsf.ajax.html.AjaxPoll;

import org.apache.log4j.Logger;
import org.apache.myfaces.trinidad.component.core.CoreForm;
import org.apache.myfaces.trinidad.component.core.input.CoreInputText;
import org.apache.myfaces.trinidad.component.core.input.CoreSelectBooleanCheckbox;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.quartz.JobKey;


public class RecaudacionTesoreriaMB {
    /**
     * Logger for this class
     */
    private static final Logger logger = 
        Logger.getLogger(RecaudacionTesoreriaMB.class);

    //private static final Constant Constant = new Constant();

    private UISelectItems selectItemsEmpresa = new UISelectItems();
    private UISelectItems selectItemsMedioPago = new UISelectItems();
    private UISelectItems selectItemsNombreArchivo = new UISelectItems();
    private UISelectItems selectItemsMotivoRechazos = new UISelectItems();
    private PreviredEJB ejb;

    private List<TipoArchivoDTO> tiposArchivos;
    private List<ResumenAbonoDTO> resumenAbonoProcesado;
    private List<ResumenAbonoDTO> planillaNoInformados;
    private TriggerRendicion trigger;
    private String fileupload;
    private Double idCargaConsulta;
    private boolean renderpoll;
    private int timeout;
    private MensajesMB mensajesmb;
    private Boolean showDetail = Boolean.FALSE;
    private Boolean showMensajeEspera  = Boolean.FALSE;
    private Boolean showDetailPlanilla  = Boolean.FALSE;
    private Boolean showDescarga = Boolean.FALSE;
    private String pathAbsoluteExcel;
    private String tipopl;
   

    private static PreviredCore previredCore = null;
    private String errorFinalizar = null;
    
    
    /*FORM: RecaudacionOperacion.jspx, RecaudacionTesoreria.jspx*/
    private CoreForm recaudacionForm;
    private CoreCommandButton commandButtonProcesar;
   // private AjaxPoll poll;
    private CoreSelectOneChoice selectOneChoiceEmpresa;
    private CoreSelectOneChoice selectOneChoiceMedioPago;
    private CoreSelectOneChoice selectOneChoiceNombreArchivo;
    private CoreInputFile inputFileAdjunto;
    
    /*FORM: DetalleConsultaRendicion.jspx*/
    private CoreCommandButton cbDoExcelAction;
    
    /*FORM: RecaudacionTesoreria.jspx*/
    private CoreCommandButton cbFinProcesoSendCorreoAction;
    
    /*FORM: DetalleAbonoTesoreria.jspx*/
    private CoreInputHidden inputHiddenRow1;
    private CoreSelectBooleanCheckbox enableinput;
    private CoreSelectOneChoice selectMotivoRechazos;
    private CoreInputHidden inputHiddenRow3;
    private CoreInputText inputText2;
    private String nombreServidor;

    public RecaudacionTesoreriaMB() {
        previredCore = new PreviredCore();
        //String dirWebContent = JsfUtil.getServletContext().getRealPath("/");
        String dirWebContent = JsfUtil.getSession().getServletContext().getRealPath("/");
        String fileconfig = dirWebContent + File.separator + "config" + File.separator + "configfileabono.xml";
        previredCore.loadConfig(fileconfig);
    }


    public boolean loadData(Integer codigo, String tipopl) {
        boolean ret = false;
        try {
            ejb = ServiceLocator.getSessionEJBPrevired();
            setEmpresasComboBox(ejb.getEmpresas());
            setMedioPagoComboBox(ejb.getMediosDePago(tipopl));
            setTipoArchivoComboBox(tiposArchivos = ejb.getTipoArchivo(codigo));
            setMotivoRechazosComboBox(ejb.getMotivoRechazos());
            this.tipopl = tipopl;
            ret = true;
        } catch (Exception e) {
            logger.error("Se ha producido un error: " + e.getMessage(), e);
        }

        return ret;
    }

    /**
     * Proceso de carga y validacion
     * antes de procesar achivo
     * @return
     */
    public String doProcesarAction() {
        try {        
            this.showDetail = false;
            /*
             * Validacion de los  datos ingresados
             */
            if (doValidarDatosIngresados()) {
                /*
                 * Validar existencia del convenio empresa-Medio pago
                 */
                //Obtener convenio
                ejb = ServiceLocator.getSessionEJBPrevired();
                if (ejb == null) return null;
                
                Boolean isProcesoCorriendo = ejb.existProcesoEnEjecucion(Constant.PROCESANDO);
                if (!isProcesoCorriendo) { //validar que no haya proceso corriendo
                    
                    ConvenioPagoDTO convenioPagoDTO = ejb.getConvenioDePago((Integer)this.selectOneChoiceEmpresa.getValue(), (Integer)this.selectOneChoiceMedioPago.getValue());
    
                    if (convenioPagoDTO != null) {
                        /*
                         * Validacion del archivo
                         */
                        
                        UploadedFile file = (UploadedFile)this.inputFileAdjunto.getValue();
                        if (file == null) return null;
                        String filename = file.getFilename();
                        if(ejb.existeArchivoCargado(filename.trim()) < 1){
                            if (doValidarArchivo(filename, convenioPagoDTO)) {
                    
                                /*
                                 * Carga de Archivo para su proceso
                                 */
                                if (doLoadFile(file, convenioPagoDTO)) {
                                    /*
                                     * Insercion de la carga a la Base de Datos
                                     */
                                    
                                    Date fecha_creacion = null;
                                    if( Constant.PLANILLA_ANDES.equalsIgnoreCase(this.tipopl)  || Constant.PLANILLA_LAARAUCANA.equalsIgnoreCase(this.tipopl)){
                                        fecha_creacion = FechaUtil.getFecha("ddMMyyyy", filename.substring(14,22));
                                        
                                        // Creacion de Carga Abono
                                        CargaArchivosDTO cargaDTOAbono = new CargaArchivosDTO();
                                        cargaDTOAbono.setConvenio(convenioPagoDTO);
                                        cargaDTOAbono.setFecha_creacion(fecha_creacion);
                                        cargaDTOAbono.setId_tipo_archivo(1);
                                        cargaDTOAbono.setEstado_proceso(new Integer("0"));
                                        cargaDTOAbono.setFecha_y_hora_upload(new Date());
                                        cargaDTOAbono.setNombre_fisico_archivo(filename);
                                        cargaDTOAbono.setRemote_user(JsfUtil.getRemoteUser());
                                        ejb = ServiceLocator.getSessionEJBPrevired();
                                        cargaDTOAbono = ejb.insertCargaArchivos(cargaDTOAbono);
                                        
                                        //Cracion Catga Planilla
                                        
                                         CargaArchivosDTO cargaDTO = new CargaArchivosDTO();
                                         cargaDTO.setConvenio(convenioPagoDTO);
                                         cargaDTO.setFecha_creacion(fecha_creacion);
                                         cargaDTO.setId_tipo_archivo((Integer)this.selectOneChoiceNombreArchivo.getValue());
                                         cargaDTO.setEstado_proceso(new Integer("0"));
                                         cargaDTO.setFecha_y_hora_upload(new Date());
                                         cargaDTO.setNombre_fisico_archivo(filename);
                                         cargaDTO.setRemote_user(JsfUtil.getRemoteUser());
                                         ejb = ServiceLocator.getSessionEJBPrevired();
                                         cargaDTO = ejb.insertCargaArchivos(cargaDTO);
                                         ejb.guardarProcesoRendicion(new RendProcesoAutomatico(cargaDTO.getId_carga().longValue(),
                                                                                              (Integer)this.selectOneChoiceNombreArchivo.getValue(),
                                                                                              (Integer)this.selectOneChoiceMedioPago.getValue(),
                                                                                              new Date(),
                                                                                              this.nombreServidor,
                                                                                              Constant.PROCESANDO,
                                                                                              filename,
                                                                                              Constant.DESC_PROCESANDO,
                                                                                              "Procesando..."));
                                         if (cargaDTO.isProcesado()) {
                                             idCargaConsulta = cargaDTO.getId_carga();
                                             /*
                                              * Inicio del job
                                              */
                                             trigger = new TriggerRendicion();
                                             trigger.run(cargaDTO.getId_carga(), (Integer)this.selectOneChoiceNombreArchivo.getValue(), fileupload, previredCore, this.tipopl, cargaDTOAbono.getId_carga());
                                             trigger.getScheduler().start();
                                             logger.debug("doProcesarAction() - Job activado");
                                             /*
                                              * Activacion de refresh
                                              */
                                             this.timeout = Integer.parseInt(JsfUtil.getMessage("configurationbice", "cl.bicevida.refresh.timeout.miliseg"));
                                             this.renderpoll = true;
                    
                                             /*
                                              * Mostrar mensaje de espera
                                              */
                                             this.showMensajeEspera = true;
                                             logger.debug("doProcesarAction() - Refresh activado");
                    
                                         } else {
                                             setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.crearcarga.problema"));
                                         } 
                                    }else{
                                        fecha_creacion = FechaUtil.getFecha("yyyyMMdd_HHmm", filename.substring(8, 21)); 
                                        
                                        CargaArchivosDTO cargaDTO = new CargaArchivosDTO();
                                        cargaDTO.setConvenio(convenioPagoDTO);
                                        cargaDTO.setFecha_creacion(fecha_creacion);
                                        cargaDTO.setId_tipo_archivo((Integer)this.selectOneChoiceNombreArchivo.getValue());
                                        cargaDTO.setEstado_proceso(new Integer("0"));
                                        cargaDTO.setFecha_y_hora_upload(new Date());
                                        cargaDTO.setNombre_fisico_archivo(filename);
                                        cargaDTO.setRemote_user(JsfUtil.getRemoteUser());
                                        ejb = ServiceLocator.getSessionEJBPrevired();
                                        cargaDTO = ejb.insertCargaArchivos(cargaDTO);
                                        if (cargaDTO.isProcesado()) {
                                            idCargaConsulta = cargaDTO.getId_carga();
                                            /*
                                             * Inicio del job
                                             */
                                            trigger = new TriggerRendicion();
                                            trigger.run(cargaDTO.getId_carga(), 
                                                        (Integer)this.selectOneChoiceNombreArchivo.getValue(), 
                                                        fileupload, previredCore, this.tipopl, 0.0);
                                            trigger.getScheduler().start();
                                            logger.debug("doProcesarAction() - Job activado");
                                            /*
                                             * Activacion de refresh
                                             */
                                            this.timeout =  Integer.parseInt(JsfUtil.getMessage("configurationbice", "cl.bicevida.refresh.timeout.miliseg"));
                                            this.renderpoll = true;
                    
                                            /*
                                             * Mostrar mensaje de espera
                                             */
                                            this.showMensajeEspera = true;
                                            logger.debug("doProcesarAction() - Refresh activado");
                    
                                        } else {
                                            setAdvertencia(JsfUtil.getMessage("messages", 
                                                                              "cl.bicevida.previred.recaudacion.crearcarga.problema"));
                                        }                                
                                    }
                                } else {
                                    setAdvertencia(JsfUtil.getMessage("messages", 
                                                                      "cl.bicevida.previred.recaudacion.cargaarchivo.load.problema"));
                                }
                            } else {
                                setAdvertencia(JsfUtil.getMessage("messages", 
                                                                  "cl.bicevida.previred.recaudacion.nombrearchivo.tipoarchivo.incorrecto"));
                            }
                        }else{
                            setAdvertencia("Archivo ya fue cargado");
                        }
                    } else {
                        setAdvertencia(JsfUtil.getMessage("messages", 
                                                          "cl.bicevida.previred.recaudacion.convenio.noexiste"));
                    }
                } else {
                    logger.error(ResourcesMessagesUtil.getProperty("cl.bicevida.previred.proceso.automatico.en.ejecucion"));
                    setAdvertencia(JsfUtil.getMessage("messages", 
                                                      "cl.bicevida.previred.proceso.automatico.en.ejecucion"));
                }
            }
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doProcesarAction() - Se ha producido un error: " +   e.getMessage(), e);
        }

        return "";
    }


    /**
     * Actualiza proceso de envio
     * @return
     */
    public String doRefrescarAction() {
        try {
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
            CargaArchivosDTO cargaArchivos =  ejb.getCargaArchivosByIdCarga(idCargaConsulta);

            if (cargaArchivos != null) {
                int proceso = cargaArchivos.getEstado_proceso().intValue();
                if (!((proceso == Constant.PROCESO_PENDIENTE) || 
                      (proceso == Constant.PROCESO_LECTURA_ARCHIVO) || 
                      (proceso == Constant.PROCESO_CARGA_BASEDATOS))) {
                    /*
                     * Desactivar refresh
                     */
                    this.renderpoll = false;
                    logger.debug("doRefrescarAction() - Refresh desactivado");
                    /*
                     * Desactivar job
                     */
                    trigger.jobDestroyed(Boolean.TRUE);
                    logger.debug("doRefrescarAction() - Job desactivado");

                    /*
                     * ocultar mensaje de espera
                     */
                    this.showMensajeEspera = false;
                }

                if (proceso == Constant.PROCESO_FINALIZADO_CARGA) {
                    if (cargaArchivos.getId_tipo_archivo().intValue() == 
                        1) { //tipo Abono
                        resumenAbonoProcesado = 
                                ejb.getAbonoByIdCarga(idCargaConsulta);
                        this.showDetail = true;
                        this.showDetailPlanilla = false;
                    }
                }

                if (proceso == Constant.PROCESO_FINALIZADO_) {
                    if (cargaArchivos.getId_tipo_archivo().intValue() == 
                        2) { //tipo Abono
                        //Obtener id de carga de abono asociado a la planilla
                        resumenAbonoProcesado = ejb.getAbonoAndPlanillaByIdCarga(cargaArchivos.getId_carga_asoc(),   cargaArchivos.getId_carga());
                        planillaNoInformados = ejb.getPlanillaNoInformadosByIdCarga(cargaArchivos.getId_carga(), cargaArchivos.getId_carga_asoc());

                        this.showDetail = false;
                        this.showDetailPlanilla = true;
                        this.showDescarga = true;
                    }
                }
                
                if((proceso == Constant.PROCESO_PROBLEMA_PLANILLA_ABONO) ||
                   (proceso == Constant.PROCESO_PROBLEMA_RESUMEN_ABONO)){
                    setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.exception"));                             
                }

            }
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doRefrescarAction() - Se ha producido un error: " + e.getMessage(), e);
        }
        return "";
    }

    /**
     * Validacion de parametros ingresados
     * @return
     * @throws Exception
     */
    private boolean doValidarDatosIngresados() throws Exception {
        boolean ret = false;
        if (this.selectOneChoiceEmpresa.getValue() instanceof 
            java.lang.Integer) {
            Integer empresa = (Integer)this.selectOneChoiceEmpresa.getValue();
            if (empresa != null) {
                if (this.selectOneChoiceMedioPago.getValue() instanceof 
                    java.lang.Integer) {
                    Integer mediopago = 
                        (Integer)this.selectOneChoiceMedioPago.getValue();
                    if (mediopago != null) {
                        if (this.selectOneChoiceNombreArchivo.getValue() instanceof 
                            java.lang.Integer) {
                            Integer tipoarchivo = 
                                (Integer)this.selectOneChoiceNombreArchivo.getValue();
                            if (tipoarchivo != null) {
                                if (this.inputFileAdjunto.getValue() instanceof 
                                    UploadedFile) {
                                    UploadedFile uppathfile = 
                                        (UploadedFile)this.inputFileAdjunto.getValue();
                                    if (uppathfile != null) {
                                        if (uppathfile.getFilename() != null && 
                                            uppathfile.getFilename().length() > 
                                            0) {
                                            ret = true;
                                        } else {
                                            setAdvertencia(JsfUtil.getMessage("messages", 
                                                                              "cl.bicevida.previred.recaudacion.adjuntararchivo.noingresado"));
                                        }
                                    } else {
                                        setAdvertencia(JsfUtil.getMessage("messages", 
                                                                          "cl.bicevida.previred.recaudacion.adjuntararchivo.noingresado"));
                                    }
                                } else {
                                    setAdvertencia(JsfUtil.getMessage("messages", 
                                                                      "cl.bicevida.previred.recaudacion.adjuntararchivo.noingresado"));
                                }
                            } else {
                                setAdvertencia(JsfUtil.getMessage("messages", 
                                                                  "cl.bicevida.previred.recaudacion.tipoarchivo.noseleccionado"));
                            }
                        } else {
                            setAdvertencia(JsfUtil.getMessage("messages", 
                                                              "cl.bicevida.previred.recaudacion.tipoarchivo.noseleccionado"));
                        }
                    } else {
                        setAdvertencia(JsfUtil.getMessage("messages", 
                                                          "cl.bicevida.previred.recaudacion.mediopago.noseleccionado"));
                    }
                } else {
                    setAdvertencia(JsfUtil.getMessage("messages", 
                                                      "cl.bicevida.previred.recaudacion.mediopago.noseleccionado"));
                }
            } else {
                setAdvertencia(JsfUtil.getMessage("messages", 
                                                  "cl.bicevida.previred.recaudacion.empresa.noseleccionado"));
            }
        } else {
            setAdvertencia(JsfUtil.getMessage("messages", 
                                              "cl.bicevida.previred.recaudacion.empresa.noseleccionado"));
        }
        return ret;
    }


    /**
     * Validacion de archivo
     * @param filename
     * @param convenioPagoDTO
     * @return
     * @throws Exception
     */
    private boolean doValidarArchivo(String filename,  ConvenioPagoDTO convenioPagoDTO) {
        boolean ret = false;
        try {
            String extension = filename.substring(filename.length() - 3, filename.length());
            if (Constant.PLANILLA_ANDES.equalsIgnoreCase(this.tipopl) || Constant.PLANILLA_LAARAUCANA.equalsIgnoreCase(this.tipopl)){
            
            
                if(convenioPagoDTO.getId_medio_pago().intValue() == Constant.PLANILLA_ANDES_CONVENIO_MANUAL || 
                   convenioPagoDTO.getId_medio_pago().intValue() == Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL){
                    if(extension.equalsIgnoreCase("xls")){
                        ret = true;
                    }                
                }else{
                    if(extension.equalsIgnoreCase("txt")){
                        ret = true;
                    }                
                }
                
            }else{
                if(extension.equalsIgnoreCase("txt")){
                    String tipoArchivo = filename.substring(0, 2);
                    String recaudador = filename.substring(4, 7);
                    boolean isRecaudador = false;
                    if (convenioPagoDTO.getTipo_codbanco_medio_pago().equalsIgnoreCase(recaudador)) {
                        Iterator tiposArchivositr = tiposArchivos.iterator();
                        while (tiposArchivositr.hasNext()) {
                            TipoArchivoDTO tipo = (TipoArchivoDTO)tiposArchivositr.next();
                            if (tipo.getTipoArchivo().equalsIgnoreCase(tipoArchivo)) {
                                isRecaudador = true;
                                break;
                            }
                        }
        
                        if (!isRecaudador) {
                         //   setAdvertencia(JsfUtil.getMessage("messages", 
                         //                                     "cl.bicevida.previred.recaudacion.nombrearchivo.tiporecaudador.incorrecto"));
                         //   logger.error("doValidarArchivo() - El recaudador no corresponde: " + 
                         //                recaudador);
                         ret = false;
                        } else {
                            ret = true;
                        }
        
                       
                    } else {
                       // setAdvertencia(JsfUtil.getMessage("messages", 
                       //                                   "cl.bicevida.previred.recaudacion.nombrearchivo.tipoarchivo.incorrecto"));
                       // logger.error("doValidarArchivo() - El tipo de archivo no corresponde: " + 
                       //              tipoArchivo);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            ret = false;
        }
        return ret;
    }

    /**
     * Carga el archivo en el servidor
     * segun el path de contexto en base a path real
     * @param file
     * @param convenioPagoDTO
     * @return
     * @throws Exception
     */
    private boolean doLoadFile(UploadedFile file,  ConvenioPagoDTO convenioPagoDTO) throws Exception {
        boolean ret = false;

        //Directorio Absoluto del Contenedor 
        String dirWebContent = JsfUtil.getServletContext().getRealPath("/");

        logger.debug("doLoadFile() - Grabando en directorio WebContent Path : " + dirWebContent);
        fileupload =
            dirWebContent + File.separator + "rendicion" + File.separator + convenioPagoDTO.getDirectorio_upload() + File.separator +
            StringUtil.replaceString(file.getFilename(), ".", "_") + "_" + System.currentTimeMillis() + ".data";
        logger.debug("doLoadFile() - Path archivo creado: " + fileupload);

        //Uploading datos
        File archivo = new File(fileupload);
        archivo.createNewFile();
        FileOutputStream out = new FileOutputStream(archivo);
        InputStream in = file.getInputStream();
        int data;
        while ((data = in.read()) != -1) {
            out.write(data);
        }
        in.close();
        out.close();
        ret = true;

        return ret;
    }
    
    /**
     * Finaliza el proceso
     * @return
     */
    public String doFinProcesoSendCorreoAction() {
        try {
            boolean problemasPorSolucionar = false;
            boolean rechazados = false;
            this.errorFinalizar= null;
            
            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getEstadoProcesoAutorizacion() == null || resumenAbonoProcesado.get(i).getEstadoProcesoAutorizacion().equals("W")) {
                        problemasPorSolucionar = true;
                    }
                    if (resumenAbonoProcesado.get(i).getEstadoProcesoAutorizacion() != null && resumenAbonoProcesado.get(i).getEstadoProcesoAutorizacion().equals("R")) {
                        rechazados = true;
                    }
                }
            }
            if (problemasPorSolucionar) {
                this.errorFinalizar = "Existen procesos sin aprobar o con observaci�n, debe aprobar antes de finalizar";
            } else {
                if (rechazados) {
                    this.errorFinalizar = "Existen al menos 1 rechazo, debe aprobar todo para poder finalizar y guardar";
                }
            }
            
            
            if (this.errorFinalizar == null) {
                //GRABAR
                boolean result = true;
                boolean rechazado  = false;
                // Verifica si fue rechazado
                for (int i = 0; i < resumenAbonoProcesado.size(); i++){
                    if(resumenAbonoProcesado.get(i).getId_motivo_rechazo().intValue() > 0 ){
                        rechazado = true;
                        break;
                    }
                }
                /*
                 * Actualiza estado dependiendo si se enci�uentra rechazado
                 */
                if(rechazado){
                    ejb.updateCargaArchivosEstadoProceso(idCargaConsulta, 
                                                         Constant.PROCESO_FINALIZADO_CON_RECHAZOS, 
                                                         null, 
                                                         null);
                }else{
                    ejb.updateCargaArchivosEstadoProceso(idCargaConsulta, 
                                                         Constant.PROCESO_FINALIZADO_AUTORIZADO, 
                                                         null, 
                                                         null);
                }
                
                /*
                 * Verifica que todos los datos se encuentren aprobados
                 *           
                //int counrrechazados = 0;
                for (int i = 0; i < resumenAbonoProcesado.size(); i++){
                    if(!resumenAbonoProcesado.get(i).isProcesadoAprobacion()){
                        result = false;
                        this.errorFinalizar = JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.proceso.finalizado.incompleto");
                        break;
                    }
                }
                */
                
                
                if(result){
                    //Envio de correo
                     PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();
                     CargaArchivosDTO cargaArchivos = ejb.getCargaArchivosByIdCarga(idCargaConsulta);
                    String estadoProcesoAbono = cargaArchivos.getDescripcion_estado_proceso();     
                    String nombreArchivoAbono = cargaArchivos.getNombre_fisico_archivo();
                    String estadoProcesoPlanilla = "Pendiente";
                    String nombreArchivoPlanilla = StringUtil.replaceString(cargaArchivos.getNombre_fisico_archivo(), "AA", "PL") ;
                    String detalle = EmailUtil.generateDetalleMail(resumenAbonoProcesado);
                    String to = null;
                    String from = null;
                    String fechaactual = FechaUtil.getFechaFormateoCustom(new Date(), "dd/MM/yyy HH:mm:ss");
                    String subject = "Rendiciones Previred " + fechaactual;
                    
                    Boolean dummy = Boolean.parseBoolean(JsfUtil.getMessage("configurationbice", "cl.bicevida.email.dummy"));
                    if(dummy){
                        from = JsfUtil.getMessage("configurationbice", "cl.bicevida.email.dummy.from");
                        to = JsfUtil.getMessage("configurationbice", "cl.bicevida.email.dummy.to");
                    }else{
                        from = JsfUtil.getMessage("configurationbice", "cl.bicevida.email.from");
                        to = JsfUtil.getMessage("configurationbice", "cl.bicevida.email.to");
                    }
                    
                    if(EmailUtil.enviarEmailWS(to, from, null, null, subject, nombreArchivoAbono, estadoProcesoAbono, estadoProcesoPlanilla, nombreArchivoPlanilla, detalle)){
                        setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.email.enviado"));
                    }else{
                        this.errorFinalizar = JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.email.problema");
                    }
                    
                }
            }
            
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", 
                                              "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doProcesarAction() - Se ha producido un error: " + 
                         e.getMessage(), e);
        }

        return null;
    }
    
    public String doExcelAction() {
        try {
        
            HSSFWorkbook wb = ExcelUtil.generateExcelRecaudacionOperaciones(resumenAbonoProcesado, planillaNoInformados);
            if(wb != null){
                // se agrega
                HttpServletResponse response = JsfUtil.getServletResponse();
                String filename = "datos" + System.currentTimeMillis() + ".xls";
                //Directorio Absoluto del Contenedor 
                //String dirWebContent = JsfUtil.getServletContext().getRealPath("/");
                String dirWebContent =  JsfUtil.getSession().getServletContext().getRealPath("/");
                //Directorio Absoluto del Contenedor en donde se generar� excel
                this.pathAbsoluteExcel = dirWebContent + File.separator + "excel" + File.separator + filename;
                
                FileOutputStream out = new FileOutputStream(pathAbsoluteExcel);
                wb.write(out);
                out.close();
                out.flush();
                
                
                if(this.pathAbsoluteExcel != null){
                        response.setContentType("application/xls");
                        response.setHeader("Content-Disposition", "attachment; filename=\"RecaudacionTesoreria.xls\"");
                        FacesContext ctx = FacesContext.getCurrentInstance();
                        //DownloadExcelMB dTxt = (DownloadExcelMB)ctx.getApplication().createValueBinding("#{DownloadExcelMB}").getValue(ctx);
                        DownloadExcelMB dTxt = (DownloadExcelMB)ctx.getApplication().getExpressionFactory().createValueExpression(ctx.getELContext(),"#{DownloadExcelMB}",Object.class).getValue(ctx.getELContext());
                        dTxt.setAbsolutePathExcel(this.pathAbsoluteExcel);
                        dTxt.getArchivoExcel(response.getOutputStream());
                        response.getOutputStream().flush();
                        //DownloadExcelMB downloadExcelMB = (DownloadExcelMB) JsfUtil.getManagedBean("DownloadExcelMB");
                        //downloadExcelMB.setAbsolutePathExcel(this.pathAbsoluteExcel);
                }else{
                    setAdvertencia(JsfUtil.getMessage("messages", 
                                                      "cl.bicevida.previred.recaudacion.excel.error.generacion"));
                }
                
            }else{
                this.showDescarga = false;
                setAdvertencia(JsfUtil.getMessage("messages", 
                                                  "cl.bicevida.previred.recaudacion.excel.error.generacion"));
            }
        
            
            
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", 
                                              "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doProcesarAction() - Se ha producido un error: " + 
                         e.getMessage(), e);
        }

        return "excel";
    }
    
   
    public void setAdvertencia(String mensaje) {
        mensajesmb = (MensajesMB)JsfUtil.getManagedBean("MensajesMB");
        mensajesmb.setAdvertencia(true);
        mensajesmb.setMensaje(mensaje);
    }

    public void setError(String mensaje) {
        mensajesmb = (MensajesMB)JsfUtil.getManagedBean("MensajesMB");
        mensajesmb.setAdvertencia(false);
        mensajesmb.setMensaje(mensaje);
    }

    public void setEmpresasComboBox(List<EmpresasDTO> empresas) throws Exception {
        List<SelectItem> empresasitem = new ArrayList<SelectItem>();
        Iterator empresasitr = empresas.iterator();
        while (empresasitr.hasNext()) {
            EmpresasDTO empresa = (EmpresasDTO)empresasitr.next();
            if (empresa != null) {
                empresasitem.add(new SelectItem(empresa.getIdEmpresa(), 
                                                empresa.getDescripcionEmpresa()));
            }
        }

        this.selectItemsEmpresa.setValue(empresasitem);
    }

    public void setMedioPagoComboBox(List<MediosDePagoDTO> mediosPago) throws Exception {
        List<SelectItem> mediosPagoitem = new ArrayList<SelectItem>();
        Iterator mediosPagoitr = mediosPago.iterator();
        while (mediosPagoitr.hasNext()) {
            MediosDePagoDTO medioPago = (MediosDePagoDTO)mediosPagoitr.next();
            if (medioPago != null) {
                mediosPagoitem.add(new SelectItem(medioPago.getIdMedioPagoRendicion(), 
                                                  medioPago.getDescripcionMedioPago()));
            }
        }

        this.selectItemsMedioPago.setValue(mediosPagoitem);
    }

    public void setTipoArchivoComboBox(List<TipoArchivoDTO> tiposArchivos) throws Exception {
        List<SelectItem> tiposArchivositem = new ArrayList<SelectItem>();
        Iterator tiposArchivositr = tiposArchivos.iterator();
        while (tiposArchivositr.hasNext()) {
            TipoArchivoDTO tipoArchivo = 
                (TipoArchivoDTO)tiposArchivositr.next();
            if (tipoArchivo != null) {
                tiposArchivositem.add(new SelectItem(tipoArchivo.getIdTipoArchivo(), 
                                                     tipoArchivo.getDescripcionTipoArchivo()));
            }
        }

        this.selectItemsNombreArchivo.setValue(tiposArchivositem);
    }

    public void setMotivoRechazosComboBox(List<MotivoRechazosDTO> motivoRechazos) throws Exception {
        List<SelectItem> motivoRechazositem = new ArrayList<SelectItem>();
        Iterator motivoRechazositr = motivoRechazos.iterator();
        while (motivoRechazositr.hasNext()) {
            MotivoRechazosDTO motivoRechazo = 
                (MotivoRechazosDTO)motivoRechazositr.next();
            if (motivoRechazo != null) {
                motivoRechazositem.add(new SelectItem(motivoRechazo.getId_motivo_rechazos(), 
                                                      motivoRechazo.getDescripcion_rechazos()));
            }
        }

        this.selectItemsMotivoRechazos.setValue(motivoRechazositem);
    }

    public void doRechazadoActionListener(ActionEvent actionEvent) {
        try {
            String filacodrecaudador = 
                ((CoreInputHidden)actionEvent.getComponent().getParent().getChildren().get(0)).getValue().toString();

            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getCodigo_recaudador() != 
                        null) {
                        if (resumenAbonoProcesado.get(i).getCodigo_recaudador().equalsIgnoreCase(filacodrecaudador)) {
                            if (resumenAbonoProcesado.get(i).getTotalAbono() != 
                                null) {
                                if (resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola() != 
                                    null) {
                                    if (!NumeroUtil.isNumero(resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola())) {
                                        //setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.montoinformado"));
                                        //logger.error("doChangeMontoInfCartola() - El monto informado no es num�rico: " + resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola());
                                        resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.montoinformado"));
                                        resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                                    } else {
                                        Double montoInformado = 
                                            new Double(resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola());
                                        
                                        if(montoInformado > 0.0){
                                            resumenAbonoProcesado.get(i).setTotal_monto_inf_cartola(montoInformado);
                                            resumenAbonoProcesado.get(i).setBool_button_autobs(false);
                                            resumenAbonoProcesado.get(i).setBool_button_rechazado(true);
                                        }else{
                                            //setAdvertencia(JsfUtil.getMessage("messages","cl.bicevida.previred.recaudacion.montoinformado.menor"));
                                            resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages","cl.bicevida.previred.recaudacion.montoinformado.menor"));
                                            resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                                        }
                                        
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doRechazadoActionListener() -Se ha producido una excepcion: " +  e.getMessage(), e);
        }
    }


    public void doAutorizadObsActionListener(ActionEvent actionEvent) {
        try {
            String filacodrecaudador = 
                ((CoreInputHidden)actionEvent.getComponent().getParent().getChildren().get(0)).getValue().toString();

            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getCodigo_recaudador() != 
                        null) {
                        if (resumenAbonoProcesado.get(i).getCodigo_recaudador().equalsIgnoreCase(filacodrecaudador)) {
                            if (resumenAbonoProcesado.get(i).getTotalAbono() != 
                                null) {
                                if (resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola() != 
                                    null) {
                                    if (!NumeroUtil.isNumero(resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola())) {
                                        //setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.montoinformado"));
                                        //logger.error("doChangeMontoInfCartola() - El monto informado no es num�rico: " + resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola());
                                        resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.montoinformado"));
                                        resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                                    } else {
                                        Double montoInformado = 
                                            new Double(resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola());
                                        
                                        if(montoInformado > 0.0){
                                            resumenAbonoProcesado.get(i).setTotal_monto_inf_cartola(montoInformado);
                                            resumenAbonoProcesado.get(i).setBool_button_autobs(true);
                                            resumenAbonoProcesado.get(i).setBool_button_rechazado(false);
                                        }else{
                                           // setAdvertencia(JsfUtil.getMessage("messages",  "cl.bicevida.previred.recaudacion.montoinformado.menor"));
                                            resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages",  "cl.bicevida.previred.recaudacion.montoinformado.menor"));
                                            resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doAutorizadObsActionListener() -Se ha producido una excepcion: " +   e.getMessage(), e);
        }
    }

    public void doAutorizadoActionListener(ActionEvent actionEvent) {
        try {
            String filacodrecaudador = 
                ((CoreInputHidden)actionEvent.getComponent().getParent().getChildren().get(0)).getValue().toString();

            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getCodigo_recaudador() != 
                        null) {
                        if (resumenAbonoProcesado.get(i).getCodigo_recaudador().equalsIgnoreCase(filacodrecaudador)) {
                            if (resumenAbonoProcesado.get(i).getTotalAbono() != 
                                null) {
                                if (resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola() != 
                                    null) {
                                    if (!NumeroUtil.isNumero(resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola())) {
                                        //setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.montoinformado"));
                                        //logger.error("doAutorizadoActionListener() - El monto informado no es num�rico: " +  resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola());
                                        resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.montoinformado"));
                                        resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                                    } else {
                                        Double montoInformado = 
                                            new Double(resumenAbonoProcesado.get(i).getStrtotal_monto_inf_cartola());
                                        if(montoInformado > 0.0){
                                            if (resumenAbonoProcesado.get(i).getTotalAbono().doubleValue() != 
                                                montoInformado.doubleValue()) {
                                                //setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.montoinformado.noconcuerda"));
                                                //logger.error("doAutorizadoActionListener() - El monto informado: " +  montoInformado + ", no concuerda con el monto esperado: " + resumenAbonoProcesado.get(i).getTotalAbono());
                                                 resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.montoinformado.noconcuerda"));
                                                 resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                                            } else {
                                                resumenAbonoProcesado.get(i).setId_motivo_rechazo(0);
                                                resumenAbonoProcesado.get(i).setObservacion_rechazo("");
                                                resumenAbonoProcesado.get(i).setObservacion_autorizacion("");
                                                resumenAbonoProcesado.get(i).setValidacion_cartola_cta_cte(1);
                                                resumenAbonoProcesado.get(i).setTotal_monto_inf_cartola(montoInformado);
                                                resumenAbonoProcesado.get(i).setEstado_proceso(Constant.PROCESO_ABONO_AUTORIZADO);
                                                resumenAbonoProcesado.get(i).setEstado_proceso_excepcion("");
                                                
                                                resumenAbonoProcesado.get(i).setBool_button_autobs(false);
                                                resumenAbonoProcesado.get(i).setBool_button_rechazado(false);
                                                resumenAbonoProcesado.get(i).setProcesadoAprobacion(true);
                                                

                                                ejb = ServiceLocator.getSessionEJBPrevired();
                                                
                                                EstadoDeProcesoDTO estadoDeProcesoDTO = ejb.getEstadoDeProcesoByIdProceso(Constant.PROCESO_ABONO_AUTORIZADO);
                                                if(estadoDeProcesoDTO != null){
                                                    resumenAbonoProcesado.get(i).setDescripcion_estado_proceso(estadoDeProcesoDTO.getDecripcionEstado());
                                                    resumenAbonoProcesado.get(i).setMensajeSegunEstado("Autorizado");
                                                    resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("A");
                                                }
                                                
                                                /*ejb.updateCargaAbonoEstadoAutorizacion(idCargaConsulta, 
                                                                                       resumenAbonoProcesado.get(i));
                                                
                                                
                                                ejb.updateEstadoPorcesoFolioResumenByIdCargaBanco(idCargaConsulta, 
                                                                                                  resumenAbonoProcesado.get(i).getCodigo_recaudador(), 
                                                                                                  Constant.PROCESO_ABONO_AUTORIZADO, 
                                                                                                  null);*/
                                                
                                                ejb.updateResumenYDetalleAbonoByIdCargaResumen(idCargaConsulta, resumenAbonoProcesado.get(i));
                                                
                                                
                                                
                                            }
                                        }else{
                                            //setAdvertencia(JsfUtil.getMessage("messages",  "cl.bicevida.previred.recaudacion.montoinformado.menor"));
                                            resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages",  "cl.bicevida.previred.recaudacion.montoinformado.menor"));
                                            resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                                        }                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doAutorizadoActionListener() -Se ha producido una excepcion: " + e.getMessage(), e);
        }
    }


    public void doGuardarRechazado(ActionEvent actionEvent) {
        try {
            String filacodrecaudador = 
                ((CoreInputHidden)actionEvent.getComponent().getParent().getChildren().get(0)).getValue().toString();
            
            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getCodigo_recaudador() != 
                        null) {
                        if (resumenAbonoProcesado.get(i).getCodigo_recaudador().equalsIgnoreCase(filacodrecaudador)) {
                            if (resumenAbonoProcesado.get(i).getId_motivo_rechazo() != 
                                null && 
                                resumenAbonoProcesado.get(i).getId_motivo_rechazo().intValue() > 
                                0) {
                                if (resumenAbonoProcesado.get(i).getObservacion_rechazo() != 
                                    null && 
                                    resumenAbonoProcesado.get(i).getObservacion_rechazo().length() > 
                                    0) {
                                    
                                    resumenAbonoProcesado.get(i).setValidacion_cartola_cta_cte(1);
                                    resumenAbonoProcesado.get(i).setObservacion_autorizacion("");
                                    
                                    List<SelectItem> motivoRechazositem = (List<SelectItem>)selectItemsMotivoRechazos.getValue();
                                    
                                    for(int j = 0; j < motivoRechazositem.size(); j++){
                                        SelectItem item = motivoRechazositem.get(j);
                                        if(item.getValue() == resumenAbonoProcesado.get(i).getId_motivo_rechazo()){
                                            resumenAbonoProcesado.get(i).setDescripcion_motivo_rechazo(item.getLabel());
                                            break;
                                        }
                                    }
                                    
                                    resumenAbonoProcesado.get(i).setEstado_proceso(Constant.PROCESO_ABONO_RECHAZADO);
                                    resumenAbonoProcesado.get(i).setEstado_proceso_excepcion("Folio rechazado con la observacion: " + resumenAbonoProcesado.get(i).getObservacion_rechazo());
                                    resumenAbonoProcesado.get(i).setBool_button_rechazado(false);
                                    resumenAbonoProcesado.get(i).setProcesadoAprobacion(true);
                                    
                                    ejb = ServiceLocator.getSessionEJBPrevired();
                                    EstadoDeProcesoDTO estadoDeProcesoDTO = ejb.getEstadoDeProcesoByIdProceso(Constant.PROCESO_ABONO_RECHAZADO);
                                    if(estadoDeProcesoDTO != null){
                                        resumenAbonoProcesado.get(i).setDescripcion_estado_proceso(estadoDeProcesoDTO.getDecripcionEstado());
                                        resumenAbonoProcesado.get(i).setMensajeSegunEstado("Rechazado");
                                        resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("R");
                                    }
                                    
                                    ejb.updateResumenYDetalleAbonoByIdCargaResumen(idCargaConsulta, resumenAbonoProcesado.get(i));
                                    
                                    /*ejb.updateCargaAbonoEstadoAutorizacion(idCargaConsulta, 
                                                                           resumenAbonoProcesado.get(i));
                                   
                                    ejb.updateEstadoPorcesoFolioResumenByIdCargaBanco(idCargaConsulta, 
                                                                                      resumenAbonoProcesado.get(i).getCodigo_recaudador(), 
                                                                                      Constant.PROCESO_ABONO_RECHAZADO, 
                                                                                      "Folio rechazado con la observacion: " + 
                                                                                      resumenAbonoProcesado.get(i).getObservacion_rechazo());*/
                                    
                                    
                                    
                                } else {
                                    //setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.observacion.motivo.rechazo"));
                                    resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.observacion.motivo.rechazo"));
                                    resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                                }
                            } else {
                                //setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.select.motivo.rechazo"));
                                resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.select.motivo.rechazo"));
                                resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doGuardarRechazado() -Se ha producido una excepcion: " +  e.getMessage(), e);
        }
    }

    public void doGuardarAutorizadoObs(ActionEvent actionEvent) {
        try {
            String filacodrecaudador = 
                ((CoreInputHidden)actionEvent.getComponent().getParent().getChildren().get(0)).getValue().toString();

            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getCodigo_recaudador() != 
                        null) {
                        if (resumenAbonoProcesado.get(i).getCodigo_recaudador().equalsIgnoreCase(filacodrecaudador)) {                     
                            
                            if (resumenAbonoProcesado.get(i).getObservacion_autorizacion() != 
                                null && 
                                resumenAbonoProcesado.get(i).getObservacion_autorizacion().length() > 
                                0) {
                                
                                resumenAbonoProcesado.get(i).setId_motivo_rechazo(0);
                                resumenAbonoProcesado.get(i).setObservacion_rechazo("");
                                resumenAbonoProcesado.get(i).setValidacion_cartola_cta_cte(1);
                                resumenAbonoProcesado.get(i).setEstado_proceso(Constant.PROCESO_ABONO_AUTORIZADO_OBS);
                                resumenAbonoProcesado.get(i).setEstado_proceso_excepcion("Folio autorizado con la observacion: " + resumenAbonoProcesado.get(i).getObservacion_autorizacion());
                                resumenAbonoProcesado.get(i).setBool_button_autobs(false);
                                resumenAbonoProcesado.get(i).setProcesadoAprobacion(true);
                                resumenAbonoProcesado.get(i).setMensajeSegunEstado("Autorizado con observaciones");
                                resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("A");
                                
                                ejb = ServiceLocator.getSessionEJBPrevired();
                                EstadoDeProcesoDTO estadoDeProcesoDTO = ejb.getEstadoDeProcesoByIdProceso(Constant.PROCESO_ABONO_AUTORIZADO_OBS);
                                if(estadoDeProcesoDTO != null){
                                    resumenAbonoProcesado.get(i).setDescripcion_estado_proceso(estadoDeProcesoDTO.getDecripcionEstado());
                                }
                                
                                
                                ejb.updateResumenYDetalleAbonoByIdCargaResumen(idCargaConsulta, resumenAbonoProcesado.get(i));
                                /*ejb.updateCargaAbonoEstadoAutorizacion(idCargaConsulta, 
                                                                       resumenAbonoProcesado.get(i));
                                
                                ejb.updateEstadoPorcesoFolioResumenByIdCargaBanco(idCargaConsulta, 
                                                                                  resumenAbonoProcesado.get(i).getCodigo_recaudador(), 
                                                                                  Constant.PROCESO_ABONO_AUTORIZADO_OBS, 
                                                                                  null);*/
                                
                                
                            } else {
                                //setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.select.motivo.autorizacion"));
                                resumenAbonoProcesado.get(i).setMensajeSegunEstado(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.select.motivo.autorizacion"));
                                resumenAbonoProcesado.get(i).setEstadoProcesoAutorizacion("W");
                            }

                        }
                    }
                }
            }
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doGuardarAutorizadoObs() -Se ha producido una excepcion: " +  e.getMessage(), e);
        }
    }

    public void doVolverRechazado(ActionEvent actionEvent) {
        try {
            String filacodrecaudador = 
                ((CoreInputHidden)actionEvent.getComponent().getParent().getChildren().get(0)).getValue().toString();

            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getCodigo_recaudador() != 
                        null) {
                        if (resumenAbonoProcesado.get(i).getCodigo_recaudador().equalsIgnoreCase(filacodrecaudador)) {
                            resumenAbonoProcesado.get(i).setBool_button_rechazado(false);
                            resumenAbonoProcesado.get(i).setId_motivo_rechazo(0);
                            resumenAbonoProcesado.get(i).setObservacion_rechazo("");
                            resumenAbonoProcesado.get(i).setObservacion_autorizacion("");
                            
                        }
                    }
                }
            }
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", 
                                              "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doVolverRechazado() -Se ha producido una excepcion: " + 
                         e.getMessage(), e);
        }
    }

    public void doVolverAutorizadoObs(ActionEvent actionEvent) {
        try {
            String filacodrecaudador = 
                ((CoreInputHidden)actionEvent.getComponent().getParent().getChildren().get(0)).getValue().toString();

            for (int i = 0; i < resumenAbonoProcesado.size(); i++) {
                if (resumenAbonoProcesado.get(i) != null) {
                    if (resumenAbonoProcesado.get(i).getCodigo_recaudador() != 
                        null) {
                        if (resumenAbonoProcesado.get(i).getCodigo_recaudador().equalsIgnoreCase(filacodrecaudador)) {
                            resumenAbonoProcesado.get(i).setBool_button_autobs(false);
                            resumenAbonoProcesado.get(i).setId_motivo_rechazo(0);
                            resumenAbonoProcesado.get(i).setObservacion_rechazo("");
                            resumenAbonoProcesado.get(i).setObservacion_autorizacion("");
                        }
                    }
                }
            }
        } catch (Exception e) {
            setAdvertencia(JsfUtil.getMessage("messages", 
                                              "cl.bicevida.previred.recaudacion.exception"));
            logger.error("doVolverAutorizadoObs() -Se ha producido una excepcion: " + 
                         e.getMessage(), e);
        }
    }


    public void setSelectItemsEmpresa(UISelectItems selectItemsEmpresa) {
        this.selectItemsEmpresa = selectItemsEmpresa;
    }

    public UISelectItems getSelectItemsEmpresa() {
        return selectItemsEmpresa;
    }


    public void setSelectItemsMedioPago(UISelectItems selectItemsMedioPago) {
        this.selectItemsMedioPago = selectItemsMedioPago;
    }

    public UISelectItems getSelectItemsMedioPago() {
        return selectItemsMedioPago;
    }


    public void setSelectItemsNombreArchivo(UISelectItems selectItemsNombreArchivo) {
        this.selectItemsNombreArchivo = selectItemsNombreArchivo;
    }

    public UISelectItems getSelectItemsNombreArchivo() {
        return selectItemsNombreArchivo;
    }


    public void setRenderpoll(boolean renderpoll) {
        this.renderpoll = renderpoll;
    }

    public boolean isRenderpoll() {
        return renderpoll;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setResumenAbonoProcesado(List<ResumenAbonoDTO> resumenAbonoProcesado) {
        this.resumenAbonoProcesado = resumenAbonoProcesado;
    }

    public List<ResumenAbonoDTO> getResumenAbonoProcesado() {
        return resumenAbonoProcesado;
    }


    public void setSelectItemsMotivoRechazos(UISelectItems selectItemsMotivoRechazos) {
        this.selectItemsMotivoRechazos = selectItemsMotivoRechazos;
    }

    public UISelectItems getSelectItemsMotivoRechazos() {
        return selectItemsMotivoRechazos;
    }
   

    public void doChangeTipoArchivo(ValueChangeEvent valueChangeEvent) {
        showDetail = false;
        showMensajeEspera = false;
        showDetailPlanilla = false;
    }   

    public void setShowDetail(Boolean showDetail) {
        this.showDetail = showDetail;
    }

    public Boolean getShowDetail() {
        return showDetail;
    }

    public void setShowMensajeEspera(Boolean showMensajeEspera) {
        this.showMensajeEspera = showMensajeEspera;
    }

    public Boolean getShowMensajeEspera() {
        return showMensajeEspera;
    }

    public void setShowDetailPlanilla(Boolean showDetailPlanilla) {
        this.showDetailPlanilla = showDetailPlanilla;
    }

    public Boolean getShowDetailPlanilla() {
        return showDetailPlanilla;
    }

    public void setPlanillaNoInformados(List<ResumenAbonoDTO> planillaNoInformados) {
        this.planillaNoInformados = planillaNoInformados;
    }

    public List<ResumenAbonoDTO> getPlanillaNoInformados() {
        return planillaNoInformados;
    }
    
    public void setShowDescarga(Boolean showDescarga) {
        this.showDescarga = showDescarga;
    }

    public Boolean getShowDescarga() {
        return showDescarga;
    }

    public void setPathAbsoluteExcel(String pathAbsoluteExcel) {
        this.pathAbsoluteExcel = pathAbsoluteExcel;
    }

    public String getPathAbsoluteExcel() {
        return pathAbsoluteExcel;
    }

    public void setTipopl(String tipopl) {
        this.tipopl = tipopl;
    }

    public String getTipopl() {
        return tipopl;
    }

    public void setErrorFinalizar(String errorFinalizar) {
        this.errorFinalizar = errorFinalizar;
    }

    public String getErrorFinalizar() {
        return errorFinalizar;
    }


    public void setRecaudacionForm(CoreForm recaudacionForm) {
        this.recaudacionForm = recaudacionForm;
    }

    public CoreForm getRecaudacionForm() {
        return recaudacionForm;
    }

    public void setCommandButtonProcesar(CoreCommandButton commandButtonProcesar) {
        this.commandButtonProcesar = commandButtonProcesar;
    }

    public CoreCommandButton getCommandButtonProcesar() {
        return commandButtonProcesar;
    }

    public void setSelectOneChoiceEmpresa(CoreSelectOneChoice selectOneChoiceEmpresa) {
        this.selectOneChoiceEmpresa = selectOneChoiceEmpresa;
    }

    public CoreSelectOneChoice getSelectOneChoiceEmpresa() {
        return selectOneChoiceEmpresa;
    }

    public void setSelectOneChoiceMedioPago(CoreSelectOneChoice selectOneChoiceMedioPago) {
        this.selectOneChoiceMedioPago = selectOneChoiceMedioPago;
    }

    public CoreSelectOneChoice getSelectOneChoiceMedioPago() {
        return selectOneChoiceMedioPago;
    }

    public void setSelectOneChoiceNombreArchivo(CoreSelectOneChoice selectOneChoiceNombreArchivo) {
        this.selectOneChoiceNombreArchivo = selectOneChoiceNombreArchivo;
    }

    public CoreSelectOneChoice getSelectOneChoiceNombreArchivo() {
        return selectOneChoiceNombreArchivo;
    }

    public void setInputFileAdjunto(CoreInputFile inputFileAdjunto) {
        this.inputFileAdjunto = inputFileAdjunto;
    }

    public CoreInputFile getInputFileAdjunto() {
        return inputFileAdjunto;
    }

    public void setCbDoExcelAction(CoreCommandButton cbDoExcelAction) {
        this.cbDoExcelAction = cbDoExcelAction;
    }

    public CoreCommandButton getCbDoExcelAction() {
        return cbDoExcelAction;
    }

    public void setCbFinProcesoSendCorreoAction(CoreCommandButton cbFinProcesoSendCorreoAction) {
        this.cbFinProcesoSendCorreoAction = cbFinProcesoSendCorreoAction;
    }

    public CoreCommandButton getCbFinProcesoSendCorreoAction() {
        return cbFinProcesoSendCorreoAction;
    }

    public void setInputHiddenRow1(CoreInputHidden inputHiddenRow1) {
        this.inputHiddenRow1 = inputHiddenRow1;
    }

    public CoreInputHidden getInputHiddenRow1() {
        return inputHiddenRow1;
    }

    public void setEnableinput(CoreSelectBooleanCheckbox enableinput) {
        this.enableinput = enableinput;
    }

    public CoreSelectBooleanCheckbox getEnableinput() {
        return enableinput;
    }

    public void setSelectMotivoRechazos(CoreSelectOneChoice selectMotivoRechazos) {
        this.selectMotivoRechazos = selectMotivoRechazos;
    }

    public CoreSelectOneChoice getSelectMotivoRechazos() {
        return selectMotivoRechazos;
    }

    public void setInputHiddenRow3(CoreInputHidden inputHiddenRow3) {
        this.inputHiddenRow3 = inputHiddenRow3;
    }

    public CoreInputHidden getInputHiddenRow3() {
        return inputHiddenRow3;
    }

    public void setInputText2(CoreInputText inputText2) {
        this.inputText2 = inputText2;
    }

    public CoreInputText getInputText2() {
        return inputText2;
    }

    public void onPoll(PollEvent pollEvent) {
        this.doRefrescarAction();
    }
    
    
    public void procesoManual(){
        trigger.manualAction();
    }
    private void cargaNombreServer() {

        InetAddress address;
        try {
            address = InetAddress.getLocalHost();
            this.nombreServidor = address.getHostName();
        } catch (UnknownHostException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
}
