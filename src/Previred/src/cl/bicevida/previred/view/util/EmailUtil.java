package cl.bicevida.previred.view.util;

import cl.bicevida.previred.common.dto.ResumenAbonoDTO;
import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.common.utils.StringUtil;
import cl.bicevida.previred.proxy.Encabezado;
import cl.bicevida.previred.proxy.InSendMail;
import cl.bicevida.previred.proxy.Registro;
import cl.bicevida.previred.proxy.SendMailEJBClient;

import java.util.Date;
import java.util.List;

import javax.mail.internet.MimeUtility;

import org.apache.log4j.Logger;


public class EmailUtil {
    /**
     * Logger for this class
     */
    private static final Logger logger = 
        Logger.getLogger(EmailUtil.class);
    public EmailUtil() {
    }
    
    public static String generateDetalleMail(List<ResumenAbonoDTO> resumenAbonoProcesado){
        String detalle = ""; 
    
        try{           
            if(resumenAbonoProcesado.size() > 0){
                String iniciotabla = 
                "                            <p align=\"justify\" class=\"Estilo1\">\n" + 
                "                                <table width=\"700px\" border=\"1px\"  style=\"border-collapse: collapse;\">\n" + 
                "                                    <tr style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#666666;\">\n" + 
                "                                        <th>\n" + 
                "                                            Fecha\n" + 
                "                                        </th>\n" + 
                "                                        <th>\n" + 
                "                                            Nombre Banco\n" + 
                "                                        </th>\n" + 
                "                                        <th>\n" + 
                "                                            Estado\n" + 
                "                                        </th>\n" + 
                "                                        <th>\n" + 
                "                                            Motivo Rechazo\n" + 
                "                                        </th>\n" + 
                "                                        <th>\n" + 
                "                                            Detalle Observaci&oacute;n\n" + 
                "                                        </th>\n" + 
                "                                    </tr>\n";
                
                
                String fintabla = 
                "                                </table>\n" + 
                "                             </p>\n";
                
                for(int i = 0; i < resumenAbonoProcesado.size(); i++){
                
                    String medioPago = "";
                    String fecha = "";
                    String estadoDelProceso = "";
                    String motivoRechazo = "";
                    String observaciones = "";
                    
                    if(resumenAbonoProcesado.get(i).getGlosa_recaudador() != null){
                        medioPago = resumenAbonoProcesado.get(i).getGlosa_recaudador();
                    }
                    
                    fecha = FechaUtil.getFechaFormateoCustom(new Date(), "dd/MM/yyyy");
                    
                    if(resumenAbonoProcesado.get(i).getDescripcion_estado_proceso() != null){
                        estadoDelProceso = resumenAbonoProcesado.get(i).getDescripcion_estado_proceso();
                    }
                    
                    if(resumenAbonoProcesado.get(i).getDescripcion_motivo_rechazo() != null && resumenAbonoProcesado.get(i).getObservacion_rechazo() != null && resumenAbonoProcesado.get(i).getObservacion_rechazo().length() > 0){
                        motivoRechazo = resumenAbonoProcesado.get(i).getDescripcion_motivo_rechazo();
                        observaciones = resumenAbonoProcesado.get(i).getObservacion_rechazo();
                        observaciones = StringUtil.replaceBadCharsHtml(observaciones);
                        observaciones = StringUtil.replaceString(observaciones, "$", "&#36;");
                    }
                    
                    
                    if(resumenAbonoProcesado.get(i).getObservacion_autorizacion()!= null && resumenAbonoProcesado.get(i).getObservacion_autorizacion().length() > 0){
                        observaciones = resumenAbonoProcesado.get(i).getObservacion_autorizacion();
                        observaciones = StringUtil.replaceBadCharsHtml(observaciones);
                        observaciones = StringUtil.replaceString(observaciones, "$", "&#36;");
                    }
                                       
                    detalle = detalle + 
                    "                                    <tr style=\"font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#666666;\"\n" + 
                    "                                        align=\"center\">\n" + 
                    "                                        <td>\n" + 
                    "                                            " + fecha +"\n" + 
                    "                                        </td>\n" + 
                    "                                        <td>\n" + 
                    "                                            " + medioPago + "\n" + 
                    "                                        </td>\n" + 
                    "                                        <td>\n" + 
                    "                                            " + estadoDelProceso + "\n" + 
                    "                                        </td>\n" + 
                    "                                        <td>\n" + 
                    "                                            " + motivoRechazo + "\n" + 
                    "                                        </td>\n" + 
                    "                                        <td>\n" + 
                    "                                            " + observaciones + "\n" + 
                    "                                        </td>\n" + 
                    "                                    </tr>";
                    
                }
                
                detalle = iniciotabla + detalle + fintabla;
                
            }
            
        }catch (Exception e){
            logger.error("Se ha producido un error: " + e.getMessage(), e);
            detalle = "";
        }
        
        return detalle;
    }
    
    public static boolean enviarEmailWS(String to, String from, String cc, String cco, String subject, String nombreArchivoAbono, String estadoProcesoAbono, String estadoProcesoPlanilla, String nombreArchivoPlanilla, String detalle) {
        boolean ret = false;
        try{
            String fecha = FechaUtil.getFechaFormateoCustom(new Date(), "dd/MM/yyyy");
        
            SendMailEJBClient myPort = new SendMailEJBClient();
            myPort.setEndpoint(JsfUtil.getMessage("configurationbice", "cl.bicevida.ws.sendmail"));
            
            InSendMail in = new InSendMail();
            
            //encabezado
            Encabezado encabezado = new Encabezado();
            encabezado.setNombrePlantilla(JsfUtil.getMessage("configurationbice", "cl.bicevida.email.plantilla"));
            encabezado.setSubject(MimeUtility.encodeText(subject));
            encabezado.setFrom(from);
            
            String[] variables = new String[6];
            variables[0] = "fecha";
            variables[1] = "nombrearchivoabono";
            variables[2] = "estadodelprocesoabono";
            variables[3] = "estadodelprocesoplanilla";
            variables[4] = "nombrearchivoplanilla";
            variables[5] = "detalle";
            
            encabezado.setVariables(variables);
            in.setEncabezado(encabezado);
            
            //registro
            Registro[] registros = new Registro[1];
            registros[0] = new Registro();
            registros[0].setTo(to); 
            String[] valores = new String[6];
            valores[0] = fecha;
            valores[1] = nombreArchivoAbono;
            valores[2] = estadoProcesoAbono;
            valores[3] = estadoProcesoPlanilla;
            valores[4] = nombreArchivoPlanilla;
            valores[5] = detalle;
            
            registros[0].setValores(valores);; 
            
            in.setRegistros(registros);
            
            myPort.sendMail(in);
            
            ret = true;
            
        }catch(Exception e){
            logger.error("Se ha producido un error: " + e.getMessage(), e);
        }
        
        return ret;
    }
}
