package cl.bicevida.previred.view.job;

import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.core.abono.PreviredCore;

import cl.bicevida.previred.view.vo.ProcesoRendicionVO;

import java.io.Serializable;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class JobRendicion implements Job, Serializable {
    @SuppressWarnings("compatibility:-8299382530180904742")
    private static final long serialVersionUID = 1L;

    public static final String DELAY_TIME = Constant.DELAY_TIME;
    public static final String ID_CARGA = Constant.ID_CARGA;
    public static final String TIPO_ARCHIVO = Constant.TIPO_ARCHIVO;
    public static final String PATH_ARCHIVO = Constant.PATH_ARCHIVO;
    public static final String CORE = Constant.CORE;
    public static final String TIPOPLANILLA = Constant.TIPOPLANILLA;
    public static final String IDCARGAABONOANDES = Constant.IDCARGAABONOANDES;
    
    /**
     * Logger for this class
     */
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(JobRendicion.class);
    
    /**
     * <p>
     * Empty constructor for job initilization
     * </p>
     * <p>
     * Quartz requires a public empty constructor so that the
     * scheduler can instantiate the class whenever it needs.
     * </p>
     */
    
     /**
      * <p>
      * Called by the <code>{@link org.quartz.Scheduler}</code> when a
      * <code>{@link org.quartz.Trigger}</code> fires that is associated with
      * the <code>Job</code>.
      * </p>
      * 
      * @throws JobExecutionException
      *             if there is an exception while executing the job.
      */
    public JobRendicion() {
    }

    public void execute(JobExecutionContext jobExecutionContext) {
    
        try{            
            Double idCarga = jobExecutionContext.getJobDetail().getJobDataMap().getDouble(Constant.ID_CARGA);
            Integer tipoArchivo = jobExecutionContext.getJobDetail().getJobDataMap().getInt(Constant.TIPO_ARCHIVO);
            String pathArchivo = jobExecutionContext.getJobDetail().getJobDataMap().getString(Constant.PATH_ARCHIVO);
            String tipoPlanilla = jobExecutionContext.getJobDetail().getJobDataMap().getString(Constant.TIPOPLANILLA);
            Double idCargaAbonoAndes = jobExecutionContext.getJobDetail().getJobDataMap().getDouble(Constant.IDCARGAABONOANDES);
            Object objCoreArchivo = jobExecutionContext.getJobDetail().getJobDataMap().get(Constant.CORE); 
            
            /**
             * Define el concepto del cual procesara
             * la lectura del archivo
             */
            if(objCoreArchivo instanceof PreviredCore){
                PreviredCore coreArchivo = (PreviredCore)objCoreArchivo; 
                if(tipoArchivo.intValue() == 1){
                    ProcesoRendicionVO process = new ProcesoRendicionVO();
                    process.doProcesarAbono(idCarga, pathArchivo, coreArchivo);
                }
                
                if(tipoArchivo.intValue() == 2 && !tipoPlanilla.equalsIgnoreCase(Constant.PLANILLA_ANDES) && !tipoPlanilla.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)){
                    ProcesoRendicionVO process = new ProcesoRendicionVO();
                    process.doProcesarPlanilla(idCarga, pathArchivo, coreArchivo);
                }
                
                if(tipoArchivo.intValue() == 2 && tipoPlanilla.equalsIgnoreCase(Constant.PLANILLA_ANDES)){
                    ProcesoRendicionVO process = new ProcesoRendicionVO();
                    process.doProcesarAbonoPlanillaAndes(idCarga, pathArchivo, coreArchivo, idCargaAbonoAndes);
                }
                
                if(tipoArchivo.intValue() == 2 && tipoPlanilla.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)){
                    ProcesoRendicionVO process = new ProcesoRendicionVO();
                    process.doProcesarAbonoPlanillaLaAraucana(idCarga, pathArchivo, coreArchivo, idCargaAbonoAndes,null);
                }
                
            }else{
                throw new Exception("Problema al instaciar el la clase del Core"); 
            }
        }catch (Exception e){
            e.printStackTrace();            
        }
        
    }
}
