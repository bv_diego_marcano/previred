package cl.bicevida.previred.view.job.listener;

import java.io.Serializable;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;


/**
 * ServletListener, que elimina los job que por x motivos quedaron en pause, limpiando la base de datos del modelo quartz al momento de redeployar la app.
 */
public class QuartzListener implements Serializable, ServletContextListener {
    @SuppressWarnings("compatibility:-2160577332364512991")
    private static final long serialVersionUID = 1L;
    private static final String groupJobName = "group_carga_previred";
    private JobDetail job = null;
    private transient Scheduler scheduler;
    private transient final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(QuartzListener.class);
    

    public QuartzListener() {
        super();
    }

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        logger.info("[contextInitialized]Inicio...");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.info("[contextDestroyed]Inicio...");
        StdSchedulerFactory factory;
        
        try {
            factory = new StdSchedulerFactory();
            scheduler = factory.getScheduler();
            
            for (String group : scheduler.getJobGroupNames()) {
                // enumerate each job in group
                final GroupMatcher<JobKey> groupMatcher = GroupMatcher.groupEquals(group);
                if(groupMatcher.getCompareToValue().equalsIgnoreCase(groupJobName)){
                    for (JobKey jobKey : scheduler.getJobKeys(groupMatcher) ) {
                        logger.debug("Nombre del job en ejecucion " + jobKey);
                        this.eliminaJob(jobKey);
                    }
                }
            }
            this.jobDestroyed(scheduler);
        } catch (SchedulerException e) {
            e.printStackTrace();
    
        }
        logger.info("[contextDestroyed]FIN Job en Proceso");
    }
    
    
    public void jobDestroyed(Scheduler sch) {
    try {
            if (sch != null) {
                sch.shutdown(true);
            } else {
                logger.info("Quartz Scheduler esta nulo.");
            }

            logger.info("Quartz Scheduler baj� exitosamente.");

        } catch (Exception e) {
            logger.info("Quartz Scheduler fall� al bajar: " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Proceso que elimina la instancia de BD de quartz para asi poder levantar sin errores
     */
    public void eliminaJob(JobKey job) {
        logger.info("Quartz Scheduler procede a eliminar job activo[" + job + "]");
        try {
            scheduler.deleteJob(job);
        } catch (Exception e) {
            logger.info("Quartz Scheduler fall� al elminar la instancia de BD: " + e.toString());
        }
    }
    
    
    
}
