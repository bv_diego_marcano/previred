package cl.bicevida.previred.view.job.listener;

import cl.bicevida.previred.view.job.JobLecturalSftpPrevired;
import cl.bicevida.previred.view.job.JobRecaudacionAutomatica;

import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.servlet.ServletException;

import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import static org.quartz.CronScheduleBuilder.cronSchedule;

public class QuartzLecturaSftp implements ServletContextListener{
    
    private static final org.apache.log4j.Logger logger =
        org.apache.log4j.Logger.getLogger(QuartzLecturaSftp.class);
    
    private ServletContext context = null;
    public static final String QUARTZ_FACTORY_KEY = "org.quartz.impl.StdSchedulerFactory.KEY";
    private boolean performShutdown = true;
    private Scheduler scheduler = null;
    private JobDetail jobDetail = null;
    private static final String groupJobName = "LecturaSftpPreviredGroup";
    
    
    public QuartzLecturaSftp() {
        super();
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
        logger.info("Quartz Scheduler Lectura SFTP Iniciando...");
        StdSchedulerFactory factory;

        try {

            String configFile = context.getInitParameter("config-file");
            String shutdownPref = context.getInitParameter("shutdown-on-unload");

            if (shutdownPref != null) {
                performShutdown = Boolean.valueOf(shutdownPref).booleanValue();
            }

            // get Properties
            if (configFile != null) {
                factory = new StdSchedulerFactory(configFile);
            } else {
                factory = new StdSchedulerFactory();
            }

            scheduler = factory.getScheduler();

            // Should the Scheduler being started now or later
            String startOnLoad = context.getInitParameter("start-scheduler-on-load");

            if (startOnLoad == null || (Boolean.valueOf(startOnLoad).booleanValue())) {
                logger.info("Creando tareas...");
                task();
                scheduler.start();
                logger.info("Scheduler Lectura SFTP se ha iniciado...");

            } else {
                logger.info("Scheduler has not been started. Use scheduler.start()");
            }

            String factoryKey = context.getInitParameter("servlet-context-factory-key");
            //String factoryKey = null;
            if (factoryKey == null) {
                factoryKey = QUARTZ_FACTORY_KEY;
            }

            logger.info("Storing the Quartz Scheduler Factory in the servlet context at key: " + factoryKey);
            context.setAttribute(factoryKey, factory);

        } catch (Exception e) {
            logger.info("Quartz Scheduler failed to initialize: " + e.toString());
            try {
                throw new ServletException(e);
            } catch (ServletException f) {
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
        StdSchedulerFactory factory;
                
                try {
                    factory = new StdSchedulerFactory();
                    scheduler = factory.getScheduler();
                    
                    for (String group : scheduler.getJobGroupNames()) {
                        // enumerate each job in group
                        final GroupMatcher<JobKey> groupMatcher = GroupMatcher.groupEquals(group);
                        if(groupMatcher.getCompareToValue().equalsIgnoreCase(groupJobName)){
                            for (JobKey jobKey : scheduler.getJobKeys(groupMatcher) ) {
                                logger.debug("Nombre del job en ejecucion " + jobKey);
                                this.eliminaJob(jobKey);
                            }
                        }
                    }
                    this.jobDestroyed(scheduler);
                } catch (SchedulerException e) {
                    e.printStackTrace();
                
                }
                logger.info("[contextDestroyed]FIN Job en Proceso");
    }
    
    public void task() throws SchedulerException {
        
        try {
            this.jobDetail = newJob(JobLecturalSftpPrevired.class).withIdentity("Job_Lectura_sftp_previred",groupJobName).build();
            
            //De Lunes a Viernes de las 6 AM a las 18PM con intervalo de 30 min*/
            Trigger trigger =
                newTrigger().
                withIdentity("trigger_" + "LecturaSftpPreviredTrigger",groupJobName).
                withSchedule(cronSchedule("0 0/29 6-18 ? * MON,TUE,WED,THU,FRI *")).forJob(this.jobDetail.getKey()).build(); 
            
            /*Trigger trigger = newTrigger().withIdentity("trigger_" + "LecturaSftpPreviredTrigger",groupJobName).
                startAt(new Date()).endAt(new Date()).
                withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(2).).forJob(this.jobDetail.getKey()).build();*/
            
            // schedule a job with JobDetail and Trigger
            logger.info("Quartz subiendo JOB Job_Lectura_sftp_previred corriendo de Lunes a Viernes a las 6 AM hasta las 18PM cada 29 min");
            scheduler.scheduleJob(jobDetail, trigger);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void jobDestroyed(Scheduler sch) {
        try {
                if (sch != null) {
                    sch.shutdown(true);
                } else {
                    logger.info("Quartz Scheduler esta nulo.");
                }

                logger.info("Quartz Scheduler baj� exitosamente.");

            } catch (Exception e) {
                logger.info("Quartz Scheduler fall� al bajar: " + e.toString());
                e.printStackTrace();
            }
        }
        public void eliminaJob(JobKey job) {
            logger.info("Quartz Scheduler procede a eliminar job activo[" + job + "]");
            try {
                scheduler.deleteJob(job);
            } catch (Exception e) {
                logger.info("Quartz Scheduler fall� al elminar la instancia de BD: " + e.toString());
            }
        }
}
