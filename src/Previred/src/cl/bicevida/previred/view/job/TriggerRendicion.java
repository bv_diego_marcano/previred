package cl.bicevida.previred.view.job;

import cl.bicevida.previred.core.abono.PreviredCore;
import cl.bicevida.previred.view.util.JsfUtil;

import java.io.Serializable;

import java.util.List;

import javax.servlet.ServletContextEvent;

import org.apache.log4j.Logger;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.KeyMatcher;


/**
 * Proceso que se encarga de crear la instancia de un job
 */
public class TriggerRendicion implements Serializable {
    @SuppressWarnings("compatibility:-858666808739661502")
    private static final long serialVersionUID = 1L;

    public TriggerRendicion() {
    }

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(TriggerRendicion.class);
    private static final String QUARTZ_PROPERTIES = "configurationbice";
    private static final String groupJobName = "group_carga_previred";
    private JobDetail job = null;
    private transient Scheduler scheduler;


    /*
    public void run(Double idCarga, Integer tipoCarga, String pathArchivoRendicion, PreviredCore core, String tipoPlanilla, Double idCargaAbonoAndes) throws Exception {
        logger.debug("run() - Inicio");
        SchedulerFactory sf = new StdSchedulerFactory();
        this.scheduler = sf.getScheduler();

        logger.debug("run() - Nuevo Job: " + "job"+idCarga + ", " + "group"+idCarga );
        JobDetail job = new JobDetail("job"+idCarga, "group"+idCarga, JobRendicion.class);

        //Tiempo de retardo para inicializacion del Job
        //long timeDelay = 5000; // 5 Seconds
        //job.getJobDataMap().put(JobRendicion.DELAY_TIME, timeDelay);
        job.getJobDataMap().put(JobRendicion.ID_CARGA, idCarga);
        job.getJobDataMap().put(JobRendicion.TIPO_ARCHIVO, tipoCarga);
        job.getJobDataMap().put(JobRendicion.PATH_ARCHIVO, pathArchivoRendicion);
        job.getJobDataMap().put(JobRendicion.CORE, core);
        job.getJobDataMap().put(JobRendicion.TIPOPLANILLA, tipoPlanilla);
        job.getJobDataMap().put(JobRendicion.IDCARGAABONOANDES, idCargaAbonoAndes);


        //Datos mapeados
        logger.debug("run() - idCarga: " + idCarga);
        logger.debug("run() - tipoCarga: " + tipoCarga);
        logger.debug("run() - pathArchivoRendicion: " + pathArchivoRendicion);
        logger.debug("run() - core: " + core);
        logger.debug("run() - tipoPlanilla: " + tipoPlanilla);
        logger.debug("run() - idCargaAbonoAndes: " + idCargaAbonoAndes);


        //Ejecuci�n en forma indefinida cada 10 segundos
        SimpleTrigger trigger = new SimpleTrigger("trigger"+idCarga, "group"+idCarga, "job"+idCarga, "group"+idCarga, new Date(), null, SimpleTrigger.REPEAT_INDEFINITELY, 10000L);
        //SimpleTrigger trigger = new SimpleTrigger("trigger"+idCarga, "group"+idCarga, "job"+idCarga, "group"+idCarga, new Date(), null, 4, 10000L);

        logger.debug("run() - Instanciar JOB: " + core);
        this.scheduler.scheduleJob(job, trigger);

        //inciar  scheduler
        //scheduler.start();


        //terminar sheduler
        //scheduler.shutdown(true);

         logger.debug("run() - Termino");
    }*/

    /**
     *
     * @param idCarga
     * @param tipoCarga
     * @param pathArchivoRendicion
     * @param core
     * @param tipoPlanilla
     * @param idCargaAbonoAndes
     */
    public void run(Double idCarga, Integer tipoCarga, String pathArchivoRendicion, PreviredCore core, String tipoPlanilla, Double idCargaAbonoAndes) {

        //TODO: SE DEBE CORRER ESTO PARA EVITAR BUG DE REGISTRO DE HORA ALTER TABLE QRTZ_FIRED_TRIGGERS MODIFY ('SCHED_TIME' NULL);
        logger.debug("run() - Quartz Initializer Servlet loaded, initializing Scheduler...");
        logger.debug("run() - Nuevo Job: " + "job" + idCarga + ", " + "group" + groupJobName);
        StdSchedulerFactory factory;

        try {

            String configFile = JsfUtil.getMessage(QUARTZ_PROPERTIES, "cl.bicevida.quartz.prop.configFile");
            String startOnLoad = JsfUtil.getMessage(QUARTZ_PROPERTIES, "cl.bicevida.quartz.prop.start.scheduler.on.load");
            String cronExpression = JsfUtil.getMessage(QUARTZ_PROPERTIES, "cl.bicevida.quartz.prop.cron.expression");

            /*if (shutdownPref != null) {
                performShutdown = Boolean.valueOf(shutdownPref).booleanValue();
            }*/

            if (configFile != null) {
                factory = new StdSchedulerFactory(configFile);
            } else {
                factory = new StdSchedulerFactory();
            }
            scheduler = factory.getScheduler();


            logger.debug("run() - Job en Proceso");
            List<JobExecutionContext> currentJobs = scheduler.getCurrentlyExecutingJobs();
            for (JobExecutionContext jobCtx : currentJobs) {
                String jobName = jobCtx.getJobDetail().getDescription();
                logger.debug("run() - Job: " + jobName);
            }
            logger.debug("run() - FIN Job en Proceso");

            if (startOnLoad == null || (Boolean.valueOf(startOnLoad).booleanValue())) {
                logger.info("CronExpression Configurada : " + cronExpression);
                JobKey jobKey = new JobKey("job_" + idCarga, groupJobName);
                job = newJob(JobRendicion.class).withIdentity(jobKey).build();

                job.getJobDataMap().put(JobRendicion.ID_CARGA, idCarga);
                job.getJobDataMap().put(JobRendicion.TIPO_ARCHIVO, tipoCarga);
                job.getJobDataMap().put(JobRendicion.PATH_ARCHIVO, pathArchivoRendicion);
                job.getJobDataMap().put(JobRendicion.CORE, core);
                job.getJobDataMap().put(JobRendicion.TIPOPLANILLA, tipoPlanilla);
                job.getJobDataMap().put(JobRendicion.IDCARGAABONOANDES, idCargaAbonoAndes);

                Trigger trigger =
                    newTrigger().withIdentity("trigger_" + idCarga, groupJobName )
                                .withSchedule(cronSchedule(cronExpression))
                                .forJob(job.getKey())
                                //.forJob(job.getKey())
                                .build();
                
                scheduler.scheduleJob(job, trigger);
                //scheduler.start();
                logger.info("El calendario fue iniciado...");

            } else {
                logger.info("El calendario no fue iniciado. Use scheduler.start()");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void jobDestroyed(Boolean performShutdown) {

        if (!performShutdown) {
            return;
        }
        try {
            if (scheduler != null) {
                this.eliminaJob();
                scheduler.shutdown(true);
            } else {
                logger.info("Quartz Scheduler esta nulo.");
            }

            logger.info("Quartz Scheduler baj� exitosamente.");

        } catch (Exception e) {
            logger.info("Quartz Scheduler fall� al bajar: " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Proceso que elimina la instancia de BD de quartz para asi poder levantar sin errores
     */
    public void eliminaJob() {
        logger.info("se elimina proceso Quartz Scheduler de BD[" + job.getKey() + "]");
        try {
            scheduler.deleteJob(job.getKey());
        } catch (Exception e) {
            logger.info("Quartz Scheduler fall� al elminar la instancia de BD: " + e.toString());
        }
    }

    public void setScheduler(Scheduler scheduler) {
        this.scheduler = scheduler;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public static void main(String[] args) throws Exception {

        TriggerRendicion example = new TriggerRendicion();
        //example.run(1, "PLANILLA");
    }
    
    //TODO: ejecutar job de forma manual.
    
    public void manualAction(){
        JobKey jobKey = new JobKey(job.getKey().getName(), job.getKey().getGroup());
        try {
            this.getScheduler().triggerJob(jobKey);
        } catch (SchedulerException e) {
            System.out.println("problema al ejecutar job de forma manual"+ e.toString());
        }
    }
    
    
    
}
