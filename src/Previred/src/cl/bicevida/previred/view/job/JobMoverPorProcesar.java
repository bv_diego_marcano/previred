package cl.bicevida.previred.view.job;

import cl.bicevida.previred.async.procesoautomatico.LecturaSftpPrevired;

import cl.bicevida.previred.async.procesoautomatico.MoverPorProcesar;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * GR:29-06-2018 Job encargado de leer el directorio sftp de previred
 */
public class JobMoverPorProcesar implements Job{
    public JobMoverPorProcesar() {
        super();
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        MoverPorProcesar mpp = new MoverPorProcesar();
        try{
            mpp.doMoverArchivos();
        
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
