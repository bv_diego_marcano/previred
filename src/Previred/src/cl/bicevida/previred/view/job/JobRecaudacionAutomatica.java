package cl.bicevida.previred.view.job;

import cl.bicevida.previred.async.procesoautomatico.RecaudacionAutomaticaCajas;
import cl.bicevida.previred.async.procesoautomatico.RecaudacionAutomaticaPrevired;
import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.locator.ServiceLocator;
import cl.bicevida.previred.model.PreviredEJB;
import cl.bicevida.previred.util.ResourcesAsyncUtil;
import cl.bicevida.previred.view.vo.ProcesoRendicionVO;

import java.util.Date;

import javax.naming.NamingException;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/*
 * Se realizo en dos clases para separa solamente el codigo correspondiente a
 * previred, caja los andes y araucana.
 * */
public class JobRecaudacionAutomatica implements Job {
    private PreviredEJB ejb;

    public JobRecaudacionAutomatica() {
        super();
        ejb = ServiceLocator.getSessionEJBPrevired();
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        RecaudacionAutomaticaPrevired recAutomaticaPrevired = new RecaudacionAutomaticaPrevired();
        RecaudacionAutomaticaCajas recAutomaticaCaja = new RecaudacionAutomaticaCajas();

        //valido que sea un dia habil
        try {
            if (ejb.validaDiaHabil(new Date())) {
                // Proceso Previred
                try {
                    Thread.sleep(5000);
                    recAutomaticaPrevired.doProcesarAction();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                // Proceso Caja los Andes
                try {
                    Thread.sleep(5000);
                    String ruta = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.caja.andes");
                    recAutomaticaCaja.doProcesarActionCaja(Constant.PLANILLA_ANDES, Constant.PLANILLA_ANDES_CONVENIO,
                                                           ruta);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                // Proceso Caja la Araucana
                try {
                    Thread.sleep(5000);
                    String ruta = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.caja.araucana");
                    recAutomaticaCaja.doProcesarActionCaja(Constant.PLANILLA_LAARAUCANA,
                                                           Constant.PLANILLA_LAARAUCANA_CONVENIO, ruta);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }else{System.out.println("No es dia habil");}
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
