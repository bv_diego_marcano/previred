package cl.bicevida.previred.view.job;

import cl.bicevida.previred.async.procesoautomatico.LecturaSftpPrevired;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * GR:29-06-2018 Job encargado de leer el directorio sftp de previred
 */
public class JobLecturalSftpPrevired implements Job{
    public JobLecturalSftpPrevired() {
        super();
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try{
            new LecturaSftpPrevired().doProcesarSFTP();
        
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
