package cl.bicevida.previred.view.job.listener;

import cl.bicevida.previred.view.job.JobLecturalSftpPrevired;
import cl.bicevida.previred.view.job.JobMoverPorProcesar;
import cl.bicevida.previred.view.job.JobRecaudacionAutomatica;
import cl.bicevida.previred.util.ResourcesAsyncUtil;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import javax.servlet.ServletException;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.JobBuilder.newJob;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;
import static org.quartz.TriggerBuilder.newTrigger;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.matchers.GroupMatcher;
import static org.quartz.CronScheduleBuilder.cronSchedule;


public class QuartzMoverPorProcesar implements ServletContextListener{
    
    private static final org.apache.log4j.Logger logger =
        org.apache.log4j.Logger.getLogger(QuartzMoverPorProcesar.class);
    
    private ServletContext context = null;
    public static final String QUARTZ_FACTORY_KEY = "org.quartz.impl.StdSchedulerFactory.KEY";
    private boolean performShutdown = true;
    private Scheduler scheduler = null;
    private JobDetail jobDetail = null;
    private static final String groupJobName = "MoverPorProcesarPreviredGroup";
    
    
    public QuartzMoverPorProcesar() {
        super();
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        context = event.getServletContext();
        logger.info("Quartz Scheduler MoverPorProcesar Iniciando...");
        StdSchedulerFactory factory;

        try {

            String configFile = context.getInitParameter("config-file");
            String shutdownPref = context.getInitParameter("shutdown-on-unload");

            if (shutdownPref != null) {
                performShutdown = Boolean.valueOf(shutdownPref).booleanValue();
            }

            // get Properties
            if (configFile != null) {
                factory = new StdSchedulerFactory(configFile);
            } else {
                factory = new StdSchedulerFactory();
            }

            scheduler = factory.getScheduler();

            // Should the Scheduler being started now or later
            String startOnLoad = context.getInitParameter("start-scheduler-on-load");

            if (startOnLoad == null || (Boolean.valueOf(startOnLoad).booleanValue())) {
                logger.info("Creando tareas...");
                task();
                scheduler.start();
                logger.info("Scheduler MoverPorProcesar se ha iniciado...");

            } else {
                logger.info("Scheduler has not been started. Use scheduler.start()");
            }

            String factoryKey = context.getInitParameter("servlet-context-factory-key");
            //String factoryKey = null;
            if (factoryKey == null) {
                factoryKey = QUARTZ_FACTORY_KEY;
            }

            logger.info("Storing the Quartz Scheduler Factory in the servlet context at key: " + factoryKey);
            context.setAttribute(factoryKey, factory);

        } catch (Exception e) {
            logger.info("Quartz Scheduler failed to initialize: " + e.toString());
            try {
                throw new ServletException(e);
            } catch (ServletException f) {
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        context = event.getServletContext();
        StdSchedulerFactory factory;
                
                try {
                    factory = new StdSchedulerFactory();
                    scheduler = factory.getScheduler();
                    
                    for (String group : scheduler.getJobGroupNames()) {
                        // enumerate each job in group
                        final GroupMatcher<JobKey> groupMatcher = GroupMatcher.groupEquals(group);
                        if(groupMatcher.getCompareToValue().equalsIgnoreCase(groupJobName)){
                            for (JobKey jobKey : scheduler.getJobKeys(groupMatcher) ) {
                                logger.debug("Nombre del job en ejecucion " + jobKey);
                                this.eliminaJob(jobKey);
                            }
                        }
                    }
                    this.jobDestroyed(scheduler);
                } catch (SchedulerException e) {
                    e.printStackTrace();
                
                }
                logger.info("[contextDestroyed]FIN Job en Proceso");
    }
    
    public void task() throws SchedulerException {
        
        try {
            this.jobDetail = newJob(JobMoverPorProcesar.class).withIdentity("Job_MoverPorProcesar_previred",groupJobName).build();
            Trigger trigger =
                newTrigger().
                withIdentity("trigger_" + "MoverPorProcesarTrigger",groupJobName).
            withSchedule(cronSchedule(ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.cron.mpp"))).forJob(this.jobDetail.getKey()).build();//Esto es para pruebas
            
                //withSchedule(cronSchedule("0 * * ? * * *")).forJob(this.jobDetail.getKey()).build();//Esto es para pruebas
                //withSchedule(cronSchedule("0 0 7 1W * ? *")).forJob(this.jobDetail.getKey()).build();//Primera semana del mes a las 7 AM( 0 0 1 1W * ? * )
            
            
            /*Trigger trigger = newTrigger().withIdentity("trigger_" + "MoverPorProcesarTrigger",groupJobName).
                withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(2).repeatForever()).forJob(this.jobDetail.getKey()).build();*/
            
            // schedule a job with JobDetail and Trigger
            logger.info("Quartz subiendo JOB Job_MoverPorProcesar_previred");
            scheduler.scheduleJob(jobDetail, trigger);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void jobDestroyed(Scheduler sch) {
        try {
                if (sch != null) {
                    sch.shutdown(true);
                } else {
                    logger.info("Quartz Scheduler esta nulo.");
                }

                logger.info("Quartz Scheduler baj� exitosamente.");

            } catch (Exception e) {
                logger.info("Quartz Scheduler fall� al bajar: " + e.toString());
                e.printStackTrace();
            }
        }
        public void eliminaJob(JobKey job) {
            logger.info("Quartz Scheduler procede a eliminar job activo[" + job + "]");
            try {
                scheduler.deleteJob(job);
            } catch (Exception e) {
                logger.info("Quartz Scheduler fall� al elminar la instancia de BD: " + e.toString());
            }
        }
}
