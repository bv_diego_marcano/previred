package cl.bicevida.previred.servlet;

import cl.bicevida.previred.async.procesoautomatico.RecaudacionAutomaticaCajas;
import cl.bicevida.previred.async.procesoautomatico.RecaudacionAutomaticaPrevired;
import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.locator.ServiceLocator;

import cl.bicevida.previred.model.PreviredEJB;

import cl.bicevida.previred.util.ResourcesAsyncUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.Date;


import javax.servlet.*;
import javax.servlet.http.*;

public class ServletDePrueba extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            PreviredEJB ejb = ServiceLocator.getSessionEJBPrevired();

            //if (ejb.validaDiaHabil(new Date())) {
                /*String fechaPeriodoActual =
                    FechaUtil.getFechaFormateoCustom(FechaUtil.sumarMesesFecha(new Date(), -1), "yyyyMM");
                Date fechaValida = null;
                //Date primerDiaMes  = FechaUtil.getFistDayOfMonth();
                Date primerDiaMes = FechaUtil.getPrimerDiaMesAnterior();
                // valida el ultimo dia del periodo actual
                // 12 d�as son el plazo maximo del periodo de cotizacion
                for (int i = 0; i <= 10; i++) {
                    fechaValida = ejb.retornaDiaHabil(primerDiaMes);
                    System.out.println(FechaUtil.getFechaFormateoCustom(fechaValida, "dd-MM-yyyy"));
                    primerDiaMes = FechaUtil.sumarDiasFecha(fechaValida, 1);
                    System.out.println(FechaUtil.getFechaFormateoCustom(primerDiaMes, "dd-MM-yyyy"));
                }
                Integer diaLimite = FechaUtil.getDiaFecha(fechaValida);
                System.out.println("diaLimite " + diaLimite);*/

                try {
                    Thread.sleep(5000);
                    new RecaudacionAutomaticaPrevired().doProcesarAction();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                // Proceso Caja los Andes
                try {
                    Thread.sleep(5000);
                    String ruta = ResourcesAsyncUtil.getProperty("cl.bicevida.rendautomatica.caja.andes");
                    new RecaudacionAutomaticaCajas().doProcesarActionCaja(Constant.PLANILLA_ANDES,
                                                                          Constant.PLANILLA_ANDES_CONVENIO, ruta);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            //}

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        StringBuffer jb = new StringBuffer();
          String line = null;
          try {
            BufferedReader reader = request.getReader();
            while ((line = reader.readLine()) != null)
              jb.append(line);
          } catch (Exception e) {  }

            System.out.println("jb"+jb.toString());
    }*/
}
