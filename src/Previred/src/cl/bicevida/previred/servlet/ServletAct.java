package cl.bicevida.previred.servlet;

import cl.bicevida.previred.async.procesoautomatico.ActProcesoRendicion;
import cl.bicevida.previred.async.procesoautomatico.LecturaSftpPrevired;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

public class ServletAct extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String estado = request.getParameter("estado");
        String idCarga= request.getParameter("idCarga");
        System.out.println("Id de carga a actualizar ["+idCarga+"] con el estado ["+estado+"]");
        new ActProcesoRendicion().actualizarProceso(Integer.parseInt(estado),"OTRO ESTADO",Long.parseLong(idCarga),"OTRO ESTADO");
    }

    
}
