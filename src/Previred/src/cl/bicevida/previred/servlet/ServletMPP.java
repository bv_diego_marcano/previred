package cl.bicevida.previred.servlet;

import cl.bicevida.previred.async.procesoautomatico.MoverPorProcesar;

import cl.bicevida.previred.common.utils.FechaUtil;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

public class ServletMPP extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        new MoverPorProcesar().doMoverArchivosByFecha(FechaUtil.getAnoFecha(new java.util.Date()),
                                                      FechaUtil.getMesFecha(new java.util.Date())-1);
    }
}
