package cl.bicevida.previred.servlet;

import cl.bicevida.previred.async.procesoautomatico.LecturaSftpPrevired;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

public class ServletSFTP extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=windows-1252";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        new LecturaSftpPrevired().doProcesarSFTP();
    }
}
