// !DO NOT EDIT THIS FILE!
// This source file is generated by Oracle tools
// Contents may be subject to change
// For reporting problems, use the following
// Version = Oracle WebServices (10.1.3.5.0, build 090727.2000.36696)

package cl.bicevida.previred.proxy.runtime;

import oracle.j2ee.ws.common.encoding.*;
import oracle.j2ee.ws.common.encoding.literal.*;
import oracle.j2ee.ws.common.encoding.literal.DetailFragmentDeserializer;
import oracle.j2ee.ws.common.encoding.simpletype.*;
import oracle.j2ee.ws.common.soap.SOAPEncodingConstants;
import oracle.j2ee.ws.common.soap.SOAPEnvelopeConstants;
import oracle.j2ee.ws.common.soap.SOAPVersion;
import oracle.j2ee.ws.common.streaming.*;
import oracle.j2ee.ws.common.wsdl.document.schema.SchemaConstants;
import oracle.j2ee.ws.common.util.xml.UUID;
import javax.xml.namespace.QName;
import java.util.List;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.AttachmentPart;

public class Registro_LiteralSerializersrc extends LiteralObjectSerializerBase implements Initializable {
    private static final QName ns1_to_QNAME = new QName("http://services.mail.ws.bicevida.cl/", "to");
    private static final QName ns2_string_TYPE_QNAME = SchemaConstants.QNAME_TYPE_STRING;
    private CombinedSerializer myns2_string__java_lang_String_String_Serializer;
    private static final QName ns1_attachment_QNAME = new QName("http://services.mail.ws.bicevida.cl/", "attachment");
    private static final QName ns1_valores_QNAME = new QName("http://services.mail.ws.bicevida.cl/", "valores");
    private static final QName ns1_ListOfString_TYPE_QNAME = new QName("http://services.mail.ws.bicevida.cl/", "ListOfString");
    private CombinedSerializer myns1_ListOfString__ListOfString_LiteralSerializersrc;
    
    public Registro_LiteralSerializersrc(QName type) {
        this(type,  false);
    }
    
    public Registro_LiteralSerializersrc(QName type, boolean encodeType) {
        super(type, true, encodeType);
        setSOAPVersion(SOAPVersion.SOAP_11);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws Exception {
        myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer("", java.lang.String.class, ns2_string_TYPE_QNAME);
        myns1_ListOfString__ListOfString_LiteralSerializersrc = (CombinedSerializer)registry.getSerializer("", cl.bicevida.previred.proxy.ListOfString.class, ns1_ListOfString_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(XMLReader reader,
        SOAPDeserializationContext context) throws Exception {
        cl.bicevida.previred.proxy.Registro instance = new cl.bicevida.previred.proxy.Registro();
        java.lang.Object member=null;
        QName elementName;
        List values;
        java.lang.Object value;
        
        reader.nextElementContent();
        java.util.HashSet requiredElements = new java.util.HashSet();
        requiredElements.add("to");
        requiredElements.add("attachment");
        requiredElements.add("valores");
        for (int memberIndex = 0; memberIndex <3; memberIndex++) {
            elementName = reader.getName();
            if ((reader.getState() == XMLReader.START) && (matchQName(elementName, ns1_to_QNAME))) {
                myns2_string__java_lang_String_String_Serializer.setNullable( false );
                context.setNillable( true );
                member = myns2_string__java_lang_String_String_Serializer.deserialize(ns1_to_QNAME, reader, context);
                requiredElements.remove("to");
                instance.setTo((java.lang.String)member);
                context.setXmlFragmentWrapperName( null );
                reader.nextElementContent();
            }
            if ((reader.getState() == XMLReader.START) && (matchQName(elementName, ns1_attachment_QNAME))) {
                myns2_string__java_lang_String_String_Serializer.setNullable( false );
                context.setNillable( true );
                member = myns2_string__java_lang_String_String_Serializer.deserialize(ns1_attachment_QNAME, reader, context);
                requiredElements.remove("attachment");
                instance.setAttachment((java.lang.String)member);
                context.setXmlFragmentWrapperName( null );
                reader.nextElementContent();
            }
            if ((reader.getState() == XMLReader.START) && (matchQName(elementName, ns1_valores_QNAME))) {
                myns1_ListOfString__ListOfString_LiteralSerializersrc.setNullable( false );
                context.setNillable( true );
                member = myns1_ListOfString__ListOfString_LiteralSerializersrc.deserialize(ns1_valores_QNAME, reader, context);
                requiredElements.remove("valores");
                instance.setValores((member == null)? null : ((cl.bicevida.previred.proxy.ListOfString)member).toArray());
                context.setXmlFragmentWrapperName( null );
                reader.nextElementContent();
            }
        }
        if (!requiredElements.isEmpty()) {
            throw new DeserializationException( "literal.expectedElementName" , requiredElements.iterator().next().toString(), DeserializationException.FAULT_CODE_CLIENT );
        }
        
        if( reader.getState() != XMLReader.END)
        {
            reader.skipElement();
        }
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (java.lang.Object)instance;
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws Exception {
        cl.bicevida.previred.proxy.Registro instance = (cl.bicevida.previred.proxy.Registro)obj;
        
    }
    public void doSerializeAnyAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws Exception {
        cl.bicevida.previred.proxy.Registro instance = (cl.bicevida.previred.proxy.Registro)obj;
        
    }
    public void doSerialize(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws Exception {
        cl.bicevida.previred.proxy.Registro instance = (cl.bicevida.previred.proxy.Registro)obj;
        
        myns2_string__java_lang_String_String_Serializer.setNullable( true );
        context.setNillable( true );
        myns2_string__java_lang_String_String_Serializer.serialize(instance.getTo(), ns1_to_QNAME, null, writer, context);
        myns2_string__java_lang_String_String_Serializer.setNullable( true );
        context.setNillable( true );
        myns2_string__java_lang_String_String_Serializer.serialize(instance.getAttachment(), ns1_attachment_QNAME, null, writer, context);
        myns1_ListOfString__ListOfString_LiteralSerializersrc.setNullable( true );
        context.setNillable( true );
        cl.bicevida.previred.proxy.ListOfString instanceGetValores_arrayWrapper = (instance.getValores() == null) ? null : new cl.bicevida.previred.proxy.ListOfString(instance.getValores());
        myns1_ListOfString__ListOfString_LiteralSerializersrc.serialize(instanceGetValores_arrayWrapper, ns1_valores_QNAME, null, writer, context);
    }
}
