package cl.bicevida.previred.locator;

import cl.bicevida.previred.model.PreviredEJB;

import java.io.Serializable;

import java.util.Hashtable;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

public class ServiceLocator implements Serializable {
    @SuppressWarnings("compatibility:5606798930956893891")
    private static final long serialVersionUID = 1L;

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(ServiceLocator.class);

    // Singleton instance
    private static ServiceLocator serviceLocator = new ServiceLocator();
    
    // Cache of objects in JNDI tree
    private static Hashtable homeCache;
    
    // Initial context
    private static InitialContext defaultContext;
    
    
    /**
    * Private constructor, which initializes all the tables and the default
    * InitialContext.
    */
    private ServiceLocator() {    
        try {          
          homeCache        = new Hashtable();
          defaultContext   = new InitialContext();          
        } catch(Exception ex) {
          logger.error("Exception in the constructor of ServiceLocator class : " + ex.getMessage(), ex);
        }
    }
    
    /**
    * Method to access the Singleton instance of the ServiceLocator
    *
    * @return <b>ServiceLocator</b> The instance of this class
    */
    public static ServiceLocator getLocator() {
        return serviceLocator;
    }
    /**
    * Method to return an object in the default JNDI context, with  the supplied
    * JNDI name.
    * @param <b>jndiName</b> The JNDI name
    * @return <b>Object</b> The object in the JNDI tree for this name.
    * @throws <b>UtilityException</b> Exception this method can throw
    */
    public static Object getService(String jndiName) throws ServiceLocatorException {
        
        try {        
          if(!homeCache.containsKey(jndiName)) {
            homeCache.put(jndiName, defaultContext.lookup(jndiName));
          }
        } catch(NamingException ex) {
           logger.error("getService(String) - NamingException :" + ex.getMessage(), ex);    
          throw new ServiceLocatorException("Exception thrown from getService method of ServiceLocator class : " + ex.getMessage());
        } catch(SecurityException ex) {
          logger.error("getService(String) - SecurityException :" + ex.getMessage(), ex);        
          throw new ServiceLocatorException("Exception thrown from from getService method of ServiceLocator class : " + ex.getMessage());
        }
        Object returnObject = homeCache.get(jndiName);        
        return returnObject;
    }


    /**
     * Recupera el EJB 3.0 de ultima version con cambios
     * centralizados y funcionado perfectamente con patrones
     * de dis�o y DAO como persistencia de datos y no TOPLINK
     * @return the Local EJB Home corresponding to the homeName
     * @throws NamingException 
     */
    public static PreviredEJB getSessionEJBPrevired() throws ServiceLocatorException {        
      //  PreviredEJB ejb = (PreviredEJB)getService("PreviredEJB");
       // return ejb;    
        
        //MantenedorEJB ejb = (MantenedorEJB) getService("MantenedorEJB");      
             final String ejbLookup = "Previred-PreviredCore-PreviredEJBBean#cl.bicevida.previred.model.PreviredEJB";

                       try {
                           PreviredEJB ejb = (PreviredEJB)defaultContext.lookup(ejbLookup);
                           return ejb;
                       } catch (NamingException e) {
                           throw new ServiceLocatorException(e);
                       }
        
        
    }
}
