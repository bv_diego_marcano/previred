package cl.bicevida.previred.util;

import java.io.Serializable;

import java.util.MissingResourceException;
import java.util.ResourceBundle;


public class ResourcesMessagesUtil implements Serializable {
    @SuppressWarnings("compatibility:4626327803095648898")
    private static final long serialVersionUID = 1L;

    private static final String BUNDLE_NAME = "messages";

        private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

        public ResourcesMessagesUtil() {
        }
        
        /**
         * Recupera Propiedades del Resources
         * @param key
         * @return String
         */
        public static String getProperty(String key) {
                try {
                        return RESOURCE_BUNDLE.getString(key);
                } catch (MissingResourceException e) {
                        return '!' + key + '!';
                }
        }    
        
}
