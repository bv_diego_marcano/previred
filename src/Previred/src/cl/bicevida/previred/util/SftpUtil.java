package cl.bicevida.previred.util;

import cl.bicevida.previred.model.PreviredEJB;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import com.jcraft.jsch.SftpException;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.InputStream;
import java.io.OutputStream;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * GR 03-07-2018: Clase de utilidades para conectarse al servidor SFTP de previred
 *
 */
public class SftpUtil {

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(SftpUtil.class);

    /**
     * Sesion SFTP establecida.
     */
    private static Session session;

    public SftpUtil() {
        super();
    }

    /**
     * Metodo para establer la conexion al servidor.
     * @param username
     * @param password
     * @param host
     * @param port
     * @throws JSchException
     * @throws IllegalAccessException
     */
    public static void connect(String username, String password, String host, int port) throws JSchException,
                                                                                               IllegalAccessException {
        logger.info("Conectandose al servidor: [" + host + ":" + port + "] ");
        logger.info("Usuario: [" + username + "]");
//        logger.info("Password: [" + password + "]"); //QUITAR ESTE LOG

        if (session == null || !session.isConnected()) {
            JSch jsch = new JSch();

            session = jsch.getSession(username, host, port);
            session.setPassword(password);

            // Parametro para no validar key de conexion.
            session.setConfig("StrictHostKeyChecking", "no");

            session.connect();
            logger.info("Se ha conectado exitosamente.");
        } else {
            logger.error("Sesion SFTP ya iniciada.");
            throw new IllegalAccessException("Sesion SFTP ya iniciada.");
        }
    }

    /**
     * Metodo para agregar archivo en la ruta especificada
     * @param ftpPath
     * @param filePath
     * @param fileName
     * @throws IllegalAccessException
     * @throws IOException
     * @throws SftpException
     * @throws JSchException
     */
    public static void addFile(String ftpPath, String filePath, String fileName) throws IllegalAccessException,
                                                                                        IOException, SftpException,
                                                                                        JSchException {
        if (session != null && session.isConnected()) {

            // Abrimos un canal SFTP.
            ChannelSftp channelSftp = (ChannelSftp) session.openChannel("sftp");
            channelSftp.connect();
            // Nos ubicamos en el directorio del FTP.
            channelSftp.cd(ftpPath);


            logger.info(String.format("Creando archivo %s en el " + "directorio %s", fileName, ftpPath));
            channelSftp.put(filePath, fileName);

            logger.info("Archivo subido exitosamente");

            channelSftp.exit();
            channelSftp.disconnect();
        } else {
            disconnect();
            throw new IllegalAccessException("No existe sesion SFTP iniciada.");
        }
    }

    /**
     * Metodo para leer y escribir los archivos del SFTP
     * Retorno una lista por en caso se necesite los nombres de los archivos.
     * @param directorio
     * @return
     */
    public static List<String> leeYEscribeArchivoSFTP(String directorioSftp, String directorioDestinoPM,
                                                      Integer diaLimite,String directorioDestinoPorProcesarPM, PreviredEJB ejb) {
        List<String> listRetorno = new ArrayList<String>();
        try {
            ChannelSftp channel = (ChannelSftp) session.openChannel("sftp");
            ChannelSftp sftp = (ChannelSftp) channel;
            channel.connect();
            sftp.cd(directorioSftp);

            Vector<String> files = sftp.ls("*");
            for (int i = 0; i < files.size(); i++) {
                Object obj = files.elementAt(i);
                if (obj instanceof com.jcraft.jsch.ChannelSftp.LsEntry) {
                    com.jcraft.jsch.ChannelSftp.LsEntry entry = (com.jcraft.jsch.ChannelSftp.LsEntry) obj;

                    if (true && !entry.getAttrs().isDir()) {
                        listRetorno.add(entry.getFilename()); //sacar este array
                        String nombreArchivo = entry.getFilename();
                        Integer diaArchivo =
                            Integer.parseInt(nombreArchivo.substring(nombreArchivo.lastIndexOf("_") - 2,
                                                                     nombreArchivo.lastIndexOf("_"))); // no se me ocurrio otra forma xd
                        if (nombreArchivo != null && !"".equals(nombreArchivo)) {
                            if (!"AC".equals(entry.getFilename().substring(0, 2).toUpperCase())) {
                                if (diaArchivo <= diaLimite) {
                                    
                                    String directorio = directorioDestinoPM + "/" + nombreArchivo;
                                    logger.info("Directorio [" + directorio + "]");
                                    logger.info("File Name [" + nombreArchivo + "]");
                                    //valido que el archivo no haya sido procesado
                                    if(ejb.existeArchivoCargado(nombreArchivo.trim()) < 1){
                                        logger.info("Escribiendo Archivo: ["+nombreArchivo+"] en directorio PREVIRED");
                                        escribirArchivoDirectorio(channel.get(nombreArchivo), directorio);
                                    }
                                }else{
                                    String directorio = directorioDestinoPorProcesarPM + "/" + nombreArchivo;
                                    logger.info("Directorio por Procesar [" + directorio + "]");
                                    logger.info("File Name Por Procesar[" + nombreArchivo + "]");
                                    if(ejb.existeArchivoCargado(nombreArchivo.trim()) < 1){
                                        logger.info("Escribiendo Archivo: ["+nombreArchivo+"] en directorio POR PROCESAR");
                                        escribirArchivoDirectorio(channel.get(nombreArchivo), directorio);
                                    }
                                }
                            }

                        }
                    }
                }
            }

        } catch (JSchException e) {
            disconnect();
            logger.error("Se ha producido un error: [" + e.getMessage() + "]");
        } catch (SftpException e) {
            disconnect();
            logger.error("Se ha producido un error: [" + e.getMessage() + "]");
        } catch (Exception e) {
            disconnect();
            logger.error("Se ha producido un error: [" + e.getMessage() + "]");
        }
        return listRetorno;
    }

    /**
     * Metodo para cerrar la conexcion SFTP
     */
    public static void disconnect() {
        session.disconnect();
        logger.info("session desconectada");
    }


    public static void escribirArchivoDirectorio(InputStream dataFile, String directorio) throws Exception {
        try {
            byte[] buffer = new byte[1024];
            BufferedInputStream bis = new BufferedInputStream(dataFile);

            File newFile = new File(directorio);
            OutputStream os;

            os = new FileOutputStream(newFile);

            if (!newFile.exists()) {
                newFile.createNewFile();
            }

            BufferedOutputStream bos = new BufferedOutputStream(os);
            int readCount;
            while ((readCount = bis.read(buffer)) > 0) {
                bos.write(buffer, 0, readCount);
            }
            bis.close();
            bos.close();
        } catch (FileNotFoundException e) {
            throw new Exception("Ha ocurrido un error [" + e.getMessage() + "]");
        } catch (IOException e) {
            throw new Exception("Ha ocurrido un error [" + e.getMessage() + "]");
        }
    }
}
