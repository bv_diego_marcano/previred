package cl.bicevida.previred.util;

import java.io.File;

import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import java.text.MessageFormat;
/**
 * GR: Clase de utilidades para mover los archivos de un directorio a otro.
 */
public class FileUtil {
    public FileUtil() {
        super();
    }
    public static String moverArchivo(String rutaOrigen, String rutaDestino, String nombreArchivo, boolean borrarOrigen) {
        String mensaje = null;
        try {

            Path FROM = Paths.get(rutaOrigen + nombreArchivo);

            File file = new File(rutaDestino + nombreArchivo);
            boolean bool = file.canWrite();
            System.out.println("puedes escribir " + bool);
            if (!bool)
                file.setWritable(true);

            Path TO = Paths.get(rutaDestino + file.getName());
            //sobreescribir el fichero de destino, si existe, y copiar
            //los atributos, incluyendo los permisos rwx
            CopyOption[] options = new CopyOption[] {
                StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES };
            Files.copy(FROM, TO, options);
            if (borrarOrigen) {
                Files.delete(FROM);
            }
            mensaje = "Se ha movido el archivo ["+nombreArchivo+"] al directorio ["+rutaDestino+"]"; 
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mensaje;
    }
}
