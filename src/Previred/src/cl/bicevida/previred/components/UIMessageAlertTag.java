package cl.bicevida.previred.components;

import cl.bicevida.previred.view.backing.MensajesMB;
import cl.bicevida.previred.view.util.JsfUtil;

import java.io.IOException;

import java.io.Serializable;

import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import org.apache.log4j.Logger;

public class UIMessageAlertTag extends UIOutput implements Serializable {
    @SuppressWarnings("compatibility:-1432060652306545885")
    private static final long serialVersionUID = 1L;

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(UIMessageAlertTag.class);
    
    public void encodeEnd(FacesContext context) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
    }
    
    public void encodeBegin(FacesContext context) throws IOException {
        ResponseWriter writer = context.getResponseWriter();        
        Boolean rendered = (Boolean)getAttributes().get("rendered");        
        if(rendered != null){
            String text = getMenssages(context);
            if(rendered.booleanValue()){
                writer.write(text);  
            }              
        }
    }
    
    /**
     * Obtiene los mensajes los muestra al final de la p?gina
     * @param facesContext
     * @return
     */
    public String getMenssages(FacesContext facesContext) {
        String text = "";
        
        MensajesMB mensajesmb = (MensajesMB)JsfUtil.getManagedBean("MensajesMB");
        
        if(mensajesmb.isAdvertencia()){
            text = 
            "\n" +
            "<script languague='javascript'>\n" +
            "   alert('" + mensajesmb.getMensaje() + "');\n" +
            "</script>" +
            "\n";
            JsfUtil.resetManagedBean("MensajesMB");
        }
        
        return text;
    }
}
