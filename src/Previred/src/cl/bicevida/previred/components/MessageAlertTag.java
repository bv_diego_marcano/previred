package cl.bicevida.previred.components;

import java.io.Serializable;

import javax.el.ValueExpression;

import javax.el.ValueReference;

import javax.faces.component.UIComponent;
//import javax.faces.el.ValueBinding;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.webapp.UIComponentELTag;
//import javax.faces.webapp.UIComponentTag;

import org.apache.log4j.Logger;

//public class MessageAlertTag extends UIComponentTag implements Serializable{
    
    public class MessageAlertTag extends UIComponentELTag{
    
    
    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(MessageAlertTag.class);

    private String rendered;
    private String _rendered;

    public void release() {
        super.release();
        rendered = null;
    }
    
    /**
     * Setea propiedades
     * @param component
     */
    
    protected void setProperties(UIComponent component) {
         
         super.setProperties(component);  
         
         if (rendered != null) {
            
            ValueExpression ve = (ValueExpression)getFacesContext().getCurrentInstance().getApplication().getExpressionFactory().
            createValueExpression(getELContext(), rendered, String.class).getValue(getELContext());
           
           
            if(ve.isLiteralText()){
           
             // if (isValueReference(rendered)) {
           
                  /*ValueExpression ve = getFacesContext().getCurrentInstance().getApplication().getExpressionFactory().
                      createValueExpression(getELContext(), rendered, String.class);*/
                  
                  component.setRendered(Boolean.valueOf(ve.getExpressionString()).booleanValue());

                  component.setValueExpression("rendered", ve);
                  
              } else {
                  component.getAttributes().put("rendered", rendered);
              }
              if (getRendererType() != null) {
                    component.setRendererType(getRendererType());
              }
          }
          

         //logger.debug("setProperties(UIComponent component=" + component + ") - termina");
    
    }
    @Override
    public String getComponentType() {
        return "messagealert";
    }
    @Override
    public String getRendererType() {
        return null;
    }

    public void setRendered(String rendered) {
        this.rendered = null;
    }


    public void set_rendered(String _rendered) {
        this.rendered = _rendered;
    }

    public String getRendered() {
        return rendered;
    }
}
