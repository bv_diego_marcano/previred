package cl.bicevida.previred.common;

import java.io.Serializable;

public class Constant implements Serializable {
    @SuppressWarnings("compatibility:1080384170778043594")
    private static final long serialVersionUID = 1L;

    public Constant() {
    }
    
    /*
     * Estados de Proceso
     */
    public static final Integer PROCESO_PENDIENTE = 0;
    public static final Integer PROCESO_LECTURA_ARCHIVO = 1;
    public static final Integer PROCESO_CARGA_BASEDATOS = 2;
    public static final Integer PROCESO_FINALIZADO_CARGA = 3;
    public static final Integer PROCESO_FINALIZADO_AUTORIZADO = 4;
    public static final Integer PROCESO_PROBLEMA_RESUMEN_ABONO = 5;
    public static final Integer PROCESO_PROBLEMA_PLANILLA_ABONO = 6;    
    public static final Integer PROCESO_ABONO_AUTORIZADO = 7;
    public static final Integer PROCESO_ABONO_AUTORIZADO_OBS = 8;
    public static final Integer PROCESO_ABONO_RECHAZADO = 9;
    public static final Integer PROCESO_AUTORIZADO = 10;
    public static final Integer PROCESO_AUTORIZADO_OBS = 11;
    public static final Integer PROCESO_FINALIZADO_ = 12;
    public static final Integer PROCESO_FINALIZADO_CON_RECHAZOS = 13;
    public static final Integer PROCESO_RECHAZADO = 14;
    public static final Integer PROCESO_EJECUCION_MANUAL_CAJAS = 15;
    public static final Integer PROCESO_PROBLEMA_PAGO_CAJA = 16;  
    
    /*
     * Proceso de Planilla tipo los ANDES y Previred
     */
    public static final String PLANILLA_ANDES = "CAJAANDES"; 
    public static final String PLANILLA_LAARAUCANA = "CAJAARAUCANA"; 
    
    public static final Integer PLANILLA_ANDES_CONVENIO = 10; 
    public static final Integer PLANILLA_ANDES_CONVENIO_MANUAL = 11; 
    
    public static final Integer PLANILLA_LAARAUCANA_CONVENIO = 12; 
    public static final Integer PLANILLA_LAARAUCANA_CONVENIO_MANUAL = 13; 
    
    public static final String EXTENSION_PLANILLA_ANDES_CONVENIO_MANUAL = "xls"; 
    
    public static final String MEDIOPAGO_ANDES = "LAN"; 
    public static final String MEDIOPAGO_LARAUCANA = "ARA"; 
    
    public static final String PLANILLA_PREVIRED = "PREVIRED";
    
    /*
     * Codigos y glosa Bancaria para Caja los Andes
     */
    public static final String COD_RECAUDADOR_ANDES = "504";     
    public static final String GLOSA_RECAUDADOR_ANDES = "Caja Compensación Los Andes"; 
    public static final String COD_BANCOCHILE_ANDES = "001";     
    public static final String GLOSA_BANCOCHILE_ANDES = "Banco de Chile"; 
    public static final String CUENTA_BANCOCHILE_ANDES = "66298-04"; 
    
    /*
     * Codigos y glosa Bancaria para Caja la Araucana
     */
    public static final String COD_RECAUDADOR_LAARAUCANA = "505";     
    public static final String GLOSA_RECAUDADOR_LAARAUCANA = "Caja La Araucana"; 
    public static final String COD_BANCOSANTANDER_LAARAUCANA = "037";     
    public static final String GLOSA_BANCOSANTANDER_LAARAUCANA = "Banco Santander"; 
    public static final String CUENTA_BANCOSANTANDER_LAARAUCANA = "01-07886-0"; 
    
    
    /*
     * CONSTANTES Tipo de Proceso
     */
    public static final String CONST_ABONO = "ABONO";
    public static final String CONST_PLANILLA = "PLANILLA";  
    
    /*
     * Constantes JOB
     */
    public static final String DELAY_TIME = "DELAYTIME";
    public static final String ID_CARGA = "IDCARGA";
    public static final String TIPO_ARCHIVO = "TIPOARCHIVO";
    public static final String PATH_ARCHIVO = "PATHARCHIVO";
    public static final String CORE = "CORE";
    public static final String TIPOPLANILLA = "TIPOPLANILLA";
    public static final String IDCARGAABONOANDES = "IDCARGAABONOANDES";
    
    
    
    
    /*
     * CONSTANTES PARA PROCESO DE Rendicion AUTOMATICA
     */

        public static final int PROCESANDO = 1;
        public static final int FINALIZADO = 2;
        public static final int FINALIZADO_CON_ERROR = 3;

        public static final String DESC_PROCESANDO = "PROCESANDO";
        public static final String DESC_FINALIZADO = "FINALIZADO";
        public static final String DESC_FINALIZADO_CON_ERROR = "FINALIZADO CON ERROR";
        
    public static final Integer PROCESO_AUTOMATICO = 1;
    public static final Integer PROCESO_MANUAL = 2;

      
    
     
    
}
