package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class ArchivoPlanillaAntecedentesDTO implements Serializable {
    @SuppressWarnings("compatibility:-735093235941308959")
    private static final long serialVersionUID = 1L;

    public ArchivoPlanillaAntecedentesDTO() {
    }
    
    private Integer tipo_de_registro;
    private String razon_social_pagador;
    private String codigo_actividad_pagador;
    private String calle_pagador;
    private String numero_pagador;
    private String depart_pobla_pagador;
    private String comuna_pagador;
    private String ciudad_pagador;
    private String region_pagador;
    private String telefono_pagador;
    private String nombre_representante_legal;
    private Integer rut_representante_legal;
    private String dv_representante_legal;
    private boolean procesado;

    public void setTipo_de_registro(Integer tipo_de_registro) {
        this.tipo_de_registro = tipo_de_registro;
    }

    public Integer getTipo_de_registro() {
        return tipo_de_registro;
    }

    public void setRazon_social_pagador(String razon_social_pagador) {
        this.razon_social_pagador = razon_social_pagador;
    }

    public String getRazon_social_pagador() {
        return razon_social_pagador;
    }

    public void setCodigo_actividad_pagador(String codigo_actividad_pagador) {
        this.codigo_actividad_pagador = codigo_actividad_pagador;
    }

    public String getCodigo_actividad_pagador() {
        return codigo_actividad_pagador;
    }

    public void setCalle_pagador(String calle_pagador) {
        this.calle_pagador = calle_pagador;
    }

    public String getCalle_pagador() {
        return calle_pagador;
    }

    public void setNumero_pagador(String numero_pagador) {
        this.numero_pagador = numero_pagador;
    }

    public String getNumero_pagador() {
        return numero_pagador;
    }

    public void setDepart_pobla_pagador(String depart_pobla_pagador) {
        this.depart_pobla_pagador = depart_pobla_pagador;
    }

    public String getDepart_pobla_pagador() {
        return depart_pobla_pagador;
    }

    public void setComuna_pagador(String comuna_pagador) {
        this.comuna_pagador = comuna_pagador;
    }

    public String getComuna_pagador() {
        return comuna_pagador;
    }

    public void setCiudad_pagador(String ciudad_pagador) {
        this.ciudad_pagador = ciudad_pagador;
    }

    public String getCiudad_pagador() {
        return ciudad_pagador;
    }

    public void setRegion_pagador(String region_pagador) {
        this.region_pagador = region_pagador;
    }

    public String getRegion_pagador() {
        return region_pagador;
    }

    public void setTelefono_pagador(String telefono_pagador) {
        this.telefono_pagador = telefono_pagador;
    }

    public String getTelefono_pagador() {
        return telefono_pagador;
    }

    public void setNombre_representante_legal(String nombre_representante_legal) {
        this.nombre_representante_legal = nombre_representante_legal;
    }

    public String getNombre_representante_legal() {
        return nombre_representante_legal;
    }

    public void setRut_representante_legal(Integer rut_representante_legal) {
        this.rut_representante_legal = rut_representante_legal;
    }

    public Integer getRut_representante_legal() {
        return rut_representante_legal;
    }

    public void setDv_representante_legal(String dv_representante_legal) {
        this.dv_representante_legal = dv_representante_legal;
    }

    public String getDv_representante_legal() {
        return dv_representante_legal;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }

    public boolean isProcesado() {
        return procesado;
    }
}
