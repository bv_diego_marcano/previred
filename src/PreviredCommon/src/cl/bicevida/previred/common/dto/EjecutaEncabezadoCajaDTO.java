package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class EjecutaEncabezadoCajaDTO implements Serializable {
    @SuppressWarnings("compatibility:2261860052602222359")
    private static final long serialVersionUID = 1L;
    private ArchivoPlanillaEncabezadoDTO dto;
    private Long idFolioCaja;
    private String descripcionEnvio;
    public EjecutaEncabezadoCajaDTO() {
    }

    public void setDto(ArchivoPlanillaEncabezadoDTO dto) {
        this.dto = dto;
    }

    public ArchivoPlanillaEncabezadoDTO getDto() {
        return dto;
    }

    public void setIdFolioCaja(Long idFolioCaja) {
        this.idFolioCaja = idFolioCaja;
    }

    public Long getIdFolioCaja() {
        return idFolioCaja;
    }

    public void setDescripcionEnvio(String descripcionEnvio) {
        this.descripcionEnvio = descripcionEnvio;
    }

    public String getDescripcionEnvio() {
        return descripcionEnvio;
    }
}
