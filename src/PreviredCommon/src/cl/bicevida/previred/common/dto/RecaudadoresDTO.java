package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class RecaudadoresDTO implements Serializable {
    @SuppressWarnings("compatibility:-3403012304455441288")
    private static final long serialVersionUID = 1L;

    public RecaudadoresDTO() {
    }
    
    private Integer cod_rec;
    private String descripcion;
    private String identificador;
    private String cod_banco;

    public void setCod_rec(Integer cod_rec) {
        this.cod_rec = cod_rec;
    }

    public Integer getCod_rec() {
        return cod_rec;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setCod_banco(String cod_banco) {
        this.cod_banco = cod_banco;
    }

    public String getCod_banco() {
        return cod_banco;
    }
}
