package cl.bicevida.previred.common.dto;

import java.io.Serializable;

import java.util.Date;

public class ArchivoAbonoDTO implements Serializable {
    @SuppressWarnings("compatibility:-2890447779106684606")
    private static final long serialVersionUID = 1L;

    public ArchivoAbonoDTO() {
    }
    
    public ArchivoAbonoDTO(ArchivoAbonoDTO archivoAbonoDTO) {
        this.id_folio_abono = archivoAbonoDTO.id_folio_abono;
        this.tipo_de_registro = archivoAbonoDTO.tipo_de_registro;
        this.cod_recaudador = archivoAbonoDTO.cod_recaudador;
        this.monto_abono_fondo = archivoAbonoDTO.monto_abono_fondo;
        this.monto_abono_institucion = archivoAbonoDTO.monto_abono_institucion;
        this.monto_abono_afc = archivoAbonoDTO.monto_abono_afc;
        this.rut_empleador = archivoAbonoDTO.rut_empleador;
        this.digito_verficador = archivoAbonoDTO.digito_verficador;
        this.periodo = archivoAbonoDTO.periodo;
        this.fecha_pago = archivoAbonoDTO.fecha_pago;
        this.lote = archivoAbonoDTO.lote;
        this.sucursal = archivoAbonoDTO.sucursal;
        this.estado_proceso = archivoAbonoDTO.estado_proceso;
        this.estado_proceso_excepcion = 
                archivoAbonoDTO.estado_proceso_excepcion;
        this.procesado = archivoAbonoDTO.procesado;
        this.montoAbono = archivoAbonoDTO.montoAbono;
        this.rutEmpleador = archivoAbonoDTO.rutEmpleador;
        this.planilla = archivoAbonoDTO.planilla;
        this.concepto_ingreso_recaudacion = 
                archivoAbonoDTO.concepto_ingreso_recaudacion;
        this.poliza_asociada = archivoAbonoDTO.poliza_asociada;
    }


    


    private Double id_folio_abono;
    private Integer tipo_de_registro;
    private Integer cod_recaudador;
    private Double monto_abono_fondo;
    private Double monto_abono_institucion;
    private Double monto_abono_afc;
    private Integer rut_empleador;
    private String digito_verficador;
    private Date periodo;
    private Date fecha_pago;
    private String lote;
    private String sucursal;
    private Integer estado_proceso;
    private String estado_proceso_excepcion;
    private boolean procesado = false;
    private Double montoAbono;
    private String rutEmpleador;
    private ArchivoPlanillaEncabezadoDTO planilla;
    private String concepto_ingreso_recaudacion;
    private String poliza_asociada;
    

    public void setId_folio_abono(Double id_folio_abono) {
        this.id_folio_abono = id_folio_abono;
    }

    public Double getId_folio_abono() {
        return id_folio_abono;
    }

    public void setTipo_de_registro(Integer tipo_de_registro) {
        this.tipo_de_registro = tipo_de_registro;
    }

    public Integer getTipo_de_registro() {
        return tipo_de_registro;
    }

    public void setMonto_abono_fondo(Double monto_abono_fondo) {
        this.monto_abono_fondo = monto_abono_fondo;
    }

    public Double getMonto_abono_fondo() {
        return monto_abono_fondo;
    }

    public void setMonto_abono_institucion(Double monto_abono_institucion) {
        this.monto_abono_institucion = monto_abono_institucion;
    }

    public Double getMonto_abono_institucion() {
        return monto_abono_institucion;
    }

    public void setMonto_abono_afc(Double monto_abono_afc) {
        this.monto_abono_afc = monto_abono_afc;
    }

    public Double getMonto_abono_afc() {
        return monto_abono_afc;
    }

    public void setRut_empleador(Integer rut_empleador) {
        this.rut_empleador = rut_empleador;
    }

    public Integer getRut_empleador() {
        return rut_empleador;
    }

    public void setDigito_verficador(String digito_verficador) {
        this.digito_verficador = digito_verficador;
    }

    public String getDigito_verficador() {
        return digito_verficador;
    }

    public void setPeriodo(Date periodo) {
        this.periodo = periodo;
    }

    public Date getPeriodo() {
        return periodo;
    }

    public void setFecha_pago(Date fecha_pago) {
        this.fecha_pago = fecha_pago;
    }

    public Date getFecha_pago() {
        return fecha_pago;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getLote() {
        return lote;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setEstado_proceso(Integer estado_proceso) {
        this.estado_proceso = estado_proceso;
    }

    public Integer getEstado_proceso() {
        return estado_proceso;
    }

    public void setEstado_proceso_excepcion(String estado_proceso_excepcion) {
        this.estado_proceso_excepcion = estado_proceso_excepcion;
    }

    public String getEstado_proceso_excepcion() {
        return estado_proceso_excepcion;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }

    public boolean isProcesado() {
        return procesado;
    }

    public void setMontoAbono(Double montoAbono) {
        this.montoAbono = montoAbono;
    }

    public Double getMontoAbono() {
        montoAbono = this.getMonto_abono_afc() + this.getMonto_abono_fondo() + this.getMonto_abono_institucion();
        return montoAbono;
    }

    public void setRutEmpleador(String rutEmpleador) {
        this.rutEmpleador = rutEmpleador;
    }

    public String getRutEmpleador() {
        rutEmpleador = this.getRut_empleador().intValue() + "-" + this.getDigito_verficador().trim();
        return rutEmpleador;
    }

    public void setPlanilla(ArchivoPlanillaEncabezadoDTO planilla) {
        this.planilla = planilla;
    }

    public ArchivoPlanillaEncabezadoDTO getPlanilla() {
        return planilla;
    }

    public void setConcepto_ingreso_recaudacion(String concepto_ingreso_recaudacion) {
        this.concepto_ingreso_recaudacion = concepto_ingreso_recaudacion;
    }

    public String getConcepto_ingreso_recaudacion() {
        return concepto_ingreso_recaudacion;
    }

    public void setPoliza_asociada(String poliza_asociada) {
        this.poliza_asociada = poliza_asociada;
    }

    public String getPoliza_asociada() {
        return poliza_asociada;
    }

    public void setCod_recaudador(Integer cod_recaudador) {
        this.cod_recaudador = cod_recaudador;
    }

    public Integer getCod_recaudador() {
        return cod_recaudador;
    }
}
