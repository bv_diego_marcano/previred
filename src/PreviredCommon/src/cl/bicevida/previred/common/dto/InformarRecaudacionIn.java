package cl.bicevida.previred.common.dto;

import java.util.Calendar;

public class InformarRecaudacionIn {
    private String callerUser;
    private String callerSystem;
    
    private Long folioCajaDiario;
    private Calendar turno;
    private String usuarioCaja;
    private Long folioPago;
    private Long idTurno;
    
    public InformarRecaudacionIn() {
        super();
    }

    public void setCallerUser(String callerUser) {
        this.callerUser = callerUser;
    }

    public String getCallerUser() {
        return callerUser;
    }

    public void setCallerSystem(String callerSystem) {
        this.callerSystem = callerSystem;
    }

    public String getCallerSystem() {
        return callerSystem;
    }

    public void setFolioCajaDiario(Long folioCajaDiario) {
        this.folioCajaDiario = folioCajaDiario;
    }

    public Long getFolioCajaDiario() {
        return folioCajaDiario;
    }

    public void setTurno(Calendar turno) {
        this.turno = turno;
    }

    public Calendar getTurno() {
        return turno;
    }

    public void setUsuarioCaja(String usuarioCaja) {
        this.usuarioCaja = usuarioCaja;
    }

    public String getUsuarioCaja() {
        return usuarioCaja;
    }

    public void setFolioPago(Long folioPago) {
        this.folioPago = folioPago;
    }

    public Long getFolioPago() {
        return folioPago;
    }

    public void setIdTurno(Long idTurno) {
        this.idTurno = idTurno;
    }

    public Long getIdTurno() {
        return idTurno;
    }
}
