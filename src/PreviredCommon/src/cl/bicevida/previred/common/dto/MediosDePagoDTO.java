package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class MediosDePagoDTO implements Serializable {
    @SuppressWarnings("compatibility:-9115547121067873586")
    private static final long serialVersionUID = 1L;

    public MediosDePagoDTO() {
    }
    
    private Integer idMedioPagoRendicion;
    private String tipoMedioPago;
    private String descripcionMedioPago;

    public void setIdMedioPagoRendicion(Integer idMedioPagoRendicion) {
        this.idMedioPagoRendicion = idMedioPagoRendicion;
    }

    public Integer getIdMedioPagoRendicion() {
        return idMedioPagoRendicion;
    }

    public void setTipoMedioPago(String tipoMedioPago) {
        this.tipoMedioPago = tipoMedioPago;
    }

    public String getTipoMedioPago() {
        return tipoMedioPago;
    }

    public void setDescripcionMedioPago(String descripcionMedioPago) {
        this.descripcionMedioPago = descripcionMedioPago;
    }

    public String getDescripcionMedioPago() {
        return descripcionMedioPago;
    }
}
