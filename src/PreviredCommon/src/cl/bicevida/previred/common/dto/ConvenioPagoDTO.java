package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class ConvenioPagoDTO implements Serializable {
    @SuppressWarnings("compatibility:1343589754513013609")
    private static final long serialVersionUID = 1L;

    public ConvenioPagoDTO() {
    }
    
    private Integer id_empresa;       
    private String descripcion_empresa;
    private Integer id_medio_pago;    
    private String tipo_codbanco_medio_pago;    
    private String descripcion_medio_pago;
    private String numero_de_convenio;
    private Integer estado_convenio;  
    private String correo_electronico;
    private String directorio_upload;

    public void setId_empresa(Integer id_empresa) {
        this.id_empresa = id_empresa;
    }

    public Integer getId_empresa() {
        return id_empresa;
    }

    public void setDescripcion_empresa(String descripcion_empresa) {
        this.descripcion_empresa = descripcion_empresa;
    }

    public String getDescripcion_empresa() {
        return descripcion_empresa;
    }

    public void setId_medio_pago(Integer id_medio_pago) {
        this.id_medio_pago = id_medio_pago;
    }

    public Integer getId_medio_pago() {
        return id_medio_pago;
    }

    public void setTipo_codbanco_medio_pago(String tipo_codbanco_medio_pago) {
        this.tipo_codbanco_medio_pago = tipo_codbanco_medio_pago;
    }

    public String getTipo_codbanco_medio_pago() {
        return tipo_codbanco_medio_pago;
    }

    public void setDescripcion_medio_pago(String descripcion_medio_pago) {
        this.descripcion_medio_pago = descripcion_medio_pago;
    }

    public String getDescripcion_medio_pago() {
        return descripcion_medio_pago;
    }

    public void setNumero_de_convenio(String numero_de_convenio) {
        this.numero_de_convenio = numero_de_convenio;
    }

    public String getNumero_de_convenio() {
        return numero_de_convenio;
    }

    public void setEstado_convenio(Integer estado_convenio) {
        this.estado_convenio = estado_convenio;
    }

    public Integer getEstado_convenio() {
        return estado_convenio;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setDirectorio_upload(String directorio_upload) {
        this.directorio_upload = directorio_upload;
    }

    public String getDirectorio_upload() {
        return directorio_upload;
    }
}
