package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class ConceptosDePagoDTO implements Serializable {
    @SuppressWarnings("compatibility:8170971121565798010")
    private static final long serialVersionUID = 1L;

    public ConceptosDePagoDTO() {
    }
    
    private Integer codigoConcepto;
    private String codigoIndstrumento;
    private String descripcionConcepto;

    public void setCodigoConcepto(Integer codigoConcepto) {
        this.codigoConcepto = codigoConcepto;
    }

    public Integer getCodigoConcepto() {
        return codigoConcepto;
    }

    public void setCodigoIndstrumento(String codigoIndstrumento) {
        this.codigoIndstrumento = codigoIndstrumento;
    }

    public String getCodigoIndstrumento() {
        return codigoIndstrumento;
    }

    public void setDescripcionConcepto(String descripcionConcepto) {
        this.descripcionConcepto = descripcionConcepto;
    }

    public String getDescripcionConcepto() {
        return descripcionConcepto;
    }
}
