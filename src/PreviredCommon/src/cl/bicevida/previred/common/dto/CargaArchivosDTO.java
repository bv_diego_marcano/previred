package cl.bicevida.previred.common.dto;

import java.io.Serializable;

import java.util.Date;


public class CargaArchivosDTO implements Serializable {
    @SuppressWarnings("compatibility:-4970541501870328722")
    private static final long serialVersionUID = 1L;

    public CargaArchivosDTO() {
    }
    
    private Double id_carga;    
    private Date fecha_creacion;
    private Integer id_tipo_archivo;
    private String descripcion_tipo_archivo;
    private Integer estado_proceso;
    private String descripcion_estado_proceso;
    private Date inicio_proceso;
    private Date termino_proceso;
    private Date fecha_y_hora_upload;
    private String nombre_fisico_archivo; 
    private Integer cantidad_procesada;
    private ConvenioPagoDTO convenio;
    private ResumenAbonoDTO resumenabono;   
    private ArchivoPlanillaHeaderDTO archivoheader;
    private boolean procesado;
    private Double id_carga_asoc;    
    private String remote_user;
    public void setId_carga(Double id_carga) {
        this.id_carga = id_carga;
    }

    public Double getId_carga() {
        return id_carga;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setId_tipo_archivo(Integer id_tipo_archivo) {
        this.id_tipo_archivo = id_tipo_archivo;
    }

    public Integer getId_tipo_archivo() {
        return id_tipo_archivo;
    }

    public void setDescripcion_tipo_archivo(String descripcion_tipo_archivo) {
        this.descripcion_tipo_archivo = descripcion_tipo_archivo;
    }

    public String getDescripcion_tipo_archivo() {
        return descripcion_tipo_archivo;
    }

    public void setEstado_proceso(Integer estado_proceso) {
        this.estado_proceso = estado_proceso;
    }

    public Integer getEstado_proceso() {
        return estado_proceso;
    }

    public void setDescripcion_estado_proceso(String descripcion_estado_proceso) {
        this.descripcion_estado_proceso = descripcion_estado_proceso;
    }

    public String getDescripcion_estado_proceso() {
        return descripcion_estado_proceso;
    }

    public void setInicio_proceso(Date inicio_proceso) {
        this.inicio_proceso = inicio_proceso;
    }

    public Date getInicio_proceso() {
        return inicio_proceso;
    }

    public void setTermino_proceso(Date termino_proceso) {
        this.termino_proceso = termino_proceso;
    }

    public Date getTermino_proceso() {
        return termino_proceso;
    }

    public void setFecha_y_hora_upload(Date fecha_y_hora_upload) {
        this.fecha_y_hora_upload = fecha_y_hora_upload;
    }

    public Date getFecha_y_hora_upload() {
        return fecha_y_hora_upload;
    }

    public void setNombre_fisico_archivo(String nombre_fisico_archivo) {
        this.nombre_fisico_archivo = nombre_fisico_archivo;
    }

    public String getNombre_fisico_archivo() {
        return nombre_fisico_archivo;
    }

    public void setCantidad_procesada(Integer cantidad_procesada) {
        this.cantidad_procesada = cantidad_procesada;
    }

    public Integer getCantidad_procesada() {
        return cantidad_procesada;
    }

    public void setConvenio(ConvenioPagoDTO convenio) {
        this.convenio = convenio;
    }

    public ConvenioPagoDTO getConvenio() {
        return convenio;
    }

    public void setResumenabono(ResumenAbonoDTO resumenabono) {
        this.resumenabono = resumenabono;
    }

    public ResumenAbonoDTO getResumenabono() {
        return resumenabono;
    }

    public void setArchivoheader(ArchivoPlanillaHeaderDTO archivoheader) {
        this.archivoheader = archivoheader;
    }

    public ArchivoPlanillaHeaderDTO getArchivoheader() {
        return archivoheader;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }

    public boolean isProcesado() {
        return procesado;
    }

    public void setId_carga_asoc(Double id_carga_asoc) {
        this.id_carga_asoc = id_carga_asoc;
    }

    public Double getId_carga_asoc() {
        return id_carga_asoc;
    }

    public void setRemote_user(String remote_user) {
        this.remote_user = remote_user;
    }

    public String getRemote_user() {
        return remote_user;
    }
}
