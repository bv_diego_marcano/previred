package cl.bicevida.previred.common.dto;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

public class ArchivoPlanillaEncabezadoDTO implements Serializable {
    @SuppressWarnings("compatibility:8501420858602759053")
    private static final long serialVersionUID = 1L;

    public ArchivoPlanillaEncabezadoDTO() {
    }
    
    public ArchivoPlanillaEncabezadoDTO(ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO) {
        this.id_folio_planilla = 
                archivoPlanillaEncabezadoDTO.id_folio_planilla;
        this.tipo_de_registro = archivoPlanillaEncabezadoDTO.tipo_de_registro;
        this.tipo_pago = archivoPlanillaEncabezadoDTO.tipo_pago;
        this.fecha_hora_operacion = 
                archivoPlanillaEncabezadoDTO.fecha_hora_operacion;
        this.codigo_institucion_apv = 
                archivoPlanillaEncabezadoDTO.codigo_institucion_apv;
        this.rut_pagador = archivoPlanillaEncabezadoDTO.rut_pagador;
        this.dv_pagador = archivoPlanillaEncabezadoDTO.dv_pagador;
        this.tipo_pagador = archivoPlanillaEncabezadoDTO.tipo_pagador;
        this.correo_pagador = archivoPlanillaEncabezadoDTO.correo_pagador;
        this.antecedentes = archivoPlanillaEncabezadoDTO.antecedentes;
        this.detalle = archivoPlanillaEncabezadoDTO.detalle;
        this.detalleIndividual = 
                archivoPlanillaEncabezadoDTO.detalleIndividual;
        this.resumen = archivoPlanillaEncabezadoDTO.resumen;
        this.procesado = archivoPlanillaEncabezadoDTO.procesado;
        this.estado_proceso = archivoPlanillaEncabezadoDTO.estado_proceso;
        this.estado_proceso_exception = 
                archivoPlanillaEncabezadoDTO.estado_proceso_exception;
        this.estado_proceso_descripcion = 
                archivoPlanillaEncabezadoDTO.estado_proceso_descripcion;
        this.rutPagador = archivoPlanillaEncabezadoDTO.rutPagador;
        this.codResultPago = archivoPlanillaEncabezadoDTO.codResultPago;
        this.decsResultPago = archivoPlanillaEncabezadoDTO.decsResultPago;
        this.conceptoRecaudacion = 
                archivoPlanillaEncabezadoDTO.conceptoRecaudacion;
        this.polizaAsociada = archivoPlanillaEncabezadoDTO.polizaAsociada;
        this.fechaPagoCaja = archivoPlanillaEncabezadoDTO.fechaPagoCaja;
        this.id_folio_pagocaja = 
                archivoPlanillaEncabezadoDTO.id_folio_pagocaja;
    }
    
    
    private Double id_folio_planilla;
    private Integer tipo_de_registro;
    private Integer tipo_pago;
    private Date fecha_hora_operacion;
    private Integer codigo_institucion_apv;
    private Integer rut_pagador;
    private String dv_pagador;
    private Integer tipo_pagador;
    private String correo_pagador;
    private ArchivoPlanillaAntecedentesDTO antecedentes;
    private List<ArchivoPlanillaDetalleDTO> detalle;
    private ArchivoPlanillaDetalleDTO detalleIndividual;
    private ArchivoPlanillaResumenDTO resumen;
    private boolean procesado;
    private Integer estado_proceso;
    private String estado_proceso_exception;
    private String estado_proceso_descripcion;
    private String rutPagador;
    
    /* Antecedentes del pago en caja */    
    private Integer codResultPago = 98;
    private String decsResultPago;
    private String conceptoRecaudacion;
    private Integer polizaAsociada;
    private Date fechaPagoCaja;
    private Long id_folio_pagocaja;
    

    public void setId_folio_planilla(Double id_folio_planilla) {
        this.id_folio_planilla = id_folio_planilla;
    }

    public Double getId_folio_planilla() {
        return id_folio_planilla;
    }

    public void setTipo_de_registro(Integer tipo_de_registro) {
        this.tipo_de_registro = tipo_de_registro;
    }

    public Integer getTipo_de_registro() {
        return tipo_de_registro;
    }

    public void setTipo_pago(Integer tipo_pago) {
        this.tipo_pago = tipo_pago;
    }

    public Integer getTipo_pago() {
        return tipo_pago;
    }

    public void setFecha_hora_operacion(Date fecha_hora_operacion) {
        this.fecha_hora_operacion = fecha_hora_operacion;
    }

    public Date getFecha_hora_operacion() {
        return fecha_hora_operacion;
    }

    public void setCodigo_institucion_apv(Integer codigo_institucion_apv) {
        this.codigo_institucion_apv = codigo_institucion_apv;
    }

    public Integer getCodigo_institucion_apv() {
        return codigo_institucion_apv;
    }

    public void setRut_pagador(Integer rut_pagador) {
        this.rut_pagador = rut_pagador;
    }

    public Integer getRut_pagador() {
        return rut_pagador;
    }

    public void setDv_pagador(String dv_pagador) {
        this.dv_pagador = dv_pagador;
    }

    public String getDv_pagador() {
        return dv_pagador;
    }

    public void setTipo_pagador(Integer tipo_pagador) {
        this.tipo_pagador = tipo_pagador;
    }

    public Integer getTipo_pagador() {
        return tipo_pagador;
    }

    public void setCorreo_pagador(String correo_pagador) {
        this.correo_pagador = correo_pagador;
    }

    public String getCorreo_pagador() {
        return correo_pagador;
    }

    public void setAntecedentes(ArchivoPlanillaAntecedentesDTO antecedentes) {
        this.antecedentes = antecedentes;
    }

    public ArchivoPlanillaAntecedentesDTO getAntecedentes() {
        return antecedentes;
    }
    
    public void setResumen(ArchivoPlanillaResumenDTO resumen) {
        this.resumen = resumen;
    }

    public ArchivoPlanillaResumenDTO getResumen() {
        return resumen;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }

    public boolean isProcesado() {
        return procesado;
    }

    public void setEstado_proceso(Integer estado_proceso) {
        this.estado_proceso = estado_proceso;
    }

    public Integer getEstado_proceso() {
        return estado_proceso;
    }

    public void setEstado_proceso_exception(String estado_proceso_exception) {
        this.estado_proceso_exception = estado_proceso_exception;
    }

    public String getEstado_proceso_exception() {
        return estado_proceso_exception;
    }

    public void setEstado_proceso_descripcion(String estado_proceso_descripcion) {
        this.estado_proceso_descripcion = estado_proceso_descripcion;
    }

    public String getEstado_proceso_descripcion() {
        return estado_proceso_descripcion;
    }

    public void setRutPagador(String rutPagador) {
        this.rutPagador = rutPagador;
    }

    public String getRutPagador() {
        rutPagador = this.rut_pagador + "-" + this.dv_pagador;
        return rutPagador;
    }

    public void setCodResultPago(Integer codResultPago) {
        this.codResultPago = codResultPago;
    }

    public Integer getCodResultPago() {
        return codResultPago;
    }

    public void setDecsResultPago(String decsResultPago) {
        this.decsResultPago = decsResultPago;
    }

    public String getDecsResultPago() {
        return decsResultPago;
    }

    public void setConceptoRecaudacion(String conceptoRecaudacion) {
        this.conceptoRecaudacion = conceptoRecaudacion;
    }

    public String getConceptoRecaudacion() {
        return conceptoRecaudacion;
    }

    public void setPolizaAsociada(Integer polizaAsociada) {
        this.polizaAsociada = polizaAsociada;
    }

    public Integer getPolizaAsociada() {
        return polizaAsociada;
    }

    public void setFechaPagoCaja(Date fechaPagoCaja) {
        this.fechaPagoCaja = fechaPagoCaja;
    }

    public Date getFechaPagoCaja() {
        return fechaPagoCaja;
    }

    public void setId_folio_pagocaja(Long id_folio_pagocaja) {
        this.id_folio_pagocaja = id_folio_pagocaja;
    }

    public Long getId_folio_pagocaja() {
        return id_folio_pagocaja;
    }

    public void setDetalle(List<ArchivoPlanillaDetalleDTO> detalle) {
        this.detalle = detalle;
    }

    public List<ArchivoPlanillaDetalleDTO> getDetalle() {
        return detalle;
    }

    public void setDetalleIndividual(ArchivoPlanillaDetalleDTO detalleIndividual) {
        this.detalleIndividual = detalleIndividual;
    }

    public ArchivoPlanillaDetalleDTO getDetalleIndividual() {
        return detalleIndividual;
    }
}
