package cl.bicevida.previred.common.dto;

import java.io.Serializable;

import java.util.Date;

public class ArchivoPlanillaDetalleDTO implements Serializable {
    @SuppressWarnings("compatibility:7336754255203803258")
    private static final long serialVersionUID = 1L;

    public ArchivoPlanillaDetalleDTO() {
    }
    
    public ArchivoPlanillaDetalleDTO(ArchivoPlanillaDetalleDTO archivoPlanillaDetalleDTO) {
        this.tipo_de_registro = archivoPlanillaDetalleDTO.tipo_de_registro;
        this.rut_trabajador = archivoPlanillaDetalleDTO.rut_trabajador;
        this.dv_trabajador = archivoPlanillaDetalleDTO.dv_trabajador;
        this.apellido_paterno_trabajador = archivoPlanillaDetalleDTO.apellido_paterno_trabajador;
        this.apellido_materno_trabajador = archivoPlanillaDetalleDTO.apellido_materno_trabajador;
        this.nombres_trabajador = archivoPlanillaDetalleDTO.nombres_trabajador;
        this.sexo = archivoPlanillaDetalleDTO.sexo;
        this.nacionalidad = archivoPlanillaDetalleDTO.nacionalidad;
        this.renta_imponible_trabajador = archivoPlanillaDetalleDTO.renta_imponible_trabajador;
        this.cotizacion_voluntaria_apvi = archivoPlanillaDetalleDTO.cotizacion_voluntaria_apvi;
        this.cotizacion_voluntaria_apvi_b = archivoPlanillaDetalleDTO.cotizacion_voluntaria_apvi_b;
        this.nro_contrato_apvi = archivoPlanillaDetalleDTO.nro_contrato_apvi;
        this.deposito_convenio = archivoPlanillaDetalleDTO.deposito_convenio;
        this.total_a_pagar = archivoPlanillaDetalleDTO.total_a_pagar;
        this.periodo_pago = archivoPlanillaDetalleDTO.periodo_pago;
        this.apv_colectivo_empl = archivoPlanillaDetalleDTO.apv_colectivo_empl;
        this.apv_colectivo_trabaj = archivoPlanillaDetalleDTO.apv_colectivo_trabaj;
        this.n_contrato_apvc = archivoPlanillaDetalleDTO.n_contrato_apvc;
        this.codigo_mov_personal = archivoPlanillaDetalleDTO.codigo_mov_personal;
        this.fecha_inicio = archivoPlanillaDetalleDTO.fecha_inicio;
        this.fecha_termino = archivoPlanillaDetalleDTO.fecha_termino;
        this.procesado = archivoPlanillaDetalleDTO.procesado;
        this.rutTrabajador = archivoPlanillaDetalleDTO.rutTrabajador;
        this.nombreTrabajador = archivoPlanillaDetalleDTO.nombreTrabajador;
        
        this.decsResultPago = archivoPlanillaDetalleDTO.decsResultPago;
        this.conceptoRecaudacion = archivoPlanillaDetalleDTO.conceptoRecaudacion;
        this.polizaAsociada = archivoPlanillaDetalleDTO.polizaAsociada;
        this.fechaPagoCaja = archivoPlanillaDetalleDTO.fechaPagoCaja;
        this.id_folio_pagocaja = archivoPlanillaDetalleDTO.id_folio_pagocaja;

        this.estado_proceso_pago = archivoPlanillaDetalleDTO.estado_proceso_pago;
        this.estado_proceso_pago_exception = archivoPlanillaDetalleDTO.estado_proceso_pago_exception;
        this.estado_proceso_pago_descriocion = archivoPlanillaDetalleDTO.estado_proceso_pago_descriocion;
    
    }
    
    private Integer tipo_de_registro;
    private Integer rut_trabajador;
    private String dv_trabajador;
    private String apellido_paterno_trabajador;
    private String apellido_materno_trabajador;
    private String nombres_trabajador;
    private String sexo;
    private Integer nacionalidad;
    private Double renta_imponible_trabajador;
    private Double cotizacion_voluntaria_apvi;
    private Double cotizacion_voluntaria_apvi_b;
    private String nro_contrato_apvi;
    private Double deposito_convenio;
    private Double total_a_pagar;
    private Date periodo_pago;
    private Double apv_colectivo_empl;
    private Double apv_colectivo_trabaj;
    private String n_contrato_apvc;
    private Integer codigo_mov_personal;
    private Date fecha_inicio;
    private Date fecha_termino;
    private boolean procesado;
    private String rutTrabajador;
    private String nombreTrabajador;
    
    /* Antecedentes del pago en caja */    
    private Integer codResultPago = 98;
    private String decsResultPago;
    private String conceptoRecaudacion;
    private Integer polizaAsociada;
    private Date fechaPagoCaja;
    private Long id_folio_pagocaja;
    
    private Integer estado_proceso_pago;
    private String estado_proceso_pago_exception;
    private String estado_proceso_pago_descriocion;
    
    

    public void setTipo_de_registro(Integer tipo_de_registro) {
        this.tipo_de_registro = tipo_de_registro;
    }

    public Integer getTipo_de_registro() {
        return tipo_de_registro;
    }

    public void setRut_trabajador(Integer rut_trabajador) {
        this.rut_trabajador = rut_trabajador;
    }

    public Integer getRut_trabajador() {
        return rut_trabajador;
    }

    public void setDv_trabajador(String dv_trabajador) {
        this.dv_trabajador = dv_trabajador;
    }

    public String getDv_trabajador() {
        return dv_trabajador;
    }

    public void setApellido_paterno_trabajador(String apellido_paterno_trabajador) {
        this.apellido_paterno_trabajador = apellido_paterno_trabajador;
    }

    public String getApellido_paterno_trabajador() {
        return apellido_paterno_trabajador;
    }

    public void setApellido_materno_trabajador(String apellido_materno_trabajador) {
        this.apellido_materno_trabajador = apellido_materno_trabajador;
    }

    public String getApellido_materno_trabajador() {
        return apellido_materno_trabajador;
    }

    public void setNombres_trabajador(String nombres_trabajador) {
        this.nombres_trabajador = nombres_trabajador;
    }

    public String getNombres_trabajador() {
        return nombres_trabajador;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getSexo() {
        return sexo;
    }

    public void setNacionalidad(Integer nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Integer getNacionalidad() {
        return nacionalidad;
    }

    public void setRenta_imponible_trabajador(Double renta_imponible_trabajador) {
        this.renta_imponible_trabajador = renta_imponible_trabajador;
    }

    public Double getRenta_imponible_trabajador() {
        return renta_imponible_trabajador;
    }

    public void setCotizacion_voluntaria_apvi(Double cotizacion_voluntaria_apvi) {
        this.cotizacion_voluntaria_apvi = cotizacion_voluntaria_apvi;
    }

    public Double getCotizacion_voluntaria_apvi() {
        return cotizacion_voluntaria_apvi;
    }

    public void setCotizacion_voluntaria_apvi_b(Double cotizacion_voluntaria_apvi_b) {
        this.cotizacion_voluntaria_apvi_b = cotizacion_voluntaria_apvi_b;
    }

    public Double getCotizacion_voluntaria_apvi_b() {
        return cotizacion_voluntaria_apvi_b;
    }

    public void setNro_contrato_apvi(String nro_contrato_apvi) {
        this.nro_contrato_apvi = nro_contrato_apvi;
    }

    public String getNro_contrato_apvi() {
        return nro_contrato_apvi;
    }

    public void setDeposito_convenio(Double deposito_convenio) {
        this.deposito_convenio = deposito_convenio;
    }

    public Double getDeposito_convenio() {
        return deposito_convenio;
    }

    public void setTotal_a_pagar(Double total_a_pagar) {
        this.total_a_pagar = total_a_pagar;
    }

    public Double getTotal_a_pagar() {
        return total_a_pagar;
    }

    public void setPeriodo_pago(Date periodo_pago) {
        this.periodo_pago = periodo_pago;
    }

    public Date getPeriodo_pago() {
        return periodo_pago;
    }

    public void setApv_colectivo_empl(Double apv_colectivo_empl) {
        this.apv_colectivo_empl = apv_colectivo_empl;
    }

    public Double getApv_colectivo_empl() {
        return apv_colectivo_empl;
    }

    public void setApv_colectivo_trabaj(Double apv_colectivo_trabaj) {
        this.apv_colectivo_trabaj = apv_colectivo_trabaj;
    }

    public Double getApv_colectivo_trabaj() {
        return apv_colectivo_trabaj;
    }

    public void setN_contrato_apvc(String n_contrato_apvc) {
        this.n_contrato_apvc = n_contrato_apvc;
    }

    public String getN_contrato_apvc() {
        return n_contrato_apvc;
    }

    public void setCodigo_mov_personal(Integer codigo_mov_personal) {
        this.codigo_mov_personal = codigo_mov_personal;
    }

    public Integer getCodigo_mov_personal() {
        return codigo_mov_personal;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_termino(Date fecha_termino) {
        this.fecha_termino = fecha_termino;
    }

    public Date getFecha_termino() {
        return fecha_termino;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }

    public boolean isProcesado() {
        return procesado;
    }

    public void setRutTrabajador(String rutTrabajador) {
        this.rutTrabajador = rutTrabajador;
    }

    public String getRutTrabajador() {
        rutTrabajador = this.getRut_trabajador().intValue() + "-" + this.getDv_trabajador();
        return rutTrabajador;
    }

    public void setNombreTrabajador(String nombreTrabajador) {
        this.nombreTrabajador = nombreTrabajador;
    }

    public String getNombreTrabajador() {
        nombreTrabajador = this.getNombres_trabajador() + " " + this.getApellido_paterno_trabajador() + " " + this.getApellido_materno_trabajador();
        return nombreTrabajador;
    }

    public void setCodResultPago(Integer codResultPago) {
        this.codResultPago = codResultPago;
    }

    public Integer getCodResultPago() {
        return codResultPago;
    }

    public void setDecsResultPago(String decsResultPago) {
        this.decsResultPago = decsResultPago;
    }

    public String getDecsResultPago() {
        return decsResultPago;
    }

    public void setConceptoRecaudacion(String conceptoRecaudacion) {
        this.conceptoRecaudacion = conceptoRecaudacion;
    }

    public String getConceptoRecaudacion() {
        return conceptoRecaudacion;
    }

    public void setPolizaAsociada(Integer polizaAsociada) {
        this.polizaAsociada = polizaAsociada;
    }

    public Integer getPolizaAsociada() {
        return polizaAsociada;
    }

    public void setFechaPagoCaja(Date fechaPagoCaja) {
        this.fechaPagoCaja = fechaPagoCaja;
    }

    public Date getFechaPagoCaja() {
        return fechaPagoCaja;
    }

    public void setId_folio_pagocaja(Long id_folio_pagocaja) {
        this.id_folio_pagocaja = id_folio_pagocaja;
    }

    public Long getId_folio_pagocaja() {
        return id_folio_pagocaja;
    }

    public void setEstado_proceso_pago(Integer estado_proceso_pago) {
        this.estado_proceso_pago = estado_proceso_pago;
    }

    public Integer getEstado_proceso_pago() {
        return estado_proceso_pago;
    }

    public void setEstado_proceso_pago_exception(String estado_proceso_pago_exception) {
        this.estado_proceso_pago_exception = estado_proceso_pago_exception;
    }

    public String getEstado_proceso_pago_exception() {
        return estado_proceso_pago_exception;
    }

    public void setEstado_proceso_pago_descriocion(String estado_proceso_pago_descriocion) {
        this.estado_proceso_pago_descriocion = estado_proceso_pago_descriocion;
    }

    public String getEstado_proceso_pago_descriocion() {
        return estado_proceso_pago_descriocion;
    }
}
