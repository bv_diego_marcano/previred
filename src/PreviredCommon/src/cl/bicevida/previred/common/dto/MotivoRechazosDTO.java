package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class MotivoRechazosDTO implements Serializable {
    @SuppressWarnings("compatibility:-8169671295518049920")
    private static final long serialVersionUID = 1L;

    public MotivoRechazosDTO() {
    }
    
    private Integer id_motivo_rechazos;
    private String descripcion_rechazos;

    public void setId_motivo_rechazos(Integer id_motivo_rechazos) {
        this.id_motivo_rechazos = id_motivo_rechazos;
    }

    public Integer getId_motivo_rechazos() {
        return id_motivo_rechazos;
    }

    public void setDescripcion_rechazos(String descripcion_rechazos) {
        this.descripcion_rechazos = descripcion_rechazos;
    }

    public String getDescripcion_rechazos() {
        return descripcion_rechazos;
    }
}
