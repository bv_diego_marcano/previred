package cl.bicevida.previred.common.dto;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

public class ResumenAbonoDTO implements Serializable {
    @SuppressWarnings("compatibility:-697286693000868574")
    private static final long serialVersionUID = 1L;

    public ResumenAbonoDTO() {
    }
    
    /*
     * Datos del resumen
     */
    private String observacion_rechazo;
    private String observacion_autorizacion;
    private Integer validacion_cartola_cta_cte;
    private Double total_monto_inf_cartola;
    private Integer tipo_de_registro;
    private Integer identificador_iip;
    private Double total_monto_fondo;
    private Double total_monto_institucion;
    private Double total_monto_afc;
    private String cuenta_fondo;
    private String cuenta_institucion;
    private String cuenta_afc;
    private String identificacion_banco;
    private String codigo_banco;
    private Date fecha_abono;
    private String codigo_recaudador;
    private String glosa_recaudador;
    private Integer numero_de_planillas; 
    private boolean procesado;
    private String cuentaCorrienteAbono;
    private Double totalAbono;
    private Boolean bool_validacion_cartola_cta_cte = false;
    private Boolean bool_button_rechazado = false;
    private Boolean bool_button_autobs = false;
    private String strtotal_monto_inf_cartola;
    private Double monto_total_pagado_planilla;
    private Integer id_motivo_rechazo;
    private String descripcion_motivo_rechazo;
    private Integer estado_proceso;
    private String estado_proceso_excepcion;
    private String descripcion_estado_proceso;
    
    private String mensajeSegunEstado;
    private String estadoProcesoAutorizacion;
    /*
     * Datos para el detalle
     */
    private List<ArchivoAbonoDTO> archivoabono;    
    private Integer numero_detalle_procesados;
    private boolean proceso_detalle;
    private Double monto_total_pagado_planillaprocesados;
    private Double monto_total_pagado_planillanoprocesados;
    private boolean procesadoAprobacion = false;
    /*
     * Datos del detalle rechazados y aceptados
     */

    private List<ArchivoAbonoDTO> archivoabonoplanillaprocesados;  
    private List<ArchivoAbonoDTO> archivoabonoplanillanoprocesados;  

    public void setObservacion_rechazo(String observacion_rechazo) {
        this.observacion_rechazo = observacion_rechazo;
    }

    public String getObservacion_rechazo() {
        return observacion_rechazo;
    }

    public void setObservacion_autorizacion(String observacion_autorizacion) {
        this.observacion_autorizacion = observacion_autorizacion;
    }

    public String getObservacion_autorizacion() {
        return observacion_autorizacion;
    }

    public void setValidacion_cartola_cta_cte(Integer validacion_cartola_cta_cte) {
        this.validacion_cartola_cta_cte = validacion_cartola_cta_cte;
    }

    public Integer getValidacion_cartola_cta_cte() {
        return validacion_cartola_cta_cte;
    }

    public void setTotal_monto_inf_cartola(Double total_monto_inf_cartola) {
        this.total_monto_inf_cartola = total_monto_inf_cartola;
    }

    public Double getTotal_monto_inf_cartola() {
        return total_monto_inf_cartola;
    }

    public void setTipo_de_registro(Integer tipo_de_registro) {
        this.tipo_de_registro = tipo_de_registro;
    }

    public Integer getTipo_de_registro() {
        return tipo_de_registro;
    }

    public void setIdentificador_iip(Integer identificador_iip) {
        this.identificador_iip = identificador_iip;
    }

    public Integer getIdentificador_iip() {
        return identificador_iip;
    }

    public void setTotal_monto_fondo(Double total_monto_fondo) {
        this.total_monto_fondo = total_monto_fondo;
    }

    public Double getTotal_monto_fondo() {
        return total_monto_fondo;
    }

    public void setTotal_monto_institucion(Double total_monto_institucion) {
        this.total_monto_institucion = total_monto_institucion;
    }

    public Double getTotal_monto_institucion() {
        return total_monto_institucion;
    }

    public void setTotal_monto_afc(Double total_monto_afc) {
        this.total_monto_afc = total_monto_afc;
    }

    public Double getTotal_monto_afc() {
        return total_monto_afc;
    }

    public void setCuenta_fondo(String cuenta_fondo) {
        this.cuenta_fondo = cuenta_fondo;
    }

    public String getCuenta_fondo() {
        return cuenta_fondo;
    }

    public void setCuenta_institucion(String cuenta_institucion) {
        this.cuenta_institucion = cuenta_institucion;
    }

    public String getCuenta_institucion() {
        return cuenta_institucion;
    }

    public void setCuenta_afc(String cuenta_afc) {
        this.cuenta_afc = cuenta_afc;
    }

    public String getCuenta_afc() {
        return cuenta_afc;
    }

    public void setIdentificacion_banco(String identificacion_banco) {
        this.identificacion_banco = identificacion_banco;
    }

    public String getIdentificacion_banco() {
        return identificacion_banco;
    }

    public void setCodigo_banco(String codigo_banco) {
        this.codigo_banco = codigo_banco;
    }

    public String getCodigo_banco() {
        return codigo_banco;
    }

    public void setFecha_abono(Date fecha_abono) {
        this.fecha_abono = fecha_abono;
    }

    public Date getFecha_abono() {
        return fecha_abono;
    }

    public void setCodigo_recaudador(String codigo_recaudador) {
        this.codigo_recaudador = codigo_recaudador;
    }

    public String getCodigo_recaudador() {
        return codigo_recaudador;
    }

    public void setGlosa_recaudador(String glosa_recaudador) {
        this.glosa_recaudador = glosa_recaudador;
    }

    public String getGlosa_recaudador() {
        return glosa_recaudador;
    }

    public void setNumero_de_planillas(Integer numero_de_planillas) {
        this.numero_de_planillas = numero_de_planillas;
    }

    public Integer getNumero_de_planillas() {
        return numero_de_planillas;
    }

    public void setArchivoabono(List<ArchivoAbonoDTO> archivoabono) {
        this.archivoabono = archivoabono;
    }

    public List<ArchivoAbonoDTO> getArchivoabono() {
        return archivoabono;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }

    public boolean isProcesado() {
        return procesado;
    }

    public void setNumero_detalle_procesados(Integer numero_detalle_procesados) {
        this.numero_detalle_procesados = numero_detalle_procesados;
    }

    public Integer getNumero_detalle_procesados() {
        return numero_detalle_procesados;
    }

    public void setProceso_detalle(boolean proceso_detalle) {
        this.proceso_detalle = proceso_detalle;
    }

    public boolean isProceso_detalle() {
        return proceso_detalle;
    }

    public void setCuentaCorrienteAbono(String cuentaCorrienteAbono) {
        this.cuentaCorrienteAbono = cuentaCorrienteAbono;
    }

    public String getCuentaCorrienteAbono() {
        if(this.getCuenta_afc() != null){
            cuentaCorrienteAbono = this.getCuenta_afc();
        }
        
        if(this.getCuenta_fondo() != null){
            cuentaCorrienteAbono = this.getCuenta_fondo();
        }
        
        if(this.getCuenta_institucion() != null){
            cuentaCorrienteAbono = this.getCuenta_institucion();
        }
        
        return cuentaCorrienteAbono;
    }

    public void setTotalAbono(Double totalAbono) {
        this.totalAbono = totalAbono;
    }

    public Double getTotalAbono() {
        totalAbono = this.getTotal_monto_afc() + this.getTotal_monto_fondo() + this.getTotal_monto_institucion();
        return totalAbono;
    }

    public void setBool_validacion_cartola_cta_cte(Boolean bool_validacion_cartola_cta_cte) {
        this.bool_validacion_cartola_cta_cte = bool_validacion_cartola_cta_cte;
    }

    public Boolean getBool_validacion_cartola_cta_cte() {
        return bool_validacion_cartola_cta_cte;
    }

    public void setBool_button_rechazado(Boolean bool_button_rechazado) {
        this.bool_button_rechazado = bool_button_rechazado;
    }

    public Boolean getBool_button_rechazado() {
        return bool_button_rechazado;
    }

    public void setBool_button_autobs(Boolean bool_button_autobs) {
        this.bool_button_autobs = bool_button_autobs;
    }

    public Boolean getBool_button_autobs() {
        return bool_button_autobs;
    }

    public void setStrtotal_monto_inf_cartola(String strtotal_monto_inf_cartola) {
        this.strtotal_monto_inf_cartola = strtotal_monto_inf_cartola;
    }

    public String getStrtotal_monto_inf_cartola() {
        return strtotal_monto_inf_cartola;
    }    

    public void setMonto_total_pagado_planilla(Double monto_total_pagado_planilla) {
        this.monto_total_pagado_planilla = monto_total_pagado_planilla;
    }

    public Double getMonto_total_pagado_planilla() {
        return monto_total_pagado_planilla;
    }

    public void setMonto_total_pagado_planillaprocesados(Double monto_total_pagado_planillaprocesados) {
        this.monto_total_pagado_planillaprocesados = monto_total_pagado_planillaprocesados;
    }

    public Double getMonto_total_pagado_planillaprocesados() {
        return monto_total_pagado_planillaprocesados;
    }

    public void setMonto_total_pagado_planillanoprocesados(Double monto_total_pagado_planillanoprocesados) {
        this.monto_total_pagado_planillanoprocesados = monto_total_pagado_planillanoprocesados;
    }

    public Double getMonto_total_pagado_planillanoprocesados() {
        return monto_total_pagado_planillanoprocesados;
    }

    public void setArchivoabonoplanillaprocesados(List<ArchivoAbonoDTO> archivoabonoplanillaprocesados) {
        this.archivoabonoplanillaprocesados = archivoabonoplanillaprocesados;
    }

    public List<ArchivoAbonoDTO> getArchivoabonoplanillaprocesados() {
        return archivoabonoplanillaprocesados;
    }

    public void setArchivoabonoplanillanoprocesados(List<ArchivoAbonoDTO> archivoabonoplanillanoprocesados) {
        this.archivoabonoplanillanoprocesados = archivoabonoplanillanoprocesados;
    }

    public List<ArchivoAbonoDTO> getArchivoabonoplanillanoprocesados() {
        return archivoabonoplanillanoprocesados;
    }

    public void setProcesadoAprobacion(boolean procesadoAprobacion) {
        this.procesadoAprobacion = procesadoAprobacion;
    }

    public boolean isProcesadoAprobacion() {
        return procesadoAprobacion;
    }
    
    public void setId_motivo_rechazo(Integer id_motivo_rechazo) {
        this.id_motivo_rechazo = id_motivo_rechazo;
    }

    public Integer getId_motivo_rechazo() {
        return id_motivo_rechazo;
    }

    public void setDescripcion_motivo_rechazo(String descripcion_motivo_rechazo) {
        this.descripcion_motivo_rechazo = descripcion_motivo_rechazo;
    }

    public String getDescripcion_motivo_rechazo() {
        return descripcion_motivo_rechazo;
    }

    public void setEstado_proceso(Integer estado_proceso) {
        this.estado_proceso = estado_proceso;
    }

    public Integer getEstado_proceso() {
        return estado_proceso;
    }

    public void setEstado_proceso_excepcion(String estado_proceso_excepcion) {
        this.estado_proceso_excepcion = estado_proceso_excepcion;
    }

    public String getEstado_proceso_excepcion() {
        return estado_proceso_excepcion;
    }

    public void setDescripcion_estado_proceso(String descripcion_estado_proceso) {
        this.descripcion_estado_proceso = descripcion_estado_proceso;
    }

    public String getDescripcion_estado_proceso() {
        return descripcion_estado_proceso;
    }

    public void setMensajeSegunEstado(String mensajeSegunEstado) {
        this.mensajeSegunEstado = mensajeSegunEstado;
    }

    public String getMensajeSegunEstado() {
        return mensajeSegunEstado;
    }

    public void setEstadoProcesoAutorizacion(String estadoProcesoAutorizacion) {
        this.estadoProcesoAutorizacion = estadoProcesoAutorizacion;
    }

    public String getEstadoProcesoAutorizacion() {
        return estadoProcesoAutorizacion;
    }
}
