package cl.bicevida.previred.common.dto;

import java.io.Serializable;

import java.util.Date;

public class ArchivoPlanillaResumenDTO implements Serializable {
    @SuppressWarnings("compatibility:4204390895169126006")
    private static final long serialVersionUID = 1L;

    public ArchivoPlanillaResumenDTO() {
    }
    
    private Integer tipo_de_registro;
    private Double total_cotiza_voluntaria_apvi;
    private Double total_cotiza_voluntaria_apvi_b;
    private Double total_apv_colect_empleador;
    private Double total_apv_colect_trabajador;
    private Double total_depositos_convenidos;
    private Double total_pagar_ia;
    private Date periodo_pago;
    private Integer numero_afiliados_informados;
    private boolean procesado;


    public void setTipo_de_registro(Integer tipo_de_registro) {
        this.tipo_de_registro = tipo_de_registro;
    }

    public Integer getTipo_de_registro() {
        return tipo_de_registro;
    }

    public void setTotal_cotiza_voluntaria_apvi(Double total_cotiza_voluntaria_apvi) {
        this.total_cotiza_voluntaria_apvi = total_cotiza_voluntaria_apvi;
    }

    public Double getTotal_cotiza_voluntaria_apvi() {
        return total_cotiza_voluntaria_apvi;
    }

    public void setTotal_cotiza_voluntaria_apvi_b(Double total_cotiza_voluntaria_apvi_b) {
        this.total_cotiza_voluntaria_apvi_b = total_cotiza_voluntaria_apvi_b;
    }

    public Double getTotal_cotiza_voluntaria_apvi_b() {
        return total_cotiza_voluntaria_apvi_b;
    }

    public void setTotal_apv_colect_empleador(Double total_apv_colect_empleador) {
        this.total_apv_colect_empleador = total_apv_colect_empleador;
    }

    public Double getTotal_apv_colect_empleador() {
        return total_apv_colect_empleador;
    }

    public void setTotal_apv_colect_trabajador(Double total_apv_colect_trabajador) {
        this.total_apv_colect_trabajador = total_apv_colect_trabajador;
    }

    public Double getTotal_apv_colect_trabajador() {
        return total_apv_colect_trabajador;
    }

    public void setTotal_depositos_convenidos(Double total_depositos_convenidos) {
        this.total_depositos_convenidos = total_depositos_convenidos;
    }

    public Double getTotal_depositos_convenidos() {
        return total_depositos_convenidos;
    }

    public void setTotal_pagar_ia(Double total_pagar_ia) {
        this.total_pagar_ia = total_pagar_ia;
    }

    public Double getTotal_pagar_ia() {
        return total_pagar_ia;
    }

    public void setPeriodo_pago(Date periodo_pago) {
        this.periodo_pago = periodo_pago;
    }

    public Date getPeriodo_pago() {
        return periodo_pago;
    }

    public void setNumero_afiliados_informados(Integer numero_afiliados_informados) {
        this.numero_afiliados_informados = numero_afiliados_informados;
    }

    public Integer getNumero_afiliados_informados() {
        return numero_afiliados_informados;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }

    public boolean isProcesado() {
        return procesado;
    }
}
