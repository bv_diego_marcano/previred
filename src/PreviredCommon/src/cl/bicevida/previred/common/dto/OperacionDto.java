package cl.bicevida.previred.common.dto;

public class OperacionDto {
    private Long operacion;
    private Integer empresa;
    
    public OperacionDto() {
        super();
    }


    public void setOperacion(Long operacion) {
        this.operacion = operacion;
    }

    public Long getOperacion() {
        return operacion;
    }

    public void setEmpresa(Integer empresa) {
        this.empresa = empresa;
    }

    public Integer getEmpresa() {
        return empresa;
    }
}
