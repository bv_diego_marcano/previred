package cl.bicevida.previred.common.dto;

import java.io.Serializable;

import java.util.Date;

public class RendProcesoAutomatico implements Serializable{
    @SuppressWarnings("compatibility:-5460096883636788537")
    private static final long serialVersionUID = 1L;

    private Long idCarga;
    private Integer idTipoArchivo;
    private Integer medioPago;
    private Date fecha;
    private String servidor;
    private Integer estadoEjecucion;
    private String nombreArchivo;
    private String descripcionEstado;
    private String observacion;


    public RendProcesoAutomatico(Long idCarga, Integer idTipoArchivo, Integer medioPago, Date fecha, String servidor,
                                 Integer estadoEjecucion, String nombreArchivo, String descripcionEstado,
                                 String observacion) {
        super();
        this.idCarga = idCarga;
        this.idTipoArchivo = idTipoArchivo;
        this.medioPago = medioPago;
        this.fecha = fecha;
        this.servidor = servidor;
        this.estadoEjecucion = estadoEjecucion;
        this.nombreArchivo = nombreArchivo;
        this.descripcionEstado = descripcionEstado;
        this.observacion = observacion;
    }

    public RendProcesoAutomatico() {
        super();
    }


    public void setIdCarga(Long idCarga) {
        this.idCarga = idCarga;
    }

    public Long getIdCarga() {
        return idCarga;
    }

    public void setIdTipoArchivo(Integer idTipoArchivo) {
        this.idTipoArchivo = idTipoArchivo;
    }

    public Integer getIdTipoArchivo() {
        return idTipoArchivo;
    }

    public void setMedioPago(Integer medioPago) {
        this.medioPago = medioPago;
    }

    public Integer getMedioPago() {
        return medioPago;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setServidor(String servidor) {
        this.servidor = servidor;
    }

    public String getServidor() {
        return servidor;
    }

    public void setEstadoEjecucion(Integer estadoEjecucion) {
        this.estadoEjecucion = estadoEjecucion;
    }

    public Integer getEstadoEjecucion() {
        return estadoEjecucion;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setDescripcionEstado(String descripcionEstado) {
        this.descripcionEstado = descripcionEstado;
    }

    public String getDescripcionEstado() {
        return descripcionEstado;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getObservacion() {
        return observacion;
    }
}
