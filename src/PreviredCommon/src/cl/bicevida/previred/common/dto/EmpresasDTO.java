package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class EmpresasDTO implements Serializable {
    @SuppressWarnings("compatibility:1262529831866213389")
    private static final long serialVersionUID = 1L;

    public EmpresasDTO() {
    }
    
    private Integer idEmpresa;
    private String descripcionEmpresa;

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setDescripcionEmpresa(String descripcionEmpresa) {
        this.descripcionEmpresa = descripcionEmpresa;
    }

    public String getDescripcionEmpresa() {
        return descripcionEmpresa;
    }
}
