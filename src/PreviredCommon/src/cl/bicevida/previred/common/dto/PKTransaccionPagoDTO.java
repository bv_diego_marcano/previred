package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class PKTransaccionPagoDTO implements Serializable {
    @SuppressWarnings("compatibility:3849295860948874757")
    private static final long serialVersionUID = 1L;
    private Double idCarga;
    private Double folioPlanilla;
    private Integer codigoRecaudador;
    private Integer newEstado;
    private String newDescripcion;
    private Double idCargaPlanilla;
    private Double folioCaja;
    private String conceptoCaja;
    private Integer poliza;
    public PKTransaccionPagoDTO() {
    }

    public void setIdCarga(Double idCarga) {
        this.idCarga = idCarga;
    }

    public Double getIdCarga() {
        return idCarga;
    }

    public void setFolioPlanilla(Double folioPlanilla) {
        this.folioPlanilla = folioPlanilla;
    }

    public Double getFolioPlanilla() {
        return folioPlanilla;
    }

    public void setCodigoRecaudador(Integer codigoRecaudador) {
        this.codigoRecaudador = codigoRecaudador;
    }

    public Integer getCodigoRecaudador() {
        return codigoRecaudador;
    }

    public void setNewEstado(Integer newEstado) {
        this.newEstado = newEstado;
    }

    public Integer getNewEstado() {
        return newEstado;
    }

    public void setNewDescripcion(String newDescripcion) {
        this.newDescripcion = newDescripcion;
    }

    public String getNewDescripcion() {
        return newDescripcion;
    }

    public void setIdCargaPlanilla(Double idCargaPlanilla) {
        this.idCargaPlanilla = idCargaPlanilla;
    }

    public Double getIdCargaPlanilla() {
        return idCargaPlanilla;
    }

    public void setFolioCaja(Double folioCaja) {
        this.folioCaja = folioCaja;
    }

    public Double getFolioCaja() {
        return folioCaja;
    }

    public void setConceptoCaja(String conceptoCaja) {
        this.conceptoCaja = conceptoCaja;
    }

    public String getConceptoCaja() {
        return conceptoCaja;
    }

    public void setPoliza(Integer poliza) {
        this.poliza = poliza;
    }

    public Integer getPoliza() {
        return poliza;
    }
}
