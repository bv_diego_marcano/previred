package cl.bicevida.previred.common.dto;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

public class ResumenRendicionesVwDTO implements Serializable {
    @SuppressWarnings("compatibility:8507622361089149546")
    private static final long serialVersionUID = 1L;

    public ResumenRendicionesVwDTO() {
    }
    
    private String tipoTransaccion;
    
    private Date inicio_proceso;
    private Double id_carga_planilla;
    private Double id_carga;
    private Integer codigo_recaudador;
    private String glosa_recaudador;
    private Double monto_total_abonado;
    private Double monto_aplicado;
    private Double monto_no_aplicado;
    private String identificacion_banco;
    private Integer codigo_banco;
    private Date fecha_abono;
    private Integer numero_de_registros;
    private Double id_folio_abono;
    private Double monto_detalle_abono;
    private String rutEmpleador;
    private String rut_empleador;
    private String dv_empleador;
    private String razon_social_empleador;
    private Date fecha_pago;
    private String rutTrabajador;
    private String rut_trabajador;
    private String dv_trabajador;
    private String nombre_trabajador;
    private Double monto_detalle_planilla;
    private Date periodo_pago;
    private Double total_cotiza_voluntaria_apvi;
    private Double total_cotiza_voluntaria_apvi_b;
    private Integer estado_proceso_abono;
    private String descripcion_proceso_abono;
    private Integer estado_proceso_planilla;
    private String descripcion_proceso_planilla;
    private String obs_proceso_planilla;
    private Integer poliza_asoc;
    private String concepto_pago;
    private Date fecha_pago_caja;
    private Double id_folio_pagocaja;
    private Date fecha_hora_operacion; 
    private Double deposito_convenio;
    private List<ResumenRendicionesVwDTO> detalle_resumen_rendiciones;
    private String estadoProcesamientoManual;
    private String nombreFisicoArchivo;


    public void setInicio_proceso(Date inicio_proceso) {
        this.inicio_proceso = inicio_proceso;
    }

    public Date getInicio_proceso() {
        return inicio_proceso;
    }

    public void setId_carga(Double id_carga) {
        this.id_carga = id_carga;
    }

    public Double getId_carga() {
        return id_carga;
    }

    public void setCodigo_recaudador(Integer codigo_recaudador) {
        this.codigo_recaudador = codigo_recaudador;
    }

    public Integer getCodigo_recaudador() {
        return codigo_recaudador;
    }

    public void setGlosa_recaudador(String glosa_recaudador) {
        this.glosa_recaudador = glosa_recaudador;
    }

    public String getGlosa_recaudador() {
        return glosa_recaudador;
    }

    public void setMonto_total_abonado(Double monto_total_abonado) {
        this.monto_total_abonado = monto_total_abonado;
    }

    public Double getMonto_total_abonado() {
        return monto_total_abonado;
    }

    public void setMonto_aplicado(Double monto_aplicado) {
        this.monto_aplicado = monto_aplicado;
    }

    public Double getMonto_aplicado() {
        return monto_aplicado;
    }

    public void setMonto_no_aplicado(Double monto_no_aplicado) {
        this.monto_no_aplicado = monto_no_aplicado;
    }

    public Double getMonto_no_aplicado() {
        return monto_no_aplicado;
    }

    public void setIdentificacion_banco(String identificacion_banco) {
        this.identificacion_banco = identificacion_banco;
    }

    public String getIdentificacion_banco() {
        return identificacion_banco;
    }

    public void setCodigo_banco(Integer codigo_banco) {
        this.codigo_banco = codigo_banco;
    }

    public Integer getCodigo_banco() {
        return codigo_banco;
    }

    public void setFecha_abono(Date fecha_abono) {
        this.fecha_abono = fecha_abono;
    }

    public Date getFecha_abono() {
        return fecha_abono;
    }

    public void setNumero_de_registros(Integer numero_de_registros) {
        this.numero_de_registros = numero_de_registros;
    }

    public Integer getNumero_de_registros() {
        return numero_de_registros;
    }

    public void setId_folio_abono(Double id_folio_abono) {
        this.id_folio_abono = id_folio_abono;
    }

    public Double getId_folio_abono() {
        return id_folio_abono;
    }

    public void setMonto_detalle_abono(Double monto_detalle_abono) {
        this.monto_detalle_abono = monto_detalle_abono;
    }

    public Double getMonto_detalle_abono() {
        return monto_detalle_abono;
    }

    public void setRut_empleador(String rut_empleador) {
        this.rut_empleador = rut_empleador;
    }

    public String getRut_empleador() {
        return rut_empleador;
    }

    public void setRazon_social_empleador(String razon_social_empleador) {
        this.razon_social_empleador = razon_social_empleador;
    }

    public String getRazon_social_empleador() {
        return razon_social_empleador;
    }

    public void setFecha_pago(Date fecha_pago) {
        this.fecha_pago = fecha_pago;
    }

    public Date getFecha_pago() {
        return fecha_pago;
    }

    public void setRut_trabajador(String rut_trabajador) {
        this.rut_trabajador = rut_trabajador;
    }

    public String getRut_trabajador() {
        return rut_trabajador;
    }

    public void setNombre_trabajador(String nombre_trabajador) {
        this.nombre_trabajador = nombre_trabajador;
    }

    public String getNombre_trabajador() {
        return nombre_trabajador;
    }

    public void setMonto_detalle_planilla(Double monto_detalle_planilla) {
        this.monto_detalle_planilla = monto_detalle_planilla;
    }

    public Double getMonto_detalle_planilla() {
        return monto_detalle_planilla;
    }

    public void setPeriodo_pago(Date periodo_pago) {
        this.periodo_pago = periodo_pago;
    }

    public Date getPeriodo_pago() {
        return periodo_pago;
    }

    public void setTotal_cotiza_voluntaria_apvi(Double total_cotiza_voluntaria_apvi) {
        this.total_cotiza_voluntaria_apvi = total_cotiza_voluntaria_apvi;
    }

    public Double getTotal_cotiza_voluntaria_apvi() {
        return total_cotiza_voluntaria_apvi;
    }

    public void setTotal_cotiza_voluntaria_apvi_b(Double total_cotiza_voluntaria_apvi_b) {
        this.total_cotiza_voluntaria_apvi_b = total_cotiza_voluntaria_apvi_b;
    }

    public Double getTotal_cotiza_voluntaria_apvi_b() {
        return total_cotiza_voluntaria_apvi_b;
    }

    public void setEstado_proceso_abono(Integer estado_proceso_abono) {
        this.estado_proceso_abono = estado_proceso_abono;
    }

    public Integer getEstado_proceso_abono() {
        return estado_proceso_abono;
    }

    public void setDescripcion_proceso_abono(String descripcion_proceso_abono) {
        this.descripcion_proceso_abono = descripcion_proceso_abono;
    }

    public String getDescripcion_proceso_abono() {
        return descripcion_proceso_abono;
    }

    public void setEstado_proceso_planilla(Integer estado_proceso_planilla) {
        this.estado_proceso_planilla = estado_proceso_planilla;
    }

    public Integer getEstado_proceso_planilla() {
        return estado_proceso_planilla;
    }

    public void setDescripcion_proceso_planilla(String descripcion_proceso_planilla) {
        this.descripcion_proceso_planilla = descripcion_proceso_planilla;
    }

    public String getDescripcion_proceso_planilla() {
        return descripcion_proceso_planilla;
    }

    public void setObs_proceso_planilla(String obs_proceso_planilla) {
        this.obs_proceso_planilla = obs_proceso_planilla;
    }

    public String getObs_proceso_planilla() {
        return obs_proceso_planilla;
    }

    public void setPoliza_asoc(Integer poliza_asoc) {
        this.poliza_asoc = poliza_asoc;
    }

    public Integer getPoliza_asoc() {
        return poliza_asoc;
    }

    public void setConcepto_pago(String concepto_pago) {
        this.concepto_pago = concepto_pago;
    }

    public String getConcepto_pago() {
        return concepto_pago;
    }

    public void setFecha_pago_caja(Date fecha_pago_caja) {
        this.fecha_pago_caja = fecha_pago_caja;
    }

    public Date getFecha_pago_caja() {
        return fecha_pago_caja;
    }

    public void setDetalle_resumen_rendiciones(List<ResumenRendicionesVwDTO> detalle_resumen_rendiciones) {
        this.detalle_resumen_rendiciones = detalle_resumen_rendiciones;
    }

    public List<ResumenRendicionesVwDTO> getDetalle_resumen_rendiciones() {
        return detalle_resumen_rendiciones;
    }

    public void setEstadoProcesamientoManual(String estadoProcesamientoManual) {
        this.estadoProcesamientoManual = estadoProcesamientoManual;
    }

    public String getEstadoProcesamientoManual() {
        return estadoProcesamientoManual;
    }

    public void setId_carga_planilla(Double id_carga_planilla) {
        this.id_carga_planilla = id_carga_planilla;
    }

    public Double getId_carga_planilla() {
        return id_carga_planilla;
    }

    public void setId_folio_pagocaja(Double id_folio_pagocaja) {
        this.id_folio_pagocaja = id_folio_pagocaja;
    }

    public Double getId_folio_pagocaja() {
        return id_folio_pagocaja;
    }

    public void setDv_empleador(String dv_empleador) {
        this.dv_empleador = dv_empleador;
    }

    public String getDv_empleador() {
        return dv_empleador;
    }

    public void setDv_trabajador(String dv_trabajador) {
        this.dv_trabajador = dv_trabajador;
    }

    public String getDv_trabajador() {
        return dv_trabajador;
    }

    public void setRutEmpleador(String rutEmpleador) {
        this.rutEmpleador = rutEmpleador;
    }

    public String getRutEmpleador() {
        return (rut_empleador + "-" + dv_empleador);
    }

    public void setRutTrabajador(String rutTrabajador) {
        this.rutTrabajador = rutTrabajador;
    }

    public String getRutTrabajador() {
        return (rut_trabajador + "-" + dv_trabajador);
    }

    public void setFecha_hora_operacion(Date fecha_hora_operacion) {
        this.fecha_hora_operacion = fecha_hora_operacion;
    }

    public Date getFecha_hora_operacion() {
        return fecha_hora_operacion;
    }

    public void setDeposito_convenio(Double deposito_convenio) {
        this.deposito_convenio = deposito_convenio;
    }

    public Double getDeposito_convenio() {
        return deposito_convenio;
    }

    public void setNombreFisicoArchivo(String nombreFisicoArchivo) {
        this.nombreFisicoArchivo = nombreFisicoArchivo;
    }

    public String getNombreFisicoArchivo() {
        return nombreFisicoArchivo;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }
}
