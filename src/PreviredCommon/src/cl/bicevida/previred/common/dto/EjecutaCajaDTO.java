package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class EjecutaCajaDTO implements Serializable {
    @SuppressWarnings("compatibility:-3826036347728049453")
    private static final long serialVersionUID = 1L;
    private ArchivoPlanillaHeaderDTO dto;
    private Long numeroFolioCaja;
    private String descripcion;
    public EjecutaCajaDTO() {
    }

    public void setDto(ArchivoPlanillaHeaderDTO dto) {
        this.dto = dto;
    }

    public ArchivoPlanillaHeaderDTO getDto() {
        return dto;
    }

    public void setNumeroFolioCaja(Long numeroFolioCaja) {
        this.numeroFolioCaja = numeroFolioCaja;
    }

    public Long getNumeroFolioCaja() {
        return numeroFolioCaja;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
