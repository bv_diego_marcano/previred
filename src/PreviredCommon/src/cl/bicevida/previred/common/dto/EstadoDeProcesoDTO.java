package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class EstadoDeProcesoDTO implements Serializable {
    @SuppressWarnings("compatibility:9041830710213183520")
    private static final long serialVersionUID = 1L;

    public EstadoDeProcesoDTO() {
    }
    
    private Integer codigoEstado;
    private String decripcionEstado;

    public void setCodigoEstado(Integer codigoEstado) {
        this.codigoEstado = codigoEstado;
    }

    public Integer getCodigoEstado() {
        return codigoEstado;
    }

    public void setDecripcionEstado(String decripcionEstado) {
        this.decripcionEstado = decripcionEstado;
    }

    public String getDecripcionEstado() {
        return decripcionEstado;
    }
}
