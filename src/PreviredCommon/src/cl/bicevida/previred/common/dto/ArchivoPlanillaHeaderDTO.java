package cl.bicevida.previred.common.dto;

import java.io.Serializable;

import java.util.List;

public class ArchivoPlanillaHeaderDTO implements Serializable {
    @SuppressWarnings("compatibility:1463745918767912334")
    private static final long serialVersionUID = 1L;

    public ArchivoPlanillaHeaderDTO() {
    }
    
    private Integer numero_declaraciones;
    private List<ArchivoPlanillaEncabezadoDTO> lst_detalle_planilla;
    private Integer tipo_de_registro;
    private Integer numero_detalle_procesados;
    private boolean proceso_detalle;
    private Double idCargaAbono;

    public void setNumero_declaraciones(Integer numero_declaraciones) {
        this.numero_declaraciones = numero_declaraciones;
    }

    public Integer getNumero_declaraciones() {
        return numero_declaraciones;
    }

    public void setLst_detalle_planilla(List<ArchivoPlanillaEncabezadoDTO> lst_detalle_planilla) {
        this.lst_detalle_planilla = lst_detalle_planilla;
    }

    public List<ArchivoPlanillaEncabezadoDTO> getLst_detalle_planilla() {
        return lst_detalle_planilla;
    }

    public void setTipo_de_registro(Integer tipo_de_registro) {
        this.tipo_de_registro = tipo_de_registro;
    }

    public Integer getTipo_de_registro() {
        return tipo_de_registro;
    }

    public void setNumero_detalle_procesados(Integer numero_detalle_procesados) {
        this.numero_detalle_procesados = numero_detalle_procesados;
    }

    public Integer getNumero_detalle_procesados() {
        return numero_detalle_procesados;
    }

    public void setProceso_detalle(boolean proceso_detalle) {
        this.proceso_detalle = proceso_detalle;
    }

    public boolean isProceso_detalle() {
        return proceso_detalle;
    }

    public void setIdCargaAbono(Double idCargaAbono) {
        this.idCargaAbono = idCargaAbono;
    }

    public Double getIdCargaAbono() {
        return idCargaAbono;
    }
}
