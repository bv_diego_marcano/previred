package cl.bicevida.previred.common.dto;

import java.io.Serializable;

public class TipoArchivoDTO implements Serializable {
    @SuppressWarnings("compatibility:8734027452033638731")
    private static final long serialVersionUID = 1L;

    public TipoArchivoDTO() {
    }
    
    private Integer idTipoArchivo;
    private String tipoArchivo;
    private String descripcionTipoArchivo;

    public void setIdTipoArchivo(Integer idTipoArchivo) {
        this.idTipoArchivo = idTipoArchivo;
    }

    public Integer getIdTipoArchivo() {
        return idTipoArchivo;
    }

    public void setDescripcionTipoArchivo(String descripcionTipoArchivo) {
        this.descripcionTipoArchivo = descripcionTipoArchivo;
    }

    public String getDescripcionTipoArchivo() {
        return descripcionTipoArchivo;
    }

    public void setTipoArchivo(String tipoArchivo) {
        this.tipoArchivo = tipoArchivo;
    }

    public String getTipoArchivo() {
        return tipoArchivo;
    }
}
