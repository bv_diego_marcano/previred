package cl.bicevida.previred.common.utils;

import cl.bicevida.previred.common.dto.ArchivoAbonoDTO;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;

import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.util.CellRangeAddress;


public class ExcelUtil implements Serializable{
    @SuppressWarnings("compatibility:9024008292598392643")
    private static final long serialVersionUID = 1L;

    /**
     * Logger for this class
     */
    private static final Logger logger = Logger.getLogger(ExcelUtil.class);


    public ExcelUtil() {
    }

    public static HSSFWorkbook generateExcelRecaudacionOperaciones(List<ResumenAbonoDTO> operacionesSVIRegistradas, 
                                                                   List<ResumenAbonoDTO> operacionesSVINoRegistradas) {

        logger.debug("generateExcelRecaudacionOperaciones() - Inicio");
        HSSFWorkbook wb = null;

        try {
            if (operacionesSVIRegistradas == null && 
                operacionesSVINoRegistradas == null) {
                wb = null;
            } else {
                // Crea Libro
                wb = new HSSFWorkbook();

                /*
                 * Definici�n Fuentes
                 */
                // fuente titulo 
                HSSFFont fuenteTitulo = wb.createFont();
                fuenteTitulo.setColor(HSSFColor.DARK_BLUE.index);
                fuenteTitulo.setFontName("Calibri");
                fuenteTitulo.setFontHeightInPoints((short)10);
                fuenteTitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                // fuente normal
                HSSFFont fuenteNormal = wb.createFont();
                fuenteNormal.setColor(HSSFColor.DARK_BLUE.index);
                fuenteNormal.setFontName("Calibri");
                fuenteNormal.setFontHeightInPoints((short)10);
                fuenteNormal.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);

                /*
                 * Definici�n estilo celdas
                 */
                //Estilo Celda titulo 
                HSSFCellStyle estiloCabecera = wb.createCellStyle();
                estiloCabecera.setFont(fuenteTitulo);
                estiloCabecera.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                estiloCabecera.setDataFormat((short)0);
                estiloCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                estiloCabecera.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                //estiloCabecera.setFillBackgroundColor(HSSFColor.LIGHT_BLUE.index);
                estiloCabecera.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBorderRight(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBorderTop(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBottomBorderColor(HSSFColor.DARK_BLUE.index);

                //Estilo celda normal
                HSSFCellStyle estiloNormal = wb.createCellStyle();
                estiloNormal.setFont(fuenteNormal);
                estiloNormal.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                estiloNormal.setDataFormat((short)0);
                estiloNormal.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                estiloNormal.setFillForegroundColor(HSSFColor.WHITE.index);
                // estiloNormal.setFillBackgroundColor(HSSFColor.WHITE.index);
                estiloNormal.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBorderRight(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBorderTop(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBottomBorderColor(HSSFColor.DARK_BLUE.index);


                if (operacionesSVIRegistradas != null) {
                    for (int hoja = 0; hoja < operacionesSVIRegistradas.size(); 
                         hoja++) {
                        int rowNum = 0;
                        int colNum = 0;
                        // Crea nueva hoja
                        HSSFSheet sheet = 
                            wb.createSheet(operacionesSVIRegistradas.get(hoja).getIdentificacion_banco() + " - " + hoja);

                        /*
                         * *****************************************************************
                         * Inicio Tabla de Aprobados por Tesorer�a
                         * *****************************************************************
                         */
                        int columnMaxRecAprob = 9; //Cantidad maxima de columna

                        /*
                         * Creacion Cabecera titulo
                         */
                        // Creacion primera fila de la tabla correspondiente al titulo
                        HSSFRow row = sheet.createRow(rowNum);
                        //Titulo de Encabezado         
                        row.createCell(colNum).setCellValue(new HSSFRichTextString("Recaudaci�n Aprobada Tesorer�a - Previred" + 
                                                                                   operacionesSVIRegistradas.get(hoja).getGlosa_recaudador()));
                        for (int i = colNum + 1; i < columnMaxRecAprob; i++) {
                            row.createCell(i);
                        }

                        // Combinar celdas
                        sheet.addMergedRegion(new CellRangeAddress(rowNum, 
                                                                   rowNum, 0, 
                                                                   8));

                        // Asignar estilo a las celdas creadas
                        for (int i = colNum; i < columnMaxRecAprob; i++) {
                            row.getCell(i).setCellStyle(estiloCabecera);
                        }

                        /*
                         * Creacion Cabecera de columnas
                         */
                        rowNum++; // Fila siguiente
                        colNum = 0; //Columna inicial
                        //Encabezado de la tabla
                        row = sheet.createRow(rowNum);
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero de Folio"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Nombre Banco"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero Cta. Cte. Abono"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto Abono $"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Glosa del Recaudador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha del Abonado"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("RUT Empleador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha de Pago"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Per�odo de Cotizaci�n"));

                        //Estilo encabezado de la tabla
                        colNum = 0;
                        for (int i = colNum; i < columnMaxRecAprob; i++) {
                            row.getCell(i).setCellStyle(estiloCabecera);
                            // Ancho de las columnas fijo
                            sheet.setColumnWidth(i, 
                                                 (int)((35 * 8) / ((double)1 / 
                                                                   20)));
                            // Ancho de las columnas automatico
                            //sheet.autoSizeColumn(i);
                        }

                        /*
                         * Cargando Datos
                         */
                        colNum = 0;
                        if (operacionesSVIRegistradas.get(hoja).getArchivoabono() != 
                            null) {
                            for (int data = 0; 
                                 data < operacionesSVIRegistradas.get(hoja).getArchivoabono().size(); 
                                 data++) {

                                rowNum++;
                                ArchivoAbonoDTO archivoAbonoDTO = 
                                    operacionesSVIRegistradas.get(hoja).getArchivoabono().get(data);
                                //Crea fila de datos
                                // Fila1
                                row = sheet.createRow(rowNum);
                                if (archivoAbonoDTO.getId_folio_abono() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getId_folio_abono().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila2
                                if (operacionesSVIRegistradas.get(hoja).getIdentificacion_banco() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(operacionesSVIRegistradas.get(hoja).getIdentificacion_banco()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila3
                                if (operacionesSVIRegistradas.get(hoja).getCuentaCorrienteAbono() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(operacionesSVIRegistradas.get(hoja).getCuentaCorrienteAbono()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila4
                                if (archivoAbonoDTO.getMontoAbono() != null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(archivoAbonoDTO.getMontoAbono())));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila5
                                if (operacionesSVIRegistradas.get(hoja).getGlosa_recaudador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(operacionesSVIRegistradas.get(hoja).getGlosa_recaudador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila6
                                if (operacionesSVIRegistradas.get(hoja).getFecha_abono() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(operacionesSVIRegistradas.get(hoja).getFecha_abono(), 
                                                                                                                                  "dd/MM/yyyy")));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila7
                                if (archivoAbonoDTO.getRutEmpleador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getRutEmpleador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila8
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getFecha_hora_operacion() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(archivoAbonoDTO.getPlanilla().getFecha_hora_operacion(), 
                                                                                                                                  "dd/MM/yyyy")));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila9
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getResumen() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getResumen().getPeriodo_pago() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(archivoAbonoDTO.getPlanilla().getResumen().getPeriodo_pago(), 
                                                                                                                                  "MM/yyyy")));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }


                                // Generacion de estilos a llas columnas de datos
                                colNum = 0;
                                for (int i = colNum; i < columnMaxRecAprob; 
                                     i++) {
                                    row.getCell(i).setCellStyle(estiloNormal);
                                }
                            }
                        }


                        /*
                         * *****************************************************************
                         * Termino Tabla de Aprobados por Tesorer�a
                         * *****************************************************************
                         */

                        /*
                         * *****************************************************************
                         * Inicio Tabla de Procesados
                         * *****************************************************************
                         */
                        colNum = 0;
                        columnMaxRecAprob = 14; //Cantidad maxima de columna

                        /*
                         * Genera espacio
                         */
                        rowNum++;
                        sheet.createRow(rowNum);
                        rowNum++;
                        /*
                         * Creacion Cabecera titulo
                         */
                        // Creacion primera fila de la tabla correspondiente al titulo
                        row = sheet.createRow(rowNum);
                        //Titulo de Encabezado         
                        row.createCell(colNum).setCellValue(new HSSFRichTextString("Recaudaci�n  - Previred Procesados " + 
                                                                                   operacionesSVIRegistradas.get(hoja).getGlosa_recaudador()));
                        for (int i = colNum + 1; i < columnMaxRecAprob; i++) {
                            row.createCell(i);
                        }
                        // Combinar celdas                         
                        sheet.addMergedRegion(new CellRangeAddress(rowNum, 
                                                                   rowNum, 0, 
                                                                   12));

                        // Asignar estilo a las celdas creadas
                        for (int i = colNum; i < columnMaxRecAprob; i++) {
                            row.getCell(i).setCellStyle(estiloCabecera);
                        }
                        /*
                         * Creacion Cabecera de columnas
                         */
                        rowNum++; // Fila siguiente
                        colNum = 0; //Columna inicial
                        //Encabezado de la tabla
                        row = sheet.createRow(rowNum);
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero de Folio"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Rut Empleador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Raz�n Social del Empleador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha y Hora Operaci�n"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("RUT Trabajador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Nombre Trabajador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Cotizaci�n APV - A"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Cotizaci�n APV - B"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Dep�sito Convenido"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto Pagado $"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Per�odo de Cotizaci�n"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Concepto Ingreso Recaudaci�n"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("P�liza Asociada al Proceso"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero de Folio Caja"));


                        //Estilo encabezado de la tabla
                        colNum = 0;
                        for (int i = colNum; i < columnMaxRecAprob; i++) {
                            row.getCell(i).setCellStyle(estiloCabecera);
                            // Ancho de las columnas fijo
                            sheet.setColumnWidth(i, 
                                                 (int)((35 * 8) / ((double)1 / 
                                                                   20)));
                            // Ancho de las columnas automatico
                            //sheet.autoSizeColumn(i);
                        }

                        /*
                          * Cargando Datos
                          */
                        colNum = 0;
                        if (operacionesSVIRegistradas.get(hoja).getArchivoabonoplanillaprocesados() != 
                            null) {
                            for (int data = 0; 
                                 data < operacionesSVIRegistradas.get(hoja).getArchivoabonoplanillaprocesados().size(); 
                                 data++) {

                                rowNum++;
                                ArchivoAbonoDTO archivoAbonoDTO = 
                                    operacionesSVIRegistradas.get(hoja).getArchivoabonoplanillaprocesados().get(data);
                                //Crea fila de datos
                                row = sheet.createRow(rowNum);

                                // Fila1
                                if (archivoAbonoDTO.getId_folio_abono() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getId_folio_abono().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila2
                                if (archivoAbonoDTO.getRutEmpleador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getRutEmpleador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila3
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getAntecedentes() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getAntecedentes().getRazon_social_pagador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getAntecedentes().getRazon_social_pagador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila4
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getFecha_hora_operacion() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(archivoAbonoDTO.getPlanilla().getFecha_hora_operacion(), 
                                                                                                                                  "dd/MM/yyyy HH:hh:ss")));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila5
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getRutTrabajador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getRutTrabajador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila6
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getNombreTrabajador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getNombreTrabajador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila7
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila8
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi_b() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi_b().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila9
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getDeposito_convenio() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getDeposito_convenio().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila10
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getTotal_a_pagar() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getTotal_a_pagar().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }

                                // Fila11
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getPeriodo_pago() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getPeriodo_pago(), 
                                                                                                                                  "MM/yyyy")));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila12
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getConceptoRecaudacion() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getConceptoRecaudacion()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila13
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getPolizaAsociada() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""+archivoAbonoDTO.getPlanilla().getDetalleIndividual().getPolizaAsociada().intValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila14
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getId_folio_pagocaja() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""+archivoAbonoDTO.getPlanilla().getDetalleIndividual().getId_folio_pagocaja().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }

                                // Generacion de estilos a llas columnas de datos
                                colNum = 0;
                                for (int i = colNum; i < columnMaxRecAprob; 
                                     i++) {
                                    row.getCell(i).setCellStyle(estiloNormal);
                                }
                            }
                        }


                        /*
                         * *****************************************************************
                         * Termino Tabla de Procesados
                         * *****************************************************************
                         */

                        /*
                         * *****************************************************************
                         * Inicio Tabla de No Procesados
                         * *****************************************************************
                         */

                        columnMaxRecAprob = 13; //Cantidad maxima de columna

                        /*
                         * Genera espacio
                         */
                        rowNum++;
                        sheet.createRow(rowNum);
                        rowNum++;
                        /*
                          * Creacion Cabecera titulo
                          */
                        // Creacion primera fila de la tabla correspondiente al titulo
                        row = sheet.createRow(rowNum);
                        //Titulo de Encabezado         
                        row.createCell(colNum).setCellValue(new HSSFRichTextString("Recaudaci�n  - Previred No Procesados " + 
                                                                                   operacionesSVIRegistradas.get(hoja).getGlosa_recaudador()));

                        for (int i = colNum + 1; i < columnMaxRecAprob; i++) {
                            row.createCell(i);
                        }
                        // Combinar celdas
                        sheet.addMergedRegion(new CellRangeAddress(rowNum, 
                                                                   rowNum, 0, 
                                                                   12));

                        // Asignar estilo a las celdas creadas
                        for (int i = colNum; i < columnMaxRecAprob; i++) {
                            row.getCell(i).setCellStyle(estiloCabecera);
                        }
                        /*
                          * Creacion Cabecera de columnas
                          */
                        rowNum++; // Fila siguiente
                        colNum = 0; //Columna inicial
                        //Encabezado de la tabla
                        row = sheet.createRow(rowNum);
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero de Folio"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Rut Empleador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Raz�n Social del Empleador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha y Hora Operaci�n"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("RUT Trabajador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Nombre Trabajador"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Cotizaci�n APV - A"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Cotizaci�n APV - B"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Dep�sito Convenido"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto Pagado $"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Per�odo de Cotizaci�n"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Estado del Proceso"));
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Observaciones del Proceso"));


                        //Estilo encabezado de la tabla
                        colNum = 0;
                        for (int i = colNum; i < columnMaxRecAprob; i++) {
                            row.getCell(i).setCellStyle(estiloCabecera);
                            // Ancho de las columnas fijo
                            sheet.setColumnWidth(i, 
                                                 (int)((35 * 8) / ((double)1 / 
                                                                   20)));
                            // Ancho de las columnas automatico
                            //sheet.autoSizeColumn(i);
                        }

                        /*
                         * Cargando Datos
                         */
                        colNum = 0;
                        if (operacionesSVIRegistradas.get(hoja).getArchivoabonoplanillanoprocesados() != 
                            null) {
                            for (int data = 0; 
                                 data < operacionesSVIRegistradas.get(hoja).getArchivoabonoplanillanoprocesados().size(); 
                                 data++) {

                                rowNum++;
                                ArchivoAbonoDTO archivoAbonoDTO = 
                                    operacionesSVIRegistradas.get(hoja).getArchivoabonoplanillanoprocesados().get(data);
                                //Crea fila de datos
                                row = sheet.createRow(rowNum);
                                // Fila1
                                if (archivoAbonoDTO.getId_folio_abono() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getId_folio_abono().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila2
                                if (archivoAbonoDTO.getRutEmpleador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getRutEmpleador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila3
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getAntecedentes() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getAntecedentes().getRazon_social_pagador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getAntecedentes().getRazon_social_pagador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila4
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getFecha_hora_operacion() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(archivoAbonoDTO.getPlanilla().getFecha_hora_operacion(), 
                                                                                                                                  "dd/MM/yyyy HH:hh:ss")));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila5
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getRutTrabajador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getRutTrabajador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila6
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getNombreTrabajador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getNombreTrabajador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila7
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila8
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi_b() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi_b().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila9
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getDeposito_convenio() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getDeposito_convenio().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila10
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getTotal_a_pagar() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getTotal_a_pagar().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }

                                // Fila11
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getPeriodo_pago() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getPeriodo_pago(), 
                                                                                                                                  "MM/yyyy")));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila12
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago_descriocion() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago_descriocion()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila13
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago_exception() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago_exception()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }


                                // Generacion de estilos a llas columnas de datos
                                colNum = 0;
                                for (int i = colNum; i < columnMaxRecAprob; 
                                     i++) {
                                    row.getCell(i).setCellStyle(estiloNormal);
                                }
                            }
                        }


                        /*
                         ******************************************************************
                         * Termino Tabla de No Procesados
                         ******************************************************************
                         */
                    }
                }


                /*
                 * *****************************************************************
                 * Inicio Tabla No Registrados
                 * *****************************************************************
                 */

                HSSFSheet sheet = wb.createSheet("Registros No Informados");
                int rowNum = 0;
                int colNum = 0;


                int columnMaxRecAprob = 13; //Cantidad maxima de columna
                /*
                  * Creacion Cabecera titulo
                  */
                // Creacion primera fila de la tabla correspondiente al titulo
                HSSFRow row = sheet.createRow(rowNum);
                //Titulo de Encabezado         
                row.createCell(colNum).setCellValue(new HSSFRichTextString("Registros No Informados - Previred"));
                for (int i = colNum + 1; i < columnMaxRecAprob; i++) {
                    row.createCell(i);
                }
                // Combinar celdas
                sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 
                                                           12));

                // Asignar estilo a las celdas creadas
                for (int i = colNum; i < columnMaxRecAprob; i++) {
                    row.getCell(i).setCellStyle(estiloCabecera);
                }
                /*
                  * Creacion Cabecera de columnas
                  */
                rowNum++; // Fila siguiente
                colNum = 0; //Columna inicial
                //Encabezado de la tabla
                row = sheet.createRow(rowNum);
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero de Folio"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Rut Empleador"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Raz�n Social del Empleador"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha y Hora Operaci�n"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("RUT Trabajador"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Nombre Trabajador"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Cotizaci�n APV - A"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Cotizaci�n APV - B"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Dep�sito Convenido"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto Pagado $"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Per�odo de Cotizaci�n"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Estado del Proceso"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Observaciones del Proceso"));


                //Estilo encabezado de la tabla
                colNum = 0;
                for (int i = colNum; i < columnMaxRecAprob; i++) {
                    row.getCell(i).setCellStyle(estiloCabecera);
                    // Ancho de las columnas fijo
                    sheet.setColumnWidth(i, 
                                         (int)((35 * 8) / ((double)1 / 20)));
                    // Ancho de las columnas automatico
                    //sheet.autoSizeColumn(i);
                }

                /*
                   * Cargando Datos
                   */
                colNum = 0;
                if (operacionesSVINoRegistradas != null) {
                    for (int hoja = 0; 
                         hoja < operacionesSVINoRegistradas.size(); hoja++) {
                        if (operacionesSVINoRegistradas.get(hoja).getArchivoabono() != 
                            null) {
                            for (int data = 0; 
                                 data < operacionesSVINoRegistradas.get(hoja).getArchivoabono().size(); 
                                 data++) {

                                rowNum++;
                                ArchivoAbonoDTO archivoAbonoDTO = 
                                    operacionesSVINoRegistradas.get(hoja).getArchivoabono().get(data);
                                //Crea fila de datos
                                row = sheet.createRow(rowNum);
                                // Fila1
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getId_folio_planilla() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getId_folio_planilla().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila2
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getRutPagador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getRutPagador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila3
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getAntecedentes() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getAntecedentes().getRazon_social_pagador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getAntecedentes().getRazon_social_pagador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila4
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getFecha_hora_operacion() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(archivoAbonoDTO.getPlanilla().getFecha_hora_operacion(), 
                                                                                                                                  "dd/MM/yyyy HH:hh:ss")));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila5
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getRutTrabajador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getRutTrabajador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila6
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getNombreTrabajador() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getNombreTrabajador()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila7
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila8
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi_b() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getCotizacion_voluntaria_apvi_b().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila9
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getDeposito_convenio() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getDeposito_convenio().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila10
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getTotal_a_pagar() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                                 archivoAbonoDTO.getPlanilla().getDetalleIndividual().getTotal_a_pagar().longValue()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }

                                // Fila11
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getPeriodo_pago() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getPeriodo_pago(), 
                                                                                                                                  "MM/yyyy")));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila12
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago_descriocion() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago_descriocion()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }
                                // Fila13
                                if (archivoAbonoDTO.getPlanilla() != null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual() != 
                                    null && 
                                    archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago_exception() != 
                                    null) {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago_exception()));
                                } else {
                                    row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                                }


                                // Generacion de estilos a llas columnas de datos
                                colNum = 0;
                                for (int i = colNum; i < columnMaxRecAprob; 
                                     i++) {
                                    row.getCell(i).setCellStyle(estiloNormal);
                                }
                            }
                        }
                    }
                }


                /*
                 * *****************************************************************
                 * Termino Tabla No Registrados
                 * *****************************************************************
                 */
            }

        } catch (Exception e) {
            logger.error("generateExcelRecaudacionOperaciones() - Se produjo una excepcion: " + 
                         e.getMessage(), e);
            wb = null;
        }

        logger.debug("generateExcelRecaudacionOperaciones() - Termino");
        return wb;

    }


    public static HSSFWorkbook generateExcelResumenRecaudacion(List<ResumenRendicionesVwDTO> lstResumenRendiciones) {

        logger.debug("generateExcelRecaudacionOperaciones() - Inicio");
        HSSFWorkbook wb = null;

        try {
            if (lstResumenRendiciones == null) {
                wb = null;
            } else {
                // Crea Libro
                wb = new HSSFWorkbook();

                /*
                 * Definici�n Fuentes
                 */
                // fuente titulo 
                HSSFFont fuenteTitulo = wb.createFont();
                fuenteTitulo.setColor(HSSFColor.DARK_BLUE.index);
                fuenteTitulo.setFontName("Calibri");
                fuenteTitulo.setFontHeightInPoints((short)10);
                fuenteTitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                // fuente normal
                HSSFFont fuenteNormal = wb.createFont();
                fuenteNormal.setColor(HSSFColor.DARK_BLUE.index);
                fuenteNormal.setFontName("Calibri");
                fuenteNormal.setFontHeightInPoints((short)10);
                fuenteNormal.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);

                /*
                 * Definici�n estilo celdas
                 */
                //Estilo Celda titulo 
                HSSFCellStyle estiloCabecera = wb.createCellStyle();
                estiloCabecera.setFont(fuenteTitulo);
                estiloCabecera.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                estiloCabecera.setDataFormat((short)0);
                estiloCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                estiloCabecera.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                //estiloCabecera.setFillBackgroundColor(HSSFColor.LIGHT_BLUE.index);
                estiloCabecera.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBorderRight(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBorderTop(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBottomBorderColor(HSSFColor.DARK_BLUE.index);

                //Estilo celda normal
                HSSFCellStyle estiloNormal = wb.createCellStyle();
                estiloNormal.setFont(fuenteNormal);
                estiloNormal.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                estiloNormal.setDataFormat((short)0);
                estiloNormal.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                estiloNormal.setFillForegroundColor(HSSFColor.WHITE.index);
                // estiloNormal.setFillBackgroundColor(HSSFColor.WHITE.index);
                estiloNormal.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBorderRight(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBorderTop(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBottomBorderColor(HSSFColor.DARK_BLUE.index);


                for (int hoja = 0; hoja < lstResumenRendiciones.size(); 
                     hoja++) {
                    int rowNum = 0;
                    int colNum = 0;
                    // Crea nueva hoja
                    HSSFSheet sheet = 
                        wb.createSheet(lstResumenRendiciones.get(hoja).getGlosa_recaudador() + " - " + hoja);

                    /*
                     * *****************************************************************
                     * Inicio Tabla de Resumen
                     * *****************************************************************
                     */
                    int columnMaxResumen = 7; //Cantidad maxima de columna

                    /*
                     * Creacion Cabecera titulo
                     */
                    // Creacion primera fila de la tabla correspondiente al titulo
                    HSSFRow row = sheet.createRow(rowNum);
                    //Titulo de Encabezado         
                    row.createCell(colNum).setCellValue(new HSSFRichTextString("Recaudaci�n - Operaciones SVI - Resumen Rendiciones - " + 
                                                                               lstResumenRendiciones.get(hoja).getGlosa_recaudador()));
                    for (int i = colNum + 1; i < columnMaxResumen; i++) {
                        row.createCell(i);
                    }

                    // Combinar celdas
                    sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 6));

                    // Asignar estilo a las celdas creadas
                    for (int i = colNum; i < columnMaxResumen; i++) {
                        row.getCell(i).setCellStyle(estiloCabecera);
                    }

                    /*
                     * Creacion Cabecera de columnas
                     */
                    rowNum++; // Fila siguiente
                    colNum = 0; //Columna inicial
                    //Encabezado de la tabla
                    row = sheet.createRow(rowNum);
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Medio de Pago"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero Total de Registros"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto Total Abonado $"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto Total Aplicado $"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto Total No Aplicado $"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha del Abono"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Nombre del Archivo"));


                    //Estilo encabezado de la tabla
                    colNum = 0;
                    for (int i = colNum; i < columnMaxResumen; i++) {
                        row.getCell(i).setCellStyle(estiloCabecera);
                        // Ancho de las columnas fijo
                        sheet.setColumnWidth(i, 
                                             (int)((35 * 8) / ((double)1 / 20)));
                        // Ancho de las columnas automatico
                        //sheet.autoSizeColumn(i);
                    }

                    /*
                     * Cargando Datos
                     */
                    colNum = 0;
                    rowNum++; // Fila siguiente

                    ResumenRendicionesVwDTO resumenRendicionesVwDTO = 
                        lstResumenRendiciones.get(hoja);

                    row = sheet.createRow(rowNum);
                    if (resumenRendicionesVwDTO.getGlosa_recaudador() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Previred - " + 
                                                                                     resumenRendicionesVwDTO.getGlosa_recaudador()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getNumero_de_registros() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                     resumenRendicionesVwDTO.getNumero_de_registros()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getMonto_total_abonado() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(resumenRendicionesVwDTO.getMonto_total_abonado())));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getMonto_aplicado() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(resumenRendicionesVwDTO.getMonto_aplicado())));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getMonto_no_aplicado() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(resumenRendicionesVwDTO.getMonto_no_aplicado())));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getFecha_abono() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenRendicionesVwDTO.getFecha_abono(), 
                                                                                                                      "dd/MM/yyyy")));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }
                    
                    if (resumenRendicionesVwDTO.getNombreFisicoArchivo() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenRendicionesVwDTO.getNombreFisicoArchivo()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                 
                    // Generacion de estilos a llas columnas de datos
                    colNum = 0;
                    for (int i = colNum; i < columnMaxResumen; i++) {
                        row.getCell(i).setCellStyle(estiloNormal);
                    }
                    /*
                     * *****************************************************************
                     * Fin Tabla de Resumen
                     * *****************************************************************
                     */
                    /*
                     * Genera espacio
                     */
                    rowNum++;
                    sheet.createRow(rowNum);
                    rowNum++;
                    /*
                      * *****************************************************************
                      * Inicio Tabla de Detalle Resumen
                      * *****************************************************************
                      */
                    colNum = 0;
                    rowNum++; // Fila siguiente

                    int columnMaxDetalleResumen =  14; //Cantidad maxima de columna

                    /*
                       * Creacion Cabecera titulo
                       */
                    // Creacion primera fila de la tabla correspondiente al titulo
                    row = sheet.createRow(rowNum);
                    //Titulo de Encabezado         
                    row.createCell(colNum).setCellValue(new HSSFRichTextString("Recaudaci�n - Operaciones SVI - Detalle Resumen Rendiciones - " + 
                                                                               lstResumenRendiciones.get(hoja).getGlosa_recaudador()));
                    for (int i = colNum + 1; i < columnMaxDetalleResumen; 
                         i++) {
                        row.createCell(i);
                    }

                    // Combinar celdas
                    sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 13));

                    // Asignar estilo a las celdas creadas
                    for (int i = colNum; i < columnMaxDetalleResumen; i++) {
                        row.getCell(i).setCellStyle(estiloCabecera);
                    }

                    /*
                       * Creacion Cabecera de columnas
                       */
                    rowNum++; // Fila siguiente
                    colNum = 0; //Columna inicial
                    //Encabezado de la tabla
                    row = sheet.createRow(rowNum);
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero de Folio"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("RUT Empleador"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("RUT Trabajador"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha del Pago"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha del Abono"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha Ejecuci�n Rendici�n"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha Aplicaci�n Pago Caja"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto Aplicado Caja $"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto No Aplicado"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Periodo Cotizaci�n"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Concepto Ingreso a Sistema Caja"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("P�liza Asociada al Pago"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero de Folio Caja"));
                    row.createCell(colNum++).setCellValue(new HSSFRichTextString("Estado del Proceso"));
           

                    //Estilo encabezado de la tabla
                    colNum = 0;
                    for (int i = colNum; i < columnMaxDetalleResumen; i++) {
                        row.getCell(i).setCellStyle(estiloCabecera);
                        // Ancho de las columnas fijo
                        sheet.setColumnWidth(i, (int)((35 * 8) / ((double)1 / 20)));
                        // Ancho de las columnas automatico
                        //sheet.autoSizeColumn(i);
                    }

                    /*
                    * Cargando Datos
                    */
                    for (int data = 0; 
                         data < resumenRendicionesVwDTO.getDetalle_resumen_rendiciones().size(); 
                         data++) {

                        ResumenRendicionesVwDTO resumenDetalleRendicionesVwDTO = resumenRendicionesVwDTO.getDetalle_resumen_rendiciones().get(data);

                        colNum = 0;
                        rowNum++; // Fila siguiente

                        row = sheet.createRow(rowNum);
                        if (resumenDetalleRendicionesVwDTO.getId_folio_abono() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                         resumenDetalleRendicionesVwDTO.getId_folio_abono().longValue()));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getRut_empleador() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenDetalleRendicionesVwDTO.getRut_empleador()));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getRut_trabajador() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenDetalleRendicionesVwDTO.getRut_trabajador()));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getFecha_pago() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenDetalleRendicionesVwDTO.getFecha_pago(), 
                                                                                                                          "dd/MM/yyyy")));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getFecha_abono() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenDetalleRendicionesVwDTO.getFecha_abono(), 
                                                                                                                          "dd/MM/yyyy")));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getInicio_proceso() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenDetalleRendicionesVwDTO.getInicio_proceso(), 
                                                                                                                          "dd/MM/yyyy HH:mm:ss")));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getFecha_pago_caja() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenDetalleRendicionesVwDTO.getFecha_pago_caja(), 
                                                                                                                          "dd/MM/yyyy HH:mm:ss")));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getMonto_aplicado() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(resumenDetalleRendicionesVwDTO.getMonto_aplicado())));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getMonto_no_aplicado() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(resumenDetalleRendicionesVwDTO.getMonto_no_aplicado())));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getPeriodo_pago() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenDetalleRendicionesVwDTO.getPeriodo_pago(), 
                                                                                                                          "MM/yyyy")));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getConcepto_pago() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenDetalleRendicionesVwDTO.getConcepto_pago()));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getPoliza_asoc() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                         resumenDetalleRendicionesVwDTO.getPoliza_asoc()));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }
                        
                        if (resumenDetalleRendicionesVwDTO.getId_folio_pagocaja() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                         resumenDetalleRendicionesVwDTO.getId_folio_pagocaja().longValue()));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }

                        if (resumenDetalleRendicionesVwDTO.getDescripcion_proceso_planilla() != 
                            null) {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenDetalleRendicionesVwDTO.getDescripcion_proceso_planilla()));
                        } else {
                            row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                        }
                        

                        // Generacion de estilos a llas columnas de datos
                        colNum = 0;
                        for (int i = colNum; i < columnMaxDetalleResumen; 
                             i++) {
                            row.getCell(i).setCellStyle(estiloNormal);
                        }

                    }


                    /*
                     * *****************************************************************
                     * Fin Tabla de Detalle Resumen
                     * *****************************************************************
                     */

                }
            }

        } catch (Exception e) {
            logger.error("generateExcelRecaudacionOperaciones() - Se produjo una excepcion: " + 
                         e.getMessage(), e);
            wb = null;
        }

        logger.debug("generateExcelRecaudacionOperaciones() - Termino");
        return wb;

    }

    public static HSSFWorkbook generateExcelConsultaRecaudacion(List<ResumenRendicionesVwDTO> lstResumenRendiciones) {

        logger.debug("generateExcelRecaudacionOperaciones() - Inicio");
        HSSFWorkbook wb = null;

        try {
            if (lstResumenRendiciones == null) {
                wb = null;
            } else {
                // Crea Libro
                wb = new HSSFWorkbook();

                /*
                 * Definici�n Fuentes
                 */
                // fuente titulo 
                HSSFFont fuenteTitulo = wb.createFont();
                fuenteTitulo.setColor(HSSFColor.DARK_BLUE.index);
                fuenteTitulo.setFontName("Calibri");
                fuenteTitulo.setFontHeightInPoints((short)10);
                fuenteTitulo.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
                // fuente normal
                HSSFFont fuenteNormal = wb.createFont();
                fuenteNormal.setColor(HSSFColor.DARK_BLUE.index);
                fuenteNormal.setFontName("Calibri");
                fuenteNormal.setFontHeightInPoints((short)10);
                fuenteNormal.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);

                /*
                 * Definici�n estilo celdas
                 */
                //Estilo Celda titulo 
                HSSFCellStyle estiloCabecera = wb.createCellStyle();
                estiloCabecera.setFont(fuenteTitulo);
                estiloCabecera.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                estiloCabecera.setDataFormat((short)0);
                estiloCabecera.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                estiloCabecera.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                //estiloCabecera.setFillBackgroundColor(HSSFColor.LIGHT_BLUE.index);
                estiloCabecera.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBorderRight(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBorderTop(HSSFCellStyle.BORDER_THIN);
                estiloCabecera.setBottomBorderColor(HSSFColor.DARK_BLUE.index);

                //Estilo celda normal
                HSSFCellStyle estiloNormal = wb.createCellStyle();
                estiloNormal.setFont(fuenteNormal);
                estiloNormal.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                estiloNormal.setDataFormat((short)0);
                estiloNormal.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                estiloNormal.setFillForegroundColor(HSSFColor.WHITE.index);
                // estiloNormal.setFillBackgroundColor(HSSFColor.WHITE.index);
                estiloNormal.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBorderRight(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBorderTop(HSSFCellStyle.BORDER_THIN);
                estiloNormal.setBottomBorderColor(HSSFColor.DARK_BLUE.index);


                HSSFSheet sheet = wb.createSheet("Consulta Datos");
                /*
                 * *****************************************************************
                 * Inicio Tabla de Consulta
                 * *****************************************************************
                 */
                int rowNum = 0;
                int colNum = 0;
                int columnMaxResumen = 18; //Cantidad maxima de columna


                /*
                  * Creacion Cabecera titulo
                  */
                // Creacion primera fila de la tabla correspondiente al titulo
                HSSFRow row = sheet.createRow(rowNum);
                //Titulo de Encabezado         
                row.createCell(colNum).setCellValue(new HSSFRichTextString("Recaudaci�n - Operaciones SVI - Consulta Datos"));
                for (int i = colNum + 1; i < columnMaxResumen; i++) {
                    row.createCell(i);
                }

                // Combinar celdas
                sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, 
                                                           15));

                // Asignar estilo a las celdas creadas
                for (int i = colNum; i < columnMaxResumen; i++) {
                    row.getCell(i).setCellStyle(estiloCabecera);
                }

                /*
                  * Creacion Cabecera de columnas
                  */
                rowNum++; // Fila siguiente
                colNum = 0; //Columna inicial
                //Encabezado de la tabla
                row = sheet.createRow(rowNum);
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Nombre Medio Pago"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero de Folio"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Rut Empleador"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Rut Trabajador"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha del Pago"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha del Abono"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha Ejecuci�n Rendici�n"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Fecha Aplicaci�n Pago Caja"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Cotizaci�n APV - A"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Cotizaci�n APV - B"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto Aplicado Caja"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Monto No Aplicado"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Periodo Cotizaci�n"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Concepto Ingreso a Sistema Caja"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Poliza Asociada al Pago"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("N�mero de Folio Caja"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Estado del Proceso"));
                row.createCell(colNum++).setCellValue(new HSSFRichTextString("Nombre del Archivo"));


                //Estilo encabezado de la tabla
                colNum = 0;
                for (int i = colNum; i < columnMaxResumen; i++) {
                    row.getCell(i).setCellStyle(estiloCabecera);
                    // Ancho de las columnas fijo
                    sheet.setColumnWidth(i, 
                                         (int)((35 * 8) / ((double)1 / 20)));
                    // Ancho de las columnas automatico
                    //sheet.autoSizeColumn(i);
                }

                /*
                 * Cargando Datos
                 */
                for (int data = 0; data < lstResumenRendiciones.size(); 
                     data++) {
                    ResumenRendicionesVwDTO resumenRendicionesVwDTO = 
                        lstResumenRendiciones.get(data);
                    colNum = 0;
                    rowNum++; // Fila siguiente

                    row = sheet.createRow(rowNum);
                    if (resumenRendicionesVwDTO.getGlosa_recaudador() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("Previred - " + 
                                                                                     resumenRendicionesVwDTO.getGlosa_recaudador()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getId_folio_abono() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                     resumenRendicionesVwDTO.getId_folio_abono().longValue()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getRut_empleador() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenRendicionesVwDTO.getRut_empleador()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getRut_trabajador() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenRendicionesVwDTO.getRut_trabajador()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getFecha_pago() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenRendicionesVwDTO.getFecha_pago(), 
                                                                                                                      "dd/MM/yyyy")));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getFecha_abono() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenRendicionesVwDTO.getFecha_abono(), 
                                                                                                                      "dd/MM/yyyy")));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getInicio_proceso() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenRendicionesVwDTO.getInicio_proceso(), 
                                                                                                                      "dd/MM/yyyy HH:mm:ss")));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getFecha_pago_caja() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenRendicionesVwDTO.getFecha_pago_caja(), 
                                                                                                                      "dd/MM/yyyy HH:mm:ss")));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }


                    if (resumenRendicionesVwDTO.getTotal_cotiza_voluntaria_apvi() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(resumenRendicionesVwDTO.getTotal_cotiza_voluntaria_apvi())));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getTotal_cotiza_voluntaria_apvi_b() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(resumenRendicionesVwDTO.getTotal_cotiza_voluntaria_apvi_b())));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getMonto_aplicado() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(resumenRendicionesVwDTO.getMonto_aplicado())));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getMonto_no_aplicado() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(NumeroUtil.formatMilesNoDecimal(resumenRendicionesVwDTO.getMonto_no_aplicado())));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getPeriodo_pago() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(FechaUtil.getFechaFormateoCustom(resumenRendicionesVwDTO.getPeriodo_pago(), 
                                                                                                                      "MM/yyyy")));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getConcepto_pago() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenRendicionesVwDTO.getConcepto_pago()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getPoliza_asoc() != null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("" + 
                                                                                     resumenRendicionesVwDTO.getPoliza_asoc()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }
                    
                    if (resumenRendicionesVwDTO.getId_folio_pagocaja() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""+resumenRendicionesVwDTO.getId_folio_pagocaja().longValue()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }

                    if (resumenRendicionesVwDTO.getDescripcion_proceso_planilla() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenRendicionesVwDTO.getDescripcion_proceso_planilla()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(""));
                    }
                    
                    if (resumenRendicionesVwDTO.getNombreFisicoArchivo() != 
                        null) {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString(resumenRendicionesVwDTO.getNombreFisicoArchivo()));
                    } else {
                        row.createCell(colNum++).setCellValue(new HSSFRichTextString("----"));
                    }

                    // Generacion de estilos a llas columnas de datos
                    colNum = 0;
                    for (int i = colNum; i < columnMaxResumen; i++) {
                        row.getCell(i).setCellStyle(estiloNormal);
                    }

                }
                
                /*
                 * *****************************************************************
                 * Fin Tabla de Consulta
                 * *****************************************************************
                 */
            }

        } catch (Exception e) {
            logger.error("generateExcelRecaudacionOperaciones() - Se produjo una excepcion: " + 
                         e.getMessage(), e);
            wb = null;
        }

        logger.debug("generateExcelRecaudacionOperaciones() - Termino");
        return wb;

    }

  public static void main(String[] args) {
        try {
            List<ResumenAbonoDTO> operacionesSVI = 
                new ArrayList<ResumenAbonoDTO>();

            // Save
            FileOutputStream out = 
                new FileOutputStream("C:\\WSLongName\\Previred\\test\\workbook.xls");
            ExcelUtil.generateExcelRecaudacionOperaciones(operacionesSVI, 
                                                          operacionesSVI).write(out);
            out.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}


