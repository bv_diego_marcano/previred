package cl.bicevida.previred.model;

import cl.bicevida.previred.common.dto.ArchivoPlanillaEncabezadoDTO;

import java.io.Serializable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class PreviredEJBClient implements Serializable {
    @SuppressWarnings("compatibility:8715949491948168165")
    private static final long serialVersionUID = 1L;

    public static void main(String [] args) {
        try {
            final Context context = getInitialContext();
            PreviredEJB previredEJB = (PreviredEJB)context.lookup("PreviredEJB");
            //previredEJB.getCargaArchivosByIdCarga(new Double("1"));
            //System.out.println(previredEJB.getCargaArchivosByIdCarga(new Double("1")));
            // Call any of the Remote methods below to access the EJB
            // previredEJB.getCargaArchivosByIdCarga(  idCarga );
            //ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO = new ArchivoPlanillaEncabezadoDTO();
            //previredEJB.callGeneraTransaccion(archivoPlanillaEncabezadoDTO, false);
            //System.out.println(previredEJB.callAperturaTurnoCaja());
            //System.out.println(previredEJB.callCierreTurnoCaja());
            //System.out.println(previredEJB.callAperturaTurnoCaja());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static Context getInitialContext() throws NamingException {
        // Get InitialContext for Embedded OC4J
        // The embedded server must be running for lookups to succeed.
        return new InitialContext();
    }
}
