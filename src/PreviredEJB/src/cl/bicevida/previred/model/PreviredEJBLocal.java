package cl.bicevida.previred.model;

import cl.bicevida.previred.common.dto.InformarRecaudacionIn;

import cl.bicevida.previred.common.dto.ArchivoAbonoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaAntecedentesDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaDetalleDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaEncabezadoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaHeaderDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaResumenDTO;
import cl.bicevida.previred.common.dto.CargaArchivosDTO;

import cl.bicevida.previred.common.dto.ConceptosDePagoDTO;
import cl.bicevida.previred.common.dto.ConvenioPagoDTO;
import cl.bicevida.previred.common.dto.EmpresasDTO;
import cl.bicevida.previred.common.dto.EstadoDeProcesoDTO;
import cl.bicevida.previred.common.dto.MediosDePagoDTO;
import cl.bicevida.previred.common.dto.MotivoRechazosDTO;
import cl.bicevida.previred.common.dto.PKTransaccionPagoDTO;
import cl.bicevida.previred.common.dto.RecaudadoresDTO;
import cl.bicevida.previred.common.dto.RendProcesoAutomatico;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;

import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;
import cl.bicevida.previred.common.dto.TipoArchivoDTO;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Local;

import javax.naming.NamingException;

@Local
public interface PreviredEJBLocal {
    public CargaArchivosDTO getCargaArchivosByIdCarga(Double idCarga);
    public boolean updateCargaArchivosInicioProceso(Double idCarga);
    public boolean updateCargaArchivosTerminoProceso(Double idCarga);
    public boolean updateCargaArchivosEstadoProceso(Double idCarga, Integer estadoProceso, String observacion, Double idCargaAsoc);
    public boolean updateCargaArchivosCantidadProcesada(Double idCarga, Integer cantidadProcesada);
    public ResumenAbonoDTO insertResumenAbono(ResumenAbonoDTO resumenAbonoDTO, Double idCarga);
    public ResumenAbonoDTO insertDetalleAbono(ResumenAbonoDTO resumenDTO, Double idCarga, Integer inicio, Integer procesar,Integer tipoProceso) throws Exception;
    public CargaArchivosDTO insertCargaArchivos(CargaArchivosDTO cargaArchivosDTO);
    public boolean deleteDetalleAbono(Double idCarga);
    public boolean deleteResumenAbono(Double idCarga);
    public ArchivoPlanillaHeaderDTO insertArchivoPlanilla(ArchivoPlanillaHeaderDTO headerDTO, Double idCarga, Integer inicio, Integer procesar);
    public ArchivoPlanillaAntecedentesDTO insertAntecedentesPlanilla(ArchivoPlanillaAntecedentesDTO antecedentesDTO, Double idCarga, Double id_folio_planilla);
    public List<ArchivoPlanillaDetalleDTO> insertDetallePlanilla(List<ArchivoPlanillaDetalleDTO> detalleDTO, Double idCarga, Double id_folio_planilla);
    public ArchivoPlanillaResumenDTO insertResumenPlanilla(ArchivoPlanillaResumenDTO resumenDTO, Double idCarga, Double id_folio_planilla);
    public boolean deletePlanillaAbono(Double idCarga);
    public boolean deleteAntecedentesPlanilla(Double idCarga);
    public boolean deleteDetallePlanilla(Double idCarga);
    public boolean deleteResumenPlanilla(Double idCarga);
    public boolean deleteEncabezadoPlanilla(Double idCarga);
    public List<EmpresasDTO> getEmpresas();
    public List<MediosDePagoDTO> getMediosDePago(String tipopl);
    public List<TipoArchivoDTO> getTipoArchivo(Integer codigo);
    public ConvenioPagoDTO getConvenioDePago(Integer idEmpresa, Integer idMedioPago);
    public boolean updateEstadoPorcesoFolioResumenByIdCarga(Double idCarga, Integer oldEstadoProceso, Integer estadoProceso, String estadoProcesoObs);
    public List<ResumenAbonoDTO> getAbonoByIdCarga(Double idCarga);
    public List<MotivoRechazosDTO> getMotivoRechazos();
    public boolean updateEstadoPorcesoFolioResumenByIdCargaBanco(Double idCarga, String codRecaudador, Integer estadoProceso, String estadoProcesoObs);
    public List<ResumenAbonoDTO> getAbonoAndPlanillaByIdCarga(Double idCargaAbono, Double idCargaPlanilla);
    public List<ResumenAbonoDTO> getPlanillaNoInformadosByIdCarga(Double idCargaPlanilla, Double idCargaAbono);
    public HashMap<Integer, RecaudadoresDTO> getRecaudadores();
    public List<EstadoDeProcesoDTO> getEstadoDeProceso();
    public HashMap<Integer, ConceptosDePagoDTO> getConceptosDePago();
    public List<ResumenRendicionesVwDTO> getResumenRendiciones(Date fechaDesde, Date fechaHasta, Double folio, Integer rutEmpleador, Integer rutTrabajador, String conceptoPago, String idMedioPago, Integer idEstado, String tipopl);
    public List<ResumenRendicionesVwDTO> getConsultaRendiciones(Date fechaDesde, Date fechaHasta, Double folio, Integer rutEmpleador, Integer rutTrabajador, String conceptoPago, String idMedioPago, Integer idEstado, String tipopl);
    public ArchivoPlanillaEncabezadoDTO callGeneraTransaccion(ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO, Boolean isClient, String tipoMedioPlanilla, Integer medioPago);
    public boolean updateResumenYDetalleAbonoByIdCargaResumen(Double idCarga, ResumenAbonoDTO resumenAbonoDTO);
    public EstadoDeProcesoDTO getEstadoDeProcesoByIdProceso(Integer idProceso);
    public ArchivoPlanillaHeaderDTO insertArchivoPlanillaProceso(Long idTurno, java.util.Date fechaturno, Long foliocajaDiario,ArchivoPlanillaHeaderDTO headerDTO, Double idCarga, Integer inicio, Integer procesar, String tipoMedioPlanilla);
    public boolean updateFolioAbonoConDatosNoProcesados(Double idCarga, Integer estadoProceso, String observacion);
    public List<ResumenRendicionesVwDTO> getReprocesosManualPagos(String tipoRend);
    public void changeStatusTransaccionProccess(List<PKTransaccionPagoDTO> idTransacciones);
    public Long getFolioCajaByTurno(java.util.Date fechaTurno);
    public java.util.Date getFechaHoraTurno();
    
    
    /**
     * Apertura de turno
     * @param rendPago
     * @return
     */
    public Long callNewAperturaTurnoCaja(String rendPago) ;
    
    /**
     * Cierre de turno caja
     * @param rendPago
     * @param idTurno
     * @return
     */
    public String callNewCierreTurnoCaja(String rendPago, Long idTurno);
    
    /**
    * Retorna la fecha y hora del turno con el cual
    * se aperturo la transaccion
    * @param idturno
    * @return
    */
    public java.util.Date getNewFechaHoraTurno(Long idturno);
    
    
    /**
    * Recupera el Folio caja con el cual
    * se aperturo el turno
    * @param turno
    * @return
    */
    public Long getNewFolioCajaByTurno(java.util.Date turno);
    
    
    /**
     * Recupera pagos que deben ser reprocesados por periodo
     * @param tipoRend tipo de producto a consultar
     * @param periodoMes mes del periodo de pago imposiciones
     * @param periodoAnio anio del periodo de pago imposiciones
     * @return lista de pagos segun tipo.
     */
    public List<ResumenRendicionesVwDTO> getReprocesosManualPagosByPeriodo(String tipoRend, Integer periodoMes, Integer periodoAnio);
    
    /**
     * Inicia el reprocesamiento en sistema de Cajas
     * @param List de transacciones a reprocesar
     */ 
    public Boolean doReprocesarJMSAsync(List<ResumenRendicionesVwDTO> transacciones);
    
    /**
     * Busca la fecha y hora del turno
     * @param idturno
     * @return
     */
    public java.util.Date getFechaHoraTurno(Long idturno);
       
    
    /**
     * Distribucion de Montos en Caja
     * @param usuarioCaja
     * @param fechaTurno
     * @param numeroFolioCajaDiario
     * @param numeroFolioPago
     * @param empresa
     * @return
     */
    public Integer callDistribuirMontoCaja(String usuarioCaja,
                                           java.util.Date fechaTurno,
                                           Long numeroFolioCajaDiario,
                                           Long numeroFolioPago,
                                           Integer empresa);
    
     /**
      * Funcion que ejecuta la distribucion en caja y ejecuta el concurrente de recaudacion AR
      *
      * @param irIn
      * @throws Exception
      */
    public void ejecutarDistribucionEnCajayConcurrente(InformarRecaudacionIn irIn) throws Exception;
    
      /**
       * ejecuta la funcion de distribucion en caja
       *
       * @param irIn
       * @return
       * @throws NamingException
       * @throws Exception
       */
     public int ejecutarDistribucionEnCaja(InformarRecaudacionIn irIn) throws NamingException, Exception;
     
      /**
       * ejecuta concurrente de recaudacion
       *
       * @param irIn
       * @return
       * @throws NamingException
       * @throws Exception
       */
     public int ejecutarConcurenteRecaudacion(InformarRecaudacionIn irIn) throws NamingException, Exception;
      
    boolean existProcesoEnEjecucion(Integer estado)throws NamingException, Exception;
        
        /**
         *guarda proceso de ejecucion
         * @param proceso
         * @throws SQLException
         */
        void guardarProcesoRendicion(RendProcesoAutomatico proceso)throws NamingException, Exception;
        
        /**
         * Actualiza estado y descripcion del proceso en ejecucion
         * @param estado
         * @param descripcionEstado
         * @param idCuadratura
         * @throws SQLException
         */
        void actualizaProcesoRendicion(Integer estado, String descripcionEstado,Long idCuadratura,String observacion)throws NamingException, Exception;
        
        /**
         *  Verifica si existe un archivo procesado.
         * @param nombreArchivo
         * @param estado
         * @return
         * @throws SQLException
         */
        boolean existArchivoProcesado(String nombreArchivo,Integer estado)throws NamingException, Exception;
        
        
        /**
         * Metodo que retorna la fecha valida
         * @param fecha
         * @return
         * @throws NamingException
         * @throws Exception
         */
        Date retornaDiaHabil(Date fecha)throws NamingException, Exception;
    Integer existeArchivoCargado(String nombreArchivo)throws NamingException, Exception;
    boolean validaDiaHabil(Date fecha)throws NamingException, Exception;
    public boolean deleteProcesoAutomatico(Double idCarga)throws NamingException, Exception;
    public boolean deleteCargaArchivo(Double idCarga)throws NamingException, Exception;

}
