package cl.bicevida.previred.model;

import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.common.dto.ArchivoPlanillaAntecedentesDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaDetalleDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaEncabezadoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaResumenDTO;
import cl.bicevida.previred.common.dto.InformarRecaudacionIn;
import cl.bicevida.previred.common.dto.PKTransaccionPagoDTO;
import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;

import cl.bicevida.previred.common.utils.RutUtil;

import cl.bicevida.previred.pattern.dao.DAOFactory;
import cl.bicevida.previred.pattern.dao.OracleDAOFactory;
//import cl.bicevida.previred.services.svi.InformarRecaudacion;
//import cl.bicevida.previred.services.svi.InformarRecaudacionIn;
//import cl.bicevida.previred.services.svi.RecaudacionServiceClient;

import cl.bicevida.previred.util.ResourceBundleUtil;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.PreDestroy;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenBean;

import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

@MessageDriven(activationConfig = { 
    @ActivationConfigProperty(propertyName  = "ConnectionFactoryJndiName",propertyValue = "jms/PreviredTopicConnectionFactory"),
    @ActivationConfigProperty(propertyName  = "DestinationName",propertyValue = "jms/PreviredTopic"),
    @ActivationConfigProperty(propertyName  = "DestinationType",propertyValue = "javax.jms.Topic") }, 
    name="PreviredAsyncMDBEJBBean" , mappedName="jms/PreviredTopic")
@TransactionManagement(TransactionManagementType.CONTAINER)
public class PreviredAsyncMDBEJBBean implements MessageListener, Serializable {
    @SuppressWarnings("compatibility:7412577890486201876")
    private static final long serialVersionUID = 1L;

    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PreviredAsyncMDBEJBBean.class);


      public PreviredAsyncMDBEJBBean() {
          logger.info("PreviredAsyncMDBEJBBean CREADO");
      }
      
      public void onMessage(Message msg) {
          if (msg instanceof ObjectMessage) {
              ObjectMessage tm = (ObjectMessage) msg;
              try {
                 if (tm.getObject() != null && tm.getObject() instanceof ResumenRendicionesVwDTO) {
                     ResumenRendicionesVwDTO dto = (ResumenRendicionesVwDTO) tm.getObject();
                      logger.info("RECIBIENDO REPROCESO ASYNCRONO PREVIRED :" + dto.getRut_empleador() + " " + dto.getRut_trabajador());
                      procesarTransaccion(dto);
                 }
              } catch (JMSException e) {
                  e.printStackTrace();
              }
          }
      }
      
      @PreDestroy
      public void remove() {
          logger.info("PreviredAsyncMDBEJBBean DESTRUIDO.");
      }
      
      
      private void procesarTransaccion(ResumenRendicionesVwDTO dto) {
           Double folioCaja = null;
           PreviredEJBBean ejb = new PreviredEJBBean();
           OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
           if (dto != null) {
             
          
             //===========================================================
             //APERTURA TURNO Y ENVIA TRANSACCIONES   
             //===========================================================
             logger.info("ABRIENDO TURBNO DE CAJA");
             logger.debug("tipoDeTransaaciones" + dto.getTipoTransaccion());
             Long idturno = ejb.callNewAperturaTurnoCaja(dto.getTipoTransaccion());
            
             if (idturno != null && idturno.longValue() > 0) {
                 logger.debug("TURNO DE CAJA OBTENIDO:" + idturno);
                 Long turnoBackup = new Long(""+idturno.longValue());
                 
                //SETEA LA FECHA Y HORA APERTURA TURNO
                
                java.util.Date fechaTurno = daoFactory.getRendicionDAO().getFechaHoraTurno(idturno);
                Long foliocajadiario = daoFactory.getRendicionDAO().getFolioCajaByTurno(fechaTurno);
               
                logger.info("INFO: id turno obtenido: " + idturno);
                logger.info("INFO: fecha de turno caja: " + fechaTurno);
                logger.info("INFO: folio de caja diario: " + foliocajadiario);
               
                 
                 //RECUPERA FECHA DE APERTURA DEL TURNO
                 logger.debug("PROCESANDO EL PAGO..");
                   
                     //logger.debug("Procesando Registro :" + dto.getId_folio_abono());
                     if (dto.getEstado_proceso_planilla() != Constant.PROCESO_EJECUCION_MANUAL_CAJAS) {
                          ArchivoPlanillaEncabezadoDTO exe = new ArchivoPlanillaEncabezadoDTO();
                          ArchivoPlanillaDetalleDTO  dtoDet = new ArchivoPlanillaDetalleDTO();
                          ArchivoPlanillaAntecedentesDTO dtoAn = new ArchivoPlanillaAntecedentesDTO();
                          ArchivoPlanillaResumenDTO dtoRe = new ArchivoPlanillaResumenDTO();
                          
                          dtoDet.setRut_trabajador(Integer.parseInt(dto.getRut_trabajador()));                
                          dtoDet.setDv_trabajador(RutUtil.calculaDv(dtoDet.getRut_trabajador()));
                          dtoDet.setNombreTrabajador(dto.getNombre_trabajador());
                          dtoDet.setTotal_a_pagar(dto.getMonto_no_aplicado());
                          dtoDet.setPeriodo_pago(dto.getPeriodo_pago());
                          dtoDet.setDeposito_convenio(dto.getDeposito_convenio());
                          
                          dtoRe.setTotal_pagar_ia(dto.getMonto_no_aplicado()); //INPUTETEXT            
                          dtoRe.setPeriodo_pago(dto.getPeriodo_pago());             
                          dtoAn.setRazon_social_pagador(dto.getRazon_social_empleador());               
                          exe.setRut_pagador(Integer.parseInt(dto.getRut_empleador()));
                          exe.setDv_pagador(RutUtil.calculaDv(exe.getRut_pagador()));
                          exe.setId_folio_planilla(dto.getId_folio_abono());
                          exe.setFecha_hora_operacion(dto.getFecha_hora_operacion());   
                                             
                          exe.setDetalle(new ArrayList<ArchivoPlanillaDetalleDTO>());
                          exe.getDetalle().add(dtoDet);
                                              
                          exe.setAntecedentes(dtoAn);
                          exe.setResumen(dtoRe);
                          
                          //LLAMA PLSQL CAJA
                          logger.debug("LLAMANDO A PLSQL DE PAGO DE TRANSACCION EN CAJAS");
                          ArchivoPlanillaEncabezadoDTO res = ejb.callGeneraTransaccion(exe,  true, dto.getTipoTransaccion(), null);
                                              
                          List<PKTransaccionPagoDTO> fallo = new ArrayList<PKTransaccionPagoDTO>();
                          PKTransaccionPagoDTO upt = new PKTransaccionPagoDTO();
                          upt.setIdCarga(dto.getId_carga());
                          upt.setIdCargaPlanilla(dto.getId_carga_planilla());
                          upt.setFolioPlanilla(dto.getId_folio_abono());
                          upt.setCodigoRecaudador(dto.getCodigo_recaudador());
                          
                          
                          //Actualiza resultado operacion
                          if (res != null && res.getDetalle().get(0).getId_folio_pagocaja() != 0) {
                              logger.debug("RESULTADO DE LLAMADA A CAJA OK");
                              upt.setNewEstado(Constant.PROCESO_AUTORIZADO);
                              upt.setNewDescripcion("Procesado Correctamente");
                              upt.setFolioCaja(res.getDetalle().get(0).getId_folio_pagocaja().doubleValue());
                              upt.setPoliza(res.getDetalle().get(0).getPolizaAsociada());
                              upt.setConceptoCaja(res.getDetalle().get(0).getConceptoRecaudacion());
                              
                              //FOLIO DE CAJA GENERADO
                              folioCaja = upt.getFolioCaja();
                              
                              //DISTRIBUYO TRANSACCION
                              String usuarioCaja = ResourceBundleUtil.getProperty("integracion.webservice.notificacion.recaudacion.usuariocaja");
                              Integer resultadoDistribucion =daoFactory.getRendicionDAO().callDistribuirMontoCaja(usuarioCaja, fechaTurno, foliocajadiario, folioCaja.longValue(), 1);
                              logger.debug("Resultado de Distribucion transaccion de caja folio:" +folioCaja.longValue()+ " resultado:" + resultadoDistribucion);
                              
                               
                          } else {
                              logger.debug("RESULTADO DE LLAMADA A CAJA CON PROBLEMAS");
                              upt.setNewEstado(Constant.PROCESO_PROBLEMA_PAGO_CAJA);
                              upt.setNewDescripcion("Folio " + dto.getId_folio_abono().longValue() + " No fue procesado correctamente por el proceso de caja: " + res.getDetalle().get(0).getDecsResultPago());                    
                          }
                          
                         fallo.add(upt);
                         logger.debug("ACTUALIZA ESTADO DE TRANSACCION DE PREVIRED");
                         ejb.changeStatusTransaccionProccess(fallo);
                     }
                  
                  //===========================================================
                  //CIERRA EL TURNO DE CAJA
                  //===========================================================
                  logger.debug("CIERRE DE TURNO");
                  ejb.callNewCierreTurnoCaja(dto.getTipoTransaccion(), idturno);
                  
                  
                   //INFORMAR A AR MEDIANTE WS
                    // ============ LLAMA A WEBSERVICE PARA NOTIFICACION DE TERMINO =========
                     try {
                               logger.info("NOTIFICACION AR : PREVIRED  - INICIANDO...." );
                             //logger.debug("NOTIFICACION AR VIA WS : PREVIRED  - INICIANDO...." );
                             //Notifica Webservices de Recaudacion
                             //RecaudacionServiceClient myPort = new RecaudacionServiceClient();
                             //myPort.setEndpoint(ResourceBundleUtil.getProperty("integracion.webservice.notificacion.recaudacion.endpoint"));
                             //logger.debug(ResourceBundleUtil.getProperty("integracion.webservice.notificacion.recaudacion.endpoint"));
                             //InformarRecaudacion inx = new InformarRecaudacion();
                             InformarRecaudacionIn in = new InformarRecaudacionIn();
                             
                             in.setCallerSystem(ResourceBundleUtil.getProperty("integracion.webservice.notificacion.recaudacion.callersystem"));
                             in.setCallerUser(ResourceBundleUtil.getProperty("integracion.webservice.notificacion.recaudacion.calleruser"));
                             
                             //RECUPERA EL FOLIO DE CAJA DIARIO
                             in.setFolioCajaDiario(foliocajadiario);//SELECT
                             in.setFolioPago(folioCaja != null ? folioCaja.longValue() : null);
                             Calendar cale = new GregorianCalendar();
                             cale.setTime(fechaTurno);
                             in.setTurno(cale);
                             in.setIdTurno(turnoBackup);
                             in.setUsuarioCaja(ResourceBundleUtil.getProperty("integracion.webservice.notificacion.recaudacion.usuariocaja"));
                             //inx.setInformarRecaudacionIn_1(in);
                             ejb.ejecutarDistribucionEnCajayConcurrente(in);    
                             //myPort.informarRecaudacion(inx); 
                             
                             logger.debug("NOTIFICANDO USUARIO PREVIRED MODO MANUAL :" +in.getUsuarioCaja());
                             logger.debug("NOTIFICACION - getCallerSystem:" + in.getCallerSystem());
                             logger.debug("NOTIFICACION - getCallerUser:" + in.getCallerUser());
                             logger.debug("NOTIFICACION - getTurno:" + in.getTurno());
                             logger.debug("NOTIFICACION - getFolioPago:" + in.getFolioPago());
                             logger.debug("NOTIFICACION - getIdTurno:" + in.getIdTurno());
                             logger.debug("NOTIFICACION - getFolioCajaDiario:" + in.getFolioCajaDiario());
                             logger.debug("NOTIFICACION - getUsuarioCaja:" + in.getUsuarioCaja());
                             logger.info("NOTIFICACION AR : OK  - ID TURNO CAJA PREVIRED:"+turnoBackup );
                             //logger.debug("NOTIFICACION AR VIA WS : OK  - ID TURNO CAJA PREVIRED:"+turnoBackup );
                         
                     } catch (Exception e) {
                       e.printStackTrace();
                     }
               }    
           }
           
      }
}
