package cl.bicevida.previred.model;

import cl.bicevida.previred.common.dto.ArchivoPlanillaAntecedentesDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaDetalleDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaEncabezadoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaHeaderDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaResumenDTO;
import cl.bicevida.previred.common.dto.CargaArchivosDTO;
import cl.bicevida.previred.common.dto.ConceptosDePagoDTO;
import cl.bicevida.previred.common.dto.ConvenioPagoDTO;
import cl.bicevida.previred.common.dto.EmpresasDTO;
import cl.bicevida.previred.common.dto.EstadoDeProcesoDTO;
import cl.bicevida.previred.common.dto.InformarRecaudacionIn;
import cl.bicevida.previred.common.dto.MediosDePagoDTO;
import cl.bicevida.previred.common.dto.MotivoRechazosDTO;
import cl.bicevida.previred.common.dto.PKTransaccionPagoDTO;
import cl.bicevida.previred.common.dto.RecaudadoresDTO;
import cl.bicevida.previred.common.dto.RendProcesoAutomatico;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;
import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;
import cl.bicevida.previred.common.dto.TipoArchivoDTO;
import cl.bicevida.previred.dao.RendicionDAO;
import cl.bicevida.previred.dao.hub.ConcurrenteAr;
import cl.bicevida.previred.pattern.dao.DAOFactory;
import cl.bicevida.previred.pattern.dao.OracleDAOFactory;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import javax.ejb.TransactionManagement;

import javax.ejb.TransactionManagementType;

import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;

import javax.naming.InitialContext;
import javax.naming.NamingException;

//import oracle.j2ee.ejb.StatelessDeployment;


@Stateless(name= "PreviredEJBJAR" , mappedName = "Previred-PreviredCore-PreviredEJBBean")
//@StatelessDeployment(transactionTimeout=7200)
@Remote
@Local
@TransactionManagement(value = TransactionManagementType.BEAN)
public class PreviredEJBBean implements PreviredEJB, PreviredEJBLocal, Serializable {
    @SuppressWarnings("compatibility:-4002963449491692765")
    private static final long serialVersionUID = 1L;
    
    /**
     * Logger for this class
     */
    private transient org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PreviredEJBBean.class);
    
    public PreviredEJBBean() {
    }
    
    public CargaArchivosDTO getCargaArchivosByIdCarga(Double idCarga){
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        CargaArchivosDTO cargaArchivo = rendicionDAO.getCargaArchivosByIdCarga(idCarga);
        return cargaArchivo;
    }
    
    public boolean updateCargaArchivosInicioProceso(Double idCarga){
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.updateCargaArchivosInicioProceso(idCarga);
        return resp;
    }
    
    public boolean updateCargaArchivosTerminoProceso(Double idCarga){
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.updateCargaArchivosTerminoProceso(idCarga);
        return resp;
    }
    
    public boolean updateCargaArchivosEstadoProceso(Double idCarga, Integer estadoProceso, String observacion, Double idCargaAsoc){
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.updateCargaArchivosEstadoProceso(idCarga, estadoProceso, observacion, idCargaAsoc);
        return resp;
    }
    
    public boolean updateCargaArchivosCantidadProcesada(Double idCarga, Integer cantidadProcesada){
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.updateCargaArchivosCantidadProcesada(idCarga, cantidadProcesada);
        return resp;
    }
    
    public ResumenAbonoDTO insertResumenAbono(ResumenAbonoDTO resumenAbonoDTO, Double idCarga){
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        ResumenAbonoDTO resp = rendicionDAO.insertResumenAbono(resumenAbonoDTO, idCarga);
        return resp;
    }
    
     public ResumenAbonoDTO insertDetalleAbono(ResumenAbonoDTO resumenDTO, Double idCarga, Integer inicio, Integer procesar,Integer tipoProceso)throws Exception{
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        ResumenAbonoDTO resp = rendicionDAO.insertDetalleAbono(resumenDTO, idCarga, inicio, procesar,tipoProceso);
        return resp;
    }

    public CargaArchivosDTO insertCargaArchivos(CargaArchivosDTO cargaArchivosDTO) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        CargaArchivosDTO resp = rendicionDAO.insertCargaArchivos(cargaArchivosDTO);
        return resp;
    }

    public boolean deleteDetalleAbono(Double idCarga) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.deleteDetalleAbono(idCarga);
        return resp;
    }

    public boolean deleteResumenAbono(Double idCarga) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.deleteResumenAbono(idCarga);
        return resp;
    }

    public ArchivoPlanillaHeaderDTO insertArchivoPlanilla(ArchivoPlanillaHeaderDTO headerDTO, Double idCarga, Integer inicio, Integer procesar) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        ArchivoPlanillaHeaderDTO resp = rendicionDAO.insertArchivoPlanilla(headerDTO, idCarga, inicio, procesar);
        return resp;
    }

    public ArchivoPlanillaAntecedentesDTO insertAntecedentesPlanilla(ArchivoPlanillaAntecedentesDTO antecedentesDTO, Double idCarga, Double id_folio_planilla) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        ArchivoPlanillaAntecedentesDTO resp = rendicionDAO.insertAntecedentesPlanilla(antecedentesDTO, idCarga, id_folio_planilla);
        return resp;
    }

    public List<ArchivoPlanillaDetalleDTO> insertDetallePlanilla(List<ArchivoPlanillaDetalleDTO> detalleDTO, Double idCarga, Double id_folio_planilla) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<ArchivoPlanillaDetalleDTO> resp = rendicionDAO.insertDetallePlanilla(detalleDTO, idCarga, id_folio_planilla);
        return resp;
    }

    public ArchivoPlanillaResumenDTO insertResumenPlanilla(ArchivoPlanillaResumenDTO resumenDTO, Double idCarga, Double id_folio_planilla) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        ArchivoPlanillaResumenDTO resp = rendicionDAO.insertResumenPlanilla(resumenDTO, idCarga, id_folio_planilla);
        return resp;
    }

    public boolean deletePlanillaAbono(Double idCarga) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.deletePlanillaAbono(idCarga);
        return resp;
    }

    public boolean deleteAntecedentesPlanilla(Double idCarga) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.deleteAntecedentesPlanilla(idCarga);
        return resp;
    }

    public boolean deleteDetallePlanilla(Double idCarga) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.deleteDetallePlanilla(idCarga);
        return resp;
    }

    public boolean deleteResumenPlanilla(Double idCarga) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.deleteResumenPlanilla(idCarga);
        return resp;
    }

    public boolean deleteEncabezadoPlanilla(Double idCarga) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.deleteEncabezadoPlanilla(idCarga);
        return resp;
    }

    public List<EmpresasDTO> getEmpresas() {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<EmpresasDTO> resp = rendicionDAO.getEmpresas();
        return resp;
    }

    public List<MediosDePagoDTO> getMediosDePago(String tipopl) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<MediosDePagoDTO> resp = rendicionDAO.getMediosDePago(tipopl);
        return resp;
    }

    public List<TipoArchivoDTO> getTipoArchivo(Integer codigo) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<TipoArchivoDTO> resp = rendicionDAO.getTipoArchivo(codigo);
        return resp;
    }

    public ConvenioPagoDTO getConvenioDePago(Integer idEmpresa, Integer idMedioPago) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        ConvenioPagoDTO resp = rendicionDAO.getConvenioDePago(idEmpresa, idMedioPago);
        return resp;
    }

    public boolean updateEstadoPorcesoFolioResumenByIdCarga(Double idCarga, Integer oldEstadoProceso, Integer estadoProceso, String estadoProcesoObs) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.updateEstadoPorcesoFolioResumenByIdCarga(idCarga, oldEstadoProceso, estadoProceso, estadoProcesoObs);
        return resp;
    }

    public List<ResumenAbonoDTO> getAbonoByIdCarga(Double idCarga) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<ResumenAbonoDTO> resp = rendicionDAO.getAbonoByIdCarga(idCarga);
        return resp;
    }

    public List<MotivoRechazosDTO> getMotivoRechazos() {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<MotivoRechazosDTO> resp = rendicionDAO.getMotivoRechazos();
        return resp;
    }

   


    public boolean updateEstadoPorcesoFolioResumenByIdCargaBanco(Double idCarga, 
                                                                 String codRecaudador, 
                                                                 Integer estadoProceso, 
                                                                 String estadoProcesoObs) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.updateEstadoPorcesoFolioResumenByIdCargaBanco(idCarga, codRecaudador, estadoProceso, estadoProcesoObs) ;
        return resp;
    }

    public List<ResumenAbonoDTO> getAbonoAndPlanillaByIdCarga(Double idCargaAbono, Double idCargaPlanilla) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<ResumenAbonoDTO> resp = rendicionDAO.getAbonoAndPlanillaByIdCarga(idCargaAbono, idCargaPlanilla);
        return resp;
    }

    public List<ResumenAbonoDTO> getPlanillaNoInformadosByIdCarga(Double idCargaPlanilla, Double idCargaAbono) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<ResumenAbonoDTO> resp = rendicionDAO.getPlanillaNoInformadosByIdCarga(idCargaPlanilla, idCargaAbono);
        return resp;
    }

    public HashMap<Integer, RecaudadoresDTO> getRecaudadores() {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        HashMap<Integer, RecaudadoresDTO> resp = rendicionDAO.getRecaudadores();
        return resp;
    }

    public List<EstadoDeProcesoDTO> getEstadoDeProceso() {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<EstadoDeProcesoDTO> resp = rendicionDAO.getEstadoDeProceso();
        return resp;
    }

    public HashMap<Integer, ConceptosDePagoDTO> getConceptosDePago() {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        HashMap<Integer, ConceptosDePagoDTO> resp = rendicionDAO.getConceptosDePago();
        return resp;
    }

    public List<ResumenRendicionesVwDTO> getResumenRendiciones(Date fechaDesde, Date fechaHasta, Double folio, Integer rutEmpleador, Integer rutTrabajador, String conceptoPago, String idMedioPago, Integer idEstado, String tipopl) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<ResumenRendicionesVwDTO> resp = rendicionDAO.getResumenRendiciones(fechaDesde, fechaHasta, folio, rutEmpleador, rutTrabajador, conceptoPago, idMedioPago, idEstado, tipopl);
        return resp;
    }

    public List<ResumenRendicionesVwDTO> getConsultaRendiciones(Date fechaDesde, Date fechaHasta, Double folio, Integer rutEmpleador, Integer rutTrabajador, String conceptoPago, String idMedioPago, Integer idEstado, String tipopl) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        List<ResumenRendicionesVwDTO> resp = rendicionDAO.getConsultaRendiciones(fechaDesde, fechaHasta, folio, rutEmpleador, rutTrabajador, conceptoPago, idMedioPago, idEstado, tipopl);
        return resp;
    }

    public ArchivoPlanillaEncabezadoDTO callGeneraTransaccion(ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO, Boolean isClient, String tipoMedioPlanilla, Integer medioPago) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        ArchivoPlanillaEncabezadoDTO resp = rendicionDAO.callGeneraTransaccion(archivoPlanillaEncabezadoDTO, isClient, tipoMedioPlanilla, medioPago);
        return resp;
    }

    
    public boolean updateResumenYDetalleAbonoByIdCargaResumen(Double idCarga, ResumenAbonoDTO resumenAbonoDTO){
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean ret = rendicionDAO.updateResumenYDetalleAbonoByIdCargaResumen(idCarga, resumenAbonoDTO);
        return ret;
    }

    public EstadoDeProcesoDTO getEstadoDeProcesoByIdProceso(Integer idProceso) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        EstadoDeProcesoDTO ret = rendicionDAO.getEstadoDeProcesoByIdProceso(idProceso);
        return ret;
    }

    public ArchivoPlanillaHeaderDTO insertArchivoPlanillaProceso(Long idTurno, java.util.Date fechaturno, Long foliocajaDiario, ArchivoPlanillaHeaderDTO headerDTO, Double idCarga, Integer inicio, Integer procesar, String tipoMedioPlanilla) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        ArchivoPlanillaHeaderDTO ret = rendicionDAO.insertArchivoPlanillaProceso(idTurno, fechaturno, foliocajaDiario, headerDTO, idCarga, inicio, procesar, tipoMedioPlanilla);
        return ret;
    }

    public boolean updateFolioAbonoConDatosNoProcesados(Double idCarga, Integer estadoProceso, String observacion) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean ret = rendicionDAO.updateFolioAbonoConDatosNoProcesados(idCarga, estadoProceso, observacion);
        return ret;        
    }
    
    /**
     * Recupera la lista de pagos que deben ser reprocesados
     * de forma manual en el sistema de cajas
     * 
     * @return
     */
    public List<ResumenRendicionesVwDTO> getReprocesosManualPagos(String tipoRend) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.getReprocesosManualPagos(tipoRend);   
    }
    
    /**
     * Cambia el estado de las transacciones
     * @param idTransacciones
     */
    public void changeStatusTransaccionProccess(List<PKTransaccionPagoDTO> idTransacciones) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        daoFactory.getRendicionDAO().changeStatusTransaccionProccess(idTransacciones);
    }
    
   
    /**
     * Recupera la Fecha Turno
     * @param fechaTurno
     * @return
     */
    public java.util.Date getFechaHoraTurno() {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        return daoFactory.getRendicionDAO().getFechaHoraTurno();
    }
    
    
    /**
     * APertura el turno de caja
     * @param rendPago
     * @return
     */
    public Long callNewAperturaTurnoCaja(String rendPago) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        return daoFactory.getRendicionDAO().callNewAperturaTurnoCaja(rendPago);
    }
    
    
    /**
     * Cierre de turno caja
     * @param rendPago
     * @param idTurno
     * @return
     */
    public String callNewCierreTurnoCaja(String rendPago, Long idTurno) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        return daoFactory.getRendicionDAO().callNewCierreTurnoCaja(rendPago, idTurno);
    }
    
    /**
    * Retorna la fecha y hora del turno con el cual
    * se aperturo la transaccion
    * @param idturno
    * @return
    */
    public java.util.Date getNewFechaHoraTurno(Long idturno) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        return daoFactory.getRendicionDAO().getNewFechaHoraTurno(idturno);
    }

    
    
    /**
    * Recupera el Folio caja con el cual
    * se aperturo el turno
    * @param turno
    * @return
    */
    public Long getNewFolioCajaByTurno(java.util.Date turno) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        return daoFactory.getRendicionDAO().getNewFolioCajaByTurno(turno);
    }
    
    
    
    /**
     * Recupera pagos que deben ser reprocesados por periodo
     * @param tipoRend tipo de producto a consultar
     * @param periodoMes mes del periodo de pago imposiciones
     * @param periodoAnio anio del periodo de pago imposiciones
     * @return lista de pagos segun tipo.
     */
    public List<ResumenRendicionesVwDTO> getReprocesosManualPagosByPeriodo(String tipoRend, Integer periodoMes, Integer periodoAnio) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.getReprocesosManualPagosByPeriodo(tipoRend, periodoMes, periodoAnio);   
    }
    
    /**
     * Inicia el reprocesamiento en sistema de Cajas
     * @param List de transacciones a reprocesar
     */ 
    public Boolean doReprocesarJMSAsync(List<ResumenRendicionesVwDTO> transacciones) {
            try {
                      System.out.println("INICIANDO TRANSACCION DE PAGO POR COLA JMS");
                      InitialContext ctx = new InitialContext();
    
                      // 1: Lookup connection factory        
                      TopicConnectionFactory factory = (TopicConnectionFactory) ctx.lookup("jms/PreviredTopicConnectionFactory");
                      
                      // 2: Use connection factory to create JMS connection
                      TopicConnection connection = factory.createTopicConnection();
                      
                      // 3: Use connection to create a session
                      TopicSession session = connection.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
                      
                      // 4: Lookup destination 
                      Topic topic = (Topic)ctx.lookup("jms/PreviredTopic");        
                      
                      // 5: Create a message publisher 
                      TopicPublisher publisher = session.createPublisher(topic);
    
                      // 6: Create and publish a message
                      ObjectMessage msg = session.createObjectMessage();
                      
                      Iterator item = transacciones.iterator();
                      while (item.hasNext()) {
                          ResumenRendicionesVwDTO dto = (ResumenRendicionesVwDTO)item.next();
                          msg.setObject(dto);
                          publisher.send(msg);
                          System.out.println("ENVIANDO MENSAJE OBJETO:" + dto.toString());
                      }
                    
                      
                      // finish
                      publisher.close();
                      System.out.println("MENSAJE DE PAGO ENVIADO. QUEDA EN LA COLA CUANDO SE EJECUTE.");
                      
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("ERROR ENVIO JMS:"+e.getMessage());
            }
            return Boolean.TRUE;
        
    }
    
    
    /**
     * Recupera la Fecha Turno
     * @param fechaTurno
     * @return
     */
    public Long getFolioCajaByTurno(java.util.Date fechaTurno) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        Long folioCaja = daoFactory.getRendicionDAO().getFolioCajaByTurno(fechaTurno);
        return folioCaja;
    }


    /**
     * Busca la fecha y hora del turno
     * @param idturno
     * @return
     */
    public java.util.Date getFechaHoraTurno(Long idturno) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();
        java.util.Date fechaTurno = daoFactory.getRendicionDAO().getFechaHoraTurno(idturno);
        return fechaTurno;       
    }
    
    
    /**
     * Distribucion de Montos en Caja
     * @param usuarioCaja
     * @param fechaTurno
     * @param numeroFolioCajaDiario
     * @param numeroFolioPago
     * @param empresa
     * @return
     */
    public Integer callDistribuirMontoCaja(String usuarioCaja,
                                           java.util.Date fechaTurno,
                                           Long numeroFolioCajaDiario,
                                           Long numeroFolioPago,
                                           Integer empresa) {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();
        Integer resultado = daoFactory.getRendicionDAO().callDistribuirMontoCaja(usuarioCaja, fechaTurno, numeroFolioCajaDiario, numeroFolioPago, empresa);
        return resultado;
    }
   
   
    
    /**
     * Funcion que ejecuta la distribucion en caja y ejecuta el concurrente de recaudacion AR
     *
     * @param irIn
     * @throws Exception
     */
    public void ejecutarDistribucionEnCajayConcurrente(InformarRecaudacionIn irIn) throws Exception {
        
        logger.info("[PreviredEJBBean][ejecutarDistribucionEnCajayConcurrente]-> " + irIn.toString());
        try{
            if( this.ejecutarDistribucionEnCaja(irIn) == 0 ){
                logger.info("==> Notificando a Concurriente de AR...");
                int resultadoCon = this.ejecutarConcurenteRecaudacion(irIn);
                logger.info("==> Resultado Ejecucion a Concurriente de AR : " + resultadoCon);
            }
            else{
                throw new Exception("Error al tratar de ejecutar la distribucion en caja");
            }
        }catch(Exception e){
            logger.error("[PreviredEJBBean][ejecutarDistribucionEnCajayConcurrente]->Error", e);
            throw new Exception(e);
        }
    }
   
   
    /**
     * ejecuta la funcion de distribucion en caja
     *
     * @param irIn
     * @return
     * @throws NamingException
     * @throws Exception
     */
    public int ejecutarDistribucionEnCaja(InformarRecaudacionIn irIn) throws NamingException, Exception {
        ConcurrenteAr d = new ConcurrenteAr();
        int resultadoDist = d.ejecutarDistribucionEnCaja(irIn);
        return resultadoDist;
    }
    
    /**
     *  ejecuta concurrente de recaudacion
     *
     * @param irIn
     * @return
     * @throws NamingException
     * @throws Exception
     */
    public int ejecutarConcurenteRecaudacion(InformarRecaudacionIn irIn) throws NamingException, Exception {
        ConcurrenteAr d = new ConcurrenteAr();
        int resultadoRecaudacion = d.ejecutarConcurenteRecaudacion(irIn);
        return resultadoRecaudacion;
    }


    @Override
    public Boolean existAbonoFinalizado(String nombreArchivo, Integer estadoProceso) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.existAbonoFinalizado(nombreArchivo, estadoProceso);
    }

    @Override
    public List<String> getListGlosaRecaudador(String codigoNumerico,String codigoRecaudador) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.getListGlosaRecaudador(codigoNumerico,codigoRecaudador);
    }
    @Override
    public String getGlosaRecaudador(String codigoNumerico) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.getGlosaRecaudador(codigoNumerico);
    }
    @Override
    public boolean existProcesoEnEjecucion(Integer estado) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.existProcesoEnEjecucion(estado);
    }

    @Override
    public void guardarProcesoRendicion(RendProcesoAutomatico proceso) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        rendicionDAO.guardaEjecucion(proceso);

    }

    @Override
    public void actualizaProcesoRendicion(Integer estado, String descripcionEstado, Long idCarga,
                                           String observacion) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        rendicionDAO.actualizaEjecucion(estado, descripcionEstado, idCarga, observacion);

    }

    @Override
    public boolean existArchivoProcesado(String nombreArchivo, Integer estado) throws NamingException,
                                                                                                Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.existArchivoCargaProcesado(nombreArchivo, estado);
    }
    public Date retornaDiaHabil(Date fecha)throws NamingException,Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.retornaDiaHabil(fecha);
    }

    @Override
    public Integer existeArchivoCargado(String nombreArchivo) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.existeArchivoCargado(nombreArchivo);
    }

    @Override
    public boolean validaDiaHabil(Date fecha) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        return rendicionDAO.validaDiaHabil(fecha);
    }

    @Override
    public boolean deleteProcesoAutomatico(Double idCarga) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.deleteCargaProcesoAutomatico(idCarga);
        return resp;
    }

    @Override
    public boolean deleteCargaArchivo(Double idCarga) throws NamingException, Exception {
        OracleDAOFactory daoFactory = (OracleDAOFactory)DAOFactory.getDAOFactory();            
        RendicionDAO rendicionDAO = daoFactory.getRendicionDAO();
        boolean resp = rendicionDAO.deleteCargaArchivo(idCarga);
        return resp;
    }
}
