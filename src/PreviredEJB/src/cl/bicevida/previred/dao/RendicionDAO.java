package cl.bicevida.previred.dao;

import cl.bicevida.previred.common.dto.ArchivoPlanillaAntecedentesDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaDetalleDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaEncabezadoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaHeaderDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaResumenDTO;
import cl.bicevida.previred.common.dto.CargaArchivosDTO;
import cl.bicevida.previred.common.dto.ConceptosDePagoDTO;
import cl.bicevida.previred.common.dto.ConvenioPagoDTO;
import cl.bicevida.previred.common.dto.EjecutaEncabezadoCajaDTO;
import cl.bicevida.previred.common.dto.EmpresasDTO;
import cl.bicevida.previred.common.dto.EstadoDeProcesoDTO;
import cl.bicevida.previred.common.dto.MediosDePagoDTO;
import cl.bicevida.previred.common.dto.MotivoRechazosDTO;
import cl.bicevida.previred.common.dto.PKTransaccionPagoDTO;
import cl.bicevida.previred.common.dto.RecaudadoresDTO;
import cl.bicevida.previred.common.dto.RendProcesoAutomatico;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;

import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;
import cl.bicevida.previred.common.dto.TipoArchivoDTO;

import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.pattern.dao.OracleDAOFactory;

import java.io.Serializable;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.List;


public interface RendicionDAO {   
    public CargaArchivosDTO getCargaArchivosByIdCarga(Double idCarga);
    public boolean updateCargaArchivosInicioProceso(Double idCarga);
    public boolean updateCargaArchivosTerminoProceso(Double idCarga);
    public boolean updateCargaArchivosEstadoProceso(Double idCarga, Integer estadoProceso, String observacion, Double idCargaAsoc);
    public boolean updateCargaArchivosCantidadProcesada(Double idCarga, Integer cantidadProcesada);    
    public ResumenAbonoDTO insertResumenAbono(ResumenAbonoDTO resumenAbonoDTO, Double idCarga);
    public ResumenAbonoDTO insertDetalleAbono(ResumenAbonoDTO resumenDTO, Double idCarga, Integer inicio, Integer procesar,Integer tipoProceso)throws Exception;
    public CargaArchivosDTO insertCargaArchivos(CargaArchivosDTO cargaArchivosDTO);
    public boolean deleteDetalleAbono(Double idCarga);
    public boolean deleteResumenAbono(Double idCarga); 
    public ArchivoPlanillaHeaderDTO insertArchivoPlanilla(ArchivoPlanillaHeaderDTO headerDTO, Double idCarga, Integer inicio, Integer procesar);
    public ArchivoPlanillaAntecedentesDTO insertAntecedentesPlanilla(ArchivoPlanillaAntecedentesDTO antecedentesDTO, Double idCarga, Double id_folio_planilla);
    public List<ArchivoPlanillaDetalleDTO> insertDetallePlanilla(List<ArchivoPlanillaDetalleDTO> lstdetalleDTO, Double idCarga, Double id_folio_planilla);
    public ArchivoPlanillaResumenDTO insertResumenPlanilla(ArchivoPlanillaResumenDTO resumenDTO, Double idCarga, Double id_folio_planilla);
    public boolean deletePlanillaAbono(Double idCarga);
    public boolean deleteAntecedentesPlanilla(Double idCarga);
    public boolean deleteDetallePlanilla(Double idCarga);
    public boolean deleteResumenPlanilla(Double idCarga);
    public boolean deleteEncabezadoPlanilla(Double idCarga);
    public List<EmpresasDTO> getEmpresas();
    public List<MediosDePagoDTO> getMediosDePago(String tipopl);
    public List<TipoArchivoDTO> getTipoArchivo(Integer codigo);
    public ConvenioPagoDTO getConvenioDePago(Integer idEmpresa, Integer idMedioPago);
    public boolean updateEstadoPorcesoFolioResumenByIdCarga(Double idCarga, Integer oldEstadoProceso, Integer estadoProceso, String estadoProcesoObs);
    public List<ResumenAbonoDTO> getAbonoByIdCarga(Double idCarga);
    public List<MotivoRechazosDTO> getMotivoRechazos();
    public boolean updateEstadoPorcesoFolioResumenByIdCargaBanco(Double idCarga, String codRecaudador, Integer estadoProceso, String estadoProcesoObs);
    public List<ResumenAbonoDTO> getAbonoAndPlanillaByIdCarga(Double idCargaAbono, Double idCargaPlanilla);
    public List<ResumenAbonoDTO> getPlanillaNoInformadosByIdCarga(Double idCargaPlanilla, Double idCargaAbono);
    public CargaArchivosDTO getCargaArchivoByIdFolioPlanilla(ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO, Date periodoCotizacion);
    public HashMap<Integer, RecaudadoresDTO> getRecaudadores();
    public List<EstadoDeProcesoDTO> getEstadoDeProceso();
    public HashMap<Integer, ConceptosDePagoDTO> getConceptosDePago();
    public List<ResumenRendicionesVwDTO> getResumenRendiciones(Date fechaDesde, Date fechaHasta, Double folio, Integer rutEmpleador, Integer rutTrabajador, String conceptoPago, String idMedioPago, Integer idEstado, String tipopl);
    public List<ResumenRendicionesVwDTO> getConsultaRendiciones(Date fechaDesde, Date fechaHasta, Double folio, Integer rutEmpleador, Integer rutTrabajador, String conceptoPago, String idMedioPago, Integer idEstado, String tipopl);
    public boolean updateResumenYDetalleAbonoByIdCargaResumen(Double idCarga, ResumenAbonoDTO resumenAbonoDTO);
    public EstadoDeProcesoDTO getEstadoDeProcesoByIdProceso(Integer idProceso);
    public ArchivoPlanillaHeaderDTO insertArchivoPlanillaProceso(Long idTurno, java.util.Date fechaTurno, Long folioCajaDiario, ArchivoPlanillaHeaderDTO headerDTO, Double idCarga, Integer inicio, Integer procesar, String tipoMedioPlanilla);
    public boolean updateFolioAbonoConDatosNoProcesados(Double idCarga, Integer estadoProceso, String observacion);
    public List<ResumenRendicionesVwDTO> getReprocesosManualPagos(String tipoRend);
    public void changeStatusTransaccionProccess(List<PKTransaccionPagoDTO> idTransacciones);
    
    /**
     * Distribucion de Montos en Caja
     * @param usuarioCaja
     * @param fechaTurno
     * @param numeroFolioCajaDiario
     * @param numeroFolioPago
     * @param empresa
     * @return
     */
    public Integer callDistribuirMontoCaja(String usuarioCaja,
                                           java.util.Date fechaTurno,
                                           Long numeroFolioCajaDiario,
                                           Long numeroFolioPago,
                                           Integer empresa);


    /**
     * Metodos de envio a caja
     * @param archivoPlanillaEncabezadoDTO
     * @param isClient
     * @param tipoMedioPlanilla
     * @param medioPago
     * @return
     */
    public ArchivoPlanillaEncabezadoDTO callGeneraTransaccion(ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO, Boolean isClient, String tipoMedioPlanilla, Integer medioPago);    
    public EjecutaEncabezadoCajaDTO callGeneraTransaccion(ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO, int detalle, Boolean isClient, String tipoMedioPlanilla, Integer medioPago);
    public Long getFolioCajaByTurno(java.util.Date turno);
    public java.util.Date getFechaHoraTurno() ;
    
    
    
    /**
     * Apertura de la caja
     * @param rendPago observacion del motivo de la apertura de la caja
     * @return
     */
    public Long callNewAperturaTurnoCaja(String rendPago);
    
    /**
     * Cierre de turno caja
     * @param rendPago
     * @param idTurno
     * @return
     */
    public String callNewCierreTurnoCaja(String rendPago, Long idTurno);
    
    /**
    * Retorna la fecha y hora del turno con el cual
    * se aperturo la transaccion
    * @param idturno
    * @return
    */
    public java.util.Date getNewFechaHoraTurno(Long idturno);

  
    
    /**
    * Recupera el Folio caja con el cual
    * se aperturo el turno
    * @param turno
    * @return
    */
    public Long getNewFolioCajaByTurno(java.util.Date turno);
    
    /**
    * Recupera transacciones por periodo
    * @param tipo
    * @param mes
    * @param anio
    * @return
    */
    public List<ResumenRendicionesVwDTO> getReprocesosManualPagosByPeriodo(String tipoRend, Integer mes, Integer anio);

    /**
    * Retorna la fecha y hora del turno con el cual
    * se aperturo la transaccion
    * @param idturno
    * @return
    */
    public java.util.Date getFechaHoraTurno(Long idturno);
    
    /**
     *  Verifica si existe archivo
     * @param nombreArchivo
     * @param estadoProceso
     * @return
     */
    public Boolean existAbonoFinalizado(String nombreArchivo, Integer estadoProceso);
    
    /**
     * Obtiene la glosa de acuerdo al codigo recaudador
     * @param codigoNumerico
     * @return
     */
    public List<String> getListGlosaRecaudador(String codigoNumerico,String glosaRecaudador);
    
    
    public boolean existProcesoEnEjecucion(Integer estado);
    void guardaEjecucion(RendProcesoAutomatico procesoObj);
    void actualizaEjecucion(Integer estado, String descripcionEstado,Long idCarga,String observacion);
    public boolean existArchivoCargaProcesado(String nombreArchivo, Integer estado);
    public String getGlosaRecaudador(String codigoNumerico);
    public Date retornaDiaHabil(Date fecha);
    public Integer existeArchivoCargado(String nombreArchivo);
    public boolean validaDiaHabil(Date fecha);
    public boolean deleteCargaProcesoAutomatico(Double idCarga);
    public boolean deleteCargaArchivo(Double idCarga);
}
