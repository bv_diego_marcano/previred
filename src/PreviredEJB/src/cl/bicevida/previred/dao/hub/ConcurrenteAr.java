package cl.bicevida.previred.dao.hub;

import cl.bicevida.previred.common.dto.InformarRecaudacionIn;
import cl.bicevida.previred.common.dto.OperacionDto;

import cl.bicevida.previred.dao.log.Log;
import cl.bicevida.previred.pattern.dao.OracleDAOFactory;
import cl.bicevida.previred.util.ResourceBundleUtil;

import java.io.Serializable;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;


public class ConcurrenteAr implements Serializable{
    @SuppressWarnings("compatibility:7315188946754057442")
    private static final long serialVersionUID = 1L;

    private static Log log;
    private transient org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ConcurrenteAr.class);
    /* ***DECLARACIONES*** */
    private String dataSourceName;
    private DataSource dsIntegracion;
    private Connection connIntegracion = null;
    private DataSource dsEbs;
    private Connection connEbs = null;
    private String SQL;

    /* ***DECLARACIONES*** */

    public ConcurrenteAr() throws NamingException {
        Context ctx = new InitialContext();
       
        dataSourceName = ResourceBundleUtil.getProperty("jdbc.connection.datasource.jndi.ebs");
        dsEbs = (DataSource)ctx.lookup(dataSourceName);
        
        dataSourceName = ResourceBundleUtil.getProperty("jdbc.connection.datasource.jndi");
        dsIntegracion = (DataSource)ctx.lookup(dataSourceName);
    }


    public static void main(String[] args) throws Exception {
        ConcurrenteAr dm = new ConcurrenteAr();
        dm.ejecutarConcurenteRecaudacion(null);
    }
 

    public int ejecutarConcurenteRecaudacion(InformarRecaudacionIn irIn)  throws Exception{
        Integer processId = null;
        CallableStatement llamaPackage = null;
        try {
            
            connIntegracion = dsIntegracion.getConnection();
            connEbs = dsEbs.getConnection();
            log = new Log(connIntegracion);
            processId = log.openLogProcess("CONCURRENTE_RECAUDACION_AR");
            
            SQL =  "BEGIN\n" + 
            "  XXBV_AR_INT_SVI.BV_RECAUDACION_AR(\n" + 
            "    P_RESPONSABILIDAD => 'BICE VIDA AR ADMINISTRADOR',\n" + 
            "    P_USUARIO => ?,\n" + 
            "    P_ID_DIA => ?,\n" + 
            "    P_TURNO => ?,\n" + 
            "    P_CAJERO => ?,\n" + 
            "    P_ID_BOLETA => ?,\n" + 
            "    P_ID_TURNO => ?,\n" + 
            "    P_ID_CONCURRENTE => ? );" + 
            "END;";
            
            llamaPackage = connEbs.prepareCall(SQL);
            llamaPackage.registerOutParameter(7, OracleTypes.VARCHAR);
            
            if (irIn.getCallerUser() != null)
                llamaPackage.setString(1, irIn.getCallerUser());
            else
                llamaPackage.setNull(1, OracleTypes.VARCHAR);

            if (irIn.getFolioCajaDiario() != null)
                llamaPackage.setLong(2, irIn.getFolioCajaDiario());
            else
                llamaPackage.setNull(2, OracleTypes.INTEGER);

            if (irIn.getTurno() != null)
                llamaPackage.setTimestamp(3, new Timestamp(irIn.getTurno().getTimeInMillis()));
            else
                llamaPackage.setNull(3, OracleTypes.DATE);
                
            if (irIn.getUsuarioCaja() != null)
                llamaPackage.setString(4, irIn.getUsuarioCaja());
            else
                llamaPackage.setNull(4, OracleTypes.VARCHAR);
                
            if (irIn.getFolioPago() != null)
                llamaPackage.setLong(5, irIn.getFolioPago());
            else
                llamaPackage.setNull(5, OracleTypes.INTEGER);
                
            if (irIn.getIdTurno() != null)
                llamaPackage.setLong(6, irIn.getIdTurno());
            else
                llamaPackage.setNull(6, OracleTypes.INTEGER);                                      
           
            llamaPackage.execute();
            Integer idConcurrente = llamaPackage.getInt(7);
            log.log(processId, "Se creo concurrente " + idConcurrente, log.LOG_LEVEL_DEBUG, null);
            
            try{
                logger.info("**********EJECUTANDO CONCURRENTE: XXBV_AR_INT_SVI.BV_RECAUDACION_AR **********");
                logger.info("P_USUARIO:         ["+irIn.getCallerUser()+"]");
                logger.info("P_ID_DIA:          ["+irIn.getFolioCajaDiario()+"]");
                logger.info("P_TURNO:           ["+new Timestamp(irIn.getTurno().getTimeInMillis())+"]");
                logger.info("P_CAJERO:          ["+irIn.getUsuarioCaja()+"]");
                logger.info("P_ID_BOLETA:       ["+irIn.getFolioPago()+"]");
                logger.info("P_ID_TURNO:        ["+irIn.getIdTurno()+"]");
                logger.info("P_ID_CONCURRENTE:  ["+llamaPackage.getInt(7)+"]");
                logger.info("*******FIN EJECUTANDO CONCURRENTE: XXBV_AR_INT_SVI.BV_RECAUDACION_AR **********");
            }catch(Exception ex){
                logger.error("Error "+ ex.getMessage());

            }
            
            
            log.closeLogProcess(processId);
        } catch (Exception e) {
            e.printStackTrace();
            log.log(processId, "Fallo la llamada al proceso CONCURRENTE_RECAUDACION_AR: " + e.getMessage() , log.LOG_LEVEL_DEBUG, null);
            log.closeLogProcess(processId);
            throw new Exception("Error en metodo ejecutarConcurenteRecaudacion.", e);
        } finally {
            if (connIntegracion != null)
                try {
                    connIntegracion.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (connEbs != null)
                try {
                    connEbs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if(llamaPackage != null){
                try {
                    llamaPackage.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return 1;
    }
    
    
   
    
    
    /**
     * Ejecuta la distribucion en sistema de cajas
     * 
     * PROCEDURE Distribucion_montos(p_usuario     IN varchar2
     *                          ,p_turno           IN date
     *                          ,p_operacion       IN number
     *                          ,p_foliocaja       IN number
     *                          ,p_foliopago       IN number
     *                          ,p_empresa         IN number
     *                          ,p_CodResultado    OUT number
     *                          ,p_DescResultado   OUT varchar2
     *                          );                            
     * @param irIn
     * @return
     * @throws ApplicationModelException
     */
    public int ejecutarDistribucionEnCaja(InformarRecaudacionIn irIn) throws Exception {
     logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) inciando distribucion en cajas");
        Integer resultado = 99;
        String descripcion = "Ejecutando...";
        OperacionDto numeroOperacion = new OperacionDto();
        CallableStatement llamaPackage = null;
        try {
        
           
            //LLAMADOS DE PAGOS MASIVOS (MAS DE 1)
            if ( irIn.getUsuarioCaja() != null && ( 
                 irIn.getUsuarioCaja().equalsIgnoreCase("RENDICIONESHUB")  ||
                 irIn.getUsuarioCaja().equalsIgnoreCase("RENDICIONESAPV") ||
                 irIn.getUsuarioCaja().equalsIgnoreCase("RENDICIONESBH"))) {
            
                logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) es un proceso MASIVO");
                resultado = 0; //ejecutar concurrente
                
                //BICE HIPOTECARIA
                if (irIn.getUsuarioCaja().equalsIgnoreCase("RENDICIONESBH")) {
                    resultado = 1; //FORZO A NO EJECUTAR CONCURRENTE
                }
                
            } else {
            
                logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) es un proceso INDIVIDUAL 1 a 1");
                //TODOS LOS CAJEROS (PAGOS 1 A 1)
                logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) recuperando numero de operacion...");
                numeroOperacion = getNumeroOperacion(irIn.getFolioPago());
                logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) numero de Operacion devuelto :" + numeroOperacion.getOperacion());
                logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) Codigo de Empresa devuelto :" + numeroOperacion.getEmpresa());
               
                logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) Recuperando conexion base de datos...");
                connIntegracion = dsIntegracion.getConnection();
                SQL =  "{call PKG_DISTRIBUCION_CAJA.Distribucion_montos(?,?,?,?,?,?,?,?)}";  
                logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) PROCEDURE Distribucion_montos(p_usuario ==>" + irIn.getUsuarioCaja() + "\n" +
                "                              ,p_turno  ==>" + new Timestamp(irIn.getTurno().getTimeInMillis()) + "\n" +
                "                              ,p_operacion  ==> " + numeroOperacion.getOperacion() + "\n" + 
                "                              ,p_foliocaja  ==> " + irIn.getFolioCajaDiario() + "\n"+
                "                              ,p_foliopago  ==> " + irIn.getFolioPago() + "\n" + 
                "                              ,p_empresa    ==> " + numeroOperacion.getEmpresa() + ")");
                
                llamaPackage = connIntegracion.prepareCall(SQL);
      
                llamaPackage.setString(1, irIn.getUsuarioCaja()); //usuario
                
                 if (irIn.getTurno() != null) {
                    llamaPackage.setTimestamp(2, new Timestamp(irIn.getTurno().getTimeInMillis())); //turno
                 } else {
                     llamaPackage.setNull(2, OracleTypes.DATE);
                 }         
                 if (numeroOperacion != null && numeroOperacion.getOperacion() != null) {
                     llamaPackage.setLong(3, numeroOperacion.getOperacion()); //Numero de operacion
                 } else {
                     llamaPackage.setNull(3, OracleTypes.INTEGER);
                 }
                 if (irIn.getFolioCajaDiario() != null) {
                     llamaPackage.setLong(4, irIn.getFolioCajaDiario()); //folio de caja
                 } else {
                     llamaPackage.setNull(4, OracleTypes.INTEGER);
                 }
                 if (irIn.getFolioPago() != null) {
                     llamaPackage.setLong(5, irIn.getFolioPago()); //folio pago
                 } else {
                     llamaPackage.setNull(5, OracleTypes.INTEGER);
                 }
                 if (numeroOperacion != null && numeroOperacion.getEmpresa() != null) {
                    llamaPackage.setInt(6, numeroOperacion.getEmpresa()); // 1=BICE VIDA 4=HIPOTECARIA
                 } else {
                    llamaPackage.setNull(6, OracleTypes.INTEGER);
                 }
                
                llamaPackage.registerOutParameter(7, OracleTypes.NUMERIC);
                llamaPackage.registerOutParameter(8, OracleTypes.VARCHAR);
            
                //EJECUTA PLSQL
                llamaPackage.execute();
                
                //RECUPERA RESULTADO
                resultado = llamaPackage.getInt(7); // 0=ok, <> 0 = problema
                descripcion = llamaPackage.getString(8);
                
                
                //BICE HIPOTECARIA
                if (numeroOperacion != null && numeroOperacion.getEmpresa() != null && numeroOperacion.getEmpresa().intValue() == 4 && resultado == 0) {
                    logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) Empresa es Hipotecaria no requiere ejecutar AR");
                    resultado = 1; //FORZO A NO EJECUTAR CXONCURRENTE DE EBS INDIVIDUALES
                }
                
                logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) Se ejecuto distribucion resultado folio caja(" + irIn.getFolioPago() + ") operacion(" + numeroOperacion.getOperacion() + ") empresa (" + numeroOperacion.getEmpresa() + ") resultado :" + resultado + " - " + descripcion);
                
            }
            
            
        } catch (Exception e) {
            logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) error en distribucion :"+e.getMessage());
            e.printStackTrace();
            throw new Exception("Error en metodo ejecutarDistribucionEnCaja.", e);
        } finally {
            if (connIntegracion != null)
                try {
                    connIntegracion.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if (connEbs != null)
                try {
                    connEbs.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            if(llamaPackage != null){
                try {
                    llamaPackage.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        logger.debug("ConcurrenteAr ==> (ejecutarDistribucionEnCaja) terminando distribucion en cajas, resultado :" + resultado);
        return resultado;
    }
    
    
    /**
     * Recupera el numero de operacion
     * @param folioCaja
     * @return
     */
    public OperacionDto getNumeroOperacion(Long folioCaja) {
        OperacionDto numeroOpe = new OperacionDto();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            numeroOpe.setOperacion(new Long(0));
            numeroOpe.setEmpresa(0);
            
            if (folioCaja != null) {
                // get the user-specified connection or get a connection from the ResourceManager
                conn = dsIntegracion.getConnection();
    
                // construct the SQL statement
                final String SQL = "SELECT OPERACION, EMPRESA FROM UCAJAS.OPERACIONES WHERE FOLIO=? AND ESTADO='VIGENT'";
    
                // prepare statement
                stmt = conn.prepareStatement(SQL);
                stmt.setLong(1, folioCaja);
                rs = stmt.executeQuery();
                if (rs.next()) {
                    numeroOpe = new OperacionDto();
                    numeroOpe.setOperacion(rs.getLong("OPERACION"));
                    numeroOpe.setEmpresa(rs.getInt("EMPRESA"));
                }
                rs.close();           
                stmt.close();
            }
            
        } catch (Exception _e) {
            _e.printStackTrace();
        } finally {
            if (connIntegracion != null){
                try {
                    connIntegracion.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
                       
            OracleDAOFactory.closeAll(conn, stmt, rs);   
        }
        return numeroOpe;
    }
    
           
}    


