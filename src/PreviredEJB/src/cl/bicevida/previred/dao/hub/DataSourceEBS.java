package cl.bicevida.previred.dao.hub;

import cl.bicevida.previred.util.ResourceBundleUtil;

import java.io.Serializable;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;


public class DataSourceEBS implements Serializable {
    @SuppressWarnings("compatibility:8668331911359875745")
    private static final long serialVersionUID = 1L;
    public String connectiontype=null;
    public String username=null;
    public String password=null;
    public String url=null;
    public String driver=null;
    public String jndidatasource=null;
    public static boolean loggerinfo = false;

    /**
     * Inicializa valores de configuracion
     */
    public DataSourceEBS() {
        this.setConnectiontype(ResourceBundleUtil.getProperty("jdbc.connection.type"));
        this.setUsername(ResourceBundleUtil.getProperty("jdbc.connection.user"));
        this.setPassword(ResourceBundleUtil.getProperty("jdbc.connection.password"));
        this.setUrl(ResourceBundleUtil.getProperty("jdbc.connection.url.jdbc"));
        this.setDriver(ResourceBundleUtil.getProperty("jdbc.connection.url.driver"));
        this.setJndidatasource(ResourceBundleUtil.getProperty("jdbc.connection.datasource.jndi.ebs"));
    }

    /**
     * <p>Recupera una conexion de datos</p>
     * 
     * @see javax.sql.DataSource#getConnection()
     */
    public Connection getConnection() throws SQLException {

        //Conexion via Datasource
        if (this.connectiontype.trim().equalsIgnoreCase("datasource")) {
            InitialContext ic;
            try {
                ic = new InitialContext();
                DataSource ds = (DataSource) ic.lookup(this.jndidatasource);
                Connection returnConnection = ds.getConnection();
                return returnConnection;
            } catch (NamingException e) {
                throw new SQLException("No se logoro inicializar conexion JNDI '" + this.jndidatasource + "' de conexion a fuentes de datos!");
            }       
                    
        }
        //Conexion via URL y Driver 
        if (this.connectiontype.trim().equalsIgnoreCase("url")) {
            try {
                Class.forName(this.driver).newInstance();
                Connection returnConnection = DriverManager.getConnection(this.url, this.username, this.password);
                return returnConnection;
            } catch (Exception e) {
                throw new SQLException("No se logoro instanciar driver '" + this.driver + "' de conexion a fuentes de datos!");
            }               
        }
        throw new SQLException("No existe implementacion para el tipo de conexion '" + this.connectiontype + "', revise configuracion!");
    }

    /**
     * <p>Recupera la conexion de datos utilizando un username y password</p>
     * 
     */
    public Connection getConnection(String username, String password) throws SQLException {
        this.setUsername(username);
        this.setUsername(password);
        Connection returnConnection = this.getConnection();
        return returnConnection;
    }
       

    public String getConnectiontype() {
        return connectiontype;
    }

    public void setConnectiontype(String connectiontype) {
        this.connectiontype = connectiontype;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getJndidatasource() {
        return jndidatasource;
    }

    public void setJndidatasource(String jndidatasource) {
        this.jndidatasource = jndidatasource;
    }
    
    public static void close(Connection conn){
        try {
                if (conn != null) conn.close();
        } catch (SQLException sqle){
                sqle.printStackTrace();
        }
    }

    public static void close(PreparedStatement stmt){
        try {
                if (stmt != null) stmt.close();
        } catch (SQLException sqle){
                sqle.printStackTrace();
        }
    }
    
    public static void close(Statement stmt){
        try {
                if (stmt != null) stmt.close();
        } catch (SQLException sqle){
                sqle.printStackTrace();
        }
    }

    public static void close(ResultSet rs){
        try {
                if (rs != null) rs.close();
        }catch (SQLException sqle){
                sqle.printStackTrace();
        }
    }
}
