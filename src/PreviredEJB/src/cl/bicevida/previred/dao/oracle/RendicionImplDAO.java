package cl.bicevida.previred.dao.oracle;

import cl.bicevida.previred.common.Constant;
import cl.bicevida.previred.common.dto.ArchivoAbonoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaAntecedentesDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaDetalleDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaEncabezadoDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaHeaderDTO;
import cl.bicevida.previred.common.dto.ArchivoPlanillaResumenDTO;
import cl.bicevida.previred.common.dto.CargaArchivosDTO;
import cl.bicevida.previred.common.dto.ConceptosDePagoDTO;
import cl.bicevida.previred.common.dto.ConvenioPagoDTO;
import cl.bicevida.previred.common.dto.EjecutaCajaDTO;
import cl.bicevida.previred.common.dto.EjecutaEncabezadoCajaDTO;
import cl.bicevida.previred.common.dto.EmpresasDTO;
import cl.bicevida.previred.common.dto.EstadoDeProcesoDTO;
import cl.bicevida.previred.common.dto.MediosDePagoDTO;
import cl.bicevida.previred.common.dto.MotivoRechazosDTO;
import cl.bicevida.previred.common.dto.PKTransaccionPagoDTO;
import cl.bicevida.previred.common.dto.RecaudadoresDTO;
import cl.bicevida.previred.common.dto.RendProcesoAutomatico;
import cl.bicevida.previred.common.dto.ResumenAbonoDTO;
import cl.bicevida.previred.common.dto.ResumenRendicionesVwDTO;
import cl.bicevida.previred.common.dto.TipoArchivoDTO;
import cl.bicevida.previred.common.utils.FechaUtil;
import cl.bicevida.previred.dao.RendicionDAO;
import cl.bicevida.previred.pattern.dao.OracleDAOFactory;

import cl.bicevida.previred.util.ResourceBundleUtil;

import cl.bicevida.previred.util.SimpleDate;

import java.io.Serializable;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;



import oracle.jdbc.OracleTypes;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;


public class RendicionImplDAO implements RendicionDAO, Serializable {
    @SuppressWarnings("compatibility:-5003566982437933110")
    private static final long serialVersionUID = 1L;

    /**
     * Logger for this class
     */
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(RendicionImplDAO.class);

    private static final String JDBC_UCAJAS = "jdbc/ruda_UCAJAS_PREVIRED";

    private static final Constant constantes = new Constant();

    private static Predicate ACEPTADOS = new Predicate() {
        public boolean evaluate(Object input) {
            boolean ret = false;
            if (input != null) {
                if (input instanceof ArchivoAbonoDTO) {
                    ArchivoAbonoDTO archivoAbonoDTO = (ArchivoAbonoDTO) input;
                    if (archivoAbonoDTO.getPlanilla() != null &&
                        archivoAbonoDTO.getPlanilla().getDetalleIndividual() != null &&
                        archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago() != null) {
                        if ((archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago().intValue() ==
                             Constant.PROCESO_AUTORIZADO) ||
                            (archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago().intValue() ==
                             Constant.PROCESO_AUTORIZADO_OBS)) {
                            ret = true;
                        }
                    }
                }
            }
            return ret;
        }
    };

    private static Predicate NOACEPTADOS = new Predicate() {
        public boolean evaluate(Object input) {
            boolean ret = false;
            if (input != null) {
                if (input instanceof ArchivoAbonoDTO) {
                    ArchivoAbonoDTO archivoAbonoDTO = (ArchivoAbonoDTO) input;
                    if (archivoAbonoDTO.getPlanilla() != null &&
                        archivoAbonoDTO.getPlanilla().getEstado_proceso() != null) {
                        if ((archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago().intValue() !=
                             Constant.PROCESO_AUTORIZADO) &&
                            (archivoAbonoDTO.getPlanilla().getDetalleIndividual().getEstado_proceso_pago().intValue() !=
                             Constant.PROCESO_AUTORIZADO_OBS)) {
                            ret = true;
                        }
                    }
                }
            }
            return ret;
        }
    };

    private final int RUNNIG = 15;
    
    Date periodoCotiza = new Date();
    
    /**
     * Implementaci�n de la Consulta de la carga de archivos utlizando el id de la carga
     * @param idCarga
     * @return CargaArchivosDTO
     */
    public CargaArchivosDTO getCargaArchivosByIdCarga(Double idCarga) {

        CargaArchivosDTO resp = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT CARGA.ID_CARGA         AS ID_CARGA,\n" +
            "  CARGA.NUMERO_DE_CONVENIO    AS NUMERO_DE_CONVENIO,\n" +
            "  CARGA.FECHA_CREACION        AS FECHA_CREACION,\n" +
            "  CARGA.ID_TIPO_ARCHIVO       AS ID_TIPO_ARCHIVO,\n" +
            "  CARGA.ESTADO_PROCESO        AS ESTADO_PROCESO,\n" +
            "  CARGA.INICIO_PROCESO        AS INICIO_PROCESO,\n" +
            "  CARGA.TERMINO_PROCESO       AS TERMINO_PROCESO,\n" +
            "  CARGA.FECHA_Y_HORA_UPLOAD   AS FECHA_Y_HORA_UPLOAD,\n" +
            "  CARGA.NOMBRE_FISICO_ARCHIVO AS NOMBRE_FISICO_ARCHIVO,\n" +
            "  CARGA.CANTIDAD_PROCESADA    AS CANTIDAD_PROCESADA,\n" +
            "  CONVENIO.ID_EMPRESA         AS ID_EMPRESA,\n" + "  CONVENIO.ID_MEDIO_PAGO      AS ID_MEDIO_PAGO,\n" +
            "  CONVENIO.ESTADO_CONVENIO    AS ESTADO_CONVENIO,\n" +
            "  CONVENIO.CORREO_ELECTRONICO AS CORREO_ELECTRONICO,\n" +
            "  CONVENIO.DIRECTORIO_UPLOAD  AS DIRECTORIO_UPLOAD,\n" +
            "  ESTADO.DESCRIPCION_PROCESO  AS DESCRIPCION_PROCESO,\n" +
            "  TIPO.DESCRIPCION            AS DESCRIPCION_TIPO,\n" +
            "  MEDIOS.TIPO                 AS TIPO_MEDIO_BANCO,\n" +
            "  MEDIOS.DESCRIPCION          AS DESCRIPCION_MEDIO_BANCO,\n" +
            "  EMPRESAS.DESCRIPCION        AS DESCRIPCION_EMPRESA,\n" +
            "  CARGA.ID_CARGA_ASOC         AS ID_CARGA_ASOC\n" + "FROM REND_CARGA_DE_ARCHIVOS CARGA,\n" +
            "  REND_CONVENIO_DE_PAGO CONVENIO,\n" + "  REND_ESTADO_DE_PROCESO ESTADO,\n" +
            "  REND_TIPO_DE_ARCHIVO_UPLOAD TIPO,\n" + "  REND_MEDIOS_DE_PAGO MEDIOS,\n" + "  REND_EMPRESAS EMPRESAS\n" +
            "WHERE CARGA.ID_CARGA            = ?\n" + "AND CONVENIO.NUMERO_DE_CONVENIO = CARGA.NUMERO_DE_CONVENIO\n" +
            "AND TIPO.ID_TIPO_ARCHIVO        = CARGA.ID_TIPO_ARCHIVO\n" +
            "AND ESTADO.ESTADO_PROCESO       = CARGA.ESTADO_PROCESO\n" +
            "AND MEDIOS.ID_MEDIO_PAGO        = CONVENIO.ID_MEDIO_PAGO\n" +
            "AND EMPRESAS.ID_EMPRESA         = CONVENIO.ID_EMPRESA";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);

            rs = stmt.executeQuery();

            while (rs.next()) {
                CargaArchivosDTO carga = new CargaArchivosDTO();
                carga.setId_carga(idCarga);

                java.sql.Date fechacreacion = rs.getDate("FECHA_CREACION");
                if (fechacreacion != null) {
                    carga.setFecha_creacion(new Date(fechacreacion.getTime()));
                }

                carga.setId_tipo_archivo(rs.getInt("ID_TIPO_ARCHIVO"));
                carga.setDescripcion_tipo_archivo(rs.getString("DESCRIPCION_TIPO"));
                carga.setEstado_proceso(rs.getInt("ESTADO_PROCESO"));
                carga.setDescripcion_estado_proceso(rs.getString("DESCRIPCION_PROCESO"));

                Timestamp inicioproceso = rs.getTimestamp("INICIO_PROCESO");
                if (inicioproceso != null) {
                    carga.setInicio_proceso(new Date(inicioproceso.getTime()));
                }

                Timestamp terminoproceso = rs.getTimestamp("TERMINO_PROCESO");
                if (terminoproceso != null) {
                    carga.setTermino_proceso(new Date(terminoproceso.getTime()));
                }


                Timestamp fechaupload = rs.getTimestamp("FECHA_Y_HORA_UPLOAD");
                if (fechaupload != null) {
                    carga.setFecha_y_hora_upload(new Date(fechaupload.getTime()));
                }

                carga.setNombre_fisico_archivo(rs.getString("NOMBRE_FISICO_ARCHIVO"));
                carga.setCantidad_procesada(rs.getInt("CANTIDAD_PROCESADA"));

                carga.setId_carga_asoc(rs.getDouble("ID_CARGA_ASOC"));

                if (rs.getString("NUMERO_DE_CONVENIO") != null) {
                    ConvenioPagoDTO convenio = new ConvenioPagoDTO();
                    convenio.setNumero_de_convenio(rs.getString("NUMERO_DE_CONVENIO"));
                    convenio.setId_empresa(rs.getInt("ID_EMPRESA"));
                    convenio.setDescripcion_empresa(rs.getString("DESCRIPCION_EMPRESA"));
                    convenio.setId_medio_pago(rs.getInt("ID_MEDIO_PAGO"));
                    convenio.setTipo_codbanco_medio_pago(rs.getString("TIPO_MEDIO_BANCO"));
                    convenio.setDescripcion_medio_pago(rs.getString("DESCRIPCION_MEDIO_BANCO"));
                    convenio.setEstado_convenio(rs.getInt("ESTADO_CONVENIO"));
                    convenio.setCorreo_electronico(rs.getString("CORREO_ELECTRONICO"));
                    convenio.setDirectorio_upload(rs.getString("DIRECTORIO_UPLOAD"));
                    carga.setConvenio(convenio);
                }

                resp = carga;
            }

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return resp;
    }

    /**
     * Implementaci�n de la Consulta de la carga de archivos utlizando el id de la carga
     * @param idCarga
     * @return CargaArchivosDTO
     */
    public CargaArchivosDTO getCargaArchivosByIdCargaEstado(Double idCarga, Integer estadoProceso) {

        CargaArchivosDTO resp = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT CARGA.ID_CARGA         AS ID_CARGA,\n" +
            "  CARGA.NUMERO_DE_CONVENIO    AS NUMERO_DE_CONVENIO,\n" +
            "  CARGA.FECHA_CREACION        AS FECHA_CREACION,\n" +
            "  CARGA.ID_TIPO_ARCHIVO       AS ID_TIPO_ARCHIVO,\n" +
            "  CARGA.ESTADO_PROCESO        AS ESTADO_PROCESO,\n" +
            "  CARGA.INICIO_PROCESO        AS INICIO_PROCESO,\n" +
            "  CARGA.TERMINO_PROCESO       AS TERMINO_PROCESO,\n" +
            "  CARGA.FECHA_Y_HORA_UPLOAD   AS FECHA_Y_HORA_UPLOAD,\n" +
            "  CARGA.NOMBRE_FISICO_ARCHIVO AS NOMBRE_FISICO_ARCHIVO,\n" +
            "  CARGA.CANTIDAD_PROCESADA    AS CANTIDAD_PROCESADA,\n" +
            "  CONVENIO.ID_EMPRESA         AS ID_EMPRESA,\n" + "  CONVENIO.ID_MEDIO_PAGO      AS ID_MEDIO_PAGO,\n" +
            "  CONVENIO.ESTADO_CONVENIO    AS ESTADO_CONVENIO,\n" +
            "  CONVENIO.CORREO_ELECTRONICO AS CORREO_ELECTRONICO,\n" +
            "  CONVENIO.DIRECTORIO_UPLOAD  AS DIRECTORIO_UPLOAD,\n" +
            "  ESTADO.DESCRIPCION_PROCESO  AS DESCRIPCION_PROCESO,\n" +
            "  TIPO.DESCRIPCION            AS DESCRIPCION_TIPO,\n" +
            "  MEDIOS.TIPO                 AS TIPO_MEDIO_BANCO,\n" +
            "  MEDIOS.DESCRIPCION          AS DESCRIPCION_MEDIO_BANCO,\n" +
            "  EMPRESAS.DESCRIPCION        AS DESCRIPCION_EMPRESA,\n" +
            "  CARGA.ID_CARGA_ASOC         AS ID_CARGA_ASOC\n" + "FROM REND_CARGA_DE_ARCHIVOS CARGA,\n" +
            "  REND_CONVENIO_DE_PAGO CONVENIO,\n" + "  REND_ESTADO_DE_PROCESO ESTADO,\n" +
            "  REND_TIPO_DE_ARCHIVO_UPLOAD TIPO,\n" + "  REND_MEDIOS_DE_PAGO MEDIOS,\n" + "  REND_EMPRESAS EMPRESAS\n" +
            "WHERE CARGA.ID_CARGA            = ?\n" + "AND CARGA.ESTADO_PROCESO        = ?\n" +
            "AND CONVENIO.NUMERO_DE_CONVENIO = CARGA.NUMERO_DE_CONVENIO\n" +
            "AND TIPO.ID_TIPO_ARCHIVO        = CARGA.ID_TIPO_ARCHIVO\n" +
            "AND ESTADO.ESTADO_PROCESO       = CARGA.ESTADO_PROCESO\n" +
            "AND MEDIOS.ID_MEDIO_PAGO        = CONVENIO.ID_MEDIO_PAGO\n" +
            "AND EMPRESAS.ID_EMPRESA         = CONVENIO.ID_EMPRESA";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.setInt(2, estadoProceso);

            rs = stmt.executeQuery();

            while (rs.next()) {
                CargaArchivosDTO carga = new CargaArchivosDTO();
                carga.setId_carga(idCarga);

                java.sql.Date fechacreacion = rs.getDate("FECHA_CREACION");
                if (fechacreacion != null) {
                    carga.setFecha_creacion(new Date(fechacreacion.getTime()));
                }

                carga.setId_tipo_archivo(rs.getInt("ID_TIPO_ARCHIVO"));
                carga.setDescripcion_tipo_archivo(rs.getString("DESCRIPCION_TIPO"));
                carga.setEstado_proceso(rs.getInt("ESTADO_PROCESO"));
                carga.setDescripcion_estado_proceso(rs.getString("DESCRIPCION_PROCESO"));

                Timestamp inicioproceso = rs.getTimestamp("INICIO_PROCESO");
                if (inicioproceso != null) {
                    carga.setInicio_proceso(new Date(inicioproceso.getTime()));
                }

                Timestamp terminoproceso = rs.getTimestamp("TERMINO_PROCESO");
                if (terminoproceso != null) {
                    carga.setTermino_proceso(new Date(terminoproceso.getTime()));
                }

                Timestamp fechaupload = rs.getTimestamp("FECHA_Y_HORA_UPLOAD");
                if (fechaupload != null) {
                    carga.setFecha_y_hora_upload(new Date(fechaupload.getTime()));
                }

                carga.setNombre_fisico_archivo(rs.getString("NOMBRE_FISICO_ARCHIVO"));
                carga.setCantidad_procesada(rs.getInt("CANTIDAD_PROCESADA"));

                carga.setId_carga_asoc(rs.getDouble("ID_CARGA_ASOC"));

                if (rs.getString("NUMERO_DE_CONVENIO") != null) {
                    ConvenioPagoDTO convenio = new ConvenioPagoDTO();
                    convenio.setNumero_de_convenio(rs.getString("NUMERO_DE_CONVENIO"));
                    convenio.setId_empresa(rs.getInt("ID_EMPRESA"));
                    convenio.setDescripcion_empresa(rs.getString("DESCRIPCION_EMPRESA"));
                    convenio.setId_medio_pago(rs.getInt("ID_MEDIO_PAGO"));
                    convenio.setTipo_codbanco_medio_pago(rs.getString("TIPO_MEDIO_BANCO"));
                    convenio.setDescripcion_medio_pago(rs.getString("DESCRIPCION_MEDIO_BANCO"));
                    convenio.setEstado_convenio(rs.getInt("ESTADO_CONVENIO"));
                    convenio.setCorreo_electronico(rs.getString("CORREO_ELECTRONICO"));
                    convenio.setDirectorio_upload(rs.getString("DIRECTORIO_UPLOAD"));
                    carga.setConvenio(convenio);
                }

                resp = carga;
            }

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return resp;
    }

    /**
     * Actualiza la fecha de inicio del proceso
     * @param idCarga
     * @return boolean
     */
    public boolean updateCargaArchivosInicioProceso(Double idCarga) {

        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "UPDATE REND_CARGA_DE_ARCHIVOS SET INICIO_PROCESO = SYSDATE WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCarga);
            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Actualiza la fecha del termino del proceso
     * @param idCarga
     * @return boolean
     */
    public boolean updateCargaArchivosTerminoProceso(Double idCarga) {

        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "UPDATE REND_CARGA_DE_ARCHIVOS SET TERMINO_PROCESO = SYSDATE WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCarga);
            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Actualiza el estado del proceso de la carga
     * @param idCarga
     * @param estadoProceso
     * @return boolean
     */
    public boolean updateCargaArchivosEstadoProceso(Double idCarga, Integer estadoProceso, String observacion,
                                                    Double idCargaAsoc) {

        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "UPDATE REND_CARGA_DE_ARCHIVOS\n" + "SET ESTADO_PROCESO    = ?,\n" + "  OBSERVACION_PROCESO = ?,\n" +
            "  ID_CARGA_ASOC       = ?\n" + "WHERE ID_CARGA        = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setInt(1, estadoProceso.intValue());
            if (observacion != null && observacion.length() > 0) {
                stmt.setString(2, observacion);
            } else {
                stmt.setNull(2, OracleTypes.VARCHAR);
            }
            if (idCargaAsoc != null) {
                stmt.setDouble(3, idCargaAsoc);
            } else {
                stmt.setNull(3, OracleTypes.DOUBLE);
            }

            stmt.setDouble(4, idCarga);
            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Actualizada la cantidad de registros procesados
     * @param idCarga
     * @param cantidadProcesada
     * @return boolean
     */
    public boolean updateCargaArchivosCantidadProcesada(Double idCarga, Integer cantidadProcesada) {

        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "UPDATE REND_CARGA_DE_ARCHIVOS SET CANTIDAD_PROCESADA = ? WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setInt(1, cantidadProcesada.intValue());
            stmt.setDouble(2, idCarga);
            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Inserta el resumen del abono obtenido del archivo
     * @param resumenAbonoDTO
     * @param idCarga
     * @return boolean
     */
    public ResumenAbonoDTO insertResumenAbono(ResumenAbonoDTO resumenAbonoDTO, Double idCarga) {

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        resumenAbonoDTO.setProcesado(false);

        String query =
            "" + "INSERT \n" + "INTO REND_RESUMEN_ABONO\n" + "  (\n" + "    ID_CARGA,\n" + "    CODIGO_RECAUDADOR,\n" +
            "    GLOSA_RECAUDADOR,    \n" + "    TIPO_DE_REGISTRO,\n" + "    IDENTIFICADOR_IIP,\n" +
            "    TOTAL_MONTO_FONDO,\n" + "    TOTAL_MONTO_INSTITUCION,\n" + "    TOTAL_MONTO_AFC,\n" +
            "    CUENTA_FONDO,\n" + "    CUENTA_INSTITUCION,\n" + "    CUENTA_AFC,\n" + "    IDENTIFICACION_BANCO,\n" +
            "    CODIGO_BANCO,\n" + "    FECHA_ABONO,\n" + "    NUMERO_DE_PLANILLAS\n" + "  )\n" + "  VALUES\n" +
            "  ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            if (resumenAbonoDTO.getCodigo_recaudador() != null)
                stmt.setString(2, resumenAbonoDTO.getCodigo_recaudador());
            if (resumenAbonoDTO.getGlosa_recaudador() != null)
                stmt.setString(3, resumenAbonoDTO.getGlosa_recaudador());
            if (resumenAbonoDTO.getTipo_de_registro() != null)
                stmt.setInt(4, resumenAbonoDTO.getTipo_de_registro());
            if (resumenAbonoDTO.getIdentificador_iip() != null)
                stmt.setInt(5, resumenAbonoDTO.getIdentificador_iip());
            if (resumenAbonoDTO.getTotal_monto_fondo() != null)
                stmt.setDouble(6, resumenAbonoDTO.getTotal_monto_fondo());
            if (resumenAbonoDTO.getTotal_monto_institucion() != null)
                stmt.setDouble(7, resumenAbonoDTO.getTotal_monto_institucion());
            if (resumenAbonoDTO.getTotal_monto_afc() != null)
                stmt.setDouble(8, resumenAbonoDTO.getTotal_monto_afc());
            if (resumenAbonoDTO.getCuenta_fondo() != null)
                stmt.setString(9, resumenAbonoDTO.getCuenta_fondo());
            if (resumenAbonoDTO.getCuenta_institucion() != null)
                stmt.setString(10, resumenAbonoDTO.getCuenta_institucion());
            if (resumenAbonoDTO.getCuenta_afc() != null)
                stmt.setString(11, resumenAbonoDTO.getCuenta_afc());
            if (resumenAbonoDTO.getIdentificacion_banco() != null)
                stmt.setString(12, resumenAbonoDTO.getIdentificacion_banco());
            if (resumenAbonoDTO.getCodigo_banco() != null)
                stmt.setString(13, resumenAbonoDTO.getCodigo_banco());
            if (resumenAbonoDTO.getFecha_abono() != null)
                stmt.setDate(14, new java.sql.Date(resumenAbonoDTO.getFecha_abono().getTime()));
            if (resumenAbonoDTO.getNumero_de_planillas() != null)
                stmt.setInt(15, resumenAbonoDTO.getNumero_de_planillas());

            if (resumenAbonoDTO.getCodigo_recaudador() == null)
                stmt.setNull(2, OracleTypes.VARCHAR);
            if (resumenAbonoDTO.getGlosa_recaudador() == null)
                stmt.setNull(3, OracleTypes.VARCHAR);
            if (resumenAbonoDTO.getTipo_de_registro() == null)
                stmt.setNull(4, OracleTypes.NUMBER);
            if (resumenAbonoDTO.getIdentificador_iip() == null)
                stmt.setNull(5, OracleTypes.NUMBER);
            if (resumenAbonoDTO.getTotal_monto_fondo() == null)
                stmt.setNull(6, OracleTypes.DOUBLE);
            if (resumenAbonoDTO.getTotal_monto_institucion() == null)
                stmt.setNull(7, OracleTypes.DOUBLE);
            if (resumenAbonoDTO.getTotal_monto_afc() == null)
                stmt.setNull(8, OracleTypes.DOUBLE);
            if (resumenAbonoDTO.getCuenta_fondo() == null)
                stmt.setNull(9, OracleTypes.VARCHAR);
            if (resumenAbonoDTO.getCuenta_institucion() == null)
                stmt.setNull(10, OracleTypes.VARCHAR);
            if (resumenAbonoDTO.getCuenta_afc() == null)
                stmt.setNull(11, OracleTypes.VARCHAR);
            if (resumenAbonoDTO.getIdentificacion_banco() == null)
                stmt.setNull(12, OracleTypes.VARCHAR);
            if (resumenAbonoDTO.getCodigo_banco() == null)
                stmt.setNull(13, OracleTypes.VARCHAR);
            if (resumenAbonoDTO.getFecha_abono() == null)
                stmt.setNull(14, OracleTypes.DATE);
            if (resumenAbonoDTO.getNumero_de_planillas() == null)
                stmt.setNull(15, OracleTypes.NUMBER);

            stmt.execute();

            resumenAbonoDTO.setProcesado(true);


        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return resumenAbonoDTO;
    }

    /**
     * Inserta detalle del resumen del abono
     * @param resumenDTO
     * @param idCarga
     * @param inicio
     * @param procesar
     * @return boolean
     */
    public ResumenAbonoDTO insertDetalleAbono(ResumenAbonoDTO resumenDTO, Double idCarga, Integer inicio,
                                              Integer procesar,Integer tipoProceso)throws Exception {

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        resumenDTO.setProceso_detalle(false);

        String query =
            "" + "INSERT\n" + "INTO REND_ARCHIVO_ABONO\n" + "  (\n" + "    ID_CARGA,\n" + "    CODIGO_RECAUDADOR,\n" +
            "    ID_FOLIO_ABONO,\n" + "    TIPO_DE_REGISTRO,\n" + "    MONTO_ABONO_FONDO,\n" +
            "    MONTO_ABONO_INSTITUCION,\n" + "    MONTO_ABONO_AFC,\n" + "    RUT_EMPLEADOR,\n" +
            "    DIGITO_VERFICADOR,\n" + "    PERIODO,\n" + "    FECHA_PAGO,\n" + "    LOTE,\n" + "    SUCURSAL,\n" +
            "    ESTADO_PROCESO,\n" + "    ESTADO_PROCESO_EXCEPCION" + "  )\n" + "  VALUES\n" + "  (\n" +
            "    ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            for (int i = inicio.intValue();
                 (i < (procesar.intValue() + inicio.intValue())) && (i < resumenDTO.getArchivoabono().size()); i++) {
                Integer estadoProceso = Constant.PROCESO_PENDIENTE;
                String observacion = null;
                if (resumenDTO.getArchivoabono().get(i).getId_folio_abono() != null) {
                    // Verifica si el folio fue procesado anteriormente
                    CargaArchivosDTO carga =getCargaArchivoByIdFolioAbono(resumenDTO.getArchivoabono().get(i), Constant.PROCESO_FINALIZADO_);
                    
                    if (carga != null) {
                        estadoProceso = Constant.PROCESO_PROBLEMA_RESUMEN_ABONO;
                        observacion =
                            "El folio " + resumenDTO.getArchivoabono().get(i).getId_folio_abono().longValue() +
                            " fue procesado anteriormente en el archivo " + carga.getNombre_fisico_archivo();
                        logger.error("El folio " + resumenDTO.getArchivoabono().get(i).getId_folio_abono().longValue()+" fue procesado anteriormente en el archivo " + carga.getNombre_fisico_archivo());
                        if(tipoProceso != null && 1 == tipoProceso){
                            logger.error("Entro al throw del folio");
                            throw new Exception("El folio " + resumenDTO.getArchivoabono().get(i).getId_folio_abono().longValue()+" fue procesado anteriormente en el archivo " + carga.getNombre_fisico_archivo());
                        }
                    }

                    stmt.setDouble(1, idCarga);
                    if (resumenDTO.getCodigo_recaudador() != null)
                        stmt.setString(2, resumenDTO.getCodigo_recaudador());
                    if (resumenDTO.getArchivoabono().get(i).getId_folio_abono() != null){
                        
                        //stmt.setDouble(3, nuevoFolio);
                        stmt.setDouble(3, resumenDTO.getArchivoabono().get(i).getId_folio_abono());
                    }
                    if (resumenDTO.getArchivoabono().get(i).getTipo_de_registro() != null)
                        stmt.setInt(4, resumenDTO.getArchivoabono().get(i).getTipo_de_registro());
                    if (resumenDTO.getArchivoabono().get(i).getMonto_abono_fondo() != null)
                        stmt.setDouble(5, resumenDTO.getArchivoabono().get(i).getMonto_abono_fondo());
                    if (resumenDTO.getArchivoabono().get(i).getMonto_abono_institucion() != null)
                        stmt.setDouble(6, resumenDTO.getArchivoabono().get(i).getMonto_abono_institucion());
                    if (resumenDTO.getArchivoabono().get(i).getMonto_abono_afc() != null)
                        stmt.setDouble(7, resumenDTO.getArchivoabono().get(i).getMonto_abono_afc());
                    if (resumenDTO.getArchivoabono().get(i).getRut_empleador() != null)
                        stmt.setInt(8, resumenDTO.getArchivoabono().get(i).getRut_empleador());
                    if (resumenDTO.getArchivoabono().get(i).getDigito_verficador() != null)
                        stmt.setString(9, resumenDTO.getArchivoabono().get(i).getDigito_verficador());
                    if (resumenDTO.getArchivoabono().get(i).getPeriodo() != null)
                        stmt.setDate(10, new java.sql.Date(resumenDTO.getArchivoabono().get(i).getPeriodo().getTime()));
                    if (resumenDTO.getArchivoabono().get(i).getFecha_pago() != null)
                        stmt.setDate(11,
                                     new java.sql.Date(resumenDTO.getArchivoabono().get(i).getFecha_pago().getTime()));
                    if (resumenDTO.getArchivoabono().get(i).getLote() != null)
                        stmt.setString(12, resumenDTO.getArchivoabono().get(i).getLote());
                    if (resumenDTO.getArchivoabono().get(i).getSucursal() != null)
                        stmt.setString(13, resumenDTO.getArchivoabono().get(i).getSucursal());
                    if (estadoProceso != null)
                        stmt.setInt(14, estadoProceso);
                    if (observacion != null)
                        stmt.setString(15, observacion);

                    if (resumenDTO.getCodigo_recaudador() == null)
                        stmt.setNull(2, OracleTypes.VARCHAR);
                    if (resumenDTO.getArchivoabono().get(i).getId_folio_abono() == null)
                        stmt.setNull(3, OracleTypes.DOUBLE);
                    if (resumenDTO.getArchivoabono().get(i).getTipo_de_registro() == null)
                        stmt.setNull(4, OracleTypes.NUMBER);
                    if (resumenDTO.getArchivoabono().get(i).getMonto_abono_fondo() == null)
                        stmt.setNull(5, OracleTypes.DOUBLE);
                    if (resumenDTO.getArchivoabono().get(i).getMonto_abono_institucion() == null)
                        stmt.setNull(6, OracleTypes.DOUBLE);
                    if (resumenDTO.getArchivoabono().get(i).getMonto_abono_afc() == null)
                        stmt.setNull(7, OracleTypes.DOUBLE);
                    if (resumenDTO.getArchivoabono().get(i).getRut_empleador() == null)
                        stmt.setNull(8, OracleTypes.NUMBER);
                    if (resumenDTO.getArchivoabono().get(i).getDigito_verficador() == null)
                        stmt.setNull(9, OracleTypes.VARCHAR);
                    if (resumenDTO.getArchivoabono().get(i).getPeriodo() == null)
                        stmt.setNull(10, OracleTypes.DATE);
                    if (resumenDTO.getArchivoabono().get(i).getFecha_pago() == null)
                        stmt.setNull(11, OracleTypes.DATE);
                    if (resumenDTO.getArchivoabono().get(i).getLote() == null)
                        stmt.setNull(12, OracleTypes.VARCHAR);
                    if (resumenDTO.getArchivoabono().get(i).getSucursal() == null)
                        stmt.setNull(13, OracleTypes.VARCHAR);
                    if (estadoProceso == null)
                        stmt.setNull(14, OracleTypes.NUMBER);
                    if (observacion == null)
                        stmt.setNull(15, OracleTypes.VARCHAR);

                    stmt.execute();
                    resumenDTO.getArchivoabono().get(i).setProcesado(true);
                    resumenDTO.setNumero_detalle_procesados(i);
                }
            }

            resumenDTO.setProceso_detalle(true);

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return resumenDTO;
    }

    /**
     * Inserta la carga de archivos
     * @param cargaArchivosDTO
     * @return CargaArchivosDTO
     */
    public CargaArchivosDTO insertCargaArchivos(CargaArchivosDTO cargaArchivosDTO) {

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        cargaArchivosDTO.setProcesado(false);

        String query =
            "" + "INSERT\n" + "INTO REND_CARGA_DE_ARCHIVOS\n" + "  (\n" + "    ID_CARGA,\n" +
            "    NUMERO_DE_CONVENIO,\n" + "    FECHA_CREACION,\n" + "    ID_TIPO_ARCHIVO,\n" + "    ESTADO_PROCESO,\n" +
            "    FECHA_Y_HORA_UPLOAD,\n" + "    NOMBRE_FISICO_ARCHIVO,\n" + "  REMOTE_USER)\n" + "  VALUES\n" +
            "  ( ?, ?, ?, ?, ?, ?, ? , ?)";

        try {
            System.out.println("antes de entrar");
            if (!existCarga(cargaArchivosDTO)) {
                System.out.println("entro");
                Integer idCarga = getIdCarga();
                System.out.println("ID_CARGA " + idCarga);
                
                if (idCarga != null && idCarga > 0) {
                    conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
                    stmt = conn.prepareCall(query);

                    stmt.setDouble(1, idCarga.doubleValue());

                    if (cargaArchivosDTO.getConvenio().getNumero_de_convenio() != null)
                        stmt.setString(2, cargaArchivosDTO.getConvenio().getNumero_de_convenio());
                    if (cargaArchivosDTO.getFecha_creacion() != null)
                        stmt.setTimestamp(3, new Timestamp(cargaArchivosDTO.getFecha_creacion().getTime()));
                    if (cargaArchivosDTO.getId_tipo_archivo() != null)
                        stmt.setInt(4, cargaArchivosDTO.getId_tipo_archivo());
                    if (cargaArchivosDTO.getEstado_proceso() != null)
                        stmt.setInt(5, cargaArchivosDTO.getEstado_proceso());
                    if (cargaArchivosDTO.getFecha_y_hora_upload() != null)
                        stmt.setTimestamp(6, new Timestamp(cargaArchivosDTO.getFecha_y_hora_upload().getTime()));
                    if (cargaArchivosDTO.getNombre_fisico_archivo() != null)
                        stmt.setString(7, cargaArchivosDTO.getNombre_fisico_archivo());
                    if (cargaArchivosDTO.getRemote_user() != null)
                        stmt.setString(8, cargaArchivosDTO.getRemote_user());

                    if (cargaArchivosDTO.getConvenio() == null)
                        stmt.setNull(2, OracleTypes.VARCHAR);
                    if (cargaArchivosDTO.getFecha_creacion() == null)
                        stmt.setNull(3, OracleTypes.TIMESTAMP);
                    if (cargaArchivosDTO.getId_tipo_archivo() == null)
                        stmt.setNull(4, OracleTypes.NUMBER);
                    if (cargaArchivosDTO.getEstado_proceso() == null)
                        stmt.setNull(5, OracleTypes.NUMBER);
                    if (cargaArchivosDTO.getFecha_y_hora_upload() == null)
                        stmt.setNull(6, OracleTypes.TIMESTAMP);
                    if (cargaArchivosDTO.getNombre_fisico_archivo() == null)
                        stmt.setNull(7, OracleTypes.VARCHAR);
                    if (cargaArchivosDTO.getRemote_user() == null)
                        stmt.setNull(8, OracleTypes.VARCHAR);

                    stmt.execute();
                    cargaArchivosDTO.setProcesado(true);
                    cargaArchivosDTO.setId_carga(idCarga.doubleValue());
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return cargaArchivosDTO;
    }

    /**
     * Borra el detalle y el resumen del  Abono
     * @param idCarga
     * @return boolean
     */
    public boolean deleteDetalleAbono(Double idCarga) {
        boolean result = false;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "DELETE REND_ARCHIVO_ABONO WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.execute();
            result = true;
        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        if (result) {
            result = deleteResumenAbono(idCarga);
        }

        return result;
    }

    /**
     * Borra el solo el resumen del Abono
     * @param idCarga
     * @return boolean
     */
    public boolean deleteResumenAbono(Double idCarga) {
        boolean result = false;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "DELETE REND_RESUMEN_ABONO WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.execute();
            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return result;
    }

    /**
     * Inserta el detalle de la planilla
     * @param headerDTO
     * @param idCarga
     * @param inicio
     * @param procesar
     * @return boolean
     */
    public ArchivoPlanillaHeaderDTO insertArchivoPlanilla(ArchivoPlanillaHeaderDTO headerDTO, Double idCarga,
                                                          Integer inicio, Integer procesar) {

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        headerDTO.setProceso_detalle(false);

        String query =
            "" + "INSERT \n" + "INTO REND_ARCHIVO_PLANILLA_ENCAB\n" + "  (\n" + "    ID_CARGA,\n" +
            "    ID_FOLIO_PLANILLA,\n" + "    TIPO_DE_REGISTRO,\n" + "    TIPO_PAGO,\n" +
            "    FECHA_HORA_OPERACION,\n" + "    CODIGO_INSTITUCION_APV,\n" + "    RUT_PAGADOR,\n" +
            "    DV_PAGADOR,\n" + "    TIPO_PAGADOR,\n" + "    CORREO_PAGADOR,\n" + "    ESTADO_PROCESO,\n" +
            "    ESTADO_PROCESO_EXCEPTION,\n" + "    ID_CARGAABONO,\n" + "    POLIZA_ASOC,\n" + "    CONCEPTO_PAGO,\n" +
            "    FECHA_PAGO_CAJA\n" + "  )\n" + "  VALUES\n" +
            "  ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE ) ";

        try {


            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            //CargaArchivosDTO cargaArchivosAbonoDTO = null;
            for (int i = (inicio.intValue());
                 (i < (procesar.intValue() + inicio.intValue())) && (i < headerDTO.getLst_detalle_planilla().size());
                 i++) {
                Integer estadoProceso = Constant.PROCESO_PENDIENTE;
                String observacion = null;


                /* ****************************************
                 * Inicio proceso de verificaci�n y de pago en caja
                 * ****************************************
                 */
                try {
                    if (headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla() != null) {
                        /*
                         * Validacion proceso anterior
                         */
                        CargaArchivosDTO cargaArchivosDTO =
                         getCargaArchivoByIdFolioPlanilla(headerDTO.getLst_detalle_planilla().get(i),headerDTO.getLst_detalle_planilla().get(i).getDetalle().get(i).getPeriodo_pago());
                        if (cargaArchivosDTO != null) {
                            estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                            observacion =
                                "El folio " +
                                headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() +
                                " fue procesado anteriormente en el archivo " +
                                cargaArchivosDTO.getNombre_fisico_archivo();
                        } else {
                            // Obtiene la carga de la planilla para comparar mas adelante el medio de pago
                            CargaArchivosDTO cargaArchivosPlanillaDTO = getCargaArchivosByIdCarga(idCarga);
                            /*
                             * Verfica existencia de folio abono autorizado
                             */
                            CargaArchivosDTO cargaArchivosAbonoDTO =
                                null; //getCargaArchivoByIdFolioAbono(headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla(), constantes.PROCESO_ABONO_AUTORIZADO, constantes.PROCESO_FINALIZADO_AUTORIZACION);
                            if (cargaArchivosAbonoDTO != null) {
                                /*
                                 * Si el abono autorizado existe se comparan los medios de pago de la planilla y del abono
                                 */
                                if (cargaArchivosPlanillaDTO.getConvenio().getId_medio_pago().intValue() ==
                                    cargaArchivosAbonoDTO.getConvenio().getId_medio_pago().intValue()) {
                                 /*
                                     * Si los medios coinciden se indica el ID de carga del abono
                                     */
                                    headerDTO.setIdCargaAbono(cargaArchivosAbonoDTO.getId_carga());
                                    //updateEstadoPorcesoFolioResumenByIdCarga(cargaArchivosAbonoDTO.getId_carga(), constantes.PROCESO_PENDIENTE, constantes.PROCESO_PROBLEMA_FOLIO_ABONO, "El folio abono no se encuentra en el detalle");
                                    /*
                                     * Se obtiene el detalle del abono para realizar el pago en el sistema de cajas
                                     */
                                    ArchivoAbonoDTO archivoAbonoDTO =
                                        getDetalleAbonoByIdCargaByIdFolio(cargaArchivosAbonoDTO.getId_carga(),
                                                                          headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                                    if (archivoAbonoDTO != null) {
                                        //Verifica si el rut cliente es cliente BiceVida
                                        //cl.bicevida.previred.proxy.SessionEJBDatosClienteClient myPort = new cl.bicevida.previred.proxy.SessionEJBDatosClienteClient();
                                        //myPort.setEndpoint(ResourceBundleUtil.getProperty("cl.bicevida.ws.datos.usuario"));
                                        /*ResponseVODTO responseVODTO = myPort.getDataClienteBDPPublico(headerDTO.getLst_detalle_planilla().get(i).getDetalle().getRut_trabajador().toString());
                                        if(responseVODTO.isExito()){
                                            // Es Cliente BiceVida
                                            // llamada a packages

                                            ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO = callGeneraTransaccion(headerDTO.getLst_detalle_planilla().get(i), true);
                                            headerDTO.getLst_detalle_planilla().set(i, archivoPlanillaEncabezadoDTO);

                                            //String result = callGeneraTransaccion(headerDTO.getLst_detalle_planilla().get(i).getDetalle().getRut_trabajador(), headerDTO.getLst_detalle_planilla().get(i).getResumen().getTotal_pagar_ia().longValue(), FechaUtil.getFechaFormateoCustom(headerDTO.getLst_detalle_planilla().get(i).getResumen().getPeriodo_pago(), "yyyyMM"), headerDTO.getLst_detalle_planilla().get(i).getFecha_hora_operacion(), archivoAbonoDTO.getCod_recaudador(), true);

                                            if(headerDTO.getLst_detalle_planilla().get(i).getCodResultPago().intValue() == 0){
                                                updateEstadoPorcesoFolioResumen(cargaArchivosAbonoDTO.getId_carga(), headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla(), constantes.PROCESO_AUTORIZADO, "Procesado Correctamente");

                                                estadoProceso = constantes.PROCESO_AUTORIZADO;
                                                observacion = "Procesado Correctamente";
                                            }else{
                                                estadoProceso = constantes.PROCESO_PROBLEMA_PAGO_CAJA;
                                                observacion = "El folio " + headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() + " no fue procesado correctamente por proceso de CAJA: " + headerDTO.getLst_detalle_planilla().get(i).getDecsResultPago();
                                            }

                                        }else{
                                            ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO = callGeneraTransaccion(headerDTO.getLst_detalle_planilla().get(i), false);
                                            headerDTO.getLst_detalle_planilla().set(i, archivoPlanillaEncabezadoDTO);
                                            if(headerDTO.getLst_detalle_planilla().get(i).getCodResultPago().intValue() == 0){
                                                updateEstadoPorcesoFolioResumen(cargaArchivosAbonoDTO.getId_carga(), headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla(), constantes.PROCESO_AUTORIZADO, "Procesado Correctamente");

                                                estadoProceso = constantes.PROCESO_AUTORIZADO;
                                                observacion = "Procesado Correctamente";
                                            }else{
                                                estadoProceso = constantes.PROCESO_PROBLEMA_PAGO_CAJA;
                                                observacion = "El folio " + headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() + " no fue procesado correctamente por proceso de CAJA: " + headerDTO.getLst_detalle_planilla().get(i).getDecsResultPago();
                                            }
                                        }*/



                                    } else {
                                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                        observacion =
                                            "El folio " +
                                            headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() +
                                            " no tiene presenta entidad recaudadora";
                                    }


                                } else {
                                    estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                    observacion =
                                        "El folio " +
                                        headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() +
                                        " tiene medio de pago (" +
                                        cargaArchivosPlanillaDTO.getConvenio().getDescripcion_medio_pago() +
                                        ") distinto al del informado en el abono (" +
                                        cargaArchivosAbonoDTO.getConvenio().getDescripcion_medio_pago() + ")";
                                }
                            } else {
                                /*
                                 * Verfica existencia de abono autorizado con observaciones
                                 */
                                //cargaArchivosAbonoDTO = getCargaArchivoByIdFolioAbono(headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla(), constantes.PROCESO_ABONO_AUTORIZADO_OBS, constantes.PROCESO_FINALIZADO_AUTORIZACION);
                                if (cargaArchivosAbonoDTO != null) {
                                    /*
                                     * Si el abono autorizado con observaciones existe se comparan los medios de pago de la planilla y del abono
                                     */
                                    if (cargaArchivosPlanillaDTO.getConvenio().getId_medio_pago().intValue() ==
                                        cargaArchivosAbonoDTO.getConvenio().getId_medio_pago().intValue()) {
                                     /*
                                         * Si los medios coinciden se indica el ID de carga del abono
                                         */
                                        headerDTO.setIdCargaAbono(cargaArchivosAbonoDTO.getId_carga());
                                        //updateEstadoPorcesoFolioResumenByIdCarga(cargaArchivosAbonoDTO.getId_carga(), constantes.PROCESO_PENDIENTE, constantes.PROCESO_PROBLEMA_FOLIO_ABONO, "El folio abono no se encuentra en el detalle");
                                        /*
                                         * Se obtiene el detalle del abono para realizar el pago en el sistema de cajas
                                         */
                                        ArchivoAbonoDTO archivoAbonoDTO =
                                            getDetalleAbonoByIdCargaByIdFolio(cargaArchivosAbonoDTO.getId_carga(),
                                                                              headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                                        if (archivoAbonoDTO != null) {
                                            //Verifica si el rut cliente es cliente BiceVida
                                            //cl.bicevida.previred.proxy.SessionEJBDatosClienteClient myPort = new cl.bicevida.previred.proxy.SessionEJBDatosClienteClient();
                                            //myPort.setEndpoint(ResourceBundleUtil.getProperty("cl.bicevida.ws.datos.usuario"));
                                            /*ResponseVODTO responseVODTO = myPort.getDataClienteBDPPublico(headerDTO.getLst_detalle_planilla().get(i).getDetalle().getRut_trabajador().toString());
                                             if(responseVODTO.isExito()){
                                                 // Es Cliente BiceVida
                                                 // llamada a packages
                                                 //String result = callGeneraTransaccion(headerDTO.getLst_detalle_planilla().get(i).getDetalle().getRut_trabajador(), headerDTO.getLst_detalle_planilla().get(i).getResumen().getTotal_pagar_ia().longValue(), FechaUtil.getFechaFormateoCustom(headerDTO.getLst_detalle_planilla().get(i).getResumen().getPeriodo_pago(), "yyyyMM"), headerDTO.getLst_detalle_planilla().get(i).getFecha_hora_operacion(), archivoAbonoDTO.getCod_recaudador(), true);
                                                 ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO = callGeneraTransaccion(headerDTO.getLst_detalle_planilla().get(i), true);
                                                 headerDTO.getLst_detalle_planilla().set(i, archivoPlanillaEncabezadoDTO);

                                                     if(headerDTO.getLst_detalle_planilla().get(i).getCodResultPago().intValue() == 0){
                                                     updateEstadoPorcesoFolioResumen(cargaArchivosAbonoDTO.getId_carga(), headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla(), constantes.PROCESO_AUTORIZADO, "Procesado Correctamente");

                                                     estadoProceso = constantes.PROCESO_AUTORIZADO;
                                                     observacion = "Procesado Correctamente";
                                                 }else{
                                                     estadoProceso = constantes.PROCESO_PROBLEMA_PAGO_CAJA;
                                                     observacion = "El folio " + headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() + " no fue procesado correctamente por proceso de CAJA: " + headerDTO.getLst_detalle_planilla().get(i).getDecsResultPago();
                                                 }

                                             }else{
                                                 ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO = callGeneraTransaccion(headerDTO.getLst_detalle_planilla().get(i), false);
                                                 headerDTO.getLst_detalle_planilla().set(i, archivoPlanillaEncabezadoDTO);
                                                 //String result = callGeneraTransaccion(headerDTO.getLst_detalle_planilla().get(i).getDetalle().getRut_trabajador(), headerDTO.getLst_detalle_planilla().get(i).getResumen().getTotal_pagar_ia().longValue(), FechaUtil.getFechaFormateoCustom(headerDTO.getLst_detalle_planilla().get(i).getResumen().getPeriodo_pago(), "yyyyMM"), headerDTO.getLst_detalle_planilla().get(i).getFecha_hora_operacion(), archivoAbonoDTO.getCod_recaudador(), false);
                                                 if(headerDTO.getLst_detalle_planilla().get(i).getCodResultPago().intValue() == 0){
                                                     updateEstadoPorcesoFolioResumen(cargaArchivosAbonoDTO.getId_carga(), headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla(), constantes.PROCESO_AUTORIZADO, "Procesado Correctamente");

                                                     estadoProceso = constantes.PROCESO_AUTORIZADO;
                                                     observacion = "Procesado Correctamente";
                                                 }else{
                                                     estadoProceso = constantes.PROCESO_PROBLEMA_PAGO_CAJA;
                                                     observacion = "El folio " + headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() + " no fue procesado correctamente por proceso de CAJA: " + headerDTO.getLst_detalle_planilla().get(i).getDecsResultPago();
                                                 }
                                             }*/



                                        } else {
                                            estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                            observacion =
                                                "El folio " +
                                                headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() +
                                                " tiene presenta entidad recaudadora";
                                        }
                                    } else {
                                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                        observacion =
                                            "El folio " +
                                            headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() +
                                            " tiene medio de pago (" +
                                            cargaArchivosPlanillaDTO.getConvenio().getDescripcion_medio_pago() +
                                            ") distinto al del informado en el abono (" +
                                            cargaArchivosAbonoDTO.getConvenio().getDescripcion_medio_pago() + ")";
                                    }
                                } else {
                                    //cargaArchivosAbonoDTO = getCargaArchivoByIdFolioAbono(headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla(), constantes.PROCESO_ABONO_RECHAZADO, constantes.PROCESO_FINALIZADO_AUTORIZACION);
                                    // verfica existencia de abono rechazado
                                    if (cargaArchivosAbonoDTO != null) {
                                        /*
                                         * Si los medios coinciden se indica el ID de carga del abono
                                         */
                                        headerDTO.setIdCargaAbono(cargaArchivosAbonoDTO.getId_carga());
                                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                        observacion =
                                            "El folio " +
                                            headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() +
                                            " se encuentra como abono rechazado en el proceso del archivo " +
                                            cargaArchivosAbonoDTO.getNombre_fisico_archivo();
                                    } else {
                                        estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                        observacion =
                                            "El folio " +
                                            headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() +
                                            " no se encuentra abonado";
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    logger.error("Se ha producido un excepcion: ", e);
                    estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                    observacion =
                        "El proceso del folio " +
                        headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla().longValue() +
                        " present� un problema: " + e.getMessage();
                }

                /* ****************************************
                 * Fin proceso de verificaci�n y de pago en caja
                 * ****************************************
                 */


                stmt.setDouble(1, idCarga);
                if (headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla() != null)
                    stmt.setDouble(2, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_de_registro() != null)
                    stmt.setInt(3, headerDTO.getLst_detalle_planilla().get(i).getTipo_de_registro());
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_pago() != null)
                    stmt.setInt(4, headerDTO.getLst_detalle_planilla().get(i).getTipo_pago());
                if (headerDTO.getLst_detalle_planilla().get(i).getFecha_hora_operacion() != null)
                    stmt.setDate(5,
                                 new java.sql.Date(headerDTO.getLst_detalle_planilla().get(i).getFecha_hora_operacion().getTime()));
                if (headerDTO.getLst_detalle_planilla().get(i).getCodigo_institucion_apv() != null)
                    stmt.setInt(6, headerDTO.getLst_detalle_planilla().get(i).getCodigo_institucion_apv());
                if (headerDTO.getLst_detalle_planilla().get(i).getRut_pagador() != null)
                    stmt.setInt(7, headerDTO.getLst_detalle_planilla().get(i).getRut_pagador());
                if (headerDTO.getLst_detalle_planilla().get(i).getDv_pagador() != null)
                    stmt.setString(8, headerDTO.getLst_detalle_planilla().get(i).getDv_pagador());
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_pagador() != null)
                    stmt.setInt(9, headerDTO.getLst_detalle_planilla().get(i).getTipo_pagador());
                if (headerDTO.getLst_detalle_planilla().get(i).getCorreo_pagador() != null)
                    stmt.setString(10, headerDTO.getLst_detalle_planilla().get(i).getCorreo_pagador());
                if (estadoProceso != null)
                    stmt.setInt(11, estadoProceso);
                if (observacion != null)
                    stmt.setString(12, observacion);
                if (headerDTO.getIdCargaAbono() != null)
                    stmt.setDouble(13, headerDTO.getIdCargaAbono());
                if (headerDTO.getLst_detalle_planilla().get(i).getPolizaAsociada() != null)
                    stmt.setInt(14, headerDTO.getLst_detalle_planilla().get(i).getPolizaAsociada());
                if (headerDTO.getLst_detalle_planilla().get(i).getConceptoRecaudacion() != null)
                    stmt.setString(15, headerDTO.getLst_detalle_planilla().get(i).getConceptoRecaudacion());

                //stmt.setInt(11, new Integer("2")); // No procesado
                //stmt.setString(12, "No se encuentra en archivo de abono");

                if (headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla() == null)
                    stmt.setNull(2, OracleTypes.DOUBLE);
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_de_registro() == null)
                    stmt.setNull(3, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_pago() == null)
                    stmt.setNull(4, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getFecha_hora_operacion() == null)
                    stmt.setNull(5, OracleTypes.DATE);
                if (headerDTO.getLst_detalle_planilla().get(i).getCodigo_institucion_apv() == null)
                    stmt.setNull(6, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getRut_pagador() == null)
                    stmt.setNull(7, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getDv_pagador() == null)
                    stmt.setNull(8, OracleTypes.VARCHAR);
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_pagador() == null)
                    stmt.setNull(9, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getCorreo_pagador() == null)
                    stmt.setNull(10, OracleTypes.VARCHAR);
                if (estadoProceso == null)
                    stmt.setNull(11, OracleTypes.NUMBER);
                if (observacion == null)
                    stmt.setNull(12, OracleTypes.VARCHAR);
                if (headerDTO.getIdCargaAbono() == null)
                    stmt.setNull(13, OracleTypes.DOUBLE);
                if (headerDTO.getLst_detalle_planilla().get(i).getPolizaAsociada() == null)
                    stmt.setNull(14, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getConceptoRecaudacion() == null)
                    stmt.setNull(15, OracleTypes.VARCHAR);

                stmt.execute();

                headerDTO.getLst_detalle_planilla().get(i).setProcesado(true);
                headerDTO.setNumero_detalle_procesados(i);

                if (headerDTO.getLst_detalle_planilla().get(i).isProcesado()) {
                    ArchivoPlanillaAntecedentesDTO antec =
                        insertAntecedentesPlanilla(headerDTO.getLst_detalle_planilla().get(i).getAntecedentes(),
                                                   idCarga,
                                                   headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                    if (antec.isProcesado()) {
                        /*ArchivoPlanillaDetalleDTO detalle = insertDetallePlanilla(headerDTO.getLst_detalle_planilla().get(i).getDetalle(), idCarga, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                        if(detalle.isProcesado()){
                            ArchivoPlanillaResumenDTO resumen = insertResumenPlanilla(headerDTO.getLst_detalle_planilla().get(i).getResumen(), idCarga, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                            if(!resumen.isProcesado()){
                                throw new Exception("No se proceso correctamente el resumen de la planilla");
                            }
                        }else{
                            throw new Exception("No se proceso correctamente el detalle de la planilla");
                        }*/
                    } else {
                        throw new Exception("No se proceso correctamente el antecedente de la planilla");
                    }
                }
            }

            headerDTO.setProceso_detalle(true);

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return headerDTO;
    }

    /**
     * Inserta los antecedentes de la planilla
     * @param antecedentesDTO
     * @param idCarga
     * @param id_folio_planilla
     * @return boolean
     */
    public ArchivoPlanillaAntecedentesDTO insertAntecedentesPlanilla(ArchivoPlanillaAntecedentesDTO antecedentesDTO,
                                                                     Double idCarga, Double id_folio_planilla) {

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        antecedentesDTO.setProcesado(false);

        String query =
            "" + "INSERT\n" + "INTO REND_ARCHIVO_PLANILLA_ANTEC\n" + "  (\n" + "    ID_CARGA,\n" +
            "    ID_FOLIO_PLANILLA,\n" + "    TIPO_DE_REGISTRO,\n" + "    RAZON_SOCIAL_PAGADOR,\n" +
            "    CODIGO_ACTIVIDAD_PAGADOR,\n" + "    CALLE_PAGADOR,\n" + "    NUMERO_PAGADOR,\n" +
            "    DEPART_POBLA_PAGADOR,\n" + "    COMUNA_PAGADOR,\n" + "    CIUDAD_PAGADOR,\n" +
            "    REGION_PAGADOR,\n" + "    TELEFONO_PAGADOR,\n" + "    NOMBRE_REPRESENTANTE_LEGAL,\n" +
            "    RUT_REPRESENTANTE_LEGAL,\n" + "    DV_REPRESENTANTE_LEGAL\n" + "  )\n" + "  VALUES\n" +
            "  ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            if (id_folio_planilla != null)
                stmt.setDouble(2, id_folio_planilla);
            if (antecedentesDTO.getTipo_de_registro() != null)
                stmt.setInt(3, antecedentesDTO.getTipo_de_registro());
            if (antecedentesDTO.getRazon_social_pagador() != null)
                stmt.setString(4, antecedentesDTO.getRazon_social_pagador());
            if (antecedentesDTO.getCodigo_actividad_pagador() != null)
                stmt.setString(5, antecedentesDTO.getCodigo_actividad_pagador());
            if (antecedentesDTO.getCalle_pagador() != null)
                stmt.setString(6, antecedentesDTO.getCalle_pagador());
            if (antecedentesDTO.getNumero_pagador() != null)
                stmt.setString(7, antecedentesDTO.getNumero_pagador());
            if (antecedentesDTO.getDepart_pobla_pagador() != null)
                stmt.setString(8, antecedentesDTO.getDepart_pobla_pagador());
            if (antecedentesDTO.getComuna_pagador() != null)
                stmt.setString(9, antecedentesDTO.getComuna_pagador());
            if (antecedentesDTO.getCiudad_pagador() != null)
                stmt.setString(10, antecedentesDTO.getCiudad_pagador());
            if (antecedentesDTO.getRegion_pagador() != null)
                stmt.setString(11, antecedentesDTO.getRegion_pagador());
            if (antecedentesDTO.getTelefono_pagador() != null)
                stmt.setString(12, antecedentesDTO.getTelefono_pagador());
            if (antecedentesDTO.getNombre_representante_legal() != null)
                stmt.setString(13, antecedentesDTO.getNombre_representante_legal());
            if (antecedentesDTO.getRut_representante_legal() != null)
                stmt.setInt(14, antecedentesDTO.getRut_representante_legal());
            if (antecedentesDTO.getDv_representante_legal() != null)
                stmt.setString(15, antecedentesDTO.getDv_representante_legal());


            if (id_folio_planilla == null)
                stmt.setNull(2, OracleTypes.DOUBLE);
            if (antecedentesDTO.getTipo_de_registro() == null)
                stmt.setNull(3, OracleTypes.NUMBER);
            if (antecedentesDTO.getRazon_social_pagador() == null)
                stmt.setNull(4, OracleTypes.VARCHAR);
            if (antecedentesDTO.getCodigo_actividad_pagador() == null)
                stmt.setNull(5, OracleTypes.VARCHAR);
            if (antecedentesDTO.getCalle_pagador() == null)
                stmt.setNull(6, OracleTypes.VARCHAR);
            if (antecedentesDTO.getNumero_pagador() == null)
                stmt.setNull(7, OracleTypes.VARCHAR);
            if (antecedentesDTO.getDepart_pobla_pagador() == null)
                stmt.setNull(8, OracleTypes.VARCHAR);
            if (antecedentesDTO.getComuna_pagador() == null)
                stmt.setNull(9, OracleTypes.VARCHAR);
            if (antecedentesDTO.getCiudad_pagador() == null)
                stmt.setNull(10, OracleTypes.VARCHAR);
            if (antecedentesDTO.getRegion_pagador() == null)
                stmt.setNull(11, OracleTypes.VARCHAR);
            if (antecedentesDTO.getTelefono_pagador() == null)
                stmt.setNull(12, OracleTypes.VARCHAR);
            if (antecedentesDTO.getNombre_representante_legal() == null)
                stmt.setNull(13, OracleTypes.VARCHAR);
            if (antecedentesDTO.getRut_representante_legal() == null)
                stmt.setNull(14, OracleTypes.NUMBER);
            if (antecedentesDTO.getDv_representante_legal() == null)
                stmt.setNull(15, OracleTypes.VARCHAR);

            stmt.execute();
            antecedentesDTO.setProcesado(true);

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return antecedentesDTO;
    }

    /**
     * Inserta los datos del detalle del archivo de Planilla
     * @param lstdetalleDTO
     * @param idCarga
     * @param id_folio_planilla
     * @return boolean
     */
    public List<ArchivoPlanillaDetalleDTO> insertDetallePlanilla(List<ArchivoPlanillaDetalleDTO> lstdetalleDTO,
                                                                 Double idCarga, Double id_folio_planilla) {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "INSERT\n" + "INTO REND_ARCHIVO_PLANILLA_DETALLE\n" + "  (\n" + "    ID_CARGA,\n" +
            "    ID_FOLIO_PLANILLA,\n" + //PK
            "    TIPO_DE_REGISTRO,\n" + "    RUT_TRABAJADOR,\n" + //PK
            "    DV_TRABAJADOR,\n" + "    APELLIDO_PATERNO_TRABAJADOR,\n" + "    APELLIDO_MATERNO_TRABAJADOR,\n" +
            "    NOMBRES_TRABAJADOR,\n" + "    SEXO,\n" + "    NACIONALIDAD,\n" + "    RENTA_IMPONIBLE_TRABAJADOR,\n" +
            "    COTIZACION_VOLUNTARIA_APVI,\n" + "    COTIZACION_VOLUNTARIA_APVI_B,\n" + "    NRO_CONTRATO_APVI,\n" +
            "    DEPOSITO_CONVENIO,\n" + "    TOTAL_A_PAGAR,\n" + "    PERIODO_PAGO,\n" + //PK
            "    APV_COLECTIVO_EMPL,\n" + "    APV_COLECTIVO_TRABAJ,\n" + "    N_CONTRATO_APVC,\n" +
            "    CODIGO_MOV_PERSONAL,\n" + "    FECHA_INICIO,\n" + "    FECHA_TERMINO,\n" +
            "    ESTADO_PROCESO_CAJA,\n" + "    ESTADO_PROCESO_CAJA_EXC,\n" + "    POLIZA_ASOC,\n" +
            "    CONCEPTO_PAGO,\n" + //PK
            "    FECHA_PAGO_CAJA,\n" + "    ID_FOLIO_PAGOCAJA,\n" + "    SECUENCIA\n" + "  )\n" + "  VALUES\n" +
            "  ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, SYSDATE, ?, ? ) ";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);


            for (int i = 0; i < lstdetalleDTO.size(); i++) {
                lstdetalleDTO.get(i).setProcesado(true);

                stmt.setDouble(1, idCarga);
                if (id_folio_planilla != null)
                    stmt.setDouble(2, id_folio_planilla);
                if (lstdetalleDTO.get(i).getTipo_de_registro() != null)
                    stmt.setInt(3, lstdetalleDTO.get(i).getTipo_de_registro());
                if (lstdetalleDTO.get(i).getRut_trabajador() != null)
                    stmt.setInt(4, lstdetalleDTO.get(i).getRut_trabajador());
                if (lstdetalleDTO.get(i).getDv_trabajador() != null)
                    stmt.setString(5, lstdetalleDTO.get(i).getDv_trabajador());
                if (lstdetalleDTO.get(i).getApellido_paterno_trabajador() != null)
                    stmt.setString(6, lstdetalleDTO.get(i).getApellido_paterno_trabajador());
                if (lstdetalleDTO.get(i).getApellido_materno_trabajador() != null)
                    stmt.setString(7, lstdetalleDTO.get(i).getApellido_materno_trabajador());
                if (lstdetalleDTO.get(i).getNombres_trabajador() != null)
                    stmt.setString(8, lstdetalleDTO.get(i).getNombres_trabajador());
                if (lstdetalleDTO.get(i).getSexo() != null)
                    stmt.setString(9, lstdetalleDTO.get(i).getSexo());
                if (lstdetalleDTO.get(i).getNacionalidad() != null)
                    stmt.setInt(10, lstdetalleDTO.get(i).getNacionalidad());
                if (lstdetalleDTO.get(i).getRenta_imponible_trabajador() != null)
                    stmt.setDouble(11, lstdetalleDTO.get(i).getRenta_imponible_trabajador());
                if (lstdetalleDTO.get(i).getCotizacion_voluntaria_apvi() != null)
                    stmt.setDouble(12, lstdetalleDTO.get(i).getCotizacion_voluntaria_apvi());
                if (lstdetalleDTO.get(i).getCotizacion_voluntaria_apvi_b() != null)
                    stmt.setDouble(13, lstdetalleDTO.get(i).getCotizacion_voluntaria_apvi_b());
                if (lstdetalleDTO.get(i).getNro_contrato_apvi() != null)
                    stmt.setString(14, lstdetalleDTO.get(i).getNro_contrato_apvi());
                if (lstdetalleDTO.get(i).getDeposito_convenio() != null)
                    stmt.setDouble(15, lstdetalleDTO.get(i).getDeposito_convenio());
                if (lstdetalleDTO.get(i).getTotal_a_pagar() != null)
                    stmt.setDouble(16, lstdetalleDTO.get(i).getTotal_a_pagar());
                if (lstdetalleDTO.get(i).getPeriodo_pago() != null)
                    stmt.setDate(17, new java.sql.Date(lstdetalleDTO.get(i).getPeriodo_pago().getTime()));
                if (lstdetalleDTO.get(i).getApv_colectivo_empl() != null)
                    stmt.setDouble(18, lstdetalleDTO.get(i).getApv_colectivo_empl());
                if (lstdetalleDTO.get(i).getApv_colectivo_trabaj() != null)
                    stmt.setDouble(19, lstdetalleDTO.get(i).getApv_colectivo_trabaj());
                if (lstdetalleDTO.get(i).getN_contrato_apvc() != null)
                    stmt.setString(20, lstdetalleDTO.get(i).getN_contrato_apvc());
                if (lstdetalleDTO.get(i).getCodigo_mov_personal() != null)
                    stmt.setInt(21, lstdetalleDTO.get(i).getCodigo_mov_personal());
                if (lstdetalleDTO.get(i).getFecha_inicio() != null)
                    stmt.setDate(22, new java.sql.Date(lstdetalleDTO.get(i).getFecha_inicio().getTime()));
                if (lstdetalleDTO.get(i).getFecha_termino() != null)
                    stmt.setDate(23, new java.sql.Date(lstdetalleDTO.get(i).getFecha_termino().getTime()));
                if (lstdetalleDTO.get(i).getEstado_proceso_pago() != null)
                    stmt.setInt(24, lstdetalleDTO.get(i).getEstado_proceso_pago());
                if (lstdetalleDTO.get(i).getEstado_proceso_pago_exception() != null)
                    stmt.setString(25, lstdetalleDTO.get(i).getEstado_proceso_pago_exception());
                if (lstdetalleDTO.get(i).getPolizaAsociada() != null)
                    stmt.setInt(26, lstdetalleDTO.get(i).getPolizaAsociada());
                if (lstdetalleDTO.get(i).getConceptoRecaudacion() != null)
                    stmt.setString(27, lstdetalleDTO.get(i).getConceptoRecaudacion());
                if (lstdetalleDTO.get(i).getId_folio_pagocaja() != null)
                    stmt.setDouble(28, lstdetalleDTO.get(i).getId_folio_pagocaja());
                stmt.setLong(29, i);

                if (id_folio_planilla == null)
                    stmt.setNull(2, OracleTypes.DOUBLE);
                if (lstdetalleDTO.get(i).getTipo_de_registro() == null)
                    stmt.setNull(3, OracleTypes.NUMBER);
                if (lstdetalleDTO.get(i).getRut_trabajador() == null)
                    stmt.setNull(4, OracleTypes.NUMBER);
                if (lstdetalleDTO.get(i).getDv_trabajador() == null)
                    stmt.setNull(5, OracleTypes.VARCHAR);
                if (lstdetalleDTO.get(i).getApellido_paterno_trabajador() == null)
                    stmt.setNull(6, OracleTypes.VARCHAR);
                if (lstdetalleDTO.get(i).getApellido_materno_trabajador() == null)
                    stmt.setNull(7, OracleTypes.VARCHAR);
                if (lstdetalleDTO.get(i).getNombres_trabajador() == null)
                    stmt.setNull(8, OracleTypes.VARCHAR);
                if (lstdetalleDTO.get(i).getSexo() == null)
                    stmt.setNull(9, OracleTypes.VARCHAR);
                if (lstdetalleDTO.get(i).getNacionalidad() == null)
                    stmt.setNull(10, OracleTypes.NUMBER);
                if (lstdetalleDTO.get(i).getRenta_imponible_trabajador() == null)
                    stmt.setNull(11, OracleTypes.DOUBLE);
                if (lstdetalleDTO.get(i).getCotizacion_voluntaria_apvi() == null)
                    stmt.setNull(12, OracleTypes.DOUBLE);
                if (lstdetalleDTO.get(i).getCotizacion_voluntaria_apvi_b() == null)
                    stmt.setNull(13, OracleTypes.DOUBLE);
                if (lstdetalleDTO.get(i).getNro_contrato_apvi() == null)
                    stmt.setNull(14, OracleTypes.VARCHAR);
                if (lstdetalleDTO.get(i).getDeposito_convenio() == null)
                    stmt.setNull(15, OracleTypes.DOUBLE);
                if (lstdetalleDTO.get(i).getTotal_a_pagar() == null)
                    stmt.setNull(16, OracleTypes.DOUBLE);
                if (lstdetalleDTO.get(i).getPeriodo_pago() == null)
                    stmt.setNull(17, OracleTypes.DATE);
                if (lstdetalleDTO.get(i).getApv_colectivo_empl() == null)
                    stmt.setNull(18, OracleTypes.DOUBLE);
                if (lstdetalleDTO.get(i).getApv_colectivo_trabaj() == null)
                    stmt.setNull(19, OracleTypes.DOUBLE);
                if (lstdetalleDTO.get(i).getN_contrato_apvc() == null)
                    stmt.setNull(20, OracleTypes.VARCHAR);
                if (lstdetalleDTO.get(i).getCodigo_mov_personal() == null)
                    stmt.setNull(21, OracleTypes.NUMBER);
                if (lstdetalleDTO.get(i).getFecha_inicio() == null)
                    stmt.setNull(22, OracleTypes.DATE);
                if (lstdetalleDTO.get(i).getFecha_termino() == null)
                    stmt.setNull(23, OracleTypes.DATE);
                if (lstdetalleDTO.get(i).getEstado_proceso_pago() == null)
                    stmt.setNull(24, OracleTypes.NUMBER);
                if (lstdetalleDTO.get(i).getEstado_proceso_pago_exception() == null)
                    stmt.setNull(25, OracleTypes.VARCHAR);
                if (lstdetalleDTO.get(i).getPolizaAsociada() == null)
                    stmt.setNull(26, OracleTypes.NUMBER);
                if (lstdetalleDTO.get(i).getConceptoRecaudacion() == null)
                    stmt.setNull(27, OracleTypes.VARCHAR);
                if (lstdetalleDTO.get(i).getId_folio_pagocaja() == null)
                    stmt.setNull(28, OracleTypes.DOUBLE);

                System.out.println("Insertando detalle Carga:" + idCarga.toString() + " planilla:" +
                                   id_folio_planilla.toString() + " rut trabajador:" +
                                   lstdetalleDTO.get(i).getRut_trabajador() + " total a pagar:" +
                                   lstdetalleDTO.get(i).getTotal_a_pagar() + " folio caja:" +
                                   lstdetalleDTO.get(i).getId_folio_pagocaja() + " concepto caja:" +
                                   lstdetalleDTO.get(i).getConceptoRecaudacion());
                stmt.addBatch();
                lstdetalleDTO.get(i).setProcesado(true);

            }

            stmt.executeBatch();

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstdetalleDTO;
    }

    /**
     * Inserta el resumen del detalle de la planilla
     * @param resumenDTO
     * @param idCarga
     * @param id_folio_planilla
     * @return boolean
     */
    public ArchivoPlanillaResumenDTO insertResumenPlanilla(ArchivoPlanillaResumenDTO resumenDTO, Double idCarga,
                                                           Double id_folio_planilla) {

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        resumenDTO.setProcesado(false);

        String query =
            "" + "INSERT\n" + "INTO REND_ARCHIVO_PLANILLA_RESUMEN\n" + "  (\n" + "    ID_CARGA,\n" +
            "    ID_FOLIO_PLANILLA,\n" + "    TIPO_DE_REGISTRO,\n" + "    TOTAL_COTIZA_VOLUNTARIA_APVI,\n" +
            "    TOTAL_COTIZA_VOLUNTARIA_APVI_B,\n" + "    TOTAL_APV_COLECT_EMPLEADOR,\n" +
            "    TOTAL_APV_COLECT_TRABAJADOR,\n" + "    TOTAL_DEPOSITOS_CONVENIDOS,\n" + "    TOTAL_PAGAR_IA,\n" +
            "    PERIODO_PAGO,\n" + "    NUMERO_AFILIADOS_INFORMADOS\n" + "  )\n" + "  VALUES\n" +
            "  ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            if (id_folio_planilla != null)
                stmt.setDouble(2, id_folio_planilla);
            if (resumenDTO.getTipo_de_registro() != null)
                stmt.setInt(3, resumenDTO.getTipo_de_registro());
            if (resumenDTO.getTotal_cotiza_voluntaria_apvi() != null)
                stmt.setDouble(4, resumenDTO.getTotal_cotiza_voluntaria_apvi());
            if (resumenDTO.getTotal_cotiza_voluntaria_apvi_b() != null)
                stmt.setDouble(5, resumenDTO.getTotal_cotiza_voluntaria_apvi_b());
            if (resumenDTO.getTotal_apv_colect_empleador() != null)
                stmt.setDouble(6, resumenDTO.getTotal_apv_colect_empleador());
            if (resumenDTO.getTotal_apv_colect_trabajador() != null)
                stmt.setDouble(7, resumenDTO.getTotal_apv_colect_trabajador());
            if (resumenDTO.getTotal_depositos_convenidos() != null)
                stmt.setDouble(8, resumenDTO.getTotal_depositos_convenidos());
            if (resumenDTO.getTotal_pagar_ia() != null)
                stmt.setDouble(9, resumenDTO.getTotal_pagar_ia());
            if (resumenDTO.getPeriodo_pago() != null)
                stmt.setDate(10, new java.sql.Date(resumenDTO.getPeriodo_pago().getTime()));
            if (resumenDTO.getNumero_afiliados_informados() != null)
                stmt.setInt(11, resumenDTO.getNumero_afiliados_informados());

            if (id_folio_planilla == null)
                stmt.setNull(2, OracleTypes.DOUBLE);
            if (resumenDTO.getTipo_de_registro() == null)
                stmt.setNull(3, OracleTypes.NUMBER);
            if (resumenDTO.getTotal_cotiza_voluntaria_apvi() == null)
                stmt.setNull(4, OracleTypes.DOUBLE);
            if (resumenDTO.getTotal_cotiza_voluntaria_apvi_b() == null)
                stmt.setNull(5, OracleTypes.DOUBLE);
            if (resumenDTO.getTotal_apv_colect_empleador() == null)
                stmt.setNull(6, OracleTypes.DOUBLE);
            if (resumenDTO.getTotal_apv_colect_trabajador() == null)
                stmt.setNull(7, OracleTypes.DOUBLE);
            if (resumenDTO.getTotal_depositos_convenidos() == null)
                stmt.setNull(8, OracleTypes.DOUBLE);
            if (resumenDTO.getTotal_pagar_ia() == null)
                stmt.setNull(9, OracleTypes.DOUBLE);
            if (resumenDTO.getPeriodo_pago() == null)
                stmt.setNull(10, OracleTypes.DATE);
            if (resumenDTO.getNumero_afiliados_informados() == null)
                stmt.setNull(11, OracleTypes.NUMBER);

            stmt.execute();
            resumenDTO.setProcesado(true);

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return resumenDTO;
    }


    public boolean deletePlanillaAbono(Double idCarga) {
        boolean result = false;

        try {
            deleteAntecedentesPlanilla(idCarga);
            deleteDetallePlanilla(idCarga);
            deleteResumenPlanilla(idCarga);
            deleteEncabezadoPlanilla(idCarga);
            result = true;
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        }

        return result;
    }

    /**
     * Borra los antecedentes asociados a la planilla
     * @param idCarga
     * @return
     */
    public boolean deleteAntecedentesPlanilla(Double idCarga) {
        boolean result = false;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "DELETE REND_ARCHIVO_PLANILLA_ANTEC WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.execute();
            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return result;
    }

    /**
     * Borra los detalles asociados a la planilla
     * @param idCarga
     * @return
     */
    public boolean deleteDetallePlanilla(Double idCarga) {
        boolean result = false;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "DELETE REND_ARCHIVO_PLANILLA_DETALLE WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.execute();
            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return result;
    }

    /**
     * Borra el resumen asociado a la planilla
     * @param idCarga
     * @return
     */
    public boolean deleteResumenPlanilla(Double idCarga) {
        boolean result = false;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "DELETE REND_ARCHIVO_PLANILLA_RESUMEN WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.execute();
            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return result;
    }

    /**
     * Borra el encabezado asociado a la planilla
     * @param idCarga
     * @return
     */
    public boolean deleteEncabezadoPlanilla(Double idCarga) {
        boolean result = false;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "DELETE REND_ARCHIVO_PLANILLA_ENCAB WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.execute();
            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return result;
    }

    /**
     * Obtiene la id de carga relacionado al id de carga de la planilla
     * @param idCarga
     * @return
     */
    public Double getIdCargaAbono(Double idCarga) {
        Double result = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA\n" + "FROM REND_CARGA_DE_ARCHIVOS\n" + "WHERE NUMERO_DE_CONVENIO =\n" +
            "  (SELECT NUMERO_DE_CONVENIO FROM REND_CARGA_DE_ARCHIVOS WHERE ID_CARGA = ?)\n" +
            "AND FECHA_CONTABLE =\n" + "  (SELECT FECHA_CONTABLE FROM REND_CARGA_DE_ARCHIVOS WHERE ID_CARGA = ?\n" +
            "  )";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.setDouble(2, idCarga);

            rs = stmt.executeQuery();

            if (rs.next()) {
                result = rs.getDouble("ID_CARGA");
            }

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Verifica si existe la relacion idcarga idfolio en el abono
     * @param idCarga
     * @param idFolio
     * @return
     */
    public boolean existIdCargaIdFolio(Double idCarga, Double idFolio) {
        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA, ID_FOLIO_ABONO FROM REND_ARCHIVO_ABONO\n" + "WHERE ID_CARGA = ?\n" +
            "AND ID_FOLIO_ABONO = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.setDouble(2, idFolio);

            rs = stmt.executeQuery();

            if (rs.next()) {
                result = true;
            }

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Actualiza el estado del folio procesado en el abono
     * @param idCarga
     * @param idFolio
     * @param estadoProceso
     * @param estadoProcesoObs
     * @return
     */
    public boolean updateEstadoPorcesoFolioResumen(Double idCarga, Double idFolio, Integer estadoProceso,
                                                   String estadoProcesoObs) {
        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "UPDATE REND_ARCHIVO_ABONO\n" + "SET ESTADO_PROCESO         = ?,\n" +
            "  ESTADO_PROCESO_EXCEPCION = ?\n" + "WHERE ID_CARGA             = ?\n" + "AND ID_FOLIO_ABONO         = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setInt(1, estadoProceso);
            stmt.setString(2, estadoProcesoObs);
            stmt.setDouble(3, idCarga);
            stmt.setDouble(4, idFolio);

            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }


    /**
     * Actualiza el estado del folio procesado en el abono con un id de estado proceso definido
     * @param idCarga
     * @param idFolio
     * @param oldEstadoProceso
     * @param estadoProceso
     * @param estadoProcesoObs
     * @return
     */
    public boolean updateEstadoPorcesoFolioResumen(Double idCarga, Double idFolio, Integer oldEstadoProceso,
                                                   Integer estadoProceso, String estadoProcesoObs) {
        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "UPDATE REND_ARCHIVO_ABONO\n" + "SET ESTADO_PROCESO         = ?,\n" +
            "  ESTADO_PROCESO_EXCEPCION = ?\n" + "WHERE ID_CARGA             = ?\n" +
            "AND ESTADO_PROCESO         = ?\n" + "AND ID_FOLIO_ABONO         = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setInt(1, estadoProceso);
            stmt.setString(2, estadoProcesoObs);
            stmt.setDouble(3, idCarga);
            stmt.setDouble(4, oldEstadoProceso);
            stmt.setDouble(5, idFolio);

            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Actualiza el estado
     * @param idCarga
     * @param estadoProceso
     * @param estadoProcesoObs
     * @return
     */
    public boolean updateEstadoPorcesoFolioResumenByIdCarga(Double idCarga, Integer estadoProceso,
                                                            String estadoProcesoObs) {
        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "UPDATE REND_ARCHIVO_ABONO\n" + "SET ESTADO_PROCESO         = ?,\n" +
            "  ESTADO_PROCESO_EXCEPCION = ?\n" + "WHERE ID_CARGA             = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setInt(1, estadoProceso);
            stmt.setString(2, estadoProcesoObs);
            stmt.setDouble(3, idCarga);

            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }


    public boolean updateEstadoPorcesoFolioResumenByIdCargaBanco(Double idCarga, String codRecaudador,
                                                                 Integer estadoProceso, String estadoProcesoObs) {
        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;


        String query =
            "" + "UPDATE REND_ARCHIVO_ABONO\n" + "SET ESTADO_PROCESO         = ?,\n" +
            "  ESTADO_PROCESO_EXCEPCION = ?\n" + "WHERE ID_CARGA             = ?\n" + "AND CODIGO_RECAUDADOR      = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setInt(1, estadoProceso);
            stmt.setString(2, estadoProcesoObs);
            stmt.setDouble(3, idCarga);
            stmt.setString(4, codRecaudador);

            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }


        return result;
    }


    public boolean updateEstadoPorcesoFolioResumenByIdCarga(Double idCarga, Integer oldEstadoProceso,
                                                            Integer estadoProceso, String estadoProcesoObs) {
        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "UPDATE REND_ARCHIVO_ABONO\n" + "SET ESTADO_PROCESO         = ?,\n" +
            "  ESTADO_PROCESO_EXCEPCION = ?\n" + "WHERE ID_CARGA             = ?\n" + "AND ESTADO_PROCESO         = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setInt(1, estadoProceso);
            stmt.setString(2, estadoProcesoObs);
            stmt.setDouble(3, idCarga);
            stmt.setDouble(4, oldEstadoProceso);

            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Actualiza el estado del folio procesado en la planilla
     * @param idCarga
     * @param idFolio
     * @param estadoProceso
     * @param estadoProcesoObs
     * @return
     */
    public boolean updateEstadoPorcesoFolioPlanilla(Double idCarga, Double idFolio, Integer estadoProceso,
                                                    String estadoProcesoObs) {
        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "UPDATE REND_ARCHIVO_PLANILLA_ENCAB\n" + "SET ESTADO_PROCESO         = ?,\n" +
            "  ESTADO_PROCESO_EXCEPCION = ?\n" + "WHERE ID_CARGA             = ?\n" + "AND ID_FOLIO_PLANILLA      = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setInt(1, estadoProceso);
            stmt.setString(2, estadoProcesoObs);
            stmt.setDouble(3, idCarga);
            stmt.setDouble(4, idFolio);

            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Genera lista de items de bancos para mostrar en pantalla
     * @return List
     */
    public List<EmpresasDTO> getEmpresas() {
        List<EmpresasDTO> resp = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "SELECT ID_EMPRESA, DESCRIPCION FROM REND_EMPRESAS";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            rs = stmt.executeQuery();
            resp = new ArrayList<EmpresasDTO>();
            while (rs.next()) {

                Integer idEmpresa = rs.getInt("ID_EMPRESA");
                String descripcionEmpresa = rs.getString("DESCRIPCION");

                if (idEmpresa != null && idEmpresa.intValue() > 0 && descripcionEmpresa != null &&
                    descripcionEmpresa.length() > 0) {
                    EmpresasDTO empresaDTO = new EmpresasDTO();
                    empresaDTO.setIdEmpresa(idEmpresa);
                    empresaDTO.setDescripcionEmpresa(descripcionEmpresa);
                    resp.add(empresaDTO);
                }
            }
        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return resp;
    }

    /**
     * Obriene lista de los medios de pagos
     * @return List
     */
    public List<MediosDePagoDTO> getMediosDePago(String tipopl) {
        List<MediosDePagoDTO> resp = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String filtro = null;

        if (tipopl != null && tipopl.length() > 0) {
            if (tipopl.equalsIgnoreCase(Constant.PLANILLA_ANDES)) {
                filtro = " WHERE TIPO = '" + Constant.MEDIOPAGO_ANDES + "'";
            }
            if (tipopl.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
                filtro = " WHERE TIPO = '" + Constant.MEDIOPAGO_LARAUCANA + "'";
            }
        } else {
            filtro = " WHERE TIPO NOT IN('" + Constant.MEDIOPAGO_ANDES + "','" + Constant.MEDIOPAGO_LARAUCANA + "') ";
        }

        String query = "" + "SELECT ID_MEDIO_PAGO, DESCRIPCION, TIPO FROM REND_MEDIOS_DE_PAGO " + filtro;

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            rs = stmt.executeQuery();
            resp = new ArrayList<MediosDePagoDTO>();
            while (rs.next()) {

                Integer idMedioPagoRendicion = rs.getInt("ID_MEDIO_PAGO");
                String tipoMedioPago = rs.getString("TIPO");
                String descripcionMedioPago = rs.getString("DESCRIPCION");

                if (idMedioPagoRendicion != null && tipoMedioPago != null && tipoMedioPago.length() > 0 &&
                    descripcionMedioPago != null && descripcionMedioPago.length() > 0) {
                    MediosDePagoDTO medioPagoDTO = new MediosDePagoDTO();
                    medioPagoDTO.setIdMedioPagoRendicion(idMedioPagoRendicion);
                    medioPagoDTO.setTipoMedioPago(tipoMedioPago);
                    medioPagoDTO.setDescripcionMedioPago(descripcionMedioPago);
                    resp.add(medioPagoDTO);
                }
            }
        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return resp;
    }

    public List<TipoArchivoDTO> getTipoArchivo(Integer codigo) {
        List<TipoArchivoDTO> resp = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_TIPO_ARCHIVO,\n" + "  DESCRIPCION,\n" + "  TIPO\n" + "FROM REND_TIPO_DE_ARCHIVO_UPLOAD \n" +
            "WHERE ID_TIPO_ARCHIVO = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setInt(1, codigo);
            rs = stmt.executeQuery();
            resp = new ArrayList<TipoArchivoDTO>();
            while (rs.next()) {

                Integer idTipoArchivo = rs.getInt("ID_TIPO_ARCHIVO");
                String tipoArchivo = rs.getString("TIPO");
                String descripcionTipoArchivo = rs.getString("DESCRIPCION");

                if (idTipoArchivo != null && idTipoArchivo.intValue() > 0 && descripcionTipoArchivo != null &&
                    descripcionTipoArchivo.length() > 0 && tipoArchivo != null && tipoArchivo.length() > 0) {
                    TipoArchivoDTO tipoArchivoDTO = new TipoArchivoDTO();
                    tipoArchivoDTO.setIdTipoArchivo(idTipoArchivo);
                    tipoArchivoDTO.setTipoArchivo(tipoArchivo);
                    tipoArchivoDTO.setDescripcionTipoArchivo(descripcionTipoArchivo);
                    resp.add(tipoArchivoDTO);
                }
            }
        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return resp;
    }

    public ConvenioPagoDTO getConvenioDePago(Integer idEmpresa, Integer idMedioPago) {
        ConvenioPagoDTO convenio = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT CONVENIO.ID_EMPRESA    AS ID_EMPRESA,\n" +
            "  CONVENIO.ID_MEDIO_PAGO      AS ID_MEDIO_PAGO,\n" +
            "  CONVENIO.NUMERO_DE_CONVENIO AS NUMERO_DE_CONVENIO,\n" +
            "  CONVENIO.ESTADO_CONVENIO    AS ESTADO_CONVENIO,\n" +
            "  CONVENIO.CORREO_ELECTRONICO AS CORREO_ELECTRONICO,\n" +
            "  CONVENIO.DIRECTORIO_UPLOAD  AS DIRECTORIO_UPLOAD,\n" +
            "  EMPRESA.DESCRIPCION         AS DESCRIPCION_EMPRESA,\n" +
            "  MEDIOPAGO.DESCRIPCION       AS DESCRIPCION_MEDIOPAGO,\n" +
            "  MEDIOPAGO.TIPO              AS TIPO_MEDIO_BANCO\n" + "FROM REND_CONVENIO_DE_PAGO CONVENIO,\n" +
            "  REND_EMPRESAS EMPRESA,\n" + "  REND_MEDIOS_DE_PAGO MEDIOPAGO\n" +
            "WHERE EMPRESA.ID_EMPRESA    = CONVENIO.ID_EMPRESA\n" +
            "AND MEDIOPAGO.ID_MEDIO_PAGO = CONVENIO.ID_MEDIO_PAGO\n" + "AND CONVENIO.ID_EMPRESA     = ?\n" +
            "AND CONVENIO.ID_MEDIO_PAGO  = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setInt(1, idEmpresa);
            stmt.setInt(2, idMedioPago);

            rs = stmt.executeQuery();

            if (rs.next()) {
                convenio = new ConvenioPagoDTO();
                convenio.setId_empresa(rs.getInt("ID_EMPRESA"));
                convenio.setId_medio_pago(rs.getInt("ID_MEDIO_PAGO"));
                convenio.setNumero_de_convenio(rs.getString("NUMERO_DE_CONVENIO"));
                convenio.setEstado_convenio(rs.getInt("ESTADO_CONVENIO"));
                convenio.setCorreo_electronico(rs.getString("CORREO_ELECTRONICO"));
                convenio.setDirectorio_upload(rs.getString("DIRECTORIO_UPLOAD"));
                convenio.setDescripcion_empresa(rs.getString("DESCRIPCION_EMPRESA"));
                convenio.setDescripcion_medio_pago(rs.getString("DESCRIPCION_MEDIOPAGO"));
                convenio.setTipo_codbanco_medio_pago(rs.getString("TIPO_MEDIO_BANCO"));
            }
        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return convenio;
    }

    public Integer getIdCarga() {
        Integer valor = 0;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "SELECT REND_CARGAS_SECUENCIA.NEXTVAL FROM DUAL";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            rs = stmt.executeQuery();

            if (rs.next()) {
                valor = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return valor;
    }

    /**
     * Verifica la existencia de la carga
     * @param cargaArchivosDTO
     * @return
     */
    public boolean existCarga(CargaArchivosDTO cargaArchivosDTO) {
        boolean ret = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  NUMERO_DE_CONVENIO,\n" + "  FECHA_CREACION,\n" + "  ID_TIPO_ARCHIVO,\n" +
            "  ESTADO_PROCESO,\n" + "  INICIO_PROCESO,\n" + "  TERMINO_PROCESO,\n" + "  FECHA_Y_HORA_UPLOAD,\n" +
            "  NOMBRE_FISICO_ARCHIVO,\n" + "  CANTIDAD_PROCESADA,\n" + "  OBSERVACION_PROCESO\n" +
            "  FROM REND_CARGA_DE_ARCHIVOS\n" + "WHERE NUMERO_DE_CONVENIO = ?\n" + "AND FECHA_CREACION       = ?\n" +
            "  AND ID_TIPO_ARCHIVO      = ?\n" + "AND ESTADO_PROCESO       in (?, ?, ?, ?, ?, ?)\n"
            + "  AND FECHA_Y_HORA_UPLOAD = ? ";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setString(1, cargaArchivosDTO.getConvenio().getNumero_de_convenio().trim());
            stmt.setTimestamp(2, new java.sql.Timestamp(cargaArchivosDTO.getFecha_creacion().getTime()));
            stmt.setInt(3, cargaArchivosDTO.getId_tipo_archivo());
            stmt.setInt(4, Constant.PROCESO_PENDIENTE);
            stmt.setInt(5, Constant.PROCESO_LECTURA_ARCHIVO);
            stmt.setInt(6, Constant.PROCESO_CARGA_BASEDATOS);
            stmt.setInt(7, Constant.PROCESO_FINALIZADO_CARGA);
            stmt.setInt(8, Constant.PROCESO_FINALIZADO_AUTORIZADO);
            stmt.setInt(9, Constant.PROCESO_FINALIZADO_);
            stmt.setTimestamp(10, new java.sql.Timestamp(cargaArchivosDTO.getFecha_y_hora_upload().getTime()));   
            
            rs = stmt.executeQuery();

            if (rs.next()) {
                ret = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("existCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("existCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        System.out.println("Respuesta si existe carga " + ret);
        return ret;
    }

    /**
     * Obtiene los datos de la carga a partir del id folio del abomo
     * @param archivoAbonoDTO
     * @return
     */
    public CargaArchivosDTO getCargaArchivoByIdFolioAbono(ArchivoAbonoDTO archivoAbonoDTO, Integer estado) {
        CargaArchivosDTO cargaArchivosDTO = null;
        Double idCarga = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        /*String query =
            "" + "SELECT ID_CARGA\n" + "FROM REND_ARCHIVO_ABONO\n" + "WHERE ID_FOLIO_ABONO = ?\n" +
            "  AND ESTADO_PROCESO = ?";*/

        String query =
            " SELECT ID_CARGA FROM REND_ARCHIVO_ABONO WHERE ID_FOLIO_ABONO = ? " +
            " AND ESTADO_PROCESO = ? AND  trunc(PERIODO,'MONTH') = trunc(?,'MONTH')";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            
            stmt.setDouble(1, archivoAbonoDTO.getId_folio_abono());
            stmt.setInt(2, estado);                                
            stmt.setDate(3, new java.sql.Date(archivoAbonoDTO.getPeriodo().getTime()));
            rs = stmt.executeQuery();

            if (rs.next()) {
                idCarga = rs.getDouble("ID_CARGA");
            }

            if (idCarga != null && idCarga > 0.0) {
                cargaArchivosDTO = getCargaArchivosByIdCarga(idCarga);
            }

        } catch (SQLException e) {
            logger.error("getCargaArchivoByIdFolioAbono() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getCargaArchivoByIdFolioAbono() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return cargaArchivosDTO;
    }

    /**
     * Se obtiene la carga apartir del id de folio
     * @param idFolio
     * @param estado
     * @return
     */
    public CargaArchivosDTO getCargaArchivoByIdFolioAbono(Double idFolio, Integer estado) {
        CargaArchivosDTO cargaArchivosDTO = null;
        Double idCarga = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA\n" + "FROM REND_ARCHIVO_ABONO\n" + "WHERE ID_FOLIO_ABONO = ?\n" +
            "AND ESTADO_PROCESO = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idFolio);
            stmt.setInt(2, estado);

            rs = stmt.executeQuery();

            if (rs.next()) {
                idCarga = rs.getDouble("ID_CARGA");
            }

            if (idCarga != null && idCarga > 0.0) {
                cargaArchivosDTO = getCargaArchivosByIdCarga(idCarga);
            }

        } catch (SQLException e) {
            logger.error("getCargaArchivoByIdFolioAbono() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getCargaArchivoByIdFolioAbono() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return cargaArchivosDTO;
    }

    /**
     *
     * @param idFolio
     * @param estado
     * @param estadoCarga
     * @return
     */
    public CargaArchivosDTO getCargaArchivoByIdFolioAbono(Double idFolio, Integer estado, Integer estadoCarga) {
        CargaArchivosDTO cargaArchivosDTO = null;
        Double idCarga = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA\n" + "FROM REND_ARCHIVO_ABONO\n" + "WHERE ID_FOLIO_ABONO = ?\n" +
            "AND ESTADO_PROCESO = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idFolio);
            stmt.setInt(2, estado);

            rs = stmt.executeQuery();

            if (rs.next()) {
                idCarga = rs.getDouble("ID_CARGA");
            }

            if (idCarga != null && idCarga > 0.0) {
                cargaArchivosDTO = getCargaArchivosByIdCargaEstado(idCarga, estadoCarga);
            }

        } catch (SQLException e) {
            logger.error("getCargaArchivoByIdFolioAbono() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getCargaArchivoByIdFolioAbono() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return cargaArchivosDTO;
    }

    /**
     * Obtiene la carga del archivo por el id folio de la planilla
     * @param archivoPlanillaEncabezadoDTO
     * @return
     */
    public CargaArchivosDTO getCargaArchivoByIdFolioPlanilla(ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO,Date periodoCotizacion ) {
        CargaArchivosDTO cargaArchivosDTO = null;
        Double idCarga = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        //String query = "" + "SELECT ID_CARGA\n" + "FROM REND_ARCHIVO_PLANILLA_ENCAB\n" + "WHERE ID_FOLIO_PLANILLA = ?";
        String query = "" + "select enc.id_carga from REND_ARCHIVO_PLANILLA_ENCAB enc \n" + 
                            "inner join rend_archivo_planilla_detalle det on enc.id_carga = det.id_carga\n" + 
                            "and enc.id_folio_planilla = det.id_folio_planilla \n" + 
                            "and enc.id_folio_planilla = ?" + 
                            "and trunc(det.periodo_pago,'MONTH') = trunc(? , 'MONTH')";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, archivoPlanillaEncabezadoDTO.getId_folio_planilla());
            stmt.setDate(2, new java.sql.Date(periodoCotizacion.getTime()));
            rs = stmt.executeQuery();

            if (rs.next()) {
                idCarga = rs.getDouble("ID_CARGA");
            }

            if (idCarga != null && idCarga > 0.0) {
                cargaArchivosDTO = getCargaArchivosByIdCarga(idCarga);
            }

        } catch (SQLException e) {
            logger.error("getCargaArchivoByIdFolioPlanilla() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getCargaArchivoByIdFolioPlanilla() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return cargaArchivosDTO;
    }


    public List<ResumenAbonoDTO> getAbonoByIdCarga(Double idCarga) {
        List<ResumenAbonoDTO> lstResumenAbonoDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  CODIGO_RECAUDADOR,\n" + "  GLOSA_RECAUDADOR,\n" + "  ID_MOTIVO_RECHAZO,\n" +
            "  OBSERVACION_RECHAZO,\n" + "  OBSERVACION_AUTORIZACION,\n" + "  VALIDACION_CARTOLA_CTA_CTE,\n" +
            "  TOTAL_MONTO_INF_CARTOLA,\n" + "  TIPO_DE_REGISTRO,\n" + "  IDENTIFICADOR_IIP,\n" +
            "  TOTAL_MONTO_FONDO,\n" + "  TOTAL_MONTO_INSTITUCION,\n" + "  TOTAL_MONTO_AFC,\n" + "  CUENTA_FONDO,\n" +
            "  CUENTA_INSTITUCION,\n" + "  CUENTA_AFC,\n" + "  IDENTIFICACION_BANCO,\n" + "  CODIGO_BANCO,\n" +
            "  FECHA_ABONO,\n" + "  NUMERO_DE_PLANILLAS\n" + "FROM REND_RESUMEN_ABONO\n" + "WHERE ID_CARGA = ?\n" +
            "ORDER BY CODIGO_RECAUDADOR";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCarga);
            rs = stmt.executeQuery();

            lstResumenAbonoDTO = new ArrayList<ResumenAbonoDTO>();
            while (rs.next()) {
                ResumenAbonoDTO resumenAbonoDTO = new ResumenAbonoDTO();
                resumenAbonoDTO.setCodigo_recaudador(rs.getString("CODIGO_RECAUDADOR"));
                resumenAbonoDTO.setGlosa_recaudador(rs.getString("GLOSA_RECAUDADOR"));
                resumenAbonoDTO.setId_motivo_rechazo(rs.getInt("ID_MOTIVO_RECHAZO"));
                resumenAbonoDTO.setObservacion_rechazo(rs.getString("OBSERVACION_RECHAZO"));
                resumenAbonoDTO.setObservacion_autorizacion(rs.getString("OBSERVACION_AUTORIZACION"));
                resumenAbonoDTO.setValidacion_cartola_cta_cte(rs.getInt("VALIDACION_CARTOLA_CTA_CTE"));
                resumenAbonoDTO.setTotal_monto_inf_cartola(rs.getDouble("TOTAL_MONTO_INF_CARTOLA"));
                resumenAbonoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                resumenAbonoDTO.setIdentificador_iip(rs.getInt("IDENTIFICADOR_IIP"));
                resumenAbonoDTO.setTotal_monto_fondo(rs.getDouble("TOTAL_MONTO_FONDO"));
                resumenAbonoDTO.setTotal_monto_institucion(rs.getDouble("TOTAL_MONTO_INSTITUCION"));
                resumenAbonoDTO.setTotal_monto_afc(rs.getDouble("TOTAL_MONTO_AFC"));
                resumenAbonoDTO.setCuenta_fondo(rs.getString("CUENTA_FONDO"));
                resumenAbonoDTO.setCuenta_institucion(rs.getString("CUENTA_INSTITUCION"));
                resumenAbonoDTO.setCuenta_afc(rs.getString("CUENTA_AFC"));
                resumenAbonoDTO.setIdentificacion_banco(rs.getString("IDENTIFICACION_BANCO"));
                resumenAbonoDTO.setCodigo_banco(rs.getString("CODIGO_BANCO"));
                resumenAbonoDTO.setFecha_abono(new Date(rs.getDate("FECHA_ABONO").getTime()));
                resumenAbonoDTO.setNumero_de_planillas(rs.getInt("NUMERO_DE_PLANILLAS"));
                resumenAbonoDTO.setArchivoabono(getDetalleAbonoByIdCarga(idCarga,
                                                                         resumenAbonoDTO.getCodigo_recaudador()));

                lstResumenAbonoDTO.add(resumenAbonoDTO);

            }


        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstResumenAbonoDTO;
    }

    public List<ArchivoAbonoDTO> getDetalleAbonoByIdCarga(Double idCarga, String codigoRecaudador) {
        List<ArchivoAbonoDTO> lstArchivoAbonoDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  CODIGO_RECAUDADOR,\n" + "  ID_FOLIO_ABONO,\n" + "  TIPO_DE_REGISTRO,\n" +
            "  MONTO_ABONO_FONDO,\n" + "  MONTO_ABONO_INSTITUCION,\n" + "  MONTO_ABONO_AFC,\n" + "  RUT_EMPLEADOR,\n" +
            "  DIGITO_VERFICADOR,\n" + "  PERIODO,\n" + "  FECHA_PAGO,\n" + "  LOTE,\n" + "  SUCURSAL,\n" +
            "  ESTADO_PROCESO,\n" + "  ESTADO_PROCESO_EXCEPCION\n" + "FROM REND_ARCHIVO_ABONO\n" +
            "WHERE ID_CARGA        = ?\n" + "AND CODIGO_RECAUDADOR = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCarga);
            stmt.setString(2, codigoRecaudador);
            rs = stmt.executeQuery();

            lstArchivoAbonoDTO = new ArrayList<ArchivoAbonoDTO>();
            while (rs.next()) {
                ArchivoAbonoDTO archivoAbonoDTO = new ArchivoAbonoDTO();
                archivoAbonoDTO.setId_folio_abono(rs.getDouble("ID_FOLIO_ABONO"));
                archivoAbonoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                archivoAbonoDTO.setMonto_abono_fondo(rs.getDouble("MONTO_ABONO_FONDO"));
                archivoAbonoDTO.setMonto_abono_institucion(rs.getDouble("MONTO_ABONO_INSTITUCION"));
                archivoAbonoDTO.setMonto_abono_afc(rs.getDouble("MONTO_ABONO_AFC"));
                archivoAbonoDTO.setRut_empleador(rs.getInt("RUT_EMPLEADOR"));
                archivoAbonoDTO.setDigito_verficador(rs.getString("DIGITO_VERFICADOR"));
                archivoAbonoDTO.setPeriodo(new Date(rs.getDate("PERIODO").getTime()));
                archivoAbonoDTO.setFecha_pago(new Date(rs.getDate("FECHA_PAGO").getTime()));
                archivoAbonoDTO.setLote(rs.getString("LOTE"));
                archivoAbonoDTO.setSucursal(rs.getString("SUCURSAL"));
                archivoAbonoDTO.setEstado_proceso(rs.getInt("ESTADO_PROCESO"));
                archivoAbonoDTO.setEstado_proceso_excepcion(rs.getString("ESTADO_PROCESO_EXCEPCION"));

                lstArchivoAbonoDTO.add(archivoAbonoDTO);

            }


        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstArchivoAbonoDTO;
    }

    public ArchivoAbonoDTO getDetalleAbonoByIdCargaByIdFolio(Double idCarga, Double idFolioAbono) {
        ArchivoAbonoDTO archivoAbonoDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  CODIGO_RECAUDADOR,\n" + "  ID_FOLIO_ABONO,\n" + "  TIPO_DE_REGISTRO,\n" +
            "  MONTO_ABONO_FONDO,\n" + "  MONTO_ABONO_INSTITUCION,\n" + "  MONTO_ABONO_AFC,\n" + "  RUT_EMPLEADOR,\n" +
            "  DIGITO_VERFICADOR,\n" + "  PERIODO,\n" + "  FECHA_PAGO,\n" + "  LOTE,\n" + "  SUCURSAL,\n" +
            "  ESTADO_PROCESO,\n" + "  ESTADO_PROCESO_EXCEPCION\n" + "FROM REND_ARCHIVO_ABONO\n" +
            "WHERE ID_CARGA        = ?\n" + "AND ID_FOLIO_ABONO = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCarga);
            stmt.setDouble(2, idFolioAbono);
            rs = stmt.executeQuery();


            if (rs.next()) {
                archivoAbonoDTO = new ArchivoAbonoDTO();
                archivoAbonoDTO.setId_folio_abono(rs.getDouble("ID_FOLIO_ABONO"));
                archivoAbonoDTO.setCod_recaudador(rs.getInt("CODIGO_RECAUDADOR"));
                archivoAbonoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                archivoAbonoDTO.setMonto_abono_fondo(rs.getDouble("MONTO_ABONO_FONDO"));
                archivoAbonoDTO.setMonto_abono_institucion(rs.getDouble("MONTO_ABONO_INSTITUCION"));
                archivoAbonoDTO.setMonto_abono_afc(rs.getDouble("MONTO_ABONO_AFC"));
                archivoAbonoDTO.setRut_empleador(rs.getInt("RUT_EMPLEADOR"));
                archivoAbonoDTO.setDigito_verficador(rs.getString("DIGITO_VERFICADOR"));
                archivoAbonoDTO.setPeriodo(new Date(rs.getDate("PERIODO").getTime()));
                archivoAbonoDTO.setFecha_pago(new Date(rs.getDate("FECHA_PAGO").getTime()));
                archivoAbonoDTO.setLote(rs.getString("LOTE"));
                archivoAbonoDTO.setSucursal(rs.getString("SUCURSAL"));
                archivoAbonoDTO.setEstado_proceso(rs.getInt("ESTADO_PROCESO"));
                archivoAbonoDTO.setEstado_proceso_excepcion(rs.getString("ESTADO_PROCESO_EXCEPCION"));

            }


        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return archivoAbonoDTO;
    }

    /**
     *
     * @return
     */
    public List<MotivoRechazosDTO> getMotivoRechazos() {
        List<MotivoRechazosDTO> resp = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "SELECT ID_MOTIVO_RECHAZO, DESCRIPCION FROM REND_MOTIVO_RECHAZOS";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            rs = stmt.executeQuery();
            resp = new ArrayList<MotivoRechazosDTO>();
            while (rs.next()) {
                Integer idMotivoRechazo = rs.getInt("ID_MOTIVO_RECHAZO");
                String descripcionMotivoRechazo = rs.getString("DESCRIPCION");

                if (idMotivoRechazo != null && idMotivoRechazo.intValue() > 0 && descripcionMotivoRechazo != null &&
                    descripcionMotivoRechazo.length() > 0) {
                    MotivoRechazosDTO motivoRechazosDTO = new MotivoRechazosDTO();
                    motivoRechazosDTO.setId_motivo_rechazos(idMotivoRechazo);
                    motivoRechazosDTO.setDescripcion_rechazos(descripcionMotivoRechazo);
                    resp.add(motivoRechazosDTO);
                }
            }
        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return resp;
    }

    /**
     *
     * @param idCarga
     * @param resumenAbonoDTO
     * @return
     */
    public boolean updateResumenYDetalleAbonoByIdCargaResumen(Double idCarga, ResumenAbonoDTO resumenAbonoDTO) {

        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "UPDATE REND_RESUMEN_ABONO\n" + "SET ID_MOTIVO_RECHAZO        = ?,\n" +
            "  OBSERVACION_RECHAZO        = ?,\n" + "  OBSERVACION_AUTORIZACION   = ?,\n" +
            "  VALIDACION_CARTOLA_CTA_CTE = ?,\n" + "  TOTAL_MONTO_INF_CARTOLA    = ?,\n" +
            "  ESTADO_PROCESO             = ?,\n" + "  ESTADO_PROCESO_EXCEPCION   = ?\n" +
            "WHERE ID_CARGA               = ?\n" + "AND CODIGO_RECAUDADOR        = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setInt(1, resumenAbonoDTO.getId_motivo_rechazo());
            stmt.setString(2, resumenAbonoDTO.getObservacion_rechazo());
            stmt.setString(3, resumenAbonoDTO.getObservacion_autorizacion());
            stmt.setInt(4, resumenAbonoDTO.getValidacion_cartola_cta_cte());
            stmt.setDouble(5, resumenAbonoDTO.getTotal_monto_inf_cartola());
            stmt.setInt(6, resumenAbonoDTO.getEstado_proceso());
            stmt.setString(7, resumenAbonoDTO.getEstado_proceso_excepcion());

            stmt.setDouble(8, idCarga);
            stmt.setString(9, resumenAbonoDTO.getCodigo_recaudador());
            stmt.executeUpdate();

            result = true;

            if (result) {
                if (updateEstadoPorcesoFolioResumenByIdCargaBanco(idCarga, resumenAbonoDTO.getCodigo_recaudador(),
                                                                  resumenAbonoDTO.getEstado_proceso(),
                                                                  resumenAbonoDTO.getObservacion_autorizacion())) {
                    result = true;
                }
            }

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }


    public boolean updateResumenYDetalleAbonoByIdCarga(Double idCarga, ResumenAbonoDTO resumenAbonoDTO) {

        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "UPDATE REND_RESUMEN_ABONO\n" + "SET ID_MOTIVO_RECHAZO        = ?,\n" +
            "  OBSERVACION_RECHAZO        = ?,\n" + "  OBSERVACION_AUTORIZACION   = ?,\n" +
            "  VALIDACION_CARTOLA_CTA_CTE = ?,\n" + "  TOTAL_MONTO_INF_CARTOLA    = ?\n" +
            "WHERE ID_CARGA               = ?\n" + "AND CODIGO_RECAUDADOR        = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setInt(1, resumenAbonoDTO.getId_motivo_rechazo());
            stmt.setString(2, resumenAbonoDTO.getObservacion_rechazo());
            stmt.setString(3, resumenAbonoDTO.getObservacion_autorizacion());
            stmt.setInt(4, resumenAbonoDTO.getValidacion_cartola_cta_cte());
            stmt.setDouble(5, resumenAbonoDTO.getTotal_monto_inf_cartola());
            stmt.setDouble(6, idCarga);
            stmt.setString(7, resumenAbonoDTO.getCodigo_recaudador());
            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }


    public ArchivoPlanillaEncabezadoDTO getPlanillaByIdCargaFolio(Double idCarga, Double folio) {
        ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  PLAENC.ID_FOLIO_PLANILLA        AS ID_FOLIO_PLANILLA,\n" +
            "  PLAENC.TIPO_DE_REGISTRO         AS TIPO_DE_REGISTRO,\n" +
            "  PLAENC.TIPO_PAGO                AS TIPO_PAGO,\n" +
            "  PLAENC.FECHA_HORA_OPERACION     AS FECHA_HORA_OPERACION,\n" +
            "  PLAENC.CODIGO_INSTITUCION_APV   AS CODIGO_INSTITUCION_APV,\n" +
            "  PLAENC.RUT_PAGADOR              AS RUT_PAGADOR,\n" +
            "  PLAENC.DV_PAGADOR               AS DV_PAGADOR,\n" +
            "  PLAENC.TIPO_PAGADOR             AS TIPO_PAGADOR,\n" +
            "  PLAENC.CORREO_PAGADOR           AS CORREO_PAGADOR,\n" +
            "  PLAENC.ESTADO_PROCESO           AS ESTADO_PROCESO,\n" +
            "  PLAENC.ESTADO_PROCESO_EXCEPTION AS ESTADO_PROCESO_EXCEPTION,\n" +
            "  EST.DESCRIPCION_PROCESO         AS DESCRIPCION_PROCESO,\n" +
            "  PLAENC.ID_CARGAABONO            AS ID_CARGAABONO\n" + "FROM REND_ARCHIVO_PLANILLA_ENCAB PLAENC,\n" +
            "  REND_ESTADO_DE_PROCESO EST\n" + "WHERE ID_CARGA         = ?\n" + "AND ID_FOLIO_PLANILLA  = ?\n" +
            "AND EST.ESTADO_PROCESO = PLAENC.ESTADO_PROCESO";

        /*"SELECT ID_CARGA,\n" +
        "  PLAENC.ID_FOLIO_PLANILLA        AS ID_FOLIO_PLANILLA,\n" +
        "  PLAENC.TIPO_DE_REGISTRO         AS TIPO_DE_REGISTRO,\n" +
        "  PLAENC.TIPO_PAGO                AS TIPO_PAGO,\n" +
        "  PLAENC.FECHA_HORA_OPERACION     AS FECHA_HORA_OPERACION,\n" +
        "  PLAENC.CODIGO_INSTITUCION_APV   AS CODIGO_INSTITUCION_APV,\n" +
        "  PLAENC.RUT_PAGADOR              AS RUT_PAGADOR,\n" +
        "  PLAENC.DV_PAGADOR               AS DV_PAGADOR,\n" +
        "  PLAENC.TIPO_PAGADOR             AS TIPO_PAGADOR,\n" +
        "  PLAENC.CORREO_PAGADOR           AS CORREO_PAGADOR,\n" +
        "  PLAENC.ESTADO_PROCESO           AS ESTADO_PROCESO,\n" +
        "  PLAENC.ESTADO_PROCESO_EXCEPTION AS ESTADO_PROCESO_EXCEPTION,\n" +
        "  EST.DESCRIPCION_PROCESO         AS DESCRIPCION_PROCESO,\n" +
        "  PLAENC.ID_CARGAABONO            AS ID_CARGAABONO,\n" +
        "  nvl(PLAENC.POLIZA_ASOC, 0)      AS POLIZA_ASOC,\n" +
        "  PLAENC.CONCEPTO_PAGO            AS CONCEPTO_PAGO,\n" +
        "  nvl(\n" +
        "  (SELECT DESCRIPCION\n" +
        "  FROM REND_CONCEPTO\n" +
        "  WHERE COD_INSTRUMENTO = PLAENC.CONCEPTO_PAGO\n" +
        "  ), '')                 as DESC_CONCEPTO,\n" +
        "  PLAENC.FECHA_PAGO_CAJA AS FECHA_PAGO_CAJA,\n" +
        "  nvl(PLAENC.ID_FOLIO_PAGOCAJA, 0) AS ID_FOLIO_PAGOCAJA\n" +
        "FROM REND_ARCHIVO_PLANILLA_ENCAB PLAENC,\n" +
        "  REND_ESTADO_DE_PROCESO EST\n" +
        "WHERE ID_CARGA         = ?\n" +
        "AND ID_FOLIO_PLANILLA  = ?\n" +
        "AND EST.ESTADO_PROCESO = PLAENC.ESTADO_PROCESO";*/

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCarga);
            stmt.setDouble(2, folio);
            rs = stmt.executeQuery();


            if (rs.next()) {
                archivoPlanillaEncabezadoDTO = new ArchivoPlanillaEncabezadoDTO();
                archivoPlanillaEncabezadoDTO.setId_folio_planilla(rs.getDouble("ID_FOLIO_PLANILLA"));
                archivoPlanillaEncabezadoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                archivoPlanillaEncabezadoDTO.setTipo_de_registro(rs.getInt("TIPO_PAGO"));
                archivoPlanillaEncabezadoDTO.setFecha_hora_operacion(new Date(rs.getTimestamp("FECHA_HORA_OPERACION").getTime()));
                archivoPlanillaEncabezadoDTO.setCodigo_institucion_apv(rs.getInt("CODIGO_INSTITUCION_APV"));
                archivoPlanillaEncabezadoDTO.setRut_pagador(rs.getInt("RUT_PAGADOR"));
                archivoPlanillaEncabezadoDTO.setDv_pagador(rs.getString("DV_PAGADOR"));
                archivoPlanillaEncabezadoDTO.setTipo_pagador(rs.getInt("TIPO_PAGADOR"));
                archivoPlanillaEncabezadoDTO.setCorreo_pagador(rs.getString("CORREO_PAGADOR"));
                archivoPlanillaEncabezadoDTO.setEstado_proceso(rs.getInt("ESTADO_PROCESO"));
                archivoPlanillaEncabezadoDTO.setEstado_proceso_exception(rs.getString("ESTADO_PROCESO_EXCEPTION"));
                archivoPlanillaEncabezadoDTO.setEstado_proceso_descripcion(rs.getString("DESCRIPCION_PROCESO"));
                //archivoPlanillaEncabezadoDTO.setPolizaAsociada(rs.getInt("POLIZA_ASOC"));
                //archivoPlanillaEncabezadoDTO.setConceptoRecaudacion(rs.getString("DESC_CONCEPTO"));
                //archivoPlanillaEncabezadoDTO.setFechaPagoCaja(new Date(rs.getTimestamp("FECHA_PAGO_CAJA").getTime()));
                //archivoPlanillaEncabezadoDTO.setId_folio_pagocaja(rs.getLong("ID_FOLIO_PAGOCAJA"));
                archivoPlanillaEncabezadoDTO.setAntecedentes(getAntecPlanillaByIdCargaFolio(idCarga, folio));
                archivoPlanillaEncabezadoDTO.setDetalle(getDetallePlanillaByIdCargaFolio(idCarga, folio));
                archivoPlanillaEncabezadoDTO.setResumen(getResumenPlanillaByIdCargaFolio(idCarga, folio));
            }
        } catch (SQLException e) {
            logger.error("getPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return archivoPlanillaEncabezadoDTO;
    }

    public ArchivoPlanillaAntecedentesDTO getAntecPlanillaByIdCargaFolio(Double idCarga, Double folio) {
        ArchivoPlanillaAntecedentesDTO archivoPlanillaAntecedentesDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  ID_FOLIO_PLANILLA,\n" + "  TIPO_DE_REGISTRO,\n" +
            "  RAZON_SOCIAL_PAGADOR,\n" + "  CODIGO_ACTIVIDAD_PAGADOR,\n" + "  CALLE_PAGADOR,\n" +
            "  NUMERO_PAGADOR,\n" + "  DEPART_POBLA_PAGADOR,\n" + "  COMUNA_PAGADOR,\n" + "  CIUDAD_PAGADOR,\n" +
            "  REGION_PAGADOR,\n" + "  TELEFONO_PAGADOR,\n" + "  NOMBRE_REPRESENTANTE_LEGAL,\n" +
            "  RUT_REPRESENTANTE_LEGAL,\n" + "  DV_REPRESENTANTE_LEGAL\n" + "FROM REND_ARCHIVO_PLANILLA_ANTEC\n" +
            "WHERE ID_CARGA        = ?\n" + "AND ID_FOLIO_PLANILLA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCarga);
            stmt.setDouble(2, folio);
            rs = stmt.executeQuery();


            if (rs.next()) {
                archivoPlanillaAntecedentesDTO = new ArchivoPlanillaAntecedentesDTO();
                archivoPlanillaAntecedentesDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                archivoPlanillaAntecedentesDTO.setRazon_social_pagador(rs.getString("RAZON_SOCIAL_PAGADOR"));
                archivoPlanillaAntecedentesDTO.setCodigo_actividad_pagador(rs.getString("CODIGO_ACTIVIDAD_PAGADOR"));
                archivoPlanillaAntecedentesDTO.setCalle_pagador(rs.getString("CALLE_PAGADOR"));
                archivoPlanillaAntecedentesDTO.setNumero_pagador(rs.getString("NUMERO_PAGADOR"));
                archivoPlanillaAntecedentesDTO.setDepart_pobla_pagador(rs.getString("DEPART_POBLA_PAGADOR"));
                archivoPlanillaAntecedentesDTO.setComuna_pagador(rs.getString("COMUNA_PAGADOR"));
                archivoPlanillaAntecedentesDTO.setCiudad_pagador(rs.getString("CIUDAD_PAGADOR"));
                archivoPlanillaAntecedentesDTO.setRegion_pagador(rs.getString("REGION_PAGADOR"));
                archivoPlanillaAntecedentesDTO.setTelefono_pagador(rs.getString("TELEFONO_PAGADOR"));
                archivoPlanillaAntecedentesDTO.setNombre_representante_legal(rs.getString("NOMBRE_REPRESENTANTE_LEGAL"));
                archivoPlanillaAntecedentesDTO.setRut_representante_legal(rs.getInt("RUT_REPRESENTANTE_LEGAL"));
                archivoPlanillaAntecedentesDTO.setDv_representante_legal(rs.getString("DV_REPRESENTANTE_LEGAL"));
            }
        } catch (SQLException e) {
            logger.error("getAntecPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAntecPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return archivoPlanillaAntecedentesDTO;
    }


    public List<ArchivoPlanillaDetalleDTO> getDetallePlanillaByIdCargaFolio(Double idCarga, Double folio) {
        List<ArchivoPlanillaDetalleDTO> lstArchivoPlanillaDetalleDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT DETALLE.ID_CARGA                AS ID_CARGA,\n" +
            "  DETALLE.ID_FOLIO_PLANILLA            AS ID_FOLIO_PLANILLA,\n" +
            "  DETALLE.TIPO_DE_REGISTRO             AS TIPO_DE_REGISTRO,\n" +
            "  DETALLE.RUT_TRABAJADOR               AS RUT_TRABAJADOR,\n" +
            "  DETALLE.DV_TRABAJADOR                AS DV_TRABAJADOR,\n" +
            "  DETALLE.APELLIDO_PATERNO_TRABAJADOR  AS APELLIDO_PATERNO_TRABAJADOR,\n" +
            "  DETALLE.APELLIDO_MATERNO_TRABAJADOR  AS APELLIDO_MATERNO_TRABAJADOR,\n" +
            "  DETALLE.NOMBRES_TRABAJADOR           AS NOMBRES_TRABAJADOR,\n" +
            "  DETALLE.SEXO                         AS SEXO,\n" +
            "  DETALLE.NACIONALIDAD                 AS NACIONALIDAD,\n" +
            "  DETALLE.RENTA_IMPONIBLE_TRABAJADOR   AS RENTA_IMPONIBLE_TRABAJADOR,\n" +
            "  DETALLE.COTIZACION_VOLUNTARIA_APVI   AS COTIZACION_VOLUNTARIA_APVI,\n" +
            "  DETALLE.COTIZACION_VOLUNTARIA_APVI_B AS COTIZACION_VOLUNTARIA_APVI_B,\n" +
            "  DETALLE.NRO_CONTRATO_APVI            AS NRO_CONTRATO_APVI,\n" +
            "  DETALLE.DEPOSITO_CONVENIO            AS DEPOSITO_CONVENIO,\n" +
            "  DETALLE.TOTAL_A_PAGAR                AS TOTAL_A_PAGAR,\n" +
            "  DETALLE.PERIODO_PAGO                 AS PERIODO_PAGO,\n" +
            "  DETALLE.APV_COLECTIVO_EMPL           AS APV_COLECTIVO_EMPL,\n" +
            "  DETALLE.APV_COLECTIVO_TRABAJ         AS APV_COLECTIVO_TRABAJ,\n" +
            "  DETALLE.N_CONTRATO_APVC              AS N_CONTRATO_APVC,\n" +
            "  DETALLE.CODIGO_MOV_PERSONAL          AS CODIGO_MOV_PERSONAL,\n" +
            "  DETALLE.FECHA_INICIO                 AS FECHA_INICIO,\n" +
            "  DETALLE.FECHA_TERMINO                AS FECHA_TERMINO,\n" +
            "  DETALLE.ESTADO_PROCESO_CAJA          AS ESTADO_PROCESO_CAJA,\n" +
            "  DETALLE.ESTADO_PROCESO_CAJA_EXC      AS ESTADO_PROCESO_CAJA_EXC,\n" +
            "  EST.DESCRIPCION_PROCESO              AS DESCRIPCION_PROCESO,\n" +
            "  nvl(DETALLE.POLIZA_ASOC, 0)          AS POLIZA_ASOC,\n" + "  nvl(\n" + "  (SELECT DESCRIPCION\n" +
            "  FROM REND_CONCEPTO\n" + "  WHERE COD_INSTRUMENTO = DETALLE.CONCEPTO_PAGO\n" +
            "  ), '')                    AS DESC_CONCEPTO,\n" + "  DETALLE.FECHA_PAGO_CAJA   AS FECHA_PAGO_CAJA,\n" +
            "  DETALLE.ID_FOLIO_PAGOCAJA AS ID_FOLIO_PAGOCAJA\n" + "FROM REND_ARCHIVO_PLANILLA_DETALLE DETALLE,\n" +
            "  REND_ESTADO_DE_PROCESO EST\n" + "WHERE ID_CARGA         = ?\n" + "AND ID_FOLIO_PLANILLA  = ?\n" +
            "AND EST.ESTADO_PROCESO = DETALLE.ESTADO_PROCESO_CAJA\n";

        /*"SELECT ID_CARGA,\n" +
        "  ID_FOLIO_PLANILLA,\n" +
        "  TIPO_DE_REGISTRO,\n" +
        "  RUT_TRABAJADOR,\n" +
        "  DV_TRABAJADOR,\n" +
        "  APELLIDO_PATERNO_TRABAJADOR,\n" +
        "  APELLIDO_MATERNO_TRABAJADOR,\n" +
        "  NOMBRES_TRABAJADOR,\n" +
        "  SEXO,\n" +
        "  NACIONALIDAD,\n" +
        "  RENTA_IMPONIBLE_TRABAJADOR,\n" +
        "  COTIZACION_VOLUNTARIA_APVI,\n" +
        "  COTIZACION_VOLUNTARIA_APVI_B,\n" +
        "  NRO_CONTRATO_APVI,\n" +
        "  DEPOSITO_CONVENIO,\n" +
        "  TOTAL_A_PAGAR,\n" +
        "  PERIODO_PAGO,\n" +
        "  APV_COLECTIVO_EMPL,\n" +
        "  APV_COLECTIVO_TRABAJ,\n" +
        "  N_CONTRATO_APVC,\n" +
        "  CODIGO_MOV_PERSONAL,\n" +
        "  FECHA_INICIO,\n" +
        "  FECHA_TERMINO\n" +
        "FROM REND_ARCHIVO_PLANILLA_DETALLE\n" +
        "WHERE ID_CARGA        = ?\n" +
        "AND ID_FOLIO_PLANILLA = ?";*/

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCarga);
            stmt.setDouble(2, folio);
            rs = stmt.executeQuery();

            lstArchivoPlanillaDetalleDTO = new ArrayList<ArchivoPlanillaDetalleDTO>();
            while (rs.next()) {
                ArchivoPlanillaDetalleDTO archivoPlanillaDetalleDTO = new ArchivoPlanillaDetalleDTO();
                archivoPlanillaDetalleDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                archivoPlanillaDetalleDTO.setRut_trabajador(rs.getInt("RUT_TRABAJADOR"));
                archivoPlanillaDetalleDTO.setDv_trabajador(rs.getString("DV_TRABAJADOR"));
                archivoPlanillaDetalleDTO.setApellido_paterno_trabajador(rs.getString("APELLIDO_PATERNO_TRABAJADOR"));
                archivoPlanillaDetalleDTO.setApellido_materno_trabajador(rs.getString("APELLIDO_MATERNO_TRABAJADOR"));
                archivoPlanillaDetalleDTO.setNombres_trabajador(rs.getString("NOMBRES_TRABAJADOR"));
                archivoPlanillaDetalleDTO.setSexo(rs.getString("SEXO"));
                archivoPlanillaDetalleDTO.setNacionalidad(rs.getInt("NACIONALIDAD"));
                archivoPlanillaDetalleDTO.setRenta_imponible_trabajador(rs.getDouble("RENTA_IMPONIBLE_TRABAJADOR"));
                archivoPlanillaDetalleDTO.setCotizacion_voluntaria_apvi(rs.getDouble("COTIZACION_VOLUNTARIA_APVI"));
                archivoPlanillaDetalleDTO.setCotizacion_voluntaria_apvi_b(rs.getDouble("COTIZACION_VOLUNTARIA_APVI_B"));
                archivoPlanillaDetalleDTO.setNro_contrato_apvi(rs.getString("NRO_CONTRATO_APVI"));
                archivoPlanillaDetalleDTO.setDeposito_convenio(rs.getDouble("DEPOSITO_CONVENIO"));
                archivoPlanillaDetalleDTO.setTotal_a_pagar(rs.getDouble("TOTAL_A_PAGAR"));
                archivoPlanillaDetalleDTO.setPeriodo_pago(new Date(rs.getDate("PERIODO_PAGO").getTime()));
                archivoPlanillaDetalleDTO.setApv_colectivo_empl(rs.getDouble("APV_COLECTIVO_EMPL"));
                archivoPlanillaDetalleDTO.setApv_colectivo_trabaj(rs.getDouble("APV_COLECTIVO_TRABAJ"));
                archivoPlanillaDetalleDTO.setN_contrato_apvc(rs.getString("N_CONTRATO_APVC"));
                archivoPlanillaDetalleDTO.setCodigo_mov_personal(rs.getInt("CODIGO_MOV_PERSONAL"));
                archivoPlanillaDetalleDTO.setFecha_inicio(new Date(rs.getDate("FECHA_INICIO").getTime()));
                archivoPlanillaDetalleDTO.setFecha_termino(new Date(rs.getDate("FECHA_TERMINO").getTime()));
                archivoPlanillaDetalleDTO.setEstado_proceso_pago(rs.getInt("ESTADO_PROCESO_CAJA"));
                archivoPlanillaDetalleDTO.setEstado_proceso_pago_exception(rs.getString("ESTADO_PROCESO_CAJA_EXC"));
                archivoPlanillaDetalleDTO.setEstado_proceso_pago_descriocion(rs.getString("DESCRIPCION_PROCESO"));
                archivoPlanillaDetalleDTO.setPolizaAsociada(rs.getInt("POLIZA_ASOC"));
                archivoPlanillaDetalleDTO.setConceptoRecaudacion(rs.getString("DESC_CONCEPTO"));
                archivoPlanillaDetalleDTO.setFechaPagoCaja(new Date(rs.getDate("FECHA_PAGO_CAJA").getTime()));
                archivoPlanillaDetalleDTO.setId_folio_pagocaja(rs.getLong("ID_FOLIO_PAGOCAJA"));
                lstArchivoPlanillaDetalleDTO.add(archivoPlanillaDetalleDTO);
            }
        } catch (SQLException e) {
            logger.error("getAntecPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAntecPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return lstArchivoPlanillaDetalleDTO;
    }

    public ArchivoPlanillaResumenDTO getResumenPlanillaByIdCargaFolio(Double idCarga, Double folio) {
        ArchivoPlanillaResumenDTO archivoPlanillaResumenDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  ID_FOLIO_PLANILLA,\n" + "  TIPO_DE_REGISTRO,\n" +
            "  TOTAL_COTIZA_VOLUNTARIA_APVI,\n" + "  TOTAL_COTIZA_VOLUNTARIA_APVI_B,\n" +
            "  TOTAL_APV_COLECT_EMPLEADOR,\n" + "  TOTAL_APV_COLECT_TRABAJADOR,\n" + "  TOTAL_DEPOSITOS_CONVENIDOS,\n" +
            "  TOTAL_PAGAR_IA,\n" + "  PERIODO_PAGO,\n" + "  NUMERO_AFILIADOS_INFORMADOS\n" +
            "FROM REND_ARCHIVO_PLANILLA_RESUMEN\n" + "WHERE ID_CARGA        = ?\n" + "AND ID_FOLIO_PLANILLA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCarga);
            stmt.setDouble(2, folio);
            rs = stmt.executeQuery();


            if (rs.next()) {
                archivoPlanillaResumenDTO = new ArchivoPlanillaResumenDTO();
                archivoPlanillaResumenDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                archivoPlanillaResumenDTO.setTotal_cotiza_voluntaria_apvi(rs.getDouble("TOTAL_COTIZA_VOLUNTARIA_APVI"));
                archivoPlanillaResumenDTO.setTotal_cotiza_voluntaria_apvi_b(rs.getDouble("TOTAL_COTIZA_VOLUNTARIA_APVI_B"));
                archivoPlanillaResumenDTO.setTotal_apv_colect_empleador(rs.getDouble("TOTAL_APV_COLECT_EMPLEADOR"));
                archivoPlanillaResumenDTO.setTotal_apv_colect_trabajador(rs.getDouble("TOTAL_APV_COLECT_TRABAJADOR"));
                archivoPlanillaResumenDTO.setTotal_depositos_convenidos(rs.getDouble("TOTAL_DEPOSITOS_CONVENIDOS"));
                archivoPlanillaResumenDTO.setTotal_pagar_ia(rs.getDouble("TOTAL_PAGAR_IA"));
                archivoPlanillaResumenDTO.setPeriodo_pago(new Date(rs.getDate("PERIODO_PAGO").getTime()));
                archivoPlanillaResumenDTO.setNumero_afiliados_informados(rs.getInt("NUMERO_AFILIADOS_INFORMADOS"));
            }
        } catch (SQLException e) {
            logger.error("getAntecPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAntecPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return archivoPlanillaResumenDTO;
    }

    public List<ResumenAbonoDTO> getAbonoAndPlanillaByIdCarga(Double idCargaAbono, Double idCargaPlanilla) {
        List<ResumenAbonoDTO> lstResumenAbonoDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  CODIGO_RECAUDADOR,\n" + "  GLOSA_RECAUDADOR,\n" + "  ID_MOTIVO_RECHAZO,\n" +
            "  OBSERVACION_RECHAZO,\n" + "  OBSERVACION_AUTORIZACION,\n" + "  VALIDACION_CARTOLA_CTA_CTE,\n" +
            "  TOTAL_MONTO_INF_CARTOLA,\n" + "  TIPO_DE_REGISTRO,\n" + "  IDENTIFICADOR_IIP,\n" +
            "  TOTAL_MONTO_FONDO,\n" + "  TOTAL_MONTO_INSTITUCION,\n" + "  TOTAL_MONTO_AFC,\n" + "  CUENTA_FONDO,\n" +
            "  CUENTA_INSTITUCION,\n" + "  CUENTA_AFC,\n" + "  IDENTIFICACION_BANCO,\n" + "  CODIGO_BANCO,\n" +
            "  FECHA_ABONO,\n" + "  NUMERO_DE_PLANILLAS\n" + "FROM REND_RESUMEN_ABONO\n" + "WHERE ID_CARGA = ?\n" +
            "ORDER BY CODIGO_RECAUDADOR";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCargaAbono);
            rs = stmt.executeQuery();

            lstResumenAbonoDTO = new ArrayList<ResumenAbonoDTO>();
            while (rs.next()) {
                ResumenAbonoDTO resumenAbonoDTO = new ResumenAbonoDTO();
                resumenAbonoDTO.setCodigo_recaudador(rs.getString("CODIGO_RECAUDADOR"));
                resumenAbonoDTO.setGlosa_recaudador(rs.getString("GLOSA_RECAUDADOR"));
                resumenAbonoDTO.setId_motivo_rechazo(rs.getInt("ID_MOTIVO_RECHAZO"));
                resumenAbonoDTO.setObservacion_rechazo(rs.getString("OBSERVACION_RECHAZO"));
                resumenAbonoDTO.setObservacion_autorizacion(rs.getString("OBSERVACION_AUTORIZACION"));
                resumenAbonoDTO.setValidacion_cartola_cta_cte(rs.getInt("VALIDACION_CARTOLA_CTA_CTE"));
                resumenAbonoDTO.setTotal_monto_inf_cartola(rs.getDouble("TOTAL_MONTO_INF_CARTOLA"));
                resumenAbonoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                resumenAbonoDTO.setIdentificador_iip(rs.getInt("IDENTIFICADOR_IIP"));
                resumenAbonoDTO.setTotal_monto_fondo(rs.getDouble("TOTAL_MONTO_FONDO"));
                resumenAbonoDTO.setTotal_monto_institucion(rs.getDouble("TOTAL_MONTO_INSTITUCION"));
                resumenAbonoDTO.setTotal_monto_afc(rs.getDouble("TOTAL_MONTO_AFC"));
                resumenAbonoDTO.setCuenta_fondo(rs.getString("CUENTA_FONDO"));
                resumenAbonoDTO.setCuenta_institucion(rs.getString("CUENTA_INSTITUCION"));
                resumenAbonoDTO.setCuenta_afc(rs.getString("CUENTA_AFC"));
                resumenAbonoDTO.setIdentificacion_banco(rs.getString("IDENTIFICACION_BANCO"));
                resumenAbonoDTO.setCodigo_banco(rs.getString("CODIGO_BANCO"));
                resumenAbonoDTO.setFecha_abono(new Date(rs.getDate("FECHA_ABONO").getTime()));
                resumenAbonoDTO.setNumero_de_planillas(rs.getInt("NUMERO_DE_PLANILLAS"));
                resumenAbonoDTO.setArchivoabono(getDetalleAbonoAndPlanillaByIdCarga(idCargaAbono, idCargaPlanilla,
                                                                                    resumenAbonoDTO.getCodigo_recaudador()));
                Double totalpagado = 0.0;
                Double totalpagadoprocesado = 0.0;
                Double totalpagadonoprocesado = 0.0;

                if (resumenAbonoDTO.getArchivoabono().size() > 0) {


                    List<ArchivoAbonoDTO> lstArchivoAbonoDTOReorganized =
                        callReorganizeAbono(resumenAbonoDTO.getArchivoabono());

                    resumenAbonoDTO.setArchivoabonoplanillaprocesados(new ArrayList<ArchivoAbonoDTO>((Collection<ArchivoAbonoDTO>) CollectionUtils.select(lstArchivoAbonoDTOReorganized,
                                                                                                                                                          ACEPTADOS)));
                    resumenAbonoDTO.setArchivoabonoplanillanoprocesados(new ArrayList<ArchivoAbonoDTO>((Collection<ArchivoAbonoDTO>) CollectionUtils.select(lstArchivoAbonoDTOReorganized,
                                                                                                                                                            NOACEPTADOS)));

                    if (resumenAbonoDTO.getArchivoabonoplanillaprocesados() != null &&
                        resumenAbonoDTO.getArchivoabonoplanillaprocesados().size() > 0) {
                        for (int i = 0; i < resumenAbonoDTO.getArchivoabonoplanillaprocesados().size(); i++) {
                            if (resumenAbonoDTO.getArchivoabonoplanillaprocesados().get(i).getPlanilla() != null) {
                                if (resumenAbonoDTO.getArchivoabonoplanillaprocesados().get(i).getPlanilla().getDetalleIndividual() !=
                                    null) {
                                    if (resumenAbonoDTO.getArchivoabonoplanillaprocesados().get(i).getPlanilla().getDetalleIndividual().getTotal_a_pagar() !=
                                        null) {
                                        totalpagadoprocesado =
                                            totalpagadoprocesado +
                                            resumenAbonoDTO.getArchivoabonoplanillaprocesados().get(i).getPlanilla().getDetalleIndividual().getTotal_a_pagar();
                                    }
                                }
                            }

                        }
                    }

                    if (resumenAbonoDTO.getArchivoabonoplanillanoprocesados() != null &&
                        resumenAbonoDTO.getArchivoabonoplanillanoprocesados().size() > 0) {
                        for (int i = 0; i < resumenAbonoDTO.getArchivoabonoplanillanoprocesados().size(); i++) {
                            if (resumenAbonoDTO.getArchivoabonoplanillanoprocesados().get(i).getPlanilla() != null) {
                                if (resumenAbonoDTO.getArchivoabonoplanillanoprocesados().get(i).getPlanilla().getDetalleIndividual() !=
                                    null) {
                                    if (resumenAbonoDTO.getArchivoabonoplanillanoprocesados().get(i).getPlanilla().getDetalleIndividual().getTotal_a_pagar() !=
                                        null) {
                                        totalpagadonoprocesado =
                                            totalpagadonoprocesado +
                                            resumenAbonoDTO.getArchivoabonoplanillanoprocesados().get(i).getPlanilla().getDetalleIndividual().getTotal_a_pagar();
                                    }
                                }
                            }

                        }
                    }


                    /*for(int i = 0; i < resumenAbonoDTO.getArchivoabono().size(); i++){
                        if(resumenAbonoDTO.getArchivoabono().get(i) != null && resumenAbonoDTO.getArchivoabono().get(i).getPlanilla().getEstado_proceso().intValue() == Constant.PROCESO_AUTORIZADO){
                            totalpagadoprocesado = totalpagadoprocesado + resumenAbonoDTO.getArchivoabono().get(i).getPlanilla().getResumen().getTotal_pagar_ia();
                        }else{
                            if(resumenAbonoDTO.getArchivoabono().get(i) != null){
                                totalpagadonoprocesado = totalpagadonoprocesado + resumenAbonoDTO.getArchivoabono().get(i).getPlanilla().getResumen().getTotal_pagar_ia();
                            }
                        }

                    }*/
                    totalpagado = totalpagadoprocesado + totalpagadonoprocesado;
                    resumenAbonoDTO.setMonto_total_pagado_planilla(totalpagado);
                    resumenAbonoDTO.setMonto_total_pagado_planillaprocesados(totalpagadoprocesado);
                    resumenAbonoDTO.setMonto_total_pagado_planillanoprocesados(totalpagadonoprocesado);


                    lstResumenAbonoDTO.add(resumenAbonoDTO);
                }
            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstResumenAbonoDTO;
    }

    public List<ArchivoAbonoDTO> getDetalleAbonoAndPlanillaByIdCarga(Double idCargaAbono, Double idCargaPlanilla,
                                                                     String codigoRecaudador) {
        List<ArchivoAbonoDTO> lstArchivoAbonoDTO = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  CODIGO_RECAUDADOR,\n" + "  ID_FOLIO_ABONO,\n" + "  TIPO_DE_REGISTRO,\n" +
            "  MONTO_ABONO_FONDO,\n" + "  MONTO_ABONO_INSTITUCION,\n" + "  MONTO_ABONO_AFC,\n" + "  RUT_EMPLEADOR,\n" +
            "  DIGITO_VERFICADOR,\n" + "  PERIODO,\n" + "  FECHA_PAGO,\n" + "  LOTE,\n" + "  SUCURSAL,\n" +
            "  ESTADO_PROCESO,\n" + "  ESTADO_PROCESO_EXCEPCION\n" + "FROM REND_ARCHIVO_ABONO\n" +
            "WHERE ID_CARGA        = ?\n" + "AND CODIGO_RECAUDADOR = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCargaAbono);
            stmt.setString(2, codigoRecaudador);
            rs = stmt.executeQuery();

            lstArchivoAbonoDTO = new ArrayList<ArchivoAbonoDTO>();
            while (rs.next()) {
                ArchivoAbonoDTO archivoAbonoDTO = new ArchivoAbonoDTO();
                archivoAbonoDTO.setId_folio_abono(rs.getDouble("ID_FOLIO_ABONO"));
                archivoAbonoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                archivoAbonoDTO.setMonto_abono_fondo(rs.getDouble("MONTO_ABONO_FONDO"));
                archivoAbonoDTO.setMonto_abono_institucion(rs.getDouble("MONTO_ABONO_INSTITUCION"));
                archivoAbonoDTO.setMonto_abono_afc(rs.getDouble("MONTO_ABONO_AFC"));
                archivoAbonoDTO.setRut_empleador(rs.getInt("RUT_EMPLEADOR"));
                archivoAbonoDTO.setDigito_verficador(rs.getString("DIGITO_VERFICADOR"));
                archivoAbonoDTO.setPeriodo(new Date(rs.getDate("PERIODO").getTime()));
                archivoAbonoDTO.setFecha_pago(new Date(rs.getDate("FECHA_PAGO").getTime()));
                archivoAbonoDTO.setLote(rs.getString("LOTE"));
                archivoAbonoDTO.setSucursal(rs.getString("SUCURSAL"));
                archivoAbonoDTO.setEstado_proceso(rs.getInt("ESTADO_PROCESO"));
                archivoAbonoDTO.setEstado_proceso_excepcion(rs.getString("ESTADO_PROCESO_EXCEPCION"));
                archivoAbonoDTO.setPlanilla(getPlanillaByIdCargaFolio(idCargaPlanilla,
                                                                      archivoAbonoDTO.getId_folio_abono()));

                lstArchivoAbonoDTO.add(archivoAbonoDTO);

                /*Double id_folio_abono = rs.getDouble("ID_FOLIO_ABONO");
                ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO = getPlanillaByIdCargaFolio(idCargaPlanilla, id_folio_abono);


                if(archivoPlanillaEncabezadoDTO.getDetalle().size() > 0){
                    for(int i = 0; i < archivoPlanillaEncabezadoDTO.getDetalle().size(); i++){

                        ArchivoAbonoDTO archivoAbonoDTO = new ArchivoAbonoDTO();
                        archivoAbonoDTO.setId_folio_abono(id_folio_abono);
                        archivoAbonoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                        archivoAbonoDTO.setMonto_abono_fondo(rs.getDouble("MONTO_ABONO_FONDO"));
                        archivoAbonoDTO.setMonto_abono_institucion(rs.getDouble("MONTO_ABONO_INSTITUCION"));
                        archivoAbonoDTO.setMonto_abono_afc(rs.getDouble("MONTO_ABONO_AFC"));
                        archivoAbonoDTO.setRut_empleador(rs.getInt("RUT_EMPLEADOR"));
                        archivoAbonoDTO.setDigito_verficador(rs.getString("DIGITO_VERFICADOR"));
                        archivoAbonoDTO.setPeriodo(new Date(rs.getDate("PERIODO").getTime()));
                        archivoAbonoDTO.setFecha_pago(new Date(rs.getDate("FECHA_PAGO").getTime()));
                        archivoAbonoDTO.setLote(rs.getString("LOTE"));
                        archivoAbonoDTO.setSucursal(rs.getString("SUCURSAL"));
                        archivoAbonoDTO.setEstado_proceso(rs.getInt("ESTADO_PROCESO"));
                        archivoAbonoDTO.setEstado_proceso_excepcion(rs.getString("ESTADO_PROCESO_EXCEPCION"));

                        ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTOIndividual = new ArchivoPlanillaEncabezadoDTO(archivoPlanillaEncabezadoDTO);
                        archivoPlanillaEncabezadoDTOIndividual.setDetalleIndividual(archivoPlanillaEncabezadoDTO.getDetalle().get(i));

                        archivoAbonoDTO.setPlanilla(archivoPlanillaEncabezadoDTOIndividual);

                        //ArchivoPlanillaDetalleDTO archivoPlanillaDetalleDTO = new ArchivoPlanillaDetalleDTO(archivoPlanillaEncabezadoDTO.getDetalle().get(i));
                        //archivoAbonoDTO.getPlanilla().setDetalleIndividual(archivoPlanillaDetalleDTO);
                        lstArchivoAbonoDTO.add(archivoAbonoDTO);
                    }
                }
                */

                //archivoAbonoDTO.getPlanilla().getResumen().getTotal_pagar_ia();


            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstArchivoAbonoDTO;
    }


    public List<ArchivoAbonoDTO> callReorganizeAbono(List<ArchivoAbonoDTO> lstArchivoAbonoDTO) {
        List<ArchivoAbonoDTO> lstArchivoAbonoDTOReorganized = new ArrayList<ArchivoAbonoDTO>();

        for (int i = 0; i < lstArchivoAbonoDTO.size(); i++) {
            if (lstArchivoAbonoDTO.get(i).getPlanilla() != null &&
                lstArchivoAbonoDTO.get(i).getPlanilla().getDetalle() != null &&
                lstArchivoAbonoDTO.get(i).getPlanilla().getDetalle().size() > 0) {
                for (int j = 0; j < lstArchivoAbonoDTO.get(i).getPlanilla().getDetalle().size(); j++) {
                    ArchivoAbonoDTO archivoAbonoDTO = new ArchivoAbonoDTO(lstArchivoAbonoDTO.get(i));

                    ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTOIndividual =
                        new ArchivoPlanillaEncabezadoDTO(archivoAbonoDTO.getPlanilla());
                    archivoPlanillaEncabezadoDTOIndividual.setDetalleIndividual((archivoAbonoDTO.getPlanilla().getDetalle().get(j)));

                    archivoAbonoDTO.setPlanilla(archivoPlanillaEncabezadoDTOIndividual);

                    lstArchivoAbonoDTOReorganized.add(archivoAbonoDTO);

                }
            }
        }

        return lstArchivoAbonoDTOReorganized;
    }

    public List<ResumenAbonoDTO> getPlanillaNoInformadosByIdCarga(Double idCargaPlanilla, Double idCargaAbono) {
        List<ResumenAbonoDTO> lstresumenAbonoDTO = null;
        List<ArchivoAbonoDTO> lstarchivoAbonoDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT PLAENC.ID_FOLIO_PLANILLA   AS ID_FOLIO_PLANILLA,\n" +
            "  PLAENC.TIPO_DE_REGISTRO         AS TIPO_DE_REGISTRO,\n" +
            "  PLAENC.TIPO_PAGO                AS TIPO_PAGO,\n" +
            "  PLAENC.FECHA_HORA_OPERACION     AS FECHA_HORA_OPERACION,\n" +
            "  PLAENC.CODIGO_INSTITUCION_APV   AS CODIGO_INSTITUCION_APV,\n" +
            "  PLAENC.RUT_PAGADOR              AS RUT_PAGADOR,\n" +
            "  PLAENC.DV_PAGADOR               AS DV_PAGADOR,\n" +
            "  PLAENC.TIPO_PAGADOR             AS TIPO_PAGADOR,\n" +
            "  PLAENC.CORREO_PAGADOR           AS CORREO_PAGADOR,\n" +
            "  PLAENC.ESTADO_PROCESO           AS ESTADO_PROCESO,\n" +
            "  PLAENC.ESTADO_PROCESO_EXCEPTION AS ESTADO_PROCESO_EXCEPTION,\n" +
            "  EST.DESCRIPCION_PROCESO         AS DESCRIPCION_PROCESO\n" +
            "FROM REND_ARCHIVO_PLANILLA_ENCAB PLAENC,\n" + "  REND_ESTADO_DE_PROCESO EST\n" +
            "WHERE ID_CARGA             = ?\n" + "AND EST.ESTADO_PROCESO     = PLAENC.ESTADO_PROCESO\n" +
            "AND ID_FOLIO_PLANILLA NOT IN\n" + "  (SELECT ID_FOLIO_ABONO FROM REND_ARCHIVO_ABONO WHERE ID_CARGA = ?\n" +
            "  )";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCargaPlanilla);
            stmt.setDouble(2, idCargaAbono);
            rs = stmt.executeQuery();

            lstarchivoAbonoDTO = new ArrayList<ArchivoAbonoDTO>();
            Double totalPagado = 0.0;
            while (rs.next()) {
                ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO = new ArchivoPlanillaEncabezadoDTO();
                archivoPlanillaEncabezadoDTO.setId_folio_planilla(rs.getDouble("ID_FOLIO_PLANILLA"));
                archivoPlanillaEncabezadoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                archivoPlanillaEncabezadoDTO.setTipo_de_registro(rs.getInt("TIPO_PAGO"));
                archivoPlanillaEncabezadoDTO.setFecha_hora_operacion(new Date(rs.getTimestamp("FECHA_HORA_OPERACION").getTime()));
                archivoPlanillaEncabezadoDTO.setCodigo_institucion_apv(rs.getInt("CODIGO_INSTITUCION_APV"));
                archivoPlanillaEncabezadoDTO.setRut_pagador(rs.getInt("RUT_PAGADOR"));
                archivoPlanillaEncabezadoDTO.setDv_pagador(rs.getString("DV_PAGADOR"));
                archivoPlanillaEncabezadoDTO.setTipo_pagador(rs.getInt("TIPO_PAGADOR"));
                archivoPlanillaEncabezadoDTO.setCorreo_pagador(rs.getString("CORREO_PAGADOR"));
                archivoPlanillaEncabezadoDTO.setEstado_proceso(rs.getInt("ESTADO_PROCESO"));
                archivoPlanillaEncabezadoDTO.setEstado_proceso_exception(rs.getString("ESTADO_PROCESO_EXCEPTION"));
                archivoPlanillaEncabezadoDTO.setEstado_proceso_descripcion(rs.getString("DESCRIPCION_PROCESO"));
                archivoPlanillaEncabezadoDTO.setAntecedentes(getAntecPlanillaByIdCargaFolio(idCargaPlanilla,
                                                                                            archivoPlanillaEncabezadoDTO.getId_folio_planilla()));
                archivoPlanillaEncabezadoDTO.setDetalle(getDetallePlanillaByIdCargaFolio(idCargaPlanilla,
                                                                                         archivoPlanillaEncabezadoDTO.getId_folio_planilla()));
                archivoPlanillaEncabezadoDTO.setResumen(getResumenPlanillaByIdCargaFolio(idCargaPlanilla,
                                                                                         archivoPlanillaEncabezadoDTO.getId_folio_planilla()));

                totalPagado = totalPagado + archivoPlanillaEncabezadoDTO.getResumen().getTotal_pagar_ia();


                if (archivoPlanillaEncabezadoDTO.getDetalle().size() > 0) {
                    for (int i = 0; i < archivoPlanillaEncabezadoDTO.getDetalle().size(); i++) {
                        ArchivoAbonoDTO archivoAbonoDTO = new ArchivoAbonoDTO();

                        ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTOIndividual =
                            new ArchivoPlanillaEncabezadoDTO(archivoPlanillaEncabezadoDTO);
                        archivoPlanillaEncabezadoDTOIndividual.setDetalleIndividual(archivoPlanillaEncabezadoDTO.getDetalle().get(i));
                        archivoAbonoDTO.setPlanilla(archivoPlanillaEncabezadoDTOIndividual);
                        lstarchivoAbonoDTO.add(archivoAbonoDTO);
                    }
                }


                //lstarchivoAbonoDTO.add(archivoAbonoDTO);
            }


            if (lstarchivoAbonoDTO.size() > 0) {
                ResumenAbonoDTO resumenAbonoDTO = new ResumenAbonoDTO();
                resumenAbonoDTO.setArchivoabono(lstarchivoAbonoDTO);
                resumenAbonoDTO.setMonto_total_pagado_planilla(totalPagado);
                lstresumenAbonoDTO = new ArrayList<ResumenAbonoDTO>();
                lstresumenAbonoDTO.add(resumenAbonoDTO);
            } else {
                lstresumenAbonoDTO = new ArrayList<ResumenAbonoDTO>();
            }

        } catch (SQLException e) {
            logger.error("getPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getPlanillaByIdCargaFolio() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return lstresumenAbonoDTO;
    }
    /*
    public String callGeneraTransaccion(Integer rut, Long monto, String peridoCotiza, Date fechaPago, Integer entRec, Boolean isClient){
        Integer codResult;
        String descResult = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" +
        "{? = call PKG_MCJ_MANEJOCAJA_PRVRD.GenerarTransaccion(?, ?, ?, ?, ?, ?, ?)}";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.registerOutParameter(1, Types.NUMERIC);
            stmt.setInt(2, rut);
            stmt.setLong(3, monto);
            stmt.setString(4, peridoCotiza);
            stmt.setDate(5, new java.sql.Date(fechaPago.getTime()));
            stmt.setInt(6, entRec);
            stmt.setBoolean(7, isClient);
            stmt.registerOutParameter(8, Types.VARCHAR);
            stmt.executeQuery();

            codResult = stmt.getInt(1);
            descResult = stmt.getString(8);

            if(codResult != 0){
                logger.error("callGeneraTransaccion() - Se produjo un erro en el proceso de generaci�n de la transacci�n: " +  descResult);
            }else{
                descResult = null;
            }

        }catch (SQLException e){
            logger.error("callGeneraTransaccion() - Se ha producido un excepcion: ", e);
            descResult = null;
        }catch (Exception e){
            logger.error("callGeneraTransaccion() - Se ha producido un excepcion: ", e);
            descResult = null;
        }finally{
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return descResult;
    }
*/

    /**
     * METODO DE ENVIO A LA CAJA DE CADA TRANSACCION
     * QUE SE PROCESA A TRAVES DE SERVIPAG O CAJA LOS ANDES
     * @param archivoPlanillaEncabezadoDTO
     * @param detalle
     * @param isClient
     * @param tipoMedioPlanilla
     * @return
     */
    public EjecutaEncabezadoCajaDTO callGeneraTransaccion(ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO,
                                                          int detalle, Boolean isClient, String tipoMedioPlanilla,
                                                          Integer medioPago) {
        EjecutaEncabezadoCajaDTO dto = new EjecutaEncabezadoCajaDTO();
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" +
            "{? = call PKG_MCJ_MANEJOCAJA_PRVRD.GenerarTransaccion(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";

        /*
         * function GenerarTransaccion(
         *               2   p_Rut in number,
                         3   p_DvRut in varchar2,
                         4   p_Nombre_Con in varchar2,
                         5   p_Rut_Empleador in number,
                         6   p_DvRut_Empleador in varchar2,
                         7   p_Nombre_Empl in varchar2,
                         8   p_Folio_Previred in varchar2,

                         9   p_Monto in number,
                         10  p_Dep_Convenido in number,
                         11  p_CodigoRegimen in number, TODO: NUEVO PARAMETRO CODIGO REGIMEN TRIBUTARIO
                         12  p_PeriodoCotiza in varchar2,
                         13  p_FechRec in date,
                         14  p_IsCliente in number,
                         15  P_TipoMedioPlanilla in varchar2,
                         16  p_Tipo_Empleador in number,

                         17  p_PolizaAsoc in out nocopy number,
                         18  p_ConceptoPago in out nocopy varchar2,
                         19  p_FolioPagoCaja in out nocopy number,
                         20  p_Observ in out nocopy varchar2) return number;
         */

        try {

            long inicio = System.currentTimeMillis();
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            //===================================================
            // CASO PARA DETECTAR EL TIPO EMPLEADOR A INFORMAR
            //===================================================
            Integer tipoEmpleador = 1; //1=DEPENDIENTE 2=INDEPENDIENTE
            Integer tipoPagador = archivoPlanillaEncabezadoDTO.getTipo_pagador();
            String origen = tipoMedioPlanilla;
            if (tipoPagador == null)
                tipoPagador = 3;

            if (origen.equalsIgnoreCase("PREVIRED")) {
                if (tipoPagador.intValue() == 3 || tipoPagador.intValue() == 5)
                    tipoEmpleador = 1;
                if (tipoPagador.intValue() == 1 || tipoPagador.intValue() == 2 || tipoPagador.intValue() == 4)
                    tipoEmpleador = 2;
            }
            if (origen.equalsIgnoreCase("CAJAANDES")) {
                if (medioPago != null &&
                    medioPago ==
11) { //MANUAL
                    if (archivoPlanillaEncabezadoDTO.getRut_pagador().intValue() ==
                        archivoPlanillaEncabezadoDTO.getDetalle().get(0).getRut_trabajador().intValue()) {
                        tipoEmpleador = 2;
                    } else {
                        tipoEmpleador = 1;
                    }
                } else {
                    if (tipoPagador.intValue() == 3)
                        tipoEmpleador = 1;
                    if (tipoPagador.intValue() == 1 || tipoPagador.intValue() == 2 || tipoPagador.intValue() == 4)
                        tipoEmpleador = 2;
                }

            }
            if (origen.equalsIgnoreCase("CAJAARAUCANA")) {
                if (medioPago != null &&
                    medioPago ==
13) { //MANUAL
                    if (archivoPlanillaEncabezadoDTO.getRut_pagador().intValue() ==
                        archivoPlanillaEncabezadoDTO.getDetalle().get(0).getRut_trabajador().intValue()) {
                        tipoEmpleador = 2;
                    } else {
                        tipoEmpleador = 1;
                    }
                } else {
                    if (tipoPagador.intValue() == 3)
                        tipoEmpleador = 1;
                    if (tipoPagador.intValue() == 1 || tipoPagador.intValue() == 2 || tipoPagador.intValue() == 4)
                        tipoEmpleador = 2;
                }

            }


            //===================================================
            // CASO PARA DETECTAR EL TIPO EMPLEADOR A INFORMAR
            //===================================================

            stmt.registerOutParameter(1, Types.NUMERIC);
            ArchivoPlanillaDetalleDTO deter = archivoPlanillaEncabezadoDTO.getDetalle().get(detalle);
            stmt.setInt(2, deter.getRut_trabajador());
            stmt.setString(3, deter.getDv_trabajador());
            stmt.setString(4, deter.getNombreTrabajador());

            stmt.setInt(5, archivoPlanillaEncabezadoDTO.getRut_pagador());
            stmt.setString(6, archivoPlanillaEncabezadoDTO.getDv_pagador());
            stmt.setString(7, archivoPlanillaEncabezadoDTO.getAntecedentes().getRazon_social_pagador());

            Double montoapvi = deter.getCotizacion_voluntaria_apvi();
            Double montoapvb = deter.getCotizacion_voluntaria_apvi_b();
            Double montoapago = new Double(0);
            if (montoapvi != null && montoapvi.doubleValue() > 0.0)
                montoapago = montoapvi;
            if (montoapvb != null && montoapvb.doubleValue() > 0.0)
                montoapago = montoapvb;

            /*
             * TODO: Nuevo atributo para indicar a que regimen tributario se ingresa los montos.
             * 1 = A
             * 2 = B
             */
            Integer codigoRegimen = 0;

            if (montoapvb != null && montoapvb.doubleValue() > 0.0) {
                codigoRegimen = 2;
            } else {
                if (montoapvi != null && montoapvi.doubleValue() > 0.0) {
                    codigoRegimen = 1;
                } else {
                    codigoRegimen = 0; // -- SIN REGIMEN.
                }
            }

            System.out.println("C�digo Regimen Tributario: " + codigoRegimen);

            stmt.setString(8, String.valueOf(archivoPlanillaEncabezadoDTO.getId_folio_planilla().longValue()));
            stmt.setLong(9, montoapago.longValue());
            stmt.setLong(10, deter.getDeposito_convenio().longValue());
            stmt.setInt(11, codigoRegimen); //TODO: NUEVO PARAMETRO CODIGO REGIMEN TRIBUTARIO
            stmt.setString(12, FechaUtil.getFechaFormateoCustom(deter.getPeriodo_pago(), "yyyyMM"));
            stmt.setTimestamp(13,
                              new java.sql.Timestamp(archivoPlanillaEncabezadoDTO.getFecha_hora_operacion().getTime()));
            stmt.setBoolean(14, isClient);
            stmt.setString(15, tipoMedioPlanilla);
            stmt.setInt(16, tipoEmpleador);

            stmt.registerOutParameter(17, Types.NUMERIC);
            stmt.registerOutParameter(18, Types.VARCHAR);
            stmt.registerOutParameter(19, Types.NUMERIC);
            stmt.registerOutParameter(20, Types.VARCHAR);

            System.out.println("============================= PLSQL PREVIRED ================================ inicio:" +
                               inicio);
            System.out.println("    call PKG_MCJ_MANEJOCAJA_PRVRD.GenerarTransaccion");
            System.out.println("                        2   p_Rut in number, ==>" + deter.getRut_trabajador());
            System.out.println("                        3   p_DvRut in varchar2, ==>" + deter.getDv_trabajador());
            System.out.println("                        4   p_Nombre_Con in varchar2, ==>" +
                               deter.getNombreTrabajador());
            System.out.println("                        5   p_Rut_Empleador in number, ==>" +
                               archivoPlanillaEncabezadoDTO.getRut_pagador());
            System.out.println("                        6   p_DvRut_Empleador in varchar2, ==>" +
                               archivoPlanillaEncabezadoDTO.getDv_pagador());
            System.out.println("                        7   p_Nombre_Empl in varchar2,==>" +
                               archivoPlanillaEncabezadoDTO.getAntecedentes().getRazon_social_pagador());
            System.out.println("                        8   p_Folio_Previred in varchar2,==>" +
                               String.valueOf(archivoPlanillaEncabezadoDTO.getId_folio_planilla().longValue()));
            System.out.println("                        9   p_Monto in number,==>" + montoapago.longValue());
            System.out.println("                        10  p_Dep_Convenido in number,==>" +
                               deter.getDeposito_convenio().longValue());
            System.out.println("                        11  p_CodigoRegimen in number,==>" + codigoRegimen);
            System.out.println("                        12  p_PeriodoCotiza in varchar2,==>" +
                               FechaUtil.getFechaFormateoCustom(deter.getPeriodo_pago(), "yyyyMM"));
            System.out.println("                        13  p_FechRec in date,==>" +
                               FechaUtil.getFechaFormateoStandar(archivoPlanillaEncabezadoDTO.getFecha_hora_operacion()));
            System.out.println("                        14  p_IsCliente in boolean,==>" + isClient);
            System.out.println("                        15  P_TipoMedioPlanilla in varchar2, ==>" + tipoMedioPlanilla);
            System.out.println("                        16  p_Tipo_Empleador in number ==>" + tipoEmpleador);

            stmt.execute();

            archivoPlanillaEncabezadoDTO.getDetalle().get(detalle).setCodResultPago(stmt.getInt(1));
            archivoPlanillaEncabezadoDTO.getDetalle().get(detalle).setPolizaAsociada(stmt.getInt(17));
            archivoPlanillaEncabezadoDTO.getDetalle().get(detalle).setConceptoRecaudacion(stmt.getString(18));
            archivoPlanillaEncabezadoDTO.getDetalle().get(detalle).setId_folio_pagocaja(stmt.getLong(19));
            archivoPlanillaEncabezadoDTO.getDetalle().get(detalle).setDecsResultPago(stmt.getString(20));

            long termino = System.currentTimeMillis();
            logger.info("============ RESULTADO DE PAGO EN CAJA ================");
            logger.info("Codigo Resultado:" + stmt.getInt(1));
            logger.info("Poliza Asociada:" + stmt.getInt(17));
            logger.info("Concepto Recaudacion:" + stmt.getString(18));
            logger.info("Id Folio Caja:" + stmt.getLong(19));
            logger.info("Descripcion Resultado Caja:" + stmt.getString(20));
            logger.info("============ RESULTADO DE PAGO EN CAJA ================ termino :" + (termino - inicio) + " milesegundos");

            //ALFIN PASO EL FOLIO DE LA CAJA
            dto.setIdFolioCaja(stmt.getLong(19));
            dto.setDescripcionEnvio(stmt.getString(20));

            logger.info("Numero Folio Planilla:" +
                               String.valueOf(archivoPlanillaEncabezadoDTO.getId_folio_planilla().longValue()) +
                               " Rut Trabajador:" + deter.getRut_trabajador() + " Rut Pagador:" +
                               archivoPlanillaEncabezadoDTO.getRut_pagador() + " Monto Total a Pagar:" +
                               deter.getTotal_a_pagar() + " Monto Cotiza Voluntaria APV:" +
                               deter.getCotizacion_voluntaria_apvi() + " Deposito Convenido:" +
                               deter.getDeposito_convenio() + " Folio Caja:" + stmt.getLong(19) + " Concepto:" +
                               stmt.getString(18) + " C�digo Regimen Tributario:" + codigoRegimen +
                               " Resultado PLSQL:" + stmt.getString(20));
            //update falta del folio de la caja

        } catch (SQLException e) {
            logger.error("callGeneraTransaccion() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("callGeneraTransaccion() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        dto.setDto(archivoPlanillaEncabezadoDTO);
        return dto;
    }

    /**
     * Constructor 2 para realizar p
     * @param archivoPlanillaEncabezadoDTO
     * @param isClient
     * @param tipoMedioPlanilla
     * @param medioPago
     * @return
     */
    public ArchivoPlanillaEncabezadoDTO callGeneraTransaccion(ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO,
                                                              Boolean isClient, String tipoMedioPlanilla,
                                                              Integer medioPago) {
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" +
            "{? = call PKG_MCJ_MANEJOCAJA_PRVRD.GenerarTransaccion(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";


        try {
            long inicio = System.currentTimeMillis();
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            //===================================================
            // CASO PARA DETECTAR EL TIPO EMPLEADOR A INFORMAR
            //===================================================
            Integer tipoEmpleador = 1; //1=DEPENDIENTE 2=INDEPENDIENTE
            Integer tipoPagador = archivoPlanillaEncabezadoDTO.getTipo_pagador();
            String origen = tipoMedioPlanilla;
            if (tipoPagador == null)
                tipoPagador = 3;

            if (origen.equalsIgnoreCase("PREVIRED")) {
                if (tipoPagador.intValue() == 3 || tipoPagador.intValue() == 5)
                    tipoEmpleador = 1;
                if (tipoPagador.intValue() == 1 || tipoPagador.intValue() == 2 || tipoPagador.intValue() == 4)
                    tipoEmpleador = 2;
            }
            if (origen.equalsIgnoreCase("CAJAANDES")) {
                if (medioPago != null &&
                    medioPago ==
11) { //MANUAL
                    if (archivoPlanillaEncabezadoDTO.getRut_pagador().intValue() ==
                        archivoPlanillaEncabezadoDTO.getDetalle().get(0).getRut_trabajador().intValue()) {
                        tipoEmpleador = 2;
                    } else {
                        tipoEmpleador = 1;
                    }
                } else {
                    if (tipoPagador.intValue() == 3)
                        tipoEmpleador = 1;
                    if (tipoPagador.intValue() == 1 || tipoPagador.intValue() == 2 || tipoPagador.intValue() == 4)
                        tipoEmpleador = 2;
                }

            }
            if (origen.equalsIgnoreCase("CAJAARAUCANA")) {
                if (medioPago != null &&
                    medioPago ==
13) { //MANUAL
                    if (archivoPlanillaEncabezadoDTO.getRut_pagador().intValue() ==
                        archivoPlanillaEncabezadoDTO.getDetalle().get(0).getRut_trabajador().intValue()) {
                        tipoEmpleador = 2;
                    } else {
                        tipoEmpleador = 1;
                    }
                } else {
                    if (tipoPagador.intValue() == 3)
                        tipoEmpleador = 1;
                    if (tipoPagador.intValue() == 1 || tipoPagador.intValue() == 2 || tipoPagador.intValue() == 4)
                        tipoEmpleador = 2;
                }

            }


            //===================================================
            // CASO PARA DETECTAR EL TIPO EMPLEADOR A INFORMAR
            //===================================================

            stmt.registerOutParameter(1, Types.NUMERIC);
            ArchivoPlanillaDetalleDTO deter = archivoPlanillaEncabezadoDTO.getDetalle().get(0);
            stmt.setInt(2, deter.getRut_trabajador());
            stmt.setString(3, deter.getDv_trabajador());
            stmt.setString(4, deter.getNombreTrabajador());

            stmt.setInt(5, archivoPlanillaEncabezadoDTO.getRut_pagador());
            stmt.setString(6, archivoPlanillaEncabezadoDTO.getDv_pagador());
            stmt.setString(7, archivoPlanillaEncabezadoDTO.getAntecedentes().getRazon_social_pagador());
            String numeroFOlio = "" + archivoPlanillaEncabezadoDTO.getId_folio_planilla().longValue();
            stmt.setString(8, String.valueOf(archivoPlanillaEncabezadoDTO.getId_folio_planilla().longValue()));
            stmt.setLong(9, deter.getTotal_a_pagar().longValue());
            stmt.setLong(10, deter.getDeposito_convenio().longValue());

            Double montoapvi = deter.getCotizacion_voluntaria_apvi();
            Double montoapvb = deter.getCotizacion_voluntaria_apvi_b();
            Double montoapago = new Double(0);
            if (montoapvi != null && montoapvi.doubleValue() > 0.0)
                montoapago = montoapvi;
            if (montoapvb != null && montoapvb.doubleValue() > 0.0)
                montoapago = montoapvb;

            /*
                 * TODO: Nuevo atributo para indicar a que regimen tributario se ingresa los montos.
                 * 1 = A
                 * 2 = B
                 */
            Integer codigoRegimen = 0;

            if (montoapvb != null && montoapvb.doubleValue() > 0.0) {
                codigoRegimen = 2;
            } else {
                if (montoapvi != null && montoapvi.doubleValue() > 0.0) {
                    codigoRegimen = 1;
                } else {
                    codigoRegimen = 0;
                }
            }

            stmt.setInt(11, codigoRegimen); //TODO: NUEVO PARAMETRO CODIGO REGIMEN TRIBUTARIO

            stmt.setString(12, FechaUtil.getFechaFormateoCustom(deter.getPeriodo_pago(), "yyyyMM"));
            stmt.setTimestamp(13,
                              new java.sql.Timestamp(archivoPlanillaEncabezadoDTO.getFecha_hora_operacion().getTime()));
            stmt.setBoolean(14, isClient);
            stmt.setString(15, tipoMedioPlanilla);
            stmt.setInt(16, tipoEmpleador);

            stmt.registerOutParameter(17, Types.NUMERIC);
            stmt.registerOutParameter(18, Types.VARCHAR);
            stmt.registerOutParameter(19, Types.NUMERIC);
            stmt.registerOutParameter(20, Types.VARCHAR);

            System.out.println("============================= PLSQL PREVIRED MANUAL ================================ inicio:" +
                               inicio);
            System.out.println("    call PKG_MCJ_MANEJOCAJA_PRVRD.GenerarTransaccion");
            System.out.println("                        2   p_Rut in number, ==>" + deter.getRut_trabajador());
            System.out.println("                        3   p_DvRut in varchar2, ==>" + deter.getDv_trabajador());
            System.out.println("                        4   p_Nombre_Con in varchar2, ==>" +
                               deter.getNombreTrabajador());
            System.out.println("                        5   p_Rut_Empleador in number, ==>" +
                               archivoPlanillaEncabezadoDTO.getRut_pagador());
            System.out.println("                        6   p_DvRut_Empleador in varchar2, ==>" +
                               archivoPlanillaEncabezadoDTO.getDv_pagador());
            System.out.println("                        7   p_Nombre_Empl in varchar2,==>" +
                               archivoPlanillaEncabezadoDTO.getAntecedentes().getRazon_social_pagador());
            System.out.println("                        8   p_Folio_Previred in varchar2,==>" +
                               String.valueOf(archivoPlanillaEncabezadoDTO.getId_folio_planilla().longValue()));
            System.out.println("                        9   p_Monto in number,==>" + montoapago.longValue());
            System.out.println("                        10  p_Dep_Convenido in number,==>" +
                               deter.getDeposito_convenio().longValue());
            System.out.println("                        11  p_CodigoRegimen in number,==>" + codigoRegimen);
            System.out.println("                        12  p_PeriodoCotiza in varchar2,==>" +
                               FechaUtil.getFechaFormateoCustom(deter.getPeriodo_pago(), "yyyyMM"));
            System.out.println("                        13  p_FechRec in date,==>" +
                               FechaUtil.getFechaFormateoStandar(archivoPlanillaEncabezadoDTO.getFecha_hora_operacion()));
            System.out.println("                        14  p_IsCliente in number,==>" + isClient);
            System.out.println("                        15  P_TipoMedioPlanilla in varchar2, ==>" + tipoMedioPlanilla);
            System.out.println("                        16  p_Tipo_Empleador in number ==>" + tipoEmpleador);

            stmt.setQueryTimeout(2000);
            stmt.execute();


            archivoPlanillaEncabezadoDTO.getDetalle().get(0).setCodResultPago(stmt.getInt(1));
            archivoPlanillaEncabezadoDTO.getDetalle().get(0).setPolizaAsociada(stmt.getInt(17));
            archivoPlanillaEncabezadoDTO.getDetalle().get(0).setConceptoRecaudacion(stmt.getString(18));
            archivoPlanillaEncabezadoDTO.getDetalle().get(0).setId_folio_pagocaja(stmt.getLong(19));
            archivoPlanillaEncabezadoDTO.getDetalle().get(0).setDecsResultPago(stmt.getString(20));

            long termino = System.currentTimeMillis();
            System.out.println("============ RESULTADO DE PAGO EN CAJA MANUAL ================");
            System.out.println("Codigo Resultado:" + stmt.getInt(1));
            System.out.println("Poliza Asociada:" + stmt.getInt(17));
            System.out.println("Concepto Recaudacion:" + stmt.getString(18));
            System.out.println("Id Folio Caja:" + stmt.getLong(19));
            System.out.println("Descripcion Resultado Caja:" + stmt.getString(20));
            System.out.println("============ RESULTADO DE PAGO EN CAJA MANUAL ================ termino :" +
                               (termino - inicio) + " milesegundos");


            System.out.println("Resultado :" + archivoPlanillaEncabezadoDTO.getDetalle().get(0).getDecsResultPago());
            System.out.println("Numero Folio Planilla X: Folio Caja:" + stmt.getLong(19));


        } catch (SQLException e) {
            logger.error("callGeneraTransaccion() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("callGeneraTransaccion() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return archivoPlanillaEncabezadoDTO;
    }

    public List<ResumenAbonoDTO> getResumenRendiciones(Double idCargaPlanilla, Double idCargaAbono) {
        List<ResumenAbonoDTO> lstresumenAbonoDTO = null;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  CODIGO_RECAUDADOR,\n" + "  GLOSA_RECAUDADOR,\n" + "  ID_MOTIVO_RECHAZO,\n" +
            "  OBSERVACION_RECHAZO,\n" + "  OBSERVACION_AUTORIZACION,\n" + "  VALIDACION_CARTOLA_CTA_CTE,\n" +
            "  TOTAL_MONTO_INF_CARTOLA,\n" + "  TIPO_DE_REGISTRO,\n" + "  IDENTIFICADOR_IIP,\n" +
            "  TOTAL_MONTO_FONDO,\n" + "  TOTAL_MONTO_INSTITUCION,\n" + "  TOTAL_MONTO_AFC,\n" + "  CUENTA_FONDO,\n" +
            "  CUENTA_INSTITUCION,\n" + "  CUENTA_AFC,\n" + "  IDENTIFICACION_BANCO,\n" + "  CODIGO_BANCO,\n" +
            "  FECHA_ABONO,\n" + "  NUMERO_DE_PLANILLAS\n" + "FROM REND_RESUMEN_ABONO\n" + "WHERE ID_CARGA IN\n" +
            "  (SELECT ID_CARGA\n" + "  FROM REND_CARGA_DE_ARCHIVOS\n" + "  WHERE ESTADO_PROCESO = ?\n" +
            "  AND FECHA_CREACION   > ?\n" + "  AND FECHA_CREACION   < ?\n" + "  )";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCargaAbono);
            rs = stmt.executeQuery();

            lstresumenAbonoDTO = new ArrayList<ResumenAbonoDTO>();
            while (rs.next()) {
                ResumenAbonoDTO resumenAbonoDTO = new ResumenAbonoDTO();
                resumenAbonoDTO.setCodigo_recaudador(rs.getString("CODIGO_RECAUDADOR"));
                resumenAbonoDTO.setGlosa_recaudador(rs.getString("GLOSA_RECAUDADOR"));
                resumenAbonoDTO.setId_motivo_rechazo(rs.getInt("ID_MOTIVO_RECHAZO"));
                resumenAbonoDTO.setObservacion_rechazo(rs.getString("OBSERVACION_RECHAZO"));
                resumenAbonoDTO.setObservacion_autorizacion(rs.getString("OBSERVACION_AUTORIZACION"));
                resumenAbonoDTO.setValidacion_cartola_cta_cte(rs.getInt("VALIDACION_CARTOLA_CTA_CTE"));
                resumenAbonoDTO.setTotal_monto_inf_cartola(rs.getDouble("TOTAL_MONTO_INF_CARTOLA"));
                resumenAbonoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                resumenAbonoDTO.setIdentificador_iip(rs.getInt("IDENTIFICADOR_IIP"));
                resumenAbonoDTO.setTotal_monto_fondo(rs.getDouble("TOTAL_MONTO_FONDO"));
                resumenAbonoDTO.setTotal_monto_institucion(rs.getDouble("TOTAL_MONTO_INSTITUCION"));
                resumenAbonoDTO.setTotal_monto_afc(rs.getDouble("TOTAL_MONTO_AFC"));
                resumenAbonoDTO.setCuenta_fondo(rs.getString("CUENTA_FONDO"));
                resumenAbonoDTO.setCuenta_institucion(rs.getString("CUENTA_INSTITUCION"));
                resumenAbonoDTO.setCuenta_afc(rs.getString("CUENTA_AFC"));
                resumenAbonoDTO.setIdentificacion_banco(rs.getString("IDENTIFICACION_BANCO"));
                resumenAbonoDTO.setCodigo_banco(rs.getString("CODIGO_BANCO"));
                resumenAbonoDTO.setFecha_abono(new Date(rs.getDate("FECHA_ABONO").getTime()));
                resumenAbonoDTO.setNumero_de_planillas(rs.getInt("NUMERO_DE_PLANILLAS"));
                resumenAbonoDTO.setArchivoabono(getResumenDetalleAbonoAndPlanillaByFolio(idCargaAbono, idCargaPlanilla,
                                                                                         resumenAbonoDTO.getCodigo_recaudador()));
                Double totalpagado = 0.0;
                Double totalpagadoprocesado = 0.0;
                Double totalpagadonoprocesado = 0.0;

                for (int i = 0; i < resumenAbonoDTO.getArchivoabono().size(); i++) {
                    if (resumenAbonoDTO.getArchivoabono().get(i).getPlanilla().getEstado_proceso().intValue() ==
                        Constant.PROCESO_AUTORIZADO) {
                        totalpagadoprocesado =
                            totalpagadoprocesado +
                            resumenAbonoDTO.getArchivoabono().get(i).getPlanilla().getResumen().getTotal_pagar_ia();
                    } else {
                        totalpagadonoprocesado =
                            totalpagadonoprocesado +
                            resumenAbonoDTO.getArchivoabono().get(i).getPlanilla().getResumen().getTotal_pagar_ia();
                    }

                }
                totalpagado = totalpagadoprocesado + totalpagadonoprocesado;
                resumenAbonoDTO.setMonto_total_pagado_planilla(totalpagado);
                resumenAbonoDTO.setMonto_total_pagado_planillaprocesados(totalpagadoprocesado);
                resumenAbonoDTO.setMonto_total_pagado_planillanoprocesados(totalpagadonoprocesado);
                //resumenAbonoDTO.setArchivoabonoplanillaprocesados(new ArrayList<ArchivoAbonoDTO>(CollectionUtils.select(resumenAbonoDTO.getArchivoabono(), ACEPTADOS)));
                //resumenAbonoDTO.setArchivoabonoplanillanoprocesados(new ArrayList<ArchivoAbonoDTO>(CollectionUtils.select(resumenAbonoDTO.getArchivoabono(), NOACEPTADOS)));

                lstresumenAbonoDTO.add(resumenAbonoDTO);
            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return lstresumenAbonoDTO;
    }

    public List<ArchivoAbonoDTO> getResumenDetalleAbonoAndPlanillaByFolio(Double idCargaAbono, Double idCargaPlanilla,
                                                                          String codigoRecaudador) {
        List<ArchivoAbonoDTO> lstArchivoAbonoDTO = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ID_CARGA,\n" + "  CODIGO_RECAUDADOR,\n" + "  ID_FOLIO_ABONO,\n" + "  TIPO_DE_REGISTRO,\n" +
            "  MONTO_ABONO_FONDO,\n" + "  MONTO_ABONO_INSTITUCION,\n" + "  MONTO_ABONO_AFC,\n" + "  RUT_EMPLEADOR,\n" +
            "  DIGITO_VERFICADOR,\n" + "  PERIODO,\n" + "  FECHA_PAGO,\n" + "  LOTE,\n" + "  SUCURSAL,\n" +
            "  ESTADO_PROCESO,\n" + "  ESTADO_PROCESO_EXCEPCION\n" + "FROM REND_ARCHIVO_ABONO\n" +
            "WHERE ID_CARGA        = ?\n" + "AND CODIGO_RECAUDADOR = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setDouble(1, idCargaAbono);
            stmt.setString(2, codigoRecaudador);
            rs = stmt.executeQuery();

            lstArchivoAbonoDTO = new ArrayList<ArchivoAbonoDTO>();
            while (rs.next()) {
                ArchivoAbonoDTO archivoAbonoDTO = new ArchivoAbonoDTO();
                archivoAbonoDTO.setId_folio_abono(rs.getDouble("ID_FOLIO_ABONO"));
                archivoAbonoDTO.setTipo_de_registro(rs.getInt("TIPO_DE_REGISTRO"));
                archivoAbonoDTO.setMonto_abono_fondo(rs.getDouble("MONTO_ABONO_FONDO"));
                archivoAbonoDTO.setMonto_abono_institucion(rs.getDouble("MONTO_ABONO_INSTITUCION"));
                archivoAbonoDTO.setMonto_abono_afc(rs.getDouble("MONTO_ABONO_AFC"));
                archivoAbonoDTO.setRut_empleador(rs.getInt("RUT_EMPLEADOR"));
                archivoAbonoDTO.setDigito_verficador(rs.getString("DIGITO_VERFICADOR"));
                archivoAbonoDTO.setPeriodo(new Date(rs.getDate("PERIODO").getTime()));
                archivoAbonoDTO.setFecha_pago(new Date(rs.getDate("FECHA_PAGO").getTime()));
                archivoAbonoDTO.setLote(rs.getString("LOTE"));
                archivoAbonoDTO.setSucursal(rs.getString("SUCURSAL"));
                archivoAbonoDTO.setEstado_proceso(rs.getInt("ESTADO_PROCESO"));
                archivoAbonoDTO.setEstado_proceso_excepcion(rs.getString("ESTADO_PROCESO_EXCEPCION"));
                archivoAbonoDTO.setPlanilla(getPlanillaByIdCargaFolio(idCargaPlanilla,
                                                                      archivoAbonoDTO.getId_folio_abono()));

                archivoAbonoDTO.getPlanilla().getResumen().getTotal_pagar_ia();

                lstArchivoAbonoDTO.add(archivoAbonoDTO);

            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstArchivoAbonoDTO;
    }

    /**
     * Obtiene codigos del recaudador
     * @return
     */
    public HashMap<Integer, RecaudadoresDTO> getRecaudadores() {
        HashMap<Integer, RecaudadoresDTO> lstRecaudadores = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "SELECT CODIGO, DESCRIPCION, IDENTIFICADOR, CODBCO FROM REND_RECAUDADORES";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            rs = stmt.executeQuery();

            lstRecaudadores = new HashMap<Integer, RecaudadoresDTO>();
            while (rs.next()) {
                RecaudadoresDTO recaudadoresDTO = new RecaudadoresDTO();
                recaudadoresDTO.setCod_rec(rs.getInt("CODIGO"));
                recaudadoresDTO.setDescripcion(rs.getString("DESCRIPCION"));
                recaudadoresDTO.setIdentificador(rs.getString("IDENTIFICADOR"));
                recaudadoresDTO.setCod_banco(rs.getString("CODBCO"));
                lstRecaudadores.put(recaudadoresDTO.getCod_rec(), recaudadoresDTO);
            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstRecaudadores;
    }

    /**
     * Obtiene los estados de los procesos
     * @return
     */
    public List<EstadoDeProcesoDTO> getEstadoDeProceso() {
        List<EstadoDeProcesoDTO> lstEstadoDeProcesoDTO = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "SELECT ESTADO_PROCESO, DESCRIPCION_PROCESO FROM REND_ESTADO_DE_PROCESO";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            rs = stmt.executeQuery();

            lstEstadoDeProcesoDTO = new ArrayList<EstadoDeProcesoDTO>();
            while (rs.next()) {
                EstadoDeProcesoDTO estadoDeProcesoDTO = new EstadoDeProcesoDTO();
                estadoDeProcesoDTO.setCodigoEstado(rs.getInt("ESTADO_PROCESO"));
                estadoDeProcesoDTO.setDecripcionEstado(rs.getString("DESCRIPCION_PROCESO"));
                lstEstadoDeProcesoDTO.add(estadoDeProcesoDTO);
            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstEstadoDeProcesoDTO;
    }

    /**
     * Obtiene los estados de los procesos
     * @return
     */
    public EstadoDeProcesoDTO getEstadoDeProcesoByIdProceso(Integer idProceso) {
        EstadoDeProcesoDTO estadoDeProcesoDTO = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "SELECT ESTADO_PROCESO, DESCRIPCION_PROCESO FROM REND_ESTADO_DE_PROCESO\n" +
            "WHERE ESTADO_PROCESO = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setInt(1, idProceso);
            rs = stmt.executeQuery();


            if (rs.next()) {
                estadoDeProcesoDTO = new EstadoDeProcesoDTO();
                estadoDeProcesoDTO.setCodigoEstado(rs.getInt("ESTADO_PROCESO"));
                estadoDeProcesoDTO.setDecripcionEstado(rs.getString("DESCRIPCION_PROCESO"));

            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return estadoDeProcesoDTO;
    }

    /**
     * Obtiene conceptos de pago
     * @return
     */
    public HashMap<Integer, ConceptosDePagoDTO> getConceptosDePago() {
        HashMap<Integer, ConceptosDePagoDTO> lstConceptosDePagoDTO = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "SELECT COD_CONCEPTO, COD_INSTRUMENTO, DESCRIPCION FROM REND_CONCEPTO";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            rs = stmt.executeQuery();

            lstConceptosDePagoDTO = new HashMap<Integer, ConceptosDePagoDTO>();
            while (rs.next()) {
                ConceptosDePagoDTO conceptosDePagoDTO = new ConceptosDePagoDTO();
                conceptosDePagoDTO.setCodigoConcepto(rs.getInt("COD_CONCEPTO"));
                conceptosDePagoDTO.setCodigoIndstrumento(rs.getString("COD_INSTRUMENTO"));
                conceptosDePagoDTO.setDescripcionConcepto(rs.getString("DESCRIPCION"));
                lstConceptosDePagoDTO.put(conceptosDePagoDTO.getCodigoConcepto(), conceptosDePagoDTO);
            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstConceptosDePagoDTO;
    }


    public List<ResumenRendicionesVwDTO> getResumenRendiciones(Date fechaDesde, Date fechaHasta, Double folio,
                                                               Integer rutEmpleador, Integer rutTrabajador,
                                                               String conceptoPago, String idMedioPago,
                                                               Integer idEstado, String tipopl) {
        List<ResumenRendicionesVwDTO> lstResumenRendicionesVwDTO = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String queryFolio = "";
        String queryRutEmpleador = "";
        String queryRutTrabajador = "";
        String queryConceptoPago = "";
        String queryIdMedioPago = "";
        String queryIdEstado = "";
        String queryTipoPl = "";


        if (tipopl.equalsIgnoreCase(Constant.PLANILLA_ANDES) || tipopl.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
            if (tipopl.equalsIgnoreCase(Constant.PLANILLA_ANDES)) {
                queryTipoPl =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_ANDES_CONVENIO + ", " +
                    Constant.PLANILLA_ANDES_CONVENIO_MANUAL + ") \n";
            }
            if (tipopl.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
                queryTipoPl =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_LAARAUCANA_CONVENIO + ", " +
                    Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
            }
        } else {
            queryTipoPl =
                "AND CARGA.NUMERO_DE_CONVENIO NOT IN ( " + Constant.PLANILLA_ANDES_CONVENIO + "," +
                Constant.PLANILLA_ANDES_CONVENIO_MANUAL + "," + Constant.PLANILLA_LAARAUCANA_CONVENIO + "," +
                Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
        }


        if (folio != null) {
            queryFolio = "AND REVW.ID_FOLIO_ABONO          = ?\n";
        }

        if (rutEmpleador != null) {
            queryRutEmpleador = "AND REVW.RUT_EMPLEADOR           = ?\n";
        }

        if (rutTrabajador != null) {
            queryRutTrabajador = "AND REVW.RUT_TRABAJADOR          = ?\n";
        }

        if (conceptoPago != null) {
            queryConceptoPago = "AND REVW.CONCEPTO_PAGO           = ?\n";
        }

        if (idMedioPago != null) {
            queryIdMedioPago = "AND REVW.CODIGO_BANCO          LIKE ?\n";
        }

        if (idEstado != null) {
            queryIdEstado = "AND REVW.ESTADO_PROCESO_PLANILLA = ?\n";
        }

        String query =
            "" + "SELECT REVW.ID_CARGA              as ID_CARGA,\n" +
            "  REVW.INICIO_PROCESO             as INICIO_PROCESO,\n" +
            "  REVW.CODIGO_RECAUDADOR          as CODIGO_RECAUDADOR,\n" +
            "  REVW.GLOSA_RECAUDADOR           as GLOSA_RECAUDADOR,\n" +
            "  REVW.CODIGO_BANCO               as CODIGO_BANCO,\n" +
            "  REVW.IDENTIFICACION_BANCO       as IDENTIFICACION_BANCO,\n" +
            "  REVW.FECHA_ABONO                as FECHA_ABONO,\n" +
            "  count(REVW.NUMERO_DE_REGISTROS) as NUMERO_DE_REGISTROS,\n" +
            "  REVW.MONTO_TOTAL_ABONADO        as MONTO_TOTAL_ABONADO,\n" +
            "  sum(REVW.MONTO_APLICADO)        as MONTO_APLICADO,\n" +
            "  sum(REVW.MONTO_NO_APLICADO)     as MONTO_NO_APLICADO,\n" +
            "  CARGA.NOMBRE_FISICO_ARCHIVO     as NOMBRE_FISICO_ARCHIVO\n" +
            "FROM REND_RESUMEN_RENDICIONES_VW REVW,\n" + "  REND_CARGA_DE_ARCHIVOS CARGA\n" +
            "WHERE REVW.INICIO_PROCESO > ?\n" + "AND REVW.INICIO_PROCESO   < ?\n" +
            "AND CARGA.ID_CARGA        = REVW.ID_CARGA_PLANILLA\n" + queryTipoPl + queryFolio + queryRutEmpleador +
            queryRutTrabajador + queryConceptoPago + queryIdMedioPago + queryIdEstado + "GROUP BY REVW.ID_CARGA,\n" +
            "  REVW.INICIO_PROCESO,\n" + "  REVW.CODIGO_RECAUDADOR,\n" + "  REVW.GLOSA_RECAUDADOR,\n" +
            "  REVW.CODIGO_BANCO,\n" + "  REVW.IDENTIFICACION_BANCO,\n" + "  REVW.FECHA_ABONO,\n" +
            "  REVW.MONTO_TOTAL_ABONADO,\n" + "  CARGA.NOMBRE_FISICO_ARCHIVO\n" + "ORDER BY REVW.CODIGO_BANCO";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setTimestamp(1, new java.sql.Timestamp(fechaDesde.getTime()));
            stmt.setTimestamp(2, new java.sql.Timestamp(fechaHasta.getTime()));

            int numerosalida = 3;


            if (folio != null) {
                stmt.setDouble(numerosalida++, folio);
            }

            if (rutEmpleador != null) {
                stmt.setInt(numerosalida++, rutEmpleador);
            }

            if (rutTrabajador != null) {
                stmt.setInt(numerosalida++, rutTrabajador);
            }

            if (conceptoPago != null) {
                stmt.setString(numerosalida++, conceptoPago);
            }

            if (idMedioPago != null) {
                stmt.setString(numerosalida++, idMedioPago);
            }

            if (idEstado != null) {
                stmt.setInt(numerosalida++, idEstado);
            }

            rs = stmt.executeQuery();

            lstResumenRendicionesVwDTO = new ArrayList<ResumenRendicionesVwDTO>();
            while (rs.next()) {
                ResumenRendicionesVwDTO resumenRendicionesVwDTO = new ResumenRendicionesVwDTO();
                resumenRendicionesVwDTO.setId_carga(rs.getDouble("ID_CARGA"));
                resumenRendicionesVwDTO.setInicio_proceso(new Date(rs.getTimestamp("INICIO_PROCESO").getTime()));
                resumenRendicionesVwDTO.setCodigo_recaudador(rs.getInt("CODIGO_RECAUDADOR"));
                resumenRendicionesVwDTO.setGlosa_recaudador(rs.getString("GLOSA_RECAUDADOR"));
                resumenRendicionesVwDTO.setCodigo_banco(rs.getInt("CODIGO_BANCO"));
                resumenRendicionesVwDTO.setIdentificacion_banco(rs.getString("IDENTIFICACION_BANCO"));
                resumenRendicionesVwDTO.setFecha_abono(new Date(rs.getTimestamp("FECHA_ABONO").getTime()));
                resumenRendicionesVwDTO.setNumero_de_registros(rs.getInt("NUMERO_DE_REGISTROS"));
                resumenRendicionesVwDTO.setMonto_total_abonado(rs.getDouble("MONTO_TOTAL_ABONADO"));
                resumenRendicionesVwDTO.setMonto_aplicado(rs.getDouble("MONTO_APLICADO"));
                resumenRendicionesVwDTO.setMonto_no_aplicado(rs.getDouble("MONTO_NO_APLICADO"));
                resumenRendicionesVwDTO.setNombreFisicoArchivo(rs.getString("NOMBRE_FISICO_ARCHIVO"));
                resumenRendicionesVwDTO.setDetalle_resumen_rendiciones(getDetalleResumenRendiciones(resumenRendicionesVwDTO.getId_carga(),
                                                                                                    resumenRendicionesVwDTO.getCodigo_recaudador(),
                                                                                                    fechaDesde,
                                                                                                    fechaHasta, folio,
                                                                                                    rutEmpleador,
                                                                                                    rutTrabajador,
                                                                                                    conceptoPago,
                                                                                                    idEstado, tipopl));
                lstResumenRendicionesVwDTO.add(resumenRendicionesVwDTO);
            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstResumenRendicionesVwDTO;
    }

    public List<ResumenRendicionesVwDTO> getDetalleResumenRendiciones(Double idCarga, Integer idMedioPago,
                                                                      Date fechaDesde, Date fechaHasta, Double folio,
                                                                      Integer rutEmpleador, Integer rutTrabajador,
                                                                      String conceptoPago, Integer idEstado,
                                                                      String tipopl) {
        List<ResumenRendicionesVwDTO> lstResumenRendicionesVwDTO = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String queryFolio = "";
        String queryRutEmpleador = "";
        String queryRutTrabajador = "";
        String queryConceptoPago = "";
        String queryIdEstado = "";
        String queryTipoPl = "";

        if (tipopl.equalsIgnoreCase(Constant.PLANILLA_ANDES) || tipopl.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
            if (tipopl.equalsIgnoreCase(Constant.PLANILLA_ANDES)) {
                queryTipoPl =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_ANDES_CONVENIO + ", " +
                    Constant.PLANILLA_ANDES_CONVENIO_MANUAL + ") \n";
            }
            if (tipopl.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
                queryTipoPl =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_LAARAUCANA_CONVENIO + ", " +
                    Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
            }
        } else {
            queryTipoPl =
                "AND CARGA.NUMERO_DE_CONVENIO NOT IN ( " + Constant.PLANILLA_ANDES_CONVENIO + "," +
                Constant.PLANILLA_ANDES_CONVENIO_MANUAL + "," + Constant.PLANILLA_LAARAUCANA_CONVENIO + "," +
                Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
        }


        if (folio != null) {
            queryFolio = "AND ID_FOLIO_ABONO          = ?\n";
        }

        if (rutEmpleador != null) {
            queryRutEmpleador = "AND RUT_EMPLEADOR           = ?\n";
        }

        if (rutTrabajador != null) {
            queryRutTrabajador = "AND RUT_TRABAJADOR          = ?\n";
        }

        if (conceptoPago != null) {
            queryConceptoPago = "AND CONCEPTO_PAGO           = ?\n";
        }

        if (idEstado != null) {
            queryIdEstado = "AND ESTADO_PROCESO_PLANILLA = ?\n";
        }


        String query =
            "" + "SELECT REVW.ID_CARGA     as ID_CARGA,\n" + "  REVW.ID_FOLIO_ABONO    as ID_FOLIO_ABONO,\n" +
            "  REVW.CODIGO_RECAUDADOR as CODIGO_RECAUDADOR,\n" + "  REVW.GLOSA_RECAUDADOR  as GLOSA_RECAUDADOR,\n" +
            "  REVW.RUT_EMPLEADOR\n" + "  || '-'\n" + "  || REVW.DV_EMPLEADOR as RUT_EMPLEADOR ,\n" +
            "  REVW.RUT_TRABAJADOR\n" + "  || '-'\n" + "  || REVW.DV_TRABAJADOR             as RUT_TRABAJADOR,\n" +
            "  REVW.FECHA_PAGO                   as FECHA_PAGO,\n" +
            "  REVW.FECHA_ABONO                  as FECHA_ABONO,\n" +
            "  REVW.INICIO_PROCESO               as INICIO_PROCESO,\n" +
            "  REVW.MONTO_APLICADO               as MONTO_APLICADO,\n" +
            "  REVW.MONTO_NO_APLICADO            as MONTO_NO_APLICADO,\n" +
            "  REVW.PERIODO_PAGO                 as PERIODO_PAGO,\n" +
            "  REVW.DESCRIPCION_PROCESO_PLANILLA as DESCRIPCION_PROCESO_PLANILLA,\n" +
            "  REVW.POLIZA_ASOC                  as POLIZA_ASOC,\n" +
            "  REVW.CONCEPTO_PAGO_DESC           as CONCEPTO_PAGO_DESC,\n" +
            "  REVW.FECHA_PAGO_CAJA              as FECHA_PAGO_CAJA,\n" +
            "  REVW.ID_CARGA_PLANILLA            as ID_CARGA_PLANILLA,\n" +
            "  REVW.ID_FOLIO_PAGOCAJA            as ID_FOLIO_PAGOCAJA,\n" +
            "  REVW.DEPOSITO_CONVENIO            as DEPOSITO_CONVENIO\n" + "FROM REND_RESUMEN_RENDICIONES_VW REVW,\n" +
            "  REND_CARGA_DE_ARCHIVOS CARGA\n" + "WHERE CARGA.ID_CARGA  = REVW.ID_CARGA_PLANILLA\n" +
            "AND REVW.ID_CARGA          = ?\n" + "AND REVW.CODIGO_RECAUDADOR = ?\n" +
            "AND REVW.INICIO_PROCESO    > ?\n" + "AND REVW.INICIO_PROCESO    < ?\n" + queryTipoPl + queryFolio +
            queryRutEmpleador + queryRutTrabajador + queryConceptoPago + queryIdEstado + "ORDER BY REVW.ID_FOLIO_ABONO";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.setInt(2, idMedioPago);
            stmt.setTimestamp(3, new java.sql.Timestamp(fechaDesde.getTime()));
            stmt.setTimestamp(4, new java.sql.Timestamp(fechaHasta.getTime()));

            int numerosalida = 5;

            if (folio != null) {
                stmt.setDouble(numerosalida++, folio);
            }

            if (rutEmpleador != null) {
                stmt.setInt(numerosalida++, rutEmpleador);
            }

            if (rutTrabajador != null) {
                stmt.setInt(numerosalida++, rutTrabajador);
            }

            if (conceptoPago != null) {
                stmt.setString(numerosalida++, conceptoPago);
            }

            if (idEstado != null) {
                stmt.setInt(numerosalida++, idEstado);
            }

            rs = stmt.executeQuery();

            lstResumenRendicionesVwDTO = new ArrayList<ResumenRendicionesVwDTO>();
            while (rs.next()) {
                ResumenRendicionesVwDTO resumenRendicionesVwDTO = new ResumenRendicionesVwDTO();
                resumenRendicionesVwDTO.setId_carga(rs.getDouble("ID_CARGA"));
                resumenRendicionesVwDTO.setId_folio_abono(rs.getDouble("ID_FOLIO_ABONO"));
                resumenRendicionesVwDTO.setCodigo_recaudador(rs.getInt("CODIGO_RECAUDADOR"));
                resumenRendicionesVwDTO.setGlosa_recaudador(rs.getString("GLOSA_RECAUDADOR"));
                resumenRendicionesVwDTO.setRut_empleador(rs.getString("RUT_EMPLEADOR"));
                resumenRendicionesVwDTO.setRut_trabajador(rs.getString("RUT_TRABAJADOR"));
                resumenRendicionesVwDTO.setFecha_pago(new Date(rs.getTimestamp("FECHA_PAGO").getTime()));
                resumenRendicionesVwDTO.setFecha_abono(new Date(rs.getTimestamp("FECHA_ABONO").getTime()));
                resumenRendicionesVwDTO.setInicio_proceso(new Date(rs.getTimestamp("INICIO_PROCESO").getTime()));
                resumenRendicionesVwDTO.setMonto_aplicado(rs.getDouble("MONTO_APLICADO"));
                resumenRendicionesVwDTO.setMonto_no_aplicado(rs.getDouble("MONTO_NO_APLICADO"));
                resumenRendicionesVwDTO.setPeriodo_pago(new Date(rs.getTimestamp("PERIODO_PAGO").getTime()));
                resumenRendicionesVwDTO.setDescripcion_proceso_planilla(rs.getString("DESCRIPCION_PROCESO_PLANILLA"));
                resumenRendicionesVwDTO.setPoliza_asoc(rs.getInt("POLIZA_ASOC"));
                resumenRendicionesVwDTO.setConcepto_pago(rs.getString("CONCEPTO_PAGO_DESC"));
                resumenRendicionesVwDTO.setFecha_pago_caja(new Date(rs.getTimestamp("FECHA_PAGO_CAJA").getTime()));
                resumenRendicionesVwDTO.setId_carga_planilla(rs.getDouble("ID_CARGA_PLANILLA"));
                resumenRendicionesVwDTO.setId_folio_pagocaja(rs.getDouble("ID_FOLIO_PAGOCAJA"));
                resumenRendicionesVwDTO.setDeposito_convenio(rs.getDouble("DEPOSITO_CONVENIO"));
                lstResumenRendicionesVwDTO.add(resumenRendicionesVwDTO);
            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstResumenRendicionesVwDTO;
    }

    public List<ResumenRendicionesVwDTO> getConsultaRendiciones(Date fechaDesde, Date fechaHasta, Double folio,
                                                                Integer rutEmpleador, Integer rutTrabajador,
                                                                String conceptoPago, String idMedioPago,
                                                                Integer idEstado, String tipopl) {
        List<ResumenRendicionesVwDTO> lstResumenRendicionesVwDTO = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String queryFolio = "";
        String queryRutEmpleador = "";
        String queryRutTrabajador = "";
        String queryConceptoPago = "";
        String queryIdMedioPago = "";
        String queryIdEstado = "";
        String queryTipoPl = "";


        if (tipopl.equalsIgnoreCase(Constant.PLANILLA_ANDES) || tipopl.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
            if (tipopl.equalsIgnoreCase(Constant.PLANILLA_ANDES)) {
                queryTipoPl =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_ANDES_CONVENIO + ", " +
                    Constant.PLANILLA_ANDES_CONVENIO_MANUAL + ") \n";
            }
            if (tipopl.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
                queryTipoPl =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_LAARAUCANA_CONVENIO + ", " +
                    Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
            }
        } else {
            queryTipoPl =
                "AND CARGA.NUMERO_DE_CONVENIO NOT IN ( " + Constant.PLANILLA_ANDES_CONVENIO + "," +
                Constant.PLANILLA_ANDES_CONVENIO_MANUAL + "," + Constant.PLANILLA_LAARAUCANA_CONVENIO + "," +
                Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
        }


        if (folio != null) {
            queryFolio = "AND REVW.ID_FOLIO_ABONO          = ?\n";
        }

        if (rutEmpleador != null) {
            queryRutEmpleador = "AND REVW.RUT_EMPLEADOR           = ?\n";
        }

        if (rutTrabajador != null) {
            queryRutTrabajador = "AND REVW.RUT_TRABAJADOR          = ?\n";
        }

        if (conceptoPago != null) {
            queryConceptoPago = "AND REVW.CONCEPTO_PAGO           = ?\n";
        }

        if (idMedioPago != null) {
            queryIdMedioPago = "AND REVW.CODIGO_BANCO          LIKE ?\n";
        }

        if (idEstado != null) {
            queryIdEstado = "AND REVW.ESTADO_PROCESO_PLANILLA = ?\n";
        }


        String query =
            "" + "SELECT REVW.ID_CARGA        as ID_CARGA,\n" + "  REVW.CODIGO_RECAUDADOR    as CODIGO_RECAUDADOR,\n" +
            "  REVW.GLOSA_RECAUDADOR     as GLOSA_RECAUDADOR,\n" + "  REVW.CODIGO_BANCO         as CODIGO_BANCO,\n" +
            "  REVW.IDENTIFICACION_BANCO as IDENTIFICACION_BANCO,\n" +
            "  REVW.ID_FOLIO_ABONO       as ID_FOLIO_ABONO,\n" + "  REVW.RUT_EMPLEADOR\n" + "  || '-'\n" +
            "  || REVW.DV_EMPLEADOR as RUT_EMPLEADOR ,\n" + "  REVW.RUT_TRABAJADOR\n" + "  || '-'\n" +
            "  || REVW.DV_TRABAJADOR               as RUT_TRABAJADOR,\n" +
            "  REVW.FECHA_PAGO                     as FECHA_PAGO,\n" +
            "  REVW.FECHA_ABONO                    as FECHA_ABONO,\n" +
            "  REVW.INICIO_PROCESO                 as INICIO_PROCESO,\n" +
            "  REVW.FECHA_PAGO_CAJA                as FECHA_PAGO_CAJA,\n" +
            "  REVW.TOTAL_COTIZA_VOLUNTARIA_APVI   as TOTAL_COTIZA_VOLUNTARIA_APVI,\n" +
            "  REVW.TOTAL_COTIZA_VOLUNTARIA_APVI_B as TOTAL_COTIZA_VOLUNTARIA_APVI_B,\n" +
            "  REVW.MONTO_APLICADO                 as MONTO_APLICADO,\n" +
            "  REVW.MONTO_NO_APLICADO              as MONTO_NO_APLICADO,\n" +
            "  REVW.PERIODO_PAGO                   as PERIODO_PAGO,\n" +
            "  REVW.CONCEPTO_PAGO_DESC             as CONCEPTO_PAGO_DESC,\n" +
            "  REVW.POLIZA_ASOC                    as POLIZA_ASOC,\n" +
            "  REVW.DESCRIPCION_PROCESO_PLANILLA   as DESCRIPCION_PROCESO_PLANILLA,\n" +
            "  REVW.ID_CARGA_PLANILLA              as ID_CARGA_PLANILLA,\n" +
            "  REVW.ID_FOLIO_PAGOCAJA              as ID_FOLIO_PAGOCAJA,\n" +
            "  REVW.DEPOSITO_CONVENIO              as DEPOSITO_CONVENIO,\n" +
            "  CARGA.NUMERO_DE_CONVENIO            as NUMERO_DE_CONVENIO,\n" +
            "  CARGA.NOMBRE_FISICO_ARCHIVO         as NOMBRE_FISICO_ARCHIVO\n" +
            "FROM REND_RESUMEN_RENDICIONES_VW REVW,\n" + "  REND_CARGA_DE_ARCHIVOS CARGA\n" +
            "WHERE REVW.INICIO_PROCESO > ?\n" + "AND REVW.INICIO_PROCESO   < ?\n" +
            "AND CARGA.ID_CARGA        = REVW.ID_CARGA_PLANILLA\n" + queryTipoPl + queryFolio + queryRutEmpleador +
            queryRutTrabajador + queryConceptoPago + queryIdMedioPago + queryIdEstado + "ORDER BY REVW.ID_FOLIO_ABONO";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setTimestamp(1, new java.sql.Timestamp(fechaDesde.getTime()));
            stmt.setTimestamp(2, new java.sql.Timestamp(fechaHasta.getTime()));

            int numerosalida = 3;

            if (folio != null) {
                stmt.setDouble(numerosalida++, folio);
            }

            if (rutEmpleador != null) {
                stmt.setInt(numerosalida++, rutEmpleador);
            }

            if (rutTrabajador != null) {
                stmt.setInt(numerosalida++, rutTrabajador);
            }

            if (conceptoPago != null) {
                stmt.setString(numerosalida++, conceptoPago);
            }

            if (idMedioPago != null) {
                stmt.setString(numerosalida++, idMedioPago);
            }

            if (idEstado != null) {
                stmt.setInt(numerosalida++, idEstado);
            }

            rs = stmt.executeQuery();

            lstResumenRendicionesVwDTO = new ArrayList<ResumenRendicionesVwDTO>();
            while (rs.next()) {
                ResumenRendicionesVwDTO resumenRendicionesVwDTO = new ResumenRendicionesVwDTO();
                resumenRendicionesVwDTO.setId_carga(rs.getDouble("ID_CARGA"));
                resumenRendicionesVwDTO.setCodigo_recaudador(rs.getInt("CODIGO_RECAUDADOR"));
                resumenRendicionesVwDTO.setGlosa_recaudador(rs.getString("GLOSA_RECAUDADOR"));
                resumenRendicionesVwDTO.setCodigo_banco(rs.getInt("CODIGO_BANCO"));
                resumenRendicionesVwDTO.setIdentificacion_banco(rs.getString("IDENTIFICACION_BANCO"));
                resumenRendicionesVwDTO.setId_folio_abono(rs.getDouble("ID_FOLIO_ABONO"));
                resumenRendicionesVwDTO.setRut_empleador(rs.getString("RUT_EMPLEADOR"));
                resumenRendicionesVwDTO.setRut_trabajador(rs.getString("RUT_TRABAJADOR"));
                resumenRendicionesVwDTO.setFecha_pago(new Date(rs.getTimestamp("FECHA_PAGO").getTime()));
                resumenRendicionesVwDTO.setFecha_abono(new Date(rs.getTimestamp("FECHA_ABONO").getTime()));
                resumenRendicionesVwDTO.setInicio_proceso(new Date(rs.getTimestamp("INICIO_PROCESO").getTime()));
                resumenRendicionesVwDTO.setFecha_pago_caja(new Date(rs.getTimestamp("FECHA_PAGO_CAJA").getTime()));
                resumenRendicionesVwDTO.setTotal_cotiza_voluntaria_apvi(rs.getDouble("TOTAL_COTIZA_VOLUNTARIA_APVI"));
                resumenRendicionesVwDTO.setTotal_cotiza_voluntaria_apvi_b(rs.getDouble("TOTAL_COTIZA_VOLUNTARIA_APVI_B"));
                resumenRendicionesVwDTO.setMonto_aplicado(rs.getDouble("MONTO_APLICADO"));
                resumenRendicionesVwDTO.setMonto_no_aplicado(rs.getDouble("MONTO_NO_APLICADO"));
                resumenRendicionesVwDTO.setPeriodo_pago(new Date(rs.getTimestamp("PERIODO_PAGO").getTime()));
                resumenRendicionesVwDTO.setConcepto_pago(rs.getString("CONCEPTO_PAGO_DESC"));
                resumenRendicionesVwDTO.setPoliza_asoc(rs.getInt("POLIZA_ASOC"));
                resumenRendicionesVwDTO.setDescripcion_proceso_planilla(rs.getString("DESCRIPCION_PROCESO_PLANILLA"));
                resumenRendicionesVwDTO.setId_carga_planilla(rs.getDouble("ID_CARGA_PLANILLA"));
                resumenRendicionesVwDTO.setId_folio_pagocaja(rs.getDouble("ID_FOLIO_PAGOCAJA"));
                resumenRendicionesVwDTO.setDeposito_convenio(rs.getDouble("DEPOSITO_CONVENIO"));
                resumenRendicionesVwDTO.setNombreFisicoArchivo(rs.getString("NOMBRE_FISICO_ARCHIVO"));

                lstResumenRendicionesVwDTO.add(resumenRendicionesVwDTO);
            }

        } catch (SQLException e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getAbonoByIdCarga() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return lstResumenRendicionesVwDTO;
    }


    public ArchivoPlanillaHeaderDTO insertArchivoPlanillaProceso(Long idTurno, java.util.Date fechaTurno,
                                                                 Long folioCajaDiario,
                                                                 ArchivoPlanillaHeaderDTO headerDTO, Double idCarga,
                                                                 Integer inicio, Integer procesar,
                                                                 String tipoMedioPlanilla) {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        headerDTO.setProceso_detalle(false);

        String query =
            "" + "INSERT \n" + "INTO REND_ARCHIVO_PLANILLA_ENCAB\n" + "  (\n" + "    ID_CARGA,\n" +
            "    ID_FOLIO_PLANILLA,\n" + "    TIPO_DE_REGISTRO,\n" + "    TIPO_PAGO,\n" +
            "    FECHA_HORA_OPERACION,\n" + "    CODIGO_INSTITUCION_APV,\n" + "    RUT_PAGADOR,\n" +
            "    DV_PAGADOR,\n" + "    TIPO_PAGADOR,\n" + "    CORREO_PAGADOR,\n" + "    ESTADO_PROCESO,\n" +
            "    ESTADO_PROCESO_EXCEPTION,\n" + "    ID_CARGAABONO\n" + "  )\n" + "  VALUES\n" +
            "  ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";

        try {

            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareStatement(query);


            CargaArchivosDTO cargaArchivosDTOPlanilla = getCargaArchivosByIdCarga(idCarga);

            for (int i = (inicio.intValue());
                 (i < (procesar.intValue() + inicio.intValue())) && (i < headerDTO.getLst_detalle_planilla().size());
                 i++) {
                // INSERTA EN SISTEMA DE CAJAS
                //INSERTA EN CAJA LA TRANSACCION
                EjecutaCajaDTO respDto =
                    callProcesoArchivoPlanilla(headerDTO, cargaArchivosDTOPlanilla, i, tipoMedioPlanilla);
                headerDTO = respDto.getDto();

                Long folioPagoCaja = respDto.getNumeroFolioCaja();
                if (folioPagoCaja != null &&
                    folioPagoCaja.longValue() >
                                          0) {
                    //DISTRIBUIR TRANSACCION
                    Integer Distibucion =
callDistribuirMontoCaja("RENDICIONESAPV", fechaTurno, folioCajaDiario, folioPagoCaja, 1);
                    System.out.println("INFO: Transaccion distibuida :" + Distibucion + " numero de folio pago caja :" +
                                       folioPagoCaja);
                }

                stmt.setDouble(1, idCarga);
                if (headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla() != null)
                    stmt.setDouble(2, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_de_registro() != null)
                    stmt.setInt(3, headerDTO.getLst_detalle_planilla().get(i).getTipo_de_registro());
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_pago() != null)
                    stmt.setInt(4, headerDTO.getLst_detalle_planilla().get(i).getTipo_pago());
                if (headerDTO.getLst_detalle_planilla().get(i).getFecha_hora_operacion() != null)
                    stmt.setTimestamp(5,
                                      new java.sql.Timestamp(headerDTO.getLst_detalle_planilla().get(i).getFecha_hora_operacion().getTime()));
                if (headerDTO.getLst_detalle_planilla().get(i).getCodigo_institucion_apv() != null)
                    stmt.setInt(6, headerDTO.getLst_detalle_planilla().get(i).getCodigo_institucion_apv());
                if (headerDTO.getLst_detalle_planilla().get(i).getRut_pagador() != null)
                    stmt.setInt(7, headerDTO.getLst_detalle_planilla().get(i).getRut_pagador());
                if (headerDTO.getLst_detalle_planilla().get(i).getDv_pagador() != null)
                    stmt.setString(8, headerDTO.getLst_detalle_planilla().get(i).getDv_pagador());
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_pagador() != null)
                    stmt.setInt(9, headerDTO.getLst_detalle_planilla().get(i).getTipo_pagador());
                if (headerDTO.getLst_detalle_planilla().get(i).getCorreo_pagador() != null)
                    stmt.setString(10, headerDTO.getLst_detalle_planilla().get(i).getCorreo_pagador());
                if (headerDTO.getLst_detalle_planilla().get(i).getEstado_proceso() != null)
                    stmt.setInt(11, headerDTO.getLst_detalle_planilla().get(i).getEstado_proceso());
                if (headerDTO.getLst_detalle_planilla().get(i).getEstado_proceso_exception() != null)
                    stmt.setString(12, headerDTO.getLst_detalle_planilla().get(i).getEstado_proceso_exception());
                if (headerDTO.getIdCargaAbono() != null)
                    stmt.setDouble(13, headerDTO.getIdCargaAbono());

                //stmt.setInt(11, new Integer("2")); // No procesado
                //stmt.setString(12, "No se encuentra en archivo de abono");

                if (headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla() == null)
                    stmt.setNull(2, OracleTypes.DOUBLE);
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_de_registro() == null)
                    stmt.setNull(3, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_pago() == null)
                    stmt.setNull(4, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getFecha_hora_operacion() == null)
                    stmt.setNull(5, OracleTypes.DATE);
                if (headerDTO.getLst_detalle_planilla().get(i).getCodigo_institucion_apv() == null)
                    stmt.setNull(6, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getRut_pagador() == null)
                    stmt.setNull(7, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getDv_pagador() == null)
                    stmt.setNull(8, OracleTypes.VARCHAR);
                if (headerDTO.getLst_detalle_planilla().get(i).getTipo_pagador() == null)
                    stmt.setNull(9, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getCorreo_pagador() == null)
                    stmt.setNull(10, OracleTypes.VARCHAR);
                if (headerDTO.getLst_detalle_planilla().get(i).getEstado_proceso() == null)
                    stmt.setNull(11, OracleTypes.NUMBER);
                if (headerDTO.getLst_detalle_planilla().get(i).getEstado_proceso_exception() == null)
                    stmt.setNull(12, OracleTypes.VARCHAR);
                if (headerDTO.getIdCargaAbono() == null)
                    stmt.setNull(13, OracleTypes.DOUBLE);

                stmt.addBatch();

                headerDTO.getLst_detalle_planilla().get(i).setProcesado(true);
                headerDTO.setNumero_detalle_procesados(i);

                /*if(headerDTO.getLst_detalle_planilla().get(i).isProcesado()){
                    ArchivoPlanillaAntecedentesDTO antec = insertAntecedentesPlanilla(headerDTO.getLst_detalle_planilla().get(i).getAntecedentes(), idCarga, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                    if(antec.isProcesado()){
                        List<ArchivoPlanillaDetalleDTO> lstdetalle = insertDetallePlanilla(headerDTO.getLst_detalle_planilla().get(i).getDetalle(), idCarga, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                        for(int j = 0; j < lstdetalle.size(); j++){
                            if(!lstdetalle.get(j).isProcesado()){
                                throw new Exception("No se proceso correctamente el detalle de la planilla");
                            }
                        }

                        ArchivoPlanillaResumenDTO resumen = insertResumenPlanilla(headerDTO.getLst_detalle_planilla().get(i).getResumen(), idCarga, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                        if(!resumen.isProcesado()){
                            throw new Exception("No se proceso correctamente el resumen de la planilla");
                        }
                    }else{
                        throw new Exception("No se proceso correctamente el antecedente de la planilla");
                    }
                }*/
            }

            stmt.executeBatch();

            if (headerDTO.getLst_detalle_planilla().size() > 0) {
                for (int l = (inicio.intValue());
                     (l < (procesar.intValue() + inicio.intValue())) &&
                     (l < headerDTO.getLst_detalle_planilla().size()); l++) {
                    if (headerDTO.getLst_detalle_planilla().get(l).isProcesado()) {
                        ArchivoPlanillaAntecedentesDTO antec =
                            insertAntecedentesPlanilla(headerDTO.getLst_detalle_planilla().get(l).getAntecedentes(),
                                                       idCarga,
                                                       headerDTO.getLst_detalle_planilla().get(l).getId_folio_planilla());
                        if (antec.isProcesado()) {
                            List<ArchivoPlanillaDetalleDTO> lstdetalle =
                                insertDetallePlanilla(headerDTO.getLst_detalle_planilla().get(l).getDetalle(), idCarga,
                                                      headerDTO.getLst_detalle_planilla().get(l).getId_folio_planilla());
                            for (int j = 0; j < lstdetalle.size(); j++) {
                                if (!lstdetalle.get(j).isProcesado()) {
                                    throw new Exception("No se proceso correctamente el detalle de la planilla");
                                }
                            }

                            ArchivoPlanillaResumenDTO resumen =
                                insertResumenPlanilla(headerDTO.getLst_detalle_planilla().get(l).getResumen(), idCarga,
                                                      headerDTO.getLst_detalle_planilla().get(l).getId_folio_planilla());
                            if (!resumen.isProcesado()) {
                                throw new Exception("No se proceso correctamente el resumen de la planilla");
                            }
                        } else {
                            throw new Exception("No se proceso correctamente el antecedente de la planilla");
                        }
                    }
                }
            } else {
                throw new Exception("No se puede procesar planilla");
            }

            /*if(headerDTO.getLst_detalle_planilla().get(i).isProcesado()){
                ArchivoPlanillaAntecedentesDTO antec = insertAntecedentesPlanilla(headerDTO.getLst_detalle_planilla().get(i).getAntecedentes(), idCarga, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                if(antec.isProcesado()){
                    List<ArchivoPlanillaDetalleDTO> lstdetalle = insertDetallePlanilla(headerDTO.getLst_detalle_planilla().get(i).getDetalle(), idCarga, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                    for(int j = 0; j < lstdetalle.size(); j++){
                        if(!lstdetalle.get(j).isProcesado()){
                            throw new Exception("No se proceso correctamente el detalle de la planilla");
                        }
                    }

                    ArchivoPlanillaResumenDTO resumen = insertResumenPlanilla(headerDTO.getLst_detalle_planilla().get(i).getResumen(), idCarga, headerDTO.getLst_detalle_planilla().get(i).getId_folio_planilla());
                    if(!resumen.isProcesado()){
                        throw new Exception("No se proceso correctamente el resumen de la planilla");
                    }
                }else{
                    throw new Exception("No se proceso correctamente el antecedente de la planilla");
                }
            }*/

            headerDTO.setProceso_detalle(true);

        } catch (SQLException e) {
            headerDTO.setProceso_detalle(false);
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            headerDTO.setProceso_detalle(false);
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        return headerDTO;
    }

    /**
     * PROCESO MAESTRO PARA ENVIAR INFORMACION A SISTEMA DE CAJAS
     * @param headerDTO
     * @param cargaArchivosDTOPlanilla
     * @param datoProcesa
     * @param tipoMedioPlanilla
     * @return
     * TODO: REVISAR PROCESO QUE NO ESTA HACIENDO BIEN SU PEGA.
     */
    public EjecutaCajaDTO callProcesoArchivoPlanilla(ArchivoPlanillaHeaderDTO headerDTO,
                                                     CargaArchivosDTO cargaArchivosDTOPlanilla, int datoProcesa,
                                                     String tipoMedioPlanilla) {
        EjecutaCajaDTO dto = new EjecutaCajaDTO();
        Integer estadoProceso = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
        String observacion =
            "Problema en el procesamiento del folio " +
            headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue();
        Integer estadoProcesoPago = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
        String observacionPago =
            "Problema en el procesamiento del folio " +
            headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue();

        /* ****************************************
         * Inicio proceso de verificaci�n y de pago en caja
         * ****************************************
         */
        try {
            
            if (headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla() != null) {
                if(datoProcesa == 0){
                    periodoCotiza = headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(datoProcesa).getPeriodo_pago();
                }
                /*
                 * Validacion si fue procesado anteriormente
                 */
                System.out.println("FOLIO A PROCESAR " + headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla());
                System.out.println("PERIODO A PROCESAR " + periodoCotiza);
                CargaArchivosDTO cargaArchivosDTOProcesado =
                 getCargaArchivoByIdFolioPlanilla(headerDTO.getLst_detalle_planilla().get(datoProcesa),periodoCotiza);
                if (cargaArchivosDTOProcesado != null) {
                    throw new Exception("El folio " +
                                        headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue() +
                                        " fue procesado anteriormente en el archivo " +
                                        cargaArchivosDTOProcesado.getNombre_fisico_archivo());
                } else {
                    /*
                     * Busca el folio del abono autorizado
                     */
                    if (headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla() != null &&
                        headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla() > 0.0) {
                        
                        CargaArchivosDTO cargaArchivosDTOAbonoValido =
                            getCargaAbonoByIdCargaIdFolio(headerDTO.getIdCargaAbono(),
                                                          headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla());
                        if (cargaArchivosDTOAbonoValido != null) {
                            /*
                             * Si el abono autorizado existe se comparan los medios de pago de la planilla y del abono
                             */
                            if (cargaArchivosDTOPlanilla.getConvenio().getId_medio_pago().intValue() ==
                                cargaArchivosDTOAbonoValido.getConvenio().getId_medio_pago().intValue()) {
                                Integer medioPago =
                                    cargaArchivosDTOAbonoValido.getConvenio().getId_medio_pago().intValue();
                                /*
                                 * Si los medios coinciden se indica el ID de carga del abono
                                 */
                                headerDTO.setIdCargaAbono(cargaArchivosDTOAbonoValido.getId_carga());


                                if (headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().size() > 0) {

                                    /*
                                     * For de inicio de pagos del los detalles
                                     */
                                    for (int i = 0;
                                         i < headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().size();
                                         i++) {

                                        try {

                                            estadoProcesoPago = Constant.PROCESO_PROBLEMA_PLANILLA_ABONO;
                                            observacionPago =
                                                "Problema en el procesamiento del folio " +
                                                headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue();

                                            /*
                                             * Verifica si es Cliente
                                             */
                                            //cl.bicevida.previred.proxy.SessionEJBDatosClienteClient myPort = new cl.bicevida.previred.proxy.SessionEJBDatosClienteClient();
                                            //myPort.setEndpoint(ResourceBundleUtil.getProperty("cl.bicevida.ws.datos.usuario"));
                                            //ResponseVODTO responseVODTO = myPort.getDataClienteBDPPublico(headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).getRut_trabajador().toString());
                                            boolean isCliente = true;
                                            //if(responseVODTO.isExito()){
                                            //Es Cliente Bicevida
                                            //    isCliente = true;
                                            // }

                                            /*
                                             * Se verifica estado del Abono
                                             */
                                            ArchivoAbonoDTO archivoAbonoDTOValido =
                                                cargaArchivosDTOAbonoValido.getResumenabono().getArchivoabono().get(0);
                                            if ((archivoAbonoDTOValido.getEstado_proceso().intValue() ==
                                                 Constant.PROCESO_ABONO_AUTORIZADO) ||
                                                (archivoAbonoDTOValido.getEstado_proceso().intValue() ==
                                                 Constant.PROCESO_ABONO_AUTORIZADO_OBS)) {
                                                /*
                                                 * Verifica monto a pagar mayor que cero
                                                 */
                                                if (headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).getTotal_a_pagar() !=
                                                    null &&
                                                    headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).getTotal_a_pagar().doubleValue() >
                                                    0.0) {
                                                    /*
                                                     * Si el estado es v�lido se procede al pago por caja
                                                     * invocando proceso de pago
                                                     */
                                                    logger.info("PREVIRED ==> Enviando pago a cajas...");
                                                    
                                                    EjecutaEncabezadoCajaDTO dtoPago =
                                                        callGeneraTransaccion(headerDTO.getLst_detalle_planilla().get(datoProcesa),
                                                                              i, isCliente, tipoMedioPlanilla,
                                                                              medioPago);

                                                    //RECUPERO LA INFORMACION
                                                    ArchivoPlanillaEncabezadoDTO archivoPlanillaEncabezadoDTO =
                                                        dtoPago.getDto();
                                                    dto.setNumeroFolioCaja(dtoPago.getIdFolioCaja());
                                                    dto.setDescripcion(dtoPago.getDescripcionEnvio());


                                                    headerDTO.getLst_detalle_planilla().set(datoProcesa,
                                                                                            archivoPlanillaEncabezadoDTO);

                                                    /*
                                                     * Se verifica resultado del pago
                                                     */
                                                    if (headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).getCodResultPago().intValue() ==
                                                        0) {
                                                        if (archivoAbonoDTOValido.getEstado_proceso().intValue() ==
                                                            Constant.PROCESO_ABONO_AUTORIZADO) {
                                                            estadoProcesoPago = Constant.PROCESO_AUTORIZADO;
                                                            observacionPago = "Procesado Correctamente";
                                                        }
                                                        if (archivoAbonoDTOValido.getEstado_proceso().intValue() ==
                                                            Constant.PROCESO_ABONO_AUTORIZADO_OBS) {
                                                            estadoProcesoPago = Constant.PROCESO_AUTORIZADO_OBS;
                                                            observacionPago =
                                                                "Procesado Correctamente con Observaciones";
                                                        }
                                                    } else {
                                                        estadoProcesoPago = Constant.PROCESO_PROBLEMA_PAGO_CAJA;
                                                        throw new Exception("El folio " +
                                                                            headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue() +
                                                                            " no fue procesado correctamente por proceso de CAJA: " +
                                                                            headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).getDecsResultPago());
                                                        //throw new Exception("El folio " + headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue() + " no fue procesado correctamente por proceso de CAJA: " + headerDTO.getLst_detalle_planilla().get(datoProcesa).getDecsResultPago());
                                                    }
                                                } else {
                                                    throw new Exception("El folio " +
                                                                        headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue() +
                                                                        " no fue procesado por que el valor total a pagar no es mayor que 0");
                                                }
                                            } else if (archivoAbonoDTOValido.getEstado_proceso().intValue() ==
                                                       Constant.PROCESO_ABONO_RECHAZADO) {
                                                estadoProcesoPago = Constant.PROCESO_RECHAZADO;
                                                throw new Exception("El folio " +
                                                                    headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue() +
                                                                    " se encuentra como abono rechazado en el proceso del archivo " +
                                                                    cargaArchivosDTOAbonoValido.getNombre_fisico_archivo());
                                            } else {
                                                throw new Exception("El folio " +
                                                                    headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue() +
                                                                    " no se encuentra abonado");
                                            }
                                        } catch (Exception e) {
                                            observacionPago = e.getMessage();
                                            logger.error("Se ha producido un excepcion: ", e);
                                        }

                                        headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).setEstado_proceso_pago(estadoProcesoPago);
                                        headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).setEstado_proceso_pago_exception(observacionPago);
                                    }


                                    //EJECUTO WS PARA NOTIFICAR PAGOS REALIZ


                                    estadoProceso = Constant.PROCESO_FINALIZADO_;
                                    observacion = "";
                                    /*
                                     * Fin For de inicio de pagos del los detalles
                                     */
                                } else {
                                    throw new Exception("El folio " +
                                                        headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue() +
                                                        " no posee detalle de pago, el tama�o es de  " +
                                                        headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().size());
                                }
                            } else {
                                throw new Exception("El folio " +
                                                    headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue() +
                                                    " tiene medio de pago (" +
                                                    cargaArchivosDTOPlanilla.getConvenio().getDescripcion_medio_pago() +
                                                    ") distinto al del informado en el abono (" +
                                                    cargaArchivosDTOAbonoValido.getConvenio().getDescripcion_medio_pago() +
                                                    ")");
                            }
                        } else {
                            throw new Exception("El folio " +
                                                headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla().longValue() +
                                                " no se encuentra abonado");
                        }
                    }
                }
            }
        } catch (Exception e) {
            observacion = e.getMessage();
            if (headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().size() > 0) {
                for (int i = 0; i < headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().size(); i++) {
                    if (headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).getEstado_proceso_pago() ==
                        null) {
                        headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).setEstado_proceso_pago(Constant.PROCESO_PROBLEMA_PLANILLA_ABONO);
                    }

                    if (headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).getEstado_proceso_pago_exception() ==
                        null) {
                        headerDTO.getLst_detalle_planilla().get(datoProcesa).getDetalle().get(i).setEstado_proceso_pago_exception(observacion);
                    }
                }
            }

            logger.error("Se ha producido un excepcion: ", e);
        }

        /*
         * Actualizacion del estado del folio en el proceso del abono
         */
        if (estadoProceso == Constant.PROCESO_PROBLEMA_PLANILLA_ABONO) {
            if (headerDTO.getIdCargaAbono() != null && headerDTO.getIdCargaAbono().doubleValue() > 0.0) {
                updateEstadoPorcesoFolioResumen(headerDTO.getIdCargaAbono(),
                                                headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla(),
                                                Constant.PROCESO_PROBLEMA_RESUMEN_ABONO, observacion);
            }
        } else {
            updateEstadoPorcesoFolioResumen(headerDTO.getIdCargaAbono(),
                                            headerDTO.getLst_detalle_planilla().get(datoProcesa).getId_folio_planilla(),
                                            estadoProceso, observacion);
        }

        headerDTO.getLst_detalle_planilla().get(datoProcesa).setEstado_proceso(estadoProceso);
        headerDTO.getLst_detalle_planilla().get(datoProcesa).setEstado_proceso_exception(observacion);

        /* ****************************************
         * Fin proceso de verificaci�n y de pago en caja
         * ****************************************
         */
        dto.setDto(headerDTO);
        return dto;
    }


    public CargaArchivosDTO getCargaAbonoByIdCargaIdFolio(Double idCarga, Double idFolio) {
        CargaArchivosDTO cargaArchivosDTO = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String strIdcarga = "";
        if (idCarga != null) {
            strIdcarga = "AND CARGA.ID_CARGA              = ?\n";
        }

        String query =
            "SELECT CARGA.ID_CARGA         as ID_CARGA,\n" + "  CARGA.ID_TIPO_ARCHIVO       as ID_TIPO_ARCHIVO,\n" +
            "  CARGA.ESTADO_PROCESO        as ESTADO_PROCESO_CARGA,\n" +
            "  CARGA.ID_CARGA_ASOC         as ID_CARGA_ASOC,\n" +
            "  CARGA.NUMERO_DE_CONVENIO    as NUMERO_DE_CONVENIO,\n" +
            "  CARGA.NOMBRE_FISICO_ARCHIVO as NOMBRE_FISICO_ARCHIVO,\n" +
            "  MEDIOS.ID_MEDIO_PAGO        as ID_MEDIO_PAGO,\n" +
            "  MEDIOS.DESCRIPCION          as DESCRIPCION_MEDIO,\n" +
            "  RESUMEN.CODIGO_RECAUDADOR   as CODIGO_RECAUDADOR,\n" +
            "  RESUMEN.GLOSA_RECAUDADOR    as GLOSA_RECAUDADOR,\n" +
            "  DETALLE.ID_FOLIO_ABONO      as ID_FOLIO_ABONO,\n" +
            "  DETALLE.ESTADO_PROCESO      as ESTADO_PROCESO_FOLIO\n" + "FROM REND_CARGA_DE_ARCHIVOS CARGA,\n" +
            "  REND_RESUMEN_ABONO RESUMEN,\n" + "  REND_ARCHIVO_ABONO DETALLE,\n" +
            "  REND_CONVENIO_DE_PAGO CONVENIO,\n" + "  REND_MEDIOS_DE_PAGO MEDIOS\n" +
            "WHERE RESUMEN.ID_CARGA          = CARGA.ID_CARGA\n" +
            "AND DETALLE.ID_CARGA            = CARGA.ID_CARGA\n" +
            "AND DETALLE.CODIGO_RECAUDADOR   = RESUMEN.CODIGO_RECAUDADOR\n" +
            "AND CONVENIO.NUMERO_DE_CONVENIO = CARGA.NUMERO_DE_CONVENIO\n" +
            "AND MEDIOS.ID_MEDIO_PAGO        = CONVENIO.ID_MEDIO_PAGO\n" +
            "AND CARGA.ESTADO_PROCESO       in (?, ?)\n" + "AND DETALLE.ESTADO_PROCESO     in (?, ?, ?)\n" +
            strIdcarga + "AND DETALLE.ID_FOLIO_ABONO      = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setInt(1, Constant.PROCESO_FINALIZADO_AUTORIZADO);
            stmt.setInt(2, Constant.PROCESO_FINALIZADO_CON_RECHAZOS);
            stmt.setInt(3, Constant.PROCESO_ABONO_AUTORIZADO);
            stmt.setInt(4, Constant.PROCESO_ABONO_AUTORIZADO_OBS);
            stmt.setInt(5, Constant.PROCESO_ABONO_RECHAZADO);


            if (idCarga != null) {
                stmt.setDouble(6, idCarga);
                stmt.setDouble(7, idFolio);
            } else {
                stmt.setDouble(6, idFolio);
            }

            rs = stmt.executeQuery();

            if (rs.next()) {
                cargaArchivosDTO = new CargaArchivosDTO();

                cargaArchivosDTO.setId_carga(rs.getDouble("ID_CARGA"));
                cargaArchivosDTO.setId_tipo_archivo(rs.getInt("ID_TIPO_ARCHIVO"));
                cargaArchivosDTO.setEstado_proceso(rs.getInt("ESTADO_PROCESO_CARGA"));
                cargaArchivosDTO.setId_carga_asoc(rs.getDouble("ID_CARGA_ASOC"));
                cargaArchivosDTO.setNombre_fisico_archivo(rs.getString("NOMBRE_FISICO_ARCHIVO"));

                ConvenioPagoDTO convenioPagoDTO = new ConvenioPagoDTO();
                convenioPagoDTO.setNumero_de_convenio(rs.getString("NUMERO_DE_CONVENIO"));
                convenioPagoDTO.setDescripcion_medio_pago(rs.getString("DESCRIPCION_MEDIO"));
                convenioPagoDTO.setId_medio_pago(rs.getInt("ID_MEDIO_PAGO"));
                cargaArchivosDTO.setConvenio(convenioPagoDTO);

                ResumenAbonoDTO resumenAbonoDTO = new ResumenAbonoDTO();
                resumenAbonoDTO.setCodigo_recaudador(rs.getString("CODIGO_RECAUDADOR"));
                resumenAbonoDTO.setGlosa_recaudador(rs.getString("GLOSA_RECAUDADOR"));

                List<ArchivoAbonoDTO> lstArchivoAbonoDTO = new ArrayList<ArchivoAbonoDTO>();
                ArchivoAbonoDTO archivoAbonoDTO = new ArchivoAbonoDTO();
                archivoAbonoDTO.setId_folio_abono(rs.getDouble("ID_FOLIO_ABONO"));
                archivoAbonoDTO.setEstado_proceso(rs.getInt("ESTADO_PROCESO_FOLIO"));

                lstArchivoAbonoDTO.add(archivoAbonoDTO);

                resumenAbonoDTO.setArchivoabono(lstArchivoAbonoDTO);

                cargaArchivosDTO.setResumenabono(resumenAbonoDTO);

            }

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
            cargaArchivosDTO = null;
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
            cargaArchivosDTO = null;
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }


        return cargaArchivosDTO;
    }

    public boolean updateFolioAbonoConDatosNoProcesados(Double idCarga, Integer estadoProceso, String observacion) {

        boolean result = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" + "UPDATE REND_ARCHIVO_ABONO SET ESTADO_PROCESO = ?, ESTADO_PROCESO_EXCEPCION = ? \n" +
            "WHERE ID_CARGA = ? AND ESTADO_PROCESO IN (?, ?, ?)";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setInt(1, estadoProceso);
            stmt.setString(2, observacion);
            stmt.setDouble(3, idCarga);
            stmt.setInt(4, Constant.PROCESO_ABONO_AUTORIZADO);
            stmt.setInt(5, Constant.PROCESO_ABONO_AUTORIZADO_OBS);
            stmt.setInt(6, Constant.PROCESO_ABONO_RECHAZADO);
            stmt.executeUpdate();

            result = true;

        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return result;
    }

    /**
     * Metodo para recuperar los pagos que deben ser realizados
     * de forma manual en el sistema de cajas.
     *
     * @return Lista de Transacciones a reprocesar
     */
    public List<ResumenRendicionesVwDTO> getReprocesosManualPagos(String tipoRend) {
        List<ResumenRendicionesVwDTO> resultado = new ArrayList<ResumenRendicionesVwDTO>();
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String filtro = "";

        if (tipoRend.equalsIgnoreCase(Constant.PLANILLA_ANDES) ||
            tipoRend.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
            if (tipoRend.equalsIgnoreCase(Constant.PLANILLA_ANDES)) {
                filtro =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_ANDES_CONVENIO + ", " +
                    Constant.PLANILLA_ANDES_CONVENIO_MANUAL + ") \n";
            }
            if (tipoRend.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
                filtro =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_LAARAUCANA_CONVENIO + ", " +
                    Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
            }
        } else {
            filtro =
                "AND CARGA.NUMERO_DE_CONVENIO NOT IN ( " + Constant.PLANILLA_ANDES_CONVENIO + "," +
                Constant.PLANILLA_ANDES_CONVENIO_MANUAL + "," + Constant.PLANILLA_LAARAUCANA_CONVENIO + "," +
                Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
        }

        String query =
            "SELECT REVW.*\n" + "FROM REND_RESUMEN_RENDICIONES_VW REVW, REND_CARGA_DE_ARCHIVOS CARGA\n" +
            "WHERE REVW.CODIGO_RECAUDADOR NOT IN('000')\n" + "AND REVW.ESTADO_PROCESO_PLANILLA IN(16)\n" +
            "AND CARGA.ID_CARGA = REVW.ID_CARGA\n" + filtro + "ORDER BY REVW.IDENTIFICACION_BANCO,\n" +
            "  REVW.INICIO_PROCESO DESC";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ResumenRendicionesVwDTO dto = new ResumenRendicionesVwDTO();
                dto.setInicio_proceso(new Date(rs.getTimestamp("INICIO_PROCESO").getTime()));
                dto.setId_folio_abono(rs.getDouble("ID_FOLIO_ABONO"));
                dto.setId_carga(rs.getDouble("ID_CARGA"));
                dto.setDescripcion_proceso_abono(rs.getString("DESCRIPCION_PROCESO_ABONO"));
                dto.setMonto_aplicado(rs.getDouble("MONTO_APLICADO"));
                dto.setMonto_no_aplicado(rs.getDouble("MONTO_NO_APLICADO"));
                dto.setCodigo_recaudador(rs.getInt("CODIGO_RECAUDADOR"));
                dto.setGlosa_recaudador(rs.getString("GLOSA_RECAUDADOR"));
                dto.setRut_trabajador(rs.getString("RUT_TRABAJADOR"));
                dto.setRut_empleador(rs.getString("RUT_EMPLEADOR"));
                dto.setNombre_trabajador(rs.getString("NOMBRE_TRABAJADOR"));
                dto.setRazon_social_empleador(rs.getString("RAZON_SOCIAL_EMPLEADOR"));
                dto.setFecha_abono(new java.util.Date(rs.getTimestamp("FECHA_ABONO").getTime()));
                dto.setNumero_de_registros(rs.getInt("NUMERO_DE_REGISTROS"));
                dto.setMonto_total_abonado(rs.getDouble("MONTO_TOTAL_ABONADO"));
                dto.setPeriodo_pago(new java.util.Date(rs.getDate("PERIODO_PAGO").getTime()));
                dto.setEstado_proceso_abono(rs.getInt("ESTADO_PROCESO_ABONO"));
                dto.setDescripcion_proceso_abono(rs.getString("DESCRIPCION_PROCESO_ABONO"));
                dto.setCodigo_banco(rs.getInt("CODIGO_BANCO"));
                dto.setIdentificacion_banco(rs.getString("IDENTIFICACION_BANCO"));
                dto.setFecha_pago(new java.util.Date(rs.getTimestamp("FECHA_PAGO").getTime()));
                dto.setEstado_proceso_planilla(rs.getInt("ESTADO_PROCESO_PLANILLA"));
                dto.setDescripcion_proceso_planilla(rs.getString("DESCRIPCION_PROCESO_PLANILLA"));
                dto.setObs_proceso_planilla(rs.getString("OBS_PROCESO_PLANILLA"));
                dto.setId_carga_planilla(rs.getDouble("ID_CARGA_PLANILLA"));
                dto.setDv_trabajador(rs.getString("DV_TRABAJADOR"));
                dto.setDv_empleador(rs.getString("DV_EMPLEADOR"));
                dto.setFecha_hora_operacion(new java.util.Date(rs.getTimestamp("FECHA_HORA_OPERACION").getTime()));
                dto.setDeposito_convenio(rs.getDouble("DEPOSITO_CONVENIO"));
                if (dto.getEstado_proceso_planilla() != RUNNIG) {
                    dto.setEstadoProcesamientoManual("Reprocesar en Caja");
                } else {
                    dto.setEstadoProcesamientoManual("Procesando...");
                    dto.setObs_proceso_planilla("Procesando...");
                }
                resultado.add(dto);
            }
        } catch (SQLException e) {
            logger.error("getReprocesosManualPagos() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getReprocesosManualPagos() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return resultado;
    }

    /**
     * Actualiza el estado de las transaccion en estado
     * de ejecucion manual para no volver a ser tocadas como
     * proceso de ejecucion
     * @param idTransacciones
     */
    public void changeStatusTransaccionProccess(List<PKTransaccionPagoDTO> idTransacciones) {
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            Iterator itemsUp = idTransacciones.iterator();
            while (itemsUp.hasNext()) {
                PKTransaccionPagoDTO dto = (PKTransaccionPagoDTO) itemsUp.next();
                String query =
                    "UPDATE REND_ARCHIVO_PLANILLA_DETALLE SET ESTADO_PROCESO_CAJA = ?, ESTADO_PROCESO_CAJA_EXC = ?, ID_FOLIO_PAGOCAJA = ?, POLIZA_ASOC = ?, CONCEPTO_PAGO = ?" +
                    "WHERE ID_CARGA = ? AND ID_FOLIO_PLANILLA = ?";
                stmt = conn.prepareCall(query);

                if (dto.getNewEstado() != null)
                    stmt.setInt(1, dto.getNewEstado());
                if (dto.getNewDescripcion() != null)
                    stmt.setString(2, dto.getNewDescripcion());
                if (dto.getFolioCaja() != null)
                    stmt.setDouble(3, dto.getFolioCaja());
                if (dto.getPoliza() != null)
                    stmt.setInt(4, dto.getPoliza());
                if (dto.getConceptoCaja() != null)
                    stmt.setString(5, dto.getConceptoCaja());
                if (dto.getIdCargaPlanilla() != null)
                    stmt.setDouble(6, dto.getIdCargaPlanilla());
                if (dto.getFolioPlanilla() != null)
                    stmt.setDouble(7, dto.getFolioPlanilla());

                if (dto.getNewEstado() == null)
                    stmt.setNull(1, OracleTypes.NUMBER);
                if (dto.getNewDescripcion() == null)
                    stmt.setNull(2, OracleTypes.VARCHAR);
                if (dto.getFolioCaja() == null)
                    stmt.setNull(3, OracleTypes.DOUBLE);
                if (dto.getPoliza() == null)
                    stmt.setNull(4, OracleTypes.NUMBER);
                if (dto.getConceptoCaja() == null)
                    stmt.setNull(5, OracleTypes.VARCHAR);
                if (dto.getIdCargaPlanilla() == null)
                    stmt.setNull(6, OracleTypes.DOUBLE);
                if (dto.getFolioPlanilla() == null)
                    stmt.setNull(7, OracleTypes.DOUBLE);


                System.out.println("====>  ACTUALIZANDO REND_ARCHIVO_PLANILLA_DETALLE");
                System.out.println("ID CARGA:" + dto.getIdCargaPlanilla());
                System.out.println("FOLIO PLANILLA:" + dto.getFolioPlanilla());
                System.out.println("FOLIO CAJA:" + dto.getFolioCaja().toString());
                System.out.println("ACTUALIZANDO REND_ARCHIVO_PLANILLA_DETALLE <=======");
                stmt.executeUpdate();


                /*query = "UPDATE REND_ARCHIVO_ABONO SET ESTADO_PROCESO = ?, ESTADO_PROCESO_EXCEPCION = ? " +
                "WHERE ID_CARGA = ? AND ID_FOLIO_ABONO = ? AND CODIGO_RECAUDADOR = ? ";
                stmt = conn.prepareCall(query);
                stmt.setInt(1, dto.getNewEstado());
                stmt.setString(2, dto.getNewDescripcion());
                stmt.setDouble(3, dto.getIdCarga());
                stmt.setDouble(4, dto.getFolioPlanilla());
                stmt.setInt(5, dto.getCodigoRecaudador());
                stmt.executeUpdate();*/
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
    }


    /**
     * Retorna la fecha y hora del turno con el cual
     * se aperturo la transaccion
     * @param idturno
     * @return
     */
    public java.util.Date getFechaHoraTurno() {
        java.util.Date turno = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // get the user-specified connection or get a connection from the ResourceManager
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);

            // construct the SQL statement
            final String SQL = "SELECT TURNO FROM UCAJAS.TURNOS WHERE ABIERTO = ? AND USUARIO = ?";

            // prepare statement
            stmt = conn.prepareStatement(SQL);
            stmt.setString(1, "S");
            stmt.setString(2, "RENDICIONESAPV");
            rs = stmt.executeQuery();
            if (rs.next()) {
                turno = FechaUtil.toUtilDate(rs.getTimestamp("TURNO"));
            }

        } catch (Exception _e) {
            _e.printStackTrace();
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return turno;
    }


    /**
     * Recupera el Folio caja con el cual
     * se aperturo el turno
     * @param turno
     * @return
     */
    public Long getFolioCajaByTurno(java.util.Date turno) {

        logger.info("[RendicionImplDAO][INICIO]::getFolioCajaByTurno");

        Long folio = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // get the user-specified connection or get a connection from the ResourceManager
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);

            // construct the SQL statement
            final String SQL = "SELECT FOLIO FROM UCAJAS.FOLIOCAJA WHERE FECHA = ?";

            // prepare statement
            stmt = conn.prepareStatement(SQL);

            java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd/MM/yyyy");
            java.util.Date fechaFolioDia = dateFormat.parse(dateFormat.format(turno));

            logger.debug("Casteo de fecha a consultar[" + fechaFolioDia + "]");
            stmt.setDate(1, new java.sql.Date(fechaFolioDia.getTime()));
            rs = stmt.executeQuery();
            if (rs.next()) {
                folio = rs.getLong("FOLIO");
            }

        } catch (Exception _e) {
            _e.printStackTrace();
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        logger.info("[RendicionImplDAO][FIN][" + folio + "]::getFolioCajaByTurno");
        return folio;
    }


    /**
     * Apertura de la caja
     * @param rendPago observacion del motivo de la apertura de la caja
     * @return
     */
    public Long callNewAperturaTurnoCaja(String tiporendicion) {
        Connection conn = null;
        CallableStatement stmt = null;
        Long ret = null;
        String query = "{? = call UCAJAS.PKG_MCJ_MANEJOCAJA_PRVRD.AperturaTurnoCaja(?)}";
        try {
            // Recupera conexion en DB
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);

            stmt = conn.prepareCall(query);
            stmt.registerOutParameter(1, Types.NUMERIC);
            stmt.setString(2, tiporendicion); //CAJA LOS ANDES, PREVIRED
            stmt.executeQuery();
            ret = stmt.getLong(1);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                /*stmt.close();
                conn.close();*/
                OracleDAOFactory.closeAll(conn, stmt, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * Cierra la caja
     * @param rendPago otro log para el cierre.
     * @return
     */
    public String callNewCierreTurnoCaja(String tiporendicion, Long idTurno) {
        Connection conn = null;
        CallableStatement stmt = null;
        String ret = null;
        String query = "{? = call UCAJAS.PKG_MCJ_MANEJOCAJA_PRVRD.CierreTurnoCaja(?,?)}";
        try {
            // Recupera conexion en DB
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);

            stmt = conn.prepareCall(query);
            stmt.registerOutParameter(1, Types.VARCHAR);
            stmt.setString(2, tiporendicion);
            stmt.setLong(3, idTurno);
            stmt.executeQuery();
            ret = stmt.getString(1);

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                /*stmt.close();
                conn.close();*/
                OracleDAOFactory.closeAll(conn, stmt, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ret;
    }

    /**
     * Retorna la fecha y hora del turno con el cual
     * se aperturo la transaccion
     * @param idturno
     * @return
     */
    public java.util.Date getNewFechaHoraTurno(Long idturno) {
        java.util.Date turno = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // get the user-specified connection or get a connection from the ResourceManager
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);

            // construct the SQL statement
            final String SQL = "SELECT TURNO FROM UCAJAS.TURNOS WHERE ID_TURNO = ? AND USUARIO = ?";

            // prepare statement
            stmt = conn.prepareStatement(SQL);
            stmt.setLong(1, idturno);
            stmt.setString(2, "RENDICIONESAPV");
            rs = stmt.executeQuery();
            if (rs.next()) {
                turno = FechaUtil.toUtilDate(rs.getTimestamp("TURNO"));
            }

        } catch (Exception _e) {
            _e.printStackTrace();
        } finally {
            try {
                /*rs.close();
                stmt.close();
                conn.close();*/
                OracleDAOFactory.closeAll(conn, stmt, rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return turno;
    }


    /**
     * Recupera el Folio caja con el cual
     * se aperturo el turno
     * @param turno
     * @return
     */
    public Long getNewFolioCajaByTurno(java.util.Date turno) {
        Long folio = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // get the user-specified connection or get a connection from the ResourceManager
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            // construct the SQL statement
            final String SQL = "SELECT FOLIO FROM UCAJAS.FOLIOCAJA WHERE FECHA = ?";

            // prepare statement
            stmt = conn.prepareStatement(SQL);
            stmt.setDate(1, new java.sql.Date(turno.getTime()));
            rs = stmt.executeQuery();
            if (rs.next()) {
                folio = rs.getLong("FOLIO");
            }

        } catch (Exception _e) {
            _e.printStackTrace();
        } finally {
            try {
                /*rs.close();
                stmt.close();
                conn.close();*/
                OracleDAOFactory.closeAll(conn, stmt, rs);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return folio;
    }


    /**
     * Metodo para recuperar los pagos que deben ser realizados
     * de forma manual en el sistema de cajas.
     *
     * @return Lista de Transacciones a reprocesar
     */
    public List<ResumenRendicionesVwDTO> getReprocesosManualPagosByPeriodo(String tipoRend, Integer mes, Integer anio) {
        List<ResumenRendicionesVwDTO> resultado = new ArrayList<ResumenRendicionesVwDTO>();
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        String filtro = "";
        tipoRend = (tipoRend == null) ? "" : tipoRend;

        if (tipoRend.equalsIgnoreCase(Constant.PLANILLA_ANDES) ||
            tipoRend.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
            if (tipoRend.equalsIgnoreCase(Constant.PLANILLA_ANDES)) {
                filtro =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_ANDES_CONVENIO + ", " +
                    Constant.PLANILLA_ANDES_CONVENIO_MANUAL + ") \n";
            }
            if (tipoRend.equalsIgnoreCase(Constant.PLANILLA_LAARAUCANA)) {
                filtro =
                    "AND CARGA.NUMERO_DE_CONVENIO IN ( " + Constant.PLANILLA_LAARAUCANA_CONVENIO + ", " +
                    Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
            }
        } else {
            filtro =
                "AND CARGA.NUMERO_DE_CONVENIO NOT IN ( " + Constant.PLANILLA_ANDES_CONVENIO + "," +
                Constant.PLANILLA_ANDES_CONVENIO_MANUAL + "," + Constant.PLANILLA_LAARAUCANA_CONVENIO + "," +
                Constant.PLANILLA_LAARAUCANA_CONVENIO_MANUAL + ") \n";
        }

        String query =
            " SELECT REVW.*\n" + " FROM REND_RESUMEN_RENDICIONES_VW REVW, REND_CARGA_DE_ARCHIVOS CARGA " +
            " WHERE REVW.CODIGO_RECAUDADOR NOT IN('000') " + " AND REVW.ESTADO_PROCESO_PLANILLA IN(16) " +
            " AND CARGA.ID_CARGA = REVW.ID_CARGA " + " AND to_char(REVW.PERIODO_PAGO,'YYYY') = " + anio + " " +
            " AND to_char(REVW.PERIODO_PAGO,'MM') = " + mes + " " + " " + filtro + " " +
            " ORDER BY REVW.IDENTIFICACION_BANCO, " + "  REVW.INICIO_PROCESO DESC";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            rs = stmt.executeQuery();
            while (rs.next()) {
                ResumenRendicionesVwDTO dto = new ResumenRendicionesVwDTO();
                dto.setInicio_proceso(new Date(rs.getTimestamp("INICIO_PROCESO").getTime()));
                dto.setId_folio_abono(rs.getDouble("ID_FOLIO_ABONO"));
                dto.setId_carga(rs.getDouble("ID_CARGA"));
                dto.setDescripcion_proceso_abono(rs.getString("DESCRIPCION_PROCESO_ABONO"));
                dto.setMonto_aplicado(rs.getDouble("MONTO_APLICADO"));
                dto.setMonto_no_aplicado(rs.getDouble("MONTO_NO_APLICADO"));
                dto.setCodigo_recaudador(rs.getInt("CODIGO_RECAUDADOR"));
                dto.setGlosa_recaudador(rs.getString("GLOSA_RECAUDADOR"));
                dto.setRut_trabajador(rs.getString("RUT_TRABAJADOR"));
                dto.setRut_empleador(rs.getString("RUT_EMPLEADOR"));
                dto.setNombre_trabajador(rs.getString("NOMBRE_TRABAJADOR"));
                dto.setRazon_social_empleador(rs.getString("RAZON_SOCIAL_EMPLEADOR"));
                dto.setFecha_abono(new java.util.Date(rs.getTimestamp("FECHA_ABONO").getTime()));
                dto.setNumero_de_registros(rs.getInt("NUMERO_DE_REGISTROS"));
                dto.setMonto_total_abonado(rs.getDouble("MONTO_TOTAL_ABONADO"));
                dto.setPeriodo_pago(new java.util.Date(rs.getDate("PERIODO_PAGO").getTime()));
                dto.setEstado_proceso_abono(rs.getInt("ESTADO_PROCESO_ABONO"));
                dto.setDescripcion_proceso_abono(rs.getString("DESCRIPCION_PROCESO_ABONO"));
                dto.setCodigo_banco(rs.getInt("CODIGO_BANCO"));
                dto.setIdentificacion_banco(rs.getString("IDENTIFICACION_BANCO"));
                dto.setFecha_pago(new java.util.Date(rs.getTimestamp("FECHA_PAGO").getTime()));
                dto.setEstado_proceso_planilla(rs.getInt("ESTADO_PROCESO_PLANILLA"));
                dto.setDescripcion_proceso_planilla(rs.getString("DESCRIPCION_PROCESO_PLANILLA"));
                dto.setObs_proceso_planilla(rs.getString("OBS_PROCESO_PLANILLA"));
                dto.setId_carga_planilla(rs.getDouble("ID_CARGA_PLANILLA"));
                dto.setDv_trabajador(rs.getString("DV_TRABAJADOR"));
                dto.setDv_empleador(rs.getString("DV_EMPLEADOR"));
                dto.setFecha_hora_operacion(new java.util.Date(rs.getTimestamp("FECHA_HORA_OPERACION").getTime()));
                dto.setDeposito_convenio(rs.getDouble("DEPOSITO_CONVENIO"));
                if (dto.getEstado_proceso_planilla() != RUNNIG) {
                    dto.setEstadoProcesamientoManual("Reprocesar en Caja");
                } else {
                    dto.setEstadoProcesamientoManual("Procesando...");
                    dto.setObs_proceso_planilla("Procesando...");
                }
                resultado.add(dto);
            }
        } catch (SQLException e) {
            logger.error("getReprocesosManualPagos() - Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("getReprocesosManualPagos() - Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return resultado;
    }


    /**
     * Distribucion de Montos en Caja
     * @param usuarioCaja
     * @param fechaTurno
     * @param numeroFolioCajaDiario
     * @param numeroFolioPago
     * @param empresa
     * @return
     */
    public Integer callDistribuirMontoCaja(String usuarioCaja, java.util.Date fechaTurno, Long numeroFolioCajaDiario,
                                           Long numeroFolioPago, Integer empresa) {


        Integer resultado = null;
        Connection conn = null;
        CallableStatement stmt = null;
        String plsql = "{call PKG_DISTRIBUCION_CAJA.Distribucion_montos(?,?,?,?,?,?,?,?)}";
        try {

            // Recupera conexion en DB
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);

            stmt = conn.prepareCall(plsql);

            stmt.setString(1, usuarioCaja); //usuario
            stmt.setTimestamp(2, new Timestamp(fechaTurno.getTime())); //turno
            stmt.setNull(3, Types.NUMERIC); //Numero de operacion
            stmt.setLong(4, numeroFolioCajaDiario); //folio de caja
            stmt.setLong(5, numeroFolioPago); //folio pago
            stmt.setInt(6, (empresa != null ? empresa : 4)); // 1=BICE VIDA 4=HIPOTECARIA
            stmt.registerOutParameter(7, Types.VARCHAR);
            stmt.registerOutParameter(8, Types.VARCHAR);

            //EJECUTA PLSQL
            stmt.execute();

            //RECUPERA RESULTADO
            resultado = stmt.getInt(7); // 0=ok, <> 0 = problema
            String descripcion = stmt.getString(8);
            System.out.println("Resultado Distribucion operacion :" + resultado + " mensaje:" + descripcion);


        } catch (SQLException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            OracleDAOFactory.closeAll(conn, stmt);
        }
        return resultado;
    }

    /**
     * Retorna la fecha y hora del turno con el cual
     * se aperturo la transaccion
     * @param idturno
     * @return
     */
    public java.util.Date getFechaHoraTurno(Long idturno) {
        java.util.Date turno = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            // get the user-specified connection or get a connection from the ResourceManager
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);

            // construct the SQL statement
            final String SQL = "SELECT TURNO FROM UCAJAS.TURNOS WHERE ID_TURNO = ? AND USUARIO = ?";

            // prepare statement
            stmt = conn.prepareStatement(SQL);
            stmt.setLong(1, idturno);
            stmt.setString(2,
                           ResourceBundleUtil.getProperty("integracion.webservice.notificacion.recaudacion.usuariocaja"));
            rs = stmt.executeQuery();
            if (rs.next()) {
                turno = FechaUtil.toUtilDate(rs.getTimestamp("TURNO"));
            }

        } catch (Exception _e) {
            _e.printStackTrace();
        } finally {
            OracleDAOFactory.closeAll(conn, stmt);
        }
        return turno;
    }

    @Override
    public Boolean existAbonoFinalizado(String nombreArchivo, Integer estadoProceso) {
        boolean existe = false;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =
            "" +
            "SELECT COUNT(*) existeArchivo FROM REND_CARGA_DE_ARCHIVOS WHERE SUBSTR(nombre_fisico_archivo,5) = ? AND ESTADO_PROCESO = ? AND ID_TIPO_ARCHIVO = 1";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setString(1, nombreArchivo);
            stmt.setInt(2, estadoProceso);
            rs = stmt.executeQuery();
            if (rs.next()) {
                if (rs.getLong("existeArchivo") > 0) {
                    existe = true;
                }
            }


        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return existe;

    }


    @Override
    public List<String> getListGlosaRecaudador(String codigoNumerico,String glosaRecaudador) {
        //String glosa = null;
        List<String> glosa = new ArrayList<String>();
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query =" SELECT distinct(rec.IDENTIFICADOR) FROM REND_RECAUDADORES rec "+
        " inner join rend_resumen_abono resumen "+
        " on rec.codbco = RESUMEN.CODIGO_BANCO "+
        " and REC.CODBCO = ? or upper(rec.DESCRIPCION) like upper('%?%')";
        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setString(1, codigoNumerico);
            stmt.setString(2, glosaRecaudador);
            rs = stmt.executeQuery();
            while (rs.next()) {
                glosa.add(rs.getString(1));
            }


        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return glosa;
    }

    @Override
    public boolean existProcesoEnEjecucion(Integer estado) {
        boolean existe = false;
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            String sql = "SELECT COUNT(*) TOTAL FROM REND_PROCESOS_AUTOMATICOS WHERE ESTADO_EJECUCION = ?";
            stm = conn.prepareStatement(sql);
            stm.setInt(1, estado);
            rs = stm.executeQuery();
            if (rs.next()) {
                if (rs.getLong("TOTAL") > 0) {
                    existe = true;
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            OracleDAOFactory.closeAll(conn, stm, rs);
        }

        return existe;
    }

    @Override
    public void guardaEjecucion(RendProcesoAutomatico procesoObj) {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);

            String sql =
                "INSERT INTO REND_PROCESOS_AUTOMATICOS (ID_CARGA,ID_TIPO_ARCHIVO,MEDIO_PAGO,FECHA,SERVIDOR,ESTADO_EJECUCION,NOMBRE_ARCHIVO,DESCRIPCION_ESTADO,OBSERVACION) " +
                "VALUES (?,?,?,SYSDATE,?,?,?,?,?)";

            stm = conn.prepareStatement(sql);
            stm.setLong(1, procesoObj.getIdCarga());
            stm.setInt(2, procesoObj.getIdTipoArchivo());
            stm.setInt(3, procesoObj.getMedioPago());
            stm.setString(4, procesoObj.getServidor());
            stm.setInt(5, procesoObj.getEstadoEjecucion());
            stm.setString(6, procesoObj.getNombreArchivo());
            stm.setString(7, procesoObj.getDescripcionEstado());
            stm.setString(8, procesoObj.getObservacion());
            logger.info("Guardando ejecucion IdCarga[" + procesoObj.getIdCarga() + "] -> " + stm.executeUpdate());

        } catch (SQLException e) {
            logger.error(e);
        } finally {
            OracleDAOFactory.closeAll(conn, stm, rs);
        }
    }

    @Override
    public void actualizaEjecucion(Integer estado, String descripcionEstado, Long idCarga, String observacion) {
        Connection conn = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            String sql =
                "UPDATE REND_PROCESOS_AUTOMATICOS SET ESTADO_EJECUCION = ?, DESCRIPCION_ESTADO = ?, OBSERVACION = ? Where ID_CARGA = ? ";
            stm = conn.prepareStatement(sql);
            stm.setInt(1, estado);
            stm.setString(2, descripcionEstado);
            stm.setString(3, observacion);
            stm.setLong(4, idCarga);

            logger.info("Actualizando ejecucion ID Carga[" + idCarga + "]Estado Inicial[" + estado + "]Estado final[" +
                        descripcionEstado + "] -> " + stm.executeUpdate());

        } catch (SQLException e) {
            logger.error(e);
        } finally {
            OracleDAOFactory.closeAll(conn, stm, rs);
        }

    }

    @Override
    public boolean existArchivoCargaProcesado(String nombreArchivo, Integer estado) {
        boolean existe = false;
                Connection conn = null;
                PreparedStatement stm = null;
                ResultSet rs = null;
                try {
                    conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
                    String sql = "SELECT COUNT(*) HAYARCHIVO FROM REND_PROCESOS_AUTOMATICOS WHERE TRIM(NOMBRE_ARCHIVO) = ? AND ESTADO_EJECUCION = ?";
                    stm = conn.prepareStatement(sql);
                    stm.setString(1, nombreArchivo);
                    stm.setInt(2, estado);
                    rs = stm.executeQuery();
                    if(rs.next() ) {
                        if(rs.getLong("HAYARCHIVO")> 0) {
                            existe = true;
                        }    
                    }
                } catch(SQLException e) {
                    logger.error(e);
                } finally {
                    OracleDAOFactory.closeAll(conn, stm, rs);   
                }
                
                return existe;
    }
    @Override
    public String getGlosaRecaudador(String codigoNumerico) {
        String glosa = null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
       
        String query = "" + "SELECT IDENTIFICADOR FROM REND_RECAUDADORES WHERE CODBCO = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setString(1, codigoNumerico);
            rs = stmt.executeQuery();
            if (rs.next()) {
                glosa = rs.getString(1);
            }


        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return glosa;
    }
    @Override
    public Date retornaDiaHabil(Date fecha) {
        Date fechaRetorno = fecha;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        Long contador = 0L;
        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);

            final String SQL_VALIDACION = " SELECT fnc_obtener_diahabil(?) FROM DUAL";

            // prepare statement
            stmt = conn.prepareStatement(SQL_VALIDACION);

            for (int i = 0; i <= contador; i++) {
                Boolean isValidaFecha = false;
                stmt.setDate(1, new java.sql.Date(fechaRetorno.getTime()));
                rs = stmt.executeQuery();

                if (rs.next()) {
                    isValidaFecha = Boolean.valueOf(rs.getString(1));
                    if (isValidaFecha) {
                        break;
                    } else {
                        fechaRetorno = FechaUtil.sumarDiasFecha(fechaRetorno, 1);
                        contador++;
                    }
                }
            }
        } catch (Exception _e) {
            _e.printStackTrace();
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return fechaRetorno;
    }
    
    public Integer existeArchivoCargado(String nombreArchivo){
        Integer isCargado= null;
        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;
        
        String query = "" + "SELECT COUNT(*) from rend_carga_de_archivos WHERE TRIM(NOMBRE_FISICO_ARCHIVO) = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);
            stmt.setString(1, nombreArchivo);
            rs = stmt.executeQuery();
            if (rs.next()) {
                isCargado= rs.getInt(1);
            }


        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return isCargado;
    }

    @Override
    public boolean validaDiaHabil(Date fecha) {
        Boolean isValidaFecha = null;
        String fechaValida = null;
        Integer existeFecha = null;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            //final String SQL_VALIDACION = " SELECT FNC_OBTENER_DIAHABIL2 FROM DUAL";
            final String SQL = "select count(*) from rrhh_feriados_vw  where to_date(to_char(fecha, 'DD/MM/YYYY'),'DD/MM/YYYY') = to_date(to_char(sysdate, 'DD/MM/YYYY'),'DD/MM/YYYY')";
            //stmt = conn.prepareStatement(SQL_VALIDACION);
            stmt = conn.prepareStatement(SQL);
            rs = stmt.executeQuery();

            if (rs.next()) {
                //fechaValida = rs.getString(1);
                //System.out.println("fecha retorno BD " + fechaValida);
                //isValidaFecha = fechaValida.toLowerCase().trim().equals("true") ? true : false;
                //System.out.println("isValidaFecha " + isValidaFecha);
                
                existeFecha = rs.getInt(1);
                logger.info("retorno BD " + existeFecha);
                isValidaFecha = existeFecha == 0 ? true : false;
                //si es true, significa que la fecha no se encuentra dentro de los feriados legales y de la compania
                if(isValidaFecha){
                    //validamos que la fecha no corresponda a un fin de semana
                    SimpleDate fechaPago = new SimpleDate(fecha);                    
                    if ( fechaPago.getDayOfWeek() == Calendar.SATURDAY || fechaPago.getDayOfWeek() == Calendar.SUNDAY ) {
                        isValidaFecha = false;
                    }
                    logger.info("isValidaFecha " + isValidaFecha);
                }
            }

        } catch (Exception _e) {
            _e.printStackTrace();
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }
        return isValidaFecha;

    }

    @Override
    public boolean deleteCargaProcesoAutomatico(Double idCarga) {
        boolean result = false;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "DELETE rend_procesos_automaticos WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.execute();
            result = true;
        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }

        if (result) {
            result = deleteCargaArchivo(idCarga);
        }

        return result;
    }

    @Override
    public boolean deleteCargaArchivo(Double idCarga) {
        boolean result = false;

        Connection conn = null;
        CallableStatement stmt = null;
        ResultSet rs = null;

        String query = "" + "DELETE rend_carga_de_archivos WHERE ID_CARGA = ?";

        try {
            conn = OracleDAOFactory.createConnection(JDBC_UCAJAS);
            stmt = conn.prepareCall(query);

            stmt.setDouble(1, idCarga);
            stmt.execute();
            result = true;
        } catch (SQLException e) {
            logger.error("Se ha producido un excepcion: ", e);
        } catch (Exception e) {
            logger.error("Se ha producido un excepcion: ", e);
        } finally {
            OracleDAOFactory.closeAll(conn, stmt, rs);
        }        

        return result;
    }
}
