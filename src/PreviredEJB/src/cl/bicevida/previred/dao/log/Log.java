package cl.bicevida.previred.dao.log;

import java.io.Serializable;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.OracleTypes;


public class Log implements Serializable{
    private Connection conn = null;
    public static final Integer LOG_LEVEL_DEBUG = 5;
    public static final Integer LOG_LEVEL_INFO = 10;
    public static final Integer LOG_LEVEL_WARN = 15;
    public static final Integer LOG_LEVEL_ERROR = 20;
    
    public Log(Connection conn) throws Exception {
        this.conn = conn;
    }
    
    public Integer openLogProcess(String name) throws Exception {
        CallableStatement llamaPackage;
        String SQL;
        
        SQL = "{? = call PKG_LOG.BV_OPEN_PROCESS(P_NOMBRE => '" + name + "') }";

        try {
            llamaPackage = conn.prepareCall(SQL);
            llamaPackage.registerOutParameter(1, java.sql.Types.INTEGER);
            llamaPackage.execute();
            Integer result = llamaPackage.getInt(1);
            return result;
        } catch (SQLException e) {
             throw new Exception("Imposible registrar log de proceso.", e);
        }
    }
    
    public void closeLogProcess(Integer processId) throws Exception {
        CallableStatement llamaPackage;
        String SQL;
        
        SQL = "{call PKG_LOG.BV_CLOSE_BATCH_PROCESS(" +
            "   P_PROCESS_ID => ? ) }";

        try {
            llamaPackage = conn.prepareCall(SQL);
            llamaPackage.setInt(1, processId);
            llamaPackage.execute();
        } catch (SQLException e) {
             throw new Exception("Imposible cerrar log de proceso " + processId, e);
        }
    }
    
    public void log(Integer processId, String message, Integer level, String errCode) throws Exception {
        CallableStatement llamaPackage;
        String SQL;
        
        SQL = "{call PKG_LOG.BV_LOG(" +
            "    P_LTA_TEXT => ?, \n" + 
            "    P_LTA_LEVEL => ?,\n" + 
            "    P_LTA_ERR_CODE => ?,\n" + 
            "    P_LTA_PROCESS_ID => ?) }";

        try {
            llamaPackage = conn.prepareCall(SQL);
            llamaPackage.setString(1, message);
            llamaPackage.setInt(2, level);
            if (errCode != null)
                llamaPackage.setString(3, errCode);
            else
                llamaPackage.setNull(3, OracleTypes.VARCHAR);
            if (processId != null)
                llamaPackage.setInt(4, processId);
            else
                llamaPackage.setNull(4, OracleTypes.INTEGER);
            llamaPackage.execute();
        } catch (SQLException e) {
             throw new Exception("Imposible registrar log de detalle..", e);
        }
    }
}
