package cl.bicevida.previred.util;

import java.io.Serializable;

import java.util.ResourceBundle;

public class ResourceBundleUtil implements Serializable {
    @SuppressWarnings("compatibility:1546054593514510978")
    private static final long serialVersionUID = 1L;

    private static final String RESOURCE_BASENAME = "configurationbiceejb";
    private static ResourceBundle rb;

    /**
     * Inicializador estatico del bundle
     */
    static {
        rb = ResourceBundle.getBundle(RESOURCE_BASENAME);
    }

    /**
     * Recupera el valor de una key
     * 
     * @param key
     * @return
     */
    public static String getProperty(String key) {
        String valor = null;
        try {
            valor = rb.getString(key);
        } catch (Exception e) {
            valor = "";
        }        
        return valor;
    }
}
