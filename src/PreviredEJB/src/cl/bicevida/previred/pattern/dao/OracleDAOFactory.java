package cl.bicevida.previred.pattern.dao;


import cl.bicevida.previred.dao.RendicionDAO;

import cl.bicevida.previred.dao.oracle.RendicionImplDAO;

import java.io.Serializable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.annotation.Resource;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import javax.sql.DataSource;


public class OracleDAOFactory extends DAOFactory implements Serializable  {
    @SuppressWarnings("compatibility:-5811191134373451127")
    private static final long serialVersionUID = 1L;

    /**
     * Logger for this class
     */
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(OracleDAOFactory.class);
    
    public static Connection createConnection(String jndi) {
        Connection myConn = null;
        try {
            Context ic = new InitialContext();
            DataSource ds = (DataSource)ic.lookup(jndi);

            try {
                myConn = ds.getConnection();
                PreparedStatement stm = myConn.prepareStatement("ALTER SESSION SET NLS_DATE_FORMAT = 'DD-MM-YYYY HH24:MI:SS'");
                stm.execute();
                stm.close();
            } catch (SQLException e) {
                logger.error("Se ha producido un excepcion: ", e);
            }
        } catch (NamingException e) {
            logger.error("Se ha producido un excepcion: ", e);
        }
        return myConn;
    }
        
    /**
     * Cierra los recursos asociados a una conexion
     *
     * @param p_conn : id de la conexion
     * @param p_stmt : statement usado
     * @param p_rs   : result set obtenido
     */
    public static void closeAll(Connection p_conn, Statement p_stmt, 
                                ResultSet p_rs) {
        logger.info("cerrando las conexiones");
        // Cierra el "result set"
        try {
            if (p_rs != null) {
                p_rs.close();
            }
        } catch (Exception e) {
            logger.info("Se ha producido un excepcion al cerrar las conexiones: ", e);
        }

        // Cierra el "statement"
        try {
            if (p_stmt != null) {
                p_stmt.close();
            }
        } catch (Exception e) {
            logger.info("Se ha producido un excepcion al cerrar las conexiones: ", e);
        }

        // Cierra la conexi�n ("connection")
        try {
            if (p_conn != null) {
                p_conn.close();
            }
        } catch (Exception e) {
            logger.info("Se ha producido un excepcion al cerrar las conexiones: ", e);
        }
    }
    
    /**
     * Cierra los recursos asociados a una conexion
     *
     * @param p_conn : id de la conexion
     * @param p_stmt : statement usado
     * @param p_rs   : result set obtenido
     */
    public static void closeAll(Connection p_conn, Statement p_stmt) {
       

        // Cierra el "statement"
        try {
            if (p_stmt != null) {
                p_stmt.close();
            }
        } catch (Exception e) {
            logger.info("Se ha producido un excepcion al cerrar las conexiones: ", e);
        }

        // Cierra la conexi�n ("connection")
        try {
            if (p_conn != null) {
                p_conn.close();
            }
        } catch (Exception e) {
            logger.info("Se ha producido un excepcion al cerrar las conexiones: ", e);
        }
    }

    public RendicionDAO getRendicionDAO() {
        return new RendicionImplDAO();
    }

    
}
