package cl.bicevida.previred.pattern.dao;

import java.io.Serializable;


public abstract class DAOFactory implements Serializable {
    @SuppressWarnings("compatibility:9018944527818757972")
    private static final long serialVersionUID = 1L;

    public static DAOFactory getDAOFactory() {
        return new OracleDAOFactory();
    }
}
